

scalaVersion := "2.10.2"


libraryDependencies += "edu.berkeley.cs" %% "chisel" % "latest.release"


libraryDependencies += "org.scalatest" % "scalatest_2.11" % "2.2.0" % "test"