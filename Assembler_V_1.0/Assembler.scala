import scala.collection.mutable.ArrayBuffer
import scala.io.Source
import scala.Console
import java.io.PrintWriter
import java.util.Calendar
import java.text.SimpleDateFormat
import java.util.Date
import Chisel._


object Assembler
{
	def main(args: Array[String]): Unit =
	{
		
		var exit = true
		while(exit)
		{
			print("\033[2J")
			println("\033[32mPress enter to continue\033[0m")                //printing menu and logo
			val y = readLine
			print("\033[2J")
			Extra.logo()			
			println("<----------------------------->")
			println("\033[32m   \033[4mP2 Compiler\033[0m")
			println("Ferraro Luigi -> \033[34maloisius@berkeley.edu\033[0m")
			println("Rizzo Francesco -> \033[34mrizzof87@berkeley.edu\033[0m")
			println("<----------------------------->")
			
			print("File name: ")                           
			val file = readLine
			print("Number of TOTAL Cell: ")
			val TOTALnumber = readLine.toInt	
			print("Number of Logic Cell: ")
			val Lnumber = readLine.toInt	
			print("Number of Memory Cell: ")
			val Mnumber = readLine.toInt	
			print("Size of Logic Cell Bus: ")
			val LBus = readLine.toInt
			print("Size of Memory Cell Bus: ")
			val MBus = readLine.toInt
			print("Size of CrossBar Cell Bus: ")
			val CBus = readLine.toInt
			print("Data Size: ")
			val DataSize = readLine.toInt
		  print("ADDR Size: ")
			val ADDRSize = readLine.toInt
			print("CrossBar1/2 INPUT: ")
			val INPUTC = readLine.toInt
			print("CrossBar1/2 OUTPUT: ")
			val OUTPUTC = readLine.toInt
			println("CMD size is set to 5")
			print("Number of CrossBar Cell is set to 2")
			print("TME size is set to 6 ")	
			val TME=6		
			 



			var L = new ArrayBuffer[String]
			var M = new ArrayBuffer[String]
			var C = new ArrayBuffer[String]

			val wfile = new PrintWriter("Program.tmp" )
			var prefix = "000000"

      var finalSTR = "0"
			for(x<-0 until LBus.toInt-1)
			{
				finalSTR = finalSTR + "0"
			}
			
      print("\033[2J")
      val source = Source.fromFile(file)
      val lines = source.getLines.toArray
			println("")
//-----------------------------------------------------------------------//

      for(CELL<- 0 until TOTALnumber)
      {
      	for(x<- 0 until lines.length)
      	{
      		if(lines(x).isEmpty || lines(x)(0)=='/')                     //space and comment
      		{     		
      		}

      		else if(lines(x)(0)=='l' || lines(x)(0)=='L')
      		{
      			var space = lines(x).indexOf(" ") 
      			var equal = lines(x).indexOf("=") 
      			var space2 = lines(x).lastIndexOf(" ")                         //legge tutti i tme riferiti alla singola cella ad esempio tutte le l0

      			if(lines(x).substring(1,space).toInt==CELL)
      			{
      				L+= lines(x).substring(space2+1,lines(x).length)
      			} 		
      		}

      		else if(lines(x)(0)=='m' || lines(x)(0)=='M')
      		{
      			var space = lines(x).indexOf(" ") 
      			var equal = lines(x).indexOf("=") 
      			var space2 = lines(x).indexOf(" ",equal)                         //legge tutti i tme riferiti alla singola cella ad esempio tutte le l0

      			if(lines(x).substring(1,space).toInt==CELL)
      			{
      				M+= lines(x).substring(space2+1,lines(x).length)
      				//println(lines(x).substring(space2+1,lines(x).length))
      			} 		
      		}
      		
      		else if(lines(x)(0)=='c' || lines(x)(0)=='C')
      		{
      			var space = lines(x).indexOf(" ") 
      			var equal = lines(x).indexOf("=") 
      			var space2 = lines(x).indexOf(" ",equal)                         //legge tutti i tme riferiti alla singola cella ad esempio tutte le l0

      			if(lines(x).substring(1,space).toInt==CELL)
      			{
      				C+= lines(x).substring(space2+1,lines(x).length)
      				//println(lines(x).substring(space2+1,lines(x).length))
      			} 		
      		}
      	}


      		

      	
      	for(x<-0 until L.length)
      	{
      		  wfile.print(CELL)
      		  wfile.print(" ")
      			var part1 = "00000"
      			var part2 = 0
      			if(x==0)
      			{
      			 part2 = L(x).toInt
      			}
      			else
      			{
      			 if(L(x).toInt-L(x-1).toInt-1<0)
      			 {
      			 	println("ERROR CHECK THE TIME AT LOGIC MODULE " + CELL + " command number " + x)
      			 }
      			 else
      			 {
      			 part2 = L(x).toInt-L(x-1).toInt-1
      			 }
      			}
      			var part3 = part2.toBinaryString
      			part3=fill(part3,TME)
            var part4 = part3 + part1
            part4 = fill(part4,LBus)
      			wfile.print(part4)    		                                  //scrive su file
      		  wfile.println("")
      	}
      	L.clear






      	
      var counter=0
      for(x<-0 until M.length)
      	{
      		  
      		  wfile.print(CELL+Lnumber)
      		  wfile.print(" ")
      		  if(M(x)(0)=='i' || M(x)(0)=='I')                 //init command
      		  {
      		  	var part1 = "01010"
      		  	var space2 = M(x).lastIndexOf(" ")
      		  	var part2 = M(x).substring(space2+1,M(x).length).toInt.toBinaryString
      		  	part2 = fill(part2,DataSize)
      		  	var part3 = part2+part1
      		  	part3 = fill(part3,MBus)
      		  	wfile.print(part3)
      		  	wfile.println("")
      		  	counter=counter-1
      		  }
      		  if(M(x)(0)=='w' || M(x)(0)=='W')
      		  {
      		  	var part1 = "00000"
      		  	var part2 = "1"                                                               //write
      		  	var part3 = M(x).substring(2,M(x).indexOf(" ",2)).toInt.toBinaryString
      		  	part3 = fill(part3,ADDRSize)
      		  	var space2 = M(x).lastIndexOf(" ")
      		  	var part4 = "0"
              if(counter==0)
              {
              	part4 = M(x).substring(space2+1,M(x).length).toInt.toBinaryString
              	part4=fill(part4,TME)
              }
              else
              {
              	var space3 = M(x-1).lastIndexOf(" ")
              	part4 = (M(x).substring(space2+1,M(x).length).toInt-M(x-1).substring(space3+1,M(x-1).length).toInt-1).toBinaryString
              	part4 = fill(part4,TME)
              }
      		  	var part5 = part3+part2+part4+part1
      		  	part5 = fill(part5,MBus)
      		  	wfile.print(part5)
      		  	wfile.println("")
      		  	
      		  }
      		  
      		  if(M(x)(0)=='r' || M(x)(0)=='R')
      		  {
      		  	var part1 = "00000"                    // CMD
      		  	var part2 = "0"                        // R/W                                                     //write
      		  	var part3 = M(x).substring(2,M(x).indexOf(" ",2)).toInt.toBinaryString   
      		  	part3 = fill(part3,ADDRSize)          // ADDR
      		  	var space2 = M(x).lastIndexOf(" ")
      		  	var part4 = "0"
              if(counter==0)
              {
              	part4 = M(x).substring(space2+1,M(x).length).toInt.toBinaryString
              	part4=fill(part4,TME)
              }
              else
              {
              	var space3 = M(x-1).lastIndexOf(" ")
              	part4 = (M(x).substring(space2+1,M(x).length).toInt-M(x-1).substring(space3+1,M(x-1).length).toInt-1).toBinaryString
              	part4 = fill(part4,TME)                   // TME
              }
      		  	var part5 = part3+part2+part4+part1
      		  	part5 = fill(part5,MBus)
      		  	wfile.print(part5)
      		  	wfile.println("")
      		  }
      		counter=counter+1	
      	}
      	M.clear


      	  for(x<-0 until C.length)
      	  {	  
      		  wfile.print(CELL+Lnumber+Mnumber)
      		  wfile.print(" ")
      		  var part1 = C(x).substring(0,C(x).indexOf('>'))  
      		  var part2 = C(x).substring(C(x).indexOf('>')+1,C(x).indexOf(" "))

      		  var IN = new ArrayBuffer[String] 
      		  var OUT = new ArrayBuffer[String]
      		  var lastindex=0

      		  

      		  for(x<-0 until part1.length)           //IN vector of crossbar
      		  {
      		  	if(part1(x)==',')
      		  	{
      		  		IN+=part1.substring(lastindex,x)
      		  		lastindex=x+1
      		  	}
      		  	if(x==part1.length-1)
      		  	{
      		  		IN+=part1.substring(lastindex,part1.length)
      		  	}
      		  }

            lastindex=0
      		  for(x<-0 until part2.length)           //OUT vector of crossbar
      		  {
      		  	if(part2(x)==',')
      		  	{
      		  		OUT+=part2.substring(lastindex,x)
      		  		lastindex=x+1
      		  	}
      		  	if(x==part2.length-1)
      		  	{
      		  		OUT+=part2.substring(lastindex,part2.length)
      		  	}
      		  }

      		  var part3 = crossbarCalculator(IN,OUT,INPUTC,OUTPUTC)

      		  
						var part4 = ""

						for(x<-0 until part3.length)
						{
							part4=fill(part3(x).toInt.toBinaryString,log2Up(OUTPUTC))+part4      //STATUS
						}
						
            var part5=""

            if(x==0)
            {
							part5=(C(x).substring(C(x).lastIndexOf(' ')+1,C(x).length)).toInt.toBinaryString     //TME
            }
            else
            {
            	part5=(  (C(x).substring(C(x).lastIndexOf(' ')+1,C(x).length).toInt) - (C(x-1).substring(C(x-1).lastIndexOf(' ')+1,C(x-1).length).toInt)-1  ).toBinaryString     //TME
            }
						part5 = fill(part5,TME)                                              //TME

						var part6 = part4+part5+"00000"
						part6 = fill(part6,CBus)                          //final string

						wfile.print(part6)
						
     		    wfile.println("")
      		  
      	  }
      		 
      	C.clear
      
      }


//-----------------------------------------------------------------------//


      wfile.close()     //close the file
      
      
		
//-------------------------------------------------------------------//

//                  REORGANIZE THE FILE

      val source1 = Source.fromFile("Program.tmp")
      val lines1 = source1.getLines.toArray
      val wfile1 = new PrintWriter("Program.p2")

      wfile1.println("-LOGIC")
      
      for(x<-0 until lines1.length)
      {
      	var number = (lines1(x).substring(0,lines1(x).indexOf(" "))).toInt
      	
      	if(number<Lnumber)
      	{
      		wfile1.println(lines1(x))
      	}
      }
      wfile1.println("-MEMORY")
      for(x<-0 until lines1.length)
      {
      	var number = (lines1(x).substring(0,lines1(x).indexOf(" "))).toInt
      	
      	if(number>=Lnumber && number<Mnumber+Lnumber)
      	{
      		var array = lines1(x).split(" ")
      		array(0)=(array(0).toInt-Lnumber).toString + " "
      		
      		wfile1.println(array(0)+array(1))
      	}
      }
      
      wfile1.println("-CROSSBAR")
      for(x<-0 until lines1.length)
      {
      	var number = (lines1(x).substring(0,lines1(x).indexOf(" "))).toInt
      	
      	if(number>=Lnumber+Mnumber)
      	{
      		var array = lines1(x).split(" ")
      		array(0)=(array(0).toInt-(Lnumber+Mnumber)).toString + " "
      		
      		wfile1.println(array(0)+array(1))
      	}
      }

      wfile1.close()     //close the file
		  var pause = readLine
    }

	             
	}

	def fill(STR:String,bit:Int):String=
	{
		var out=STR
		for(x<-0 until (bit-STR.length))
		{
			out= "0" + out
		}
		out
	}

	def crossbarCalculator(IN:ArrayBuffer[String],OUT:ArrayBuffer[String],INPUTC:Int,OUTPUTC:Int):ArrayBuffer[String]=
	{
		var FINAL = new ArrayBuffer[String]
		for(x<- 0 until OUTPUTC)
		{
			FINAL+="n"
			for(y<-0 until IN.length)
			{
				if(IN(y)==x.toString)
				{
					FINAL(x)=OUT(y)
				}
			}
		}

    var freeout = 0
    var notfound = true
    
		for(x<- 0 until FINAL.length)
		{
			if(FINAL(x)=="n")
			{
				while(notfound)
				{
					if(exist(FINAL,freeout.toString))
					{
						notfound=true
						freeout = freeout+1
					}
					else
					{
						notfound=false
					}
				}
				
				FINAL(x)=freeout.toString
				freeout=freeout+1
				notfound = true
			}
		}
		FINAL
	}

	def exist(IN:ArrayBuffer[String],STR:String):Boolean=
	{
		var result = false
		for(x<-0 until IN.length)
		{
			if(IN(x)==STR)
			{
				result=true
			}
		}
		result
	}

}