////////////////////////////////////////////////////////////////////////////////
// Copyright (c) 1995-2013 Xilinx, Inc.  All rights reserved.
////////////////////////////////////////////////////////////////////////////////
//   ____  ____
//  /   /\/   /
// /___/  \  /    Vendor: Xilinx
// \   \   \/     Version: P.20131013
//  \   \         Application: netgen
//  /   /         Filename: Sub_32_8_1_UInt.v
// /___/   /\     Timestamp: Tue Aug  5 14:28:17 2014
// \   \  /  \ 
//  \___\/\___\
//             
// Command	: -w -sim -ofmt verilog /home/francesco/Xilinx/Projects/IPCOREs/ipcore_dir/tmp/_cg/Sub_32_8_1_UInt.ngc /home/francesco/Xilinx/Projects/IPCOREs/ipcore_dir/tmp/_cg/Sub_32_8_1_UInt.v 
// Device	: 3sd3400afg676-4
// Input file	: /home/francesco/Xilinx/Projects/IPCOREs/ipcore_dir/tmp/_cg/Sub_32_8_1_UInt.ngc
// Output file	: /home/francesco/Xilinx/Projects/IPCOREs/ipcore_dir/tmp/_cg/Sub_32_8_1_UInt.v
// # of Modules	: 1
// Design Name	: Sub_32_8_1_UInt
// Xilinx        : /home/francesco/Xilinx/14.7/ISE_DS/ISE/
//             
// Purpose:    
//     This verilog netlist is a verification model and uses simulation 
//     primitives which may not represent the true implementation of the 
//     device, however the netlist is functionally correct and should not 
//     be modified. This file cannot be synthesized and should only be used 
//     with supported simulation tools.
//             
// Reference:  
//     Command Line Tools User Guide, Chapter 23 and Synthesis and Simulation Design Guide, Chapter 6
//             
////////////////////////////////////////////////////////////////////////////////

`timescale 1 ns/1 ps

module Sub_32_8_1_UInt (
  clk, ce, sclr, s, a, b
)/* synthesis syn_black_box syn_noprune=1 */;
  input clk;
  input ce;
  input sclr;
  output [31 : 0] s;
  input [31 : 0] a;
  input [31 : 0] b;
  
  // synthesis translate_off
  
  wire \blk00000001/sig000001e5 ;
  wire \blk00000001/sig000001e4 ;
  wire \blk00000001/sig000001e3 ;
  wire \blk00000001/sig000001e2 ;
  wire \blk00000001/sig000001dd ;
  wire \blk00000001/sig000001dc ;
  wire \blk00000001/sig000001db ;
  wire \blk00000001/sig000001da ;
  wire \blk00000001/sig000001d9 ;
  wire \blk00000001/sig000001d8 ;
  wire \blk00000001/sig000001d7 ;
  wire \blk00000001/sig000001cc ;
  wire \blk00000001/sig000001cb ;
  wire \blk00000001/sig000001c9 ;
  wire \blk00000001/sig000001c8 ;
  wire \blk00000001/sig000001c6 ;
  wire \blk00000001/sig000001c5 ;
  wire \blk00000001/sig000001c4 ;
  wire \blk00000001/sig000001c3 ;
  wire \blk00000001/sig000001c1 ;
  wire \blk00000001/sig000001c0 ;
  wire \blk00000001/sig000001be ;
  wire \blk00000001/sig000001bd ;
  wire \blk00000001/sig000001bb ;
  wire \blk00000001/sig000001ba ;
  wire \blk00000001/sig000001b8 ;
  wire \blk00000001/sig000001b7 ;
  wire \blk00000001/sig000001b6 ;
  wire \blk00000001/sig000001b5 ;
  wire \blk00000001/sig000001b4 ;
  wire \blk00000001/sig000001b3 ;
  wire \blk00000001/sig000001b1 ;
  wire \blk00000001/sig000001b0 ;
  wire \blk00000001/sig000001ae ;
  wire \blk00000001/sig000001ad ;
  wire \blk00000001/sig000001ab ;
  wire \blk00000001/sig000001aa ;
  wire \blk00000001/sig000001a8 ;
  wire \blk00000001/sig000001a7 ;
  wire \blk00000001/sig000001a6 ;
  wire \blk00000001/sig000001a5 ;
  wire \blk00000001/sig000001a4 ;
  wire \blk00000001/sig000001a3 ;
  wire \blk00000001/sig000001a1 ;
  wire \blk00000001/sig000001a0 ;
  wire \blk00000001/sig0000019e ;
  wire \blk00000001/sig0000019d ;
  wire \blk00000001/sig0000019b ;
  wire \blk00000001/sig0000019a ;
  wire \blk00000001/sig00000198 ;
  wire \blk00000001/sig00000197 ;
  wire \blk00000001/sig00000196 ;
  wire \blk00000001/sig00000195 ;
  wire \blk00000001/sig00000194 ;
  wire \blk00000001/sig00000193 ;
  wire \blk00000001/sig00000191 ;
  wire \blk00000001/sig00000190 ;
  wire \blk00000001/sig0000018e ;
  wire \blk00000001/sig0000018d ;
  wire \blk00000001/sig0000018b ;
  wire \blk00000001/sig0000018a ;
  wire \blk00000001/sig00000188 ;
  wire \blk00000001/sig00000187 ;
  wire \blk00000001/sig00000186 ;
  wire \blk00000001/sig00000185 ;
  wire \blk00000001/sig00000184 ;
  wire \blk00000001/sig00000183 ;
  wire \blk00000001/sig00000182 ;
  wire \blk00000001/sig00000181 ;
  wire \blk00000001/sig00000180 ;
  wire \blk00000001/sig0000017f ;
  wire \blk00000001/sig0000017e ;
  wire \blk00000001/sig0000017d ;
  wire \blk00000001/sig0000017c ;
  wire \blk00000001/sig0000017b ;
  wire \blk00000001/sig0000017a ;
  wire \blk00000001/sig00000179 ;
  wire \blk00000001/sig00000178 ;
  wire \blk00000001/sig00000177 ;
  wire \blk00000001/sig00000176 ;
  wire \blk00000001/sig00000175 ;
  wire \blk00000001/sig00000174 ;
  wire \blk00000001/sig00000173 ;
  wire \blk00000001/sig00000172 ;
  wire \blk00000001/sig00000171 ;
  wire \blk00000001/sig00000170 ;
  wire \blk00000001/sig0000016f ;
  wire \blk00000001/sig0000016e ;
  wire \blk00000001/sig0000016d ;
  wire \blk00000001/sig0000016c ;
  wire \blk00000001/sig0000016b ;
  wire \blk00000001/sig0000016a ;
  wire \blk00000001/sig00000169 ;
  wire \blk00000001/sig00000168 ;
  wire \blk00000001/sig00000167 ;
  wire \blk00000001/sig00000166 ;
  wire \blk00000001/sig00000165 ;
  wire \blk00000001/sig00000164 ;
  wire \blk00000001/sig00000163 ;
  wire \blk00000001/sig00000162 ;
  wire \blk00000001/sig00000161 ;
  wire \blk00000001/sig00000160 ;
  wire \blk00000001/sig0000015f ;
  wire \blk00000001/sig0000015e ;
  wire \blk00000001/sig0000015d ;
  wire \blk00000001/sig0000015c ;
  wire \blk00000001/sig0000015b ;
  wire \blk00000001/sig0000015a ;
  wire \blk00000001/sig00000159 ;
  wire \blk00000001/sig00000158 ;
  wire \blk00000001/sig00000157 ;
  wire \blk00000001/sig00000156 ;
  wire \blk00000001/sig00000155 ;
  wire \blk00000001/sig00000154 ;
  wire \blk00000001/sig00000153 ;
  wire \blk00000001/sig00000152 ;
  wire \blk00000001/sig00000151 ;
  wire \blk00000001/sig00000150 ;
  wire \blk00000001/sig0000014f ;
  wire \blk00000001/sig0000014e ;
  wire \blk00000001/sig0000014d ;
  wire \blk00000001/sig0000014c ;
  wire \blk00000001/sig0000014b ;
  wire \blk00000001/sig0000014a ;
  wire \blk00000001/sig00000149 ;
  wire \blk00000001/sig00000148 ;
  wire \blk00000001/sig00000147 ;
  wire \blk00000001/sig00000146 ;
  wire \blk00000001/sig00000145 ;
  wire \blk00000001/sig00000144 ;
  wire \blk00000001/sig00000143 ;
  wire \blk00000001/sig00000142 ;
  wire \blk00000001/sig00000141 ;
  wire \blk00000001/sig00000140 ;
  wire \blk00000001/sig0000013f ;
  wire \blk00000001/sig0000013e ;
  wire \blk00000001/sig0000013d ;
  wire \blk00000001/sig0000013c ;
  wire \blk00000001/sig0000013b ;
  wire \blk00000001/sig0000013a ;
  wire \blk00000001/sig00000139 ;
  wire \blk00000001/sig00000138 ;
  wire \blk00000001/sig00000137 ;
  wire \blk00000001/sig00000136 ;
  wire \blk00000001/sig00000135 ;
  wire \blk00000001/sig00000134 ;
  wire \blk00000001/sig00000133 ;
  wire \blk00000001/sig00000132 ;
  wire \blk00000001/sig00000131 ;
  wire \blk00000001/sig00000130 ;
  wire \blk00000001/sig0000012f ;
  wire \blk00000001/sig0000012e ;
  wire \blk00000001/sig0000012d ;
  wire \blk00000001/sig0000012c ;
  wire \blk00000001/sig0000012b ;
  wire \blk00000001/sig0000012a ;
  wire \blk00000001/sig00000129 ;
  wire \blk00000001/sig00000128 ;
  wire \blk00000001/sig00000127 ;
  wire \blk00000001/sig00000126 ;
  wire \blk00000001/sig00000125 ;
  wire \blk00000001/sig00000124 ;
  wire \blk00000001/sig00000123 ;
  wire \blk00000001/sig00000122 ;
  wire \blk00000001/sig00000121 ;
  wire \blk00000001/sig00000120 ;
  wire \blk00000001/sig0000011f ;
  wire \blk00000001/sig0000011e ;
  wire \blk00000001/sig0000011d ;
  wire \blk00000001/sig0000011c ;
  wire \blk00000001/sig0000011b ;
  wire \blk00000001/sig0000011a ;
  wire \blk00000001/sig00000119 ;
  wire \blk00000001/sig00000118 ;
  wire \blk00000001/sig00000117 ;
  wire \blk00000001/sig00000116 ;
  wire \blk00000001/sig00000115 ;
  wire \blk00000001/sig00000114 ;
  wire \blk00000001/sig00000113 ;
  wire \blk00000001/sig00000112 ;
  wire \blk00000001/sig00000111 ;
  wire \blk00000001/sig00000110 ;
  wire \blk00000001/sig0000010f ;
  wire \blk00000001/sig0000010e ;
  wire \blk00000001/sig0000010d ;
  wire \blk00000001/sig0000010c ;
  wire \blk00000001/sig0000010b ;
  wire \blk00000001/sig0000010a ;
  wire \blk00000001/sig00000109 ;
  wire \blk00000001/sig00000108 ;
  wire \blk00000001/sig00000107 ;
  wire \blk00000001/sig00000106 ;
  wire \blk00000001/sig00000105 ;
  wire \blk00000001/sig00000104 ;
  wire \blk00000001/sig00000103 ;
  wire \blk00000001/sig00000102 ;
  wire \blk00000001/sig00000101 ;
  wire \blk00000001/sig00000100 ;
  wire \blk00000001/sig000000ff ;
  wire \blk00000001/sig000000fe ;
  wire \blk00000001/sig000000fd ;
  wire \blk00000001/sig000000fc ;
  wire \blk00000001/sig000000fb ;
  wire \blk00000001/sig000000fa ;
  wire \blk00000001/sig000000f9 ;
  wire \blk00000001/sig000000f8 ;
  wire \blk00000001/sig000000f7 ;
  wire \blk00000001/sig000000f6 ;
  wire \blk00000001/sig000000f5 ;
  wire \blk00000001/sig000000f4 ;
  wire \blk00000001/sig000000f3 ;
  wire \blk00000001/sig000000f2 ;
  wire \blk00000001/sig000000f1 ;
  wire \blk00000001/sig000000f0 ;
  wire \blk00000001/sig000000ef ;
  wire \blk00000001/sig000000ee ;
  wire \blk00000001/sig000000ed ;
  wire \blk00000001/sig000000ec ;
  wire \blk00000001/sig000000eb ;
  wire \blk00000001/sig000000ea ;
  wire \blk00000001/sig000000e9 ;
  wire \blk00000001/sig000000e8 ;
  wire \blk00000001/sig000000e7 ;
  wire \blk00000001/sig000000e6 ;
  wire \blk00000001/sig000000e5 ;
  wire \blk00000001/sig000000e4 ;
  wire \blk00000001/sig000000e3 ;
  wire \blk00000001/sig000000e2 ;
  wire \blk00000001/sig000000e1 ;
  wire \blk00000001/sig000000e0 ;
  wire \blk00000001/sig000000df ;
  wire \blk00000001/sig000000de ;
  wire \blk00000001/sig000000dd ;
  wire \blk00000001/sig000000dc ;
  wire \blk00000001/sig000000db ;
  wire \blk00000001/sig000000da ;
  wire \blk00000001/sig000000d9 ;
  wire \blk00000001/sig000000d8 ;
  wire \blk00000001/sig000000d7 ;
  wire \blk00000001/sig000000d6 ;
  wire \blk00000001/sig000000d5 ;
  wire \blk00000001/sig000000d4 ;
  wire \blk00000001/sig000000d3 ;
  wire \blk00000001/sig000000d2 ;
  wire \blk00000001/sig000000d1 ;
  wire \blk00000001/sig000000d0 ;
  wire \blk00000001/sig000000cf ;
  wire \blk00000001/sig000000ce ;
  wire \blk00000001/sig000000cd ;
  wire \blk00000001/sig000000cc ;
  wire \blk00000001/sig000000cb ;
  wire \blk00000001/sig000000ca ;
  wire \blk00000001/sig000000c9 ;
  wire \blk00000001/sig000000c8 ;
  wire \blk00000001/sig000000c7 ;
  wire \blk00000001/sig000000c6 ;
  wire \blk00000001/sig000000c5 ;
  wire \blk00000001/sig000000c4 ;
  wire \blk00000001/sig000000c3 ;
  wire \blk00000001/sig000000c2 ;
  wire \blk00000001/sig000000c1 ;
  wire \blk00000001/sig000000c0 ;
  wire \blk00000001/sig000000bf ;
  wire \blk00000001/sig000000be ;
  wire \blk00000001/sig000000bd ;
  wire \blk00000001/sig000000bc ;
  wire \blk00000001/sig000000bb ;
  wire \blk00000001/sig000000ba ;
  wire \blk00000001/sig000000b9 ;
  wire \blk00000001/sig000000b8 ;
  wire \blk00000001/sig000000b7 ;
  wire \blk00000001/sig000000b6 ;
  wire \blk00000001/sig000000b5 ;
  wire \blk00000001/sig000000b4 ;
  wire \blk00000001/sig000000b3 ;
  wire \blk00000001/sig000000b2 ;
  wire \blk00000001/sig000000b1 ;
  wire \blk00000001/sig000000b0 ;
  wire \blk00000001/sig000000af ;
  wire \blk00000001/sig000000ae ;
  wire \blk00000001/sig000000ad ;
  wire \blk00000001/sig000000ac ;
  wire \blk00000001/sig000000ab ;
  wire \blk00000001/sig000000aa ;
  wire \blk00000001/sig000000a9 ;
  wire \blk00000001/sig000000a8 ;
  wire \blk00000001/sig000000a7 ;
  wire \blk00000001/sig000000a6 ;
  wire \blk00000001/sig000000a5 ;
  wire \blk00000001/sig000000a4 ;
  wire \blk00000001/sig000000a3 ;
  wire \blk00000001/sig000000a2 ;
  wire \blk00000001/sig000000a1 ;
  wire \blk00000001/sig000000a0 ;
  wire \blk00000001/sig0000009f ;
  wire \blk00000001/sig0000009e ;
  wire \blk00000001/sig0000009d ;
  wire \blk00000001/sig0000009c ;
  wire \blk00000001/sig0000009b ;
  wire \blk00000001/sig0000009a ;
  wire \blk00000001/sig00000099 ;
  wire \blk00000001/sig00000098 ;
  wire \blk00000001/sig00000097 ;
  wire \blk00000001/sig00000096 ;
  wire \blk00000001/sig00000095 ;
  wire \blk00000001/sig00000094 ;
  wire \blk00000001/sig00000093 ;
  wire \blk00000001/sig00000092 ;
  wire \blk00000001/sig00000091 ;
  wire \blk00000001/sig00000090 ;
  wire \blk00000001/sig0000008f ;
  wire \blk00000001/sig0000008e ;
  wire \blk00000001/sig0000008d ;
  wire \blk00000001/sig0000008c ;
  wire \blk00000001/sig0000008b ;
  wire \blk00000001/sig0000008a ;
  wire \blk00000001/sig00000089 ;
  wire \blk00000001/sig00000088 ;
  wire \blk00000001/sig00000087 ;
  wire \blk00000001/sig00000086 ;
  wire \blk00000001/sig00000085 ;
  wire \blk00000001/sig00000084 ;
  wire \blk00000001/sig00000083 ;
  wire \blk00000001/sig00000082 ;
  wire \blk00000001/sig00000081 ;
  wire \blk00000001/sig00000080 ;
  wire \blk00000001/sig0000007f ;
  wire \blk00000001/sig0000007e ;
  wire \blk00000001/sig0000007d ;
  wire \blk00000001/sig0000007c ;
  wire \blk00000001/sig0000007b ;
  wire \blk00000001/sig0000007a ;
  wire \blk00000001/sig00000079 ;
  wire \blk00000001/sig00000078 ;
  wire \blk00000001/sig00000077 ;
  wire \blk00000001/sig00000076 ;
  wire \blk00000001/sig00000075 ;
  wire \blk00000001/sig00000074 ;
  wire \blk00000001/sig00000073 ;
  wire \blk00000001/sig00000072 ;
  wire \blk00000001/sig00000071 ;
  wire \blk00000001/sig00000070 ;
  wire \blk00000001/sig0000006f ;
  wire \blk00000001/sig0000006e ;
  wire \blk00000001/sig0000006d ;
  wire \blk00000001/sig0000006c ;
  wire \blk00000001/sig0000006b ;
  wire \blk00000001/sig0000006a ;
  wire \blk00000001/sig00000069 ;
  wire \blk00000001/sig00000068 ;
  wire \blk00000001/sig00000067 ;
  wire \blk00000001/sig00000066 ;
  wire \blk00000001/sig00000065 ;
  wire \blk00000001/sig00000064 ;
  wire \blk00000001/sig00000063 ;
  wire \blk00000001/sig00000062 ;
  wire \blk00000001/sig00000061 ;
  wire \blk00000001/sig00000060 ;
  wire \blk00000001/sig0000005f ;
  wire \blk00000001/sig0000005e ;
  wire \blk00000001/sig0000005d ;
  wire \blk00000001/sig0000005c ;
  wire \blk00000001/sig0000005b ;
  wire \blk00000001/sig0000005a ;
  wire \blk00000001/sig00000059 ;
  wire \blk00000001/sig00000058 ;
  wire \blk00000001/sig00000057 ;
  wire \blk00000001/sig00000056 ;
  wire \blk00000001/sig00000055 ;
  wire \blk00000001/sig00000054 ;
  wire \blk00000001/sig00000053 ;
  wire \blk00000001/sig00000052 ;
  wire \blk00000001/sig00000051 ;
  wire \blk00000001/sig00000050 ;
  wire \blk00000001/sig0000004f ;
  wire \blk00000001/sig0000004e ;
  wire \blk00000001/sig0000004d ;
  wire \blk00000001/sig0000004c ;
  wire \blk00000001/sig0000004b ;
  wire \blk00000001/sig0000004a ;
  wire \blk00000001/sig00000049 ;
  wire \blk00000001/sig00000048 ;
  wire \blk00000001/sig00000047 ;
  wire \blk00000001/sig00000046 ;
  wire \blk00000001/sig00000044 ;
  wire \blk00000001/sig00000043 ;
  wire \NLW_blk00000001/blk0000004c_O_UNCONNECTED ;
  FDRE #(
    .INIT ( 1'b0 ))
  \blk00000001/blk000001a4  (
    .C(clk),
    .CE(ce),
    .D(\blk00000001/sig00000194 ),
    .R(sclr),
    .Q(s[3])
  );
  LUT2 #(
    .INIT ( 4'h8 ))
  \blk00000001/blk000001a3  (
    .I0(\blk00000001/sig00000193 ),
    .I1(\blk00000001/sig0000004c ),
    .O(\blk00000001/sig00000194 )
  );
  FDRE #(
    .INIT ( 1'b0 ))
  \blk00000001/blk000001a2  (
    .C(clk),
    .CE(ce),
    .D(\blk00000001/sig00000191 ),
    .R(sclr),
    .Q(s[2])
  );
  LUT2 #(
    .INIT ( 4'h8 ))
  \blk00000001/blk000001a1  (
    .I0(\blk00000001/sig00000190 ),
    .I1(\blk00000001/sig0000004c ),
    .O(\blk00000001/sig00000191 )
  );
  FDRE #(
    .INIT ( 1'b0 ))
  \blk00000001/blk000001a0  (
    .C(clk),
    .CE(ce),
    .D(\blk00000001/sig0000018e ),
    .R(sclr),
    .Q(s[1])
  );
  LUT2 #(
    .INIT ( 4'h8 ))
  \blk00000001/blk0000019f  (
    .I0(\blk00000001/sig0000018d ),
    .I1(\blk00000001/sig0000004c ),
    .O(\blk00000001/sig0000018e )
  );
  FDRE #(
    .INIT ( 1'b0 ))
  \blk00000001/blk0000019e  (
    .C(clk),
    .CE(ce),
    .D(\blk00000001/sig0000018b ),
    .R(sclr),
    .Q(s[0])
  );
  LUT2 #(
    .INIT ( 4'h8 ))
  \blk00000001/blk0000019d  (
    .I0(\blk00000001/sig0000018a ),
    .I1(\blk00000001/sig0000004c ),
    .O(\blk00000001/sig0000018b )
  );
  FDRE #(
    .INIT ( 1'b0 ))
  \blk00000001/blk0000019c  (
    .C(clk),
    .CE(ce),
    .D(\blk00000001/sig0000019e ),
    .R(sclr),
    .Q(s[5])
  );
  LUT2 #(
    .INIT ( 4'h8 ))
  \blk00000001/blk0000019b  (
    .I0(\blk00000001/sig0000019d ),
    .I1(\blk00000001/sig0000004b ),
    .O(\blk00000001/sig0000019e )
  );
  FDRE #(
    .INIT ( 1'b0 ))
  \blk00000001/blk0000019a  (
    .C(clk),
    .CE(ce),
    .D(\blk00000001/sig0000019b ),
    .R(sclr),
    .Q(s[4])
  );
  LUT2 #(
    .INIT ( 4'h8 ))
  \blk00000001/blk00000199  (
    .I0(\blk00000001/sig0000019a ),
    .I1(\blk00000001/sig0000004b ),
    .O(\blk00000001/sig0000019b )
  );
  FDRE #(
    .INIT ( 1'b0 ))
  \blk00000001/blk00000198  (
    .C(clk),
    .CE(ce),
    .D(\blk00000001/sig00000184 ),
    .R(sclr),
    .Q(\blk00000001/sig00000182 )
  );
  LUT2 #(
    .INIT ( 4'h8 ))
  \blk00000001/blk00000197  (
    .I0(\blk00000001/sig00000183 ),
    .I1(\blk00000001/sig0000004b ),
    .O(\blk00000001/sig00000184 )
  );
  FDRE #(
    .INIT ( 1'b0 ))
  \blk00000001/blk00000196  (
    .C(clk),
    .CE(ce),
    .D(\blk00000001/sig00000181 ),
    .R(sclr),
    .Q(\blk00000001/sig0000017f )
  );
  LUT2 #(
    .INIT ( 4'h8 ))
  \blk00000001/blk00000195  (
    .I0(\blk00000001/sig00000180 ),
    .I1(\blk00000001/sig0000004b ),
    .O(\blk00000001/sig00000181 )
  );
  FDRE #(
    .INIT ( 1'b0 ))
  \blk00000001/blk00000194  (
    .C(clk),
    .CE(ce),
    .D(\blk00000001/sig0000017e ),
    .R(sclr),
    .Q(\blk00000001/sig0000017c )
  );
  LUT2 #(
    .INIT ( 4'h8 ))
  \blk00000001/blk00000193  (
    .I0(\blk00000001/sig0000017d ),
    .I1(\blk00000001/sig0000004b ),
    .O(\blk00000001/sig0000017e )
  );
  FDRE #(
    .INIT ( 1'b0 ))
  \blk00000001/blk00000192  (
    .C(clk),
    .CE(ce),
    .D(\blk00000001/sig0000017b ),
    .R(sclr),
    .Q(\blk00000001/sig00000179 )
  );
  LUT2 #(
    .INIT ( 4'h8 ))
  \blk00000001/blk00000191  (
    .I0(\blk00000001/sig0000017a ),
    .I1(\blk00000001/sig0000004b ),
    .O(\blk00000001/sig0000017b )
  );
  FDRE #(
    .INIT ( 1'b0 ))
  \blk00000001/blk00000190  (
    .C(clk),
    .CE(ce),
    .D(\blk00000001/sig000001ae ),
    .R(sclr),
    .Q(s[9])
  );
  LUT2 #(
    .INIT ( 4'h8 ))
  \blk00000001/blk0000018f  (
    .I0(\blk00000001/sig000001ad ),
    .I1(\blk00000001/sig0000004a ),
    .O(\blk00000001/sig000001ae )
  );
  FDRE #(
    .INIT ( 1'b0 ))
  \blk00000001/blk0000018e  (
    .C(clk),
    .CE(ce),
    .D(\blk00000001/sig000001ab ),
    .R(sclr),
    .Q(s[8])
  );
  LUT2 #(
    .INIT ( 4'h8 ))
  \blk00000001/blk0000018d  (
    .I0(\blk00000001/sig000001aa ),
    .I1(\blk00000001/sig0000004a ),
    .O(\blk00000001/sig000001ab )
  );
  FDRE #(
    .INIT ( 1'b0 ))
  \blk00000001/blk0000018c  (
    .C(clk),
    .CE(ce),
    .D(\blk00000001/sig000001a4 ),
    .R(sclr),
    .Q(s[7])
  );
  LUT2 #(
    .INIT ( 4'h8 ))
  \blk00000001/blk0000018b  (
    .I0(\blk00000001/sig000001a3 ),
    .I1(\blk00000001/sig0000004a ),
    .O(\blk00000001/sig000001a4 )
  );
  FDRE #(
    .INIT ( 1'b0 ))
  \blk00000001/blk0000018a  (
    .C(clk),
    .CE(ce),
    .D(\blk00000001/sig000001a1 ),
    .R(sclr),
    .Q(s[6])
  );
  LUT2 #(
    .INIT ( 4'h8 ))
  \blk00000001/blk00000189  (
    .I0(\blk00000001/sig000001a0 ),
    .I1(\blk00000001/sig0000004a ),
    .O(\blk00000001/sig000001a1 )
  );
  FDRE #(
    .INIT ( 1'b0 ))
  \blk00000001/blk00000188  (
    .C(clk),
    .CE(ce),
    .D(\blk00000001/sig0000015e ),
    .R(sclr),
    .Q(\blk00000001/sig0000015c )
  );
  LUT2 #(
    .INIT ( 4'h8 ))
  \blk00000001/blk00000187  (
    .I0(\blk00000001/sig0000015d ),
    .I1(\blk00000001/sig0000004a ),
    .O(\blk00000001/sig0000015e )
  );
  FDRE #(
    .INIT ( 1'b0 ))
  \blk00000001/blk00000186  (
    .C(clk),
    .CE(ce),
    .D(\blk00000001/sig0000015b ),
    .R(sclr),
    .Q(\blk00000001/sig00000159 )
  );
  LUT2 #(
    .INIT ( 4'h8 ))
  \blk00000001/blk00000185  (
    .I0(\blk00000001/sig0000015a ),
    .I1(\blk00000001/sig0000004a ),
    .O(\blk00000001/sig0000015b )
  );
  FDRE #(
    .INIT ( 1'b0 ))
  \blk00000001/blk00000184  (
    .C(clk),
    .CE(ce),
    .D(\blk00000001/sig00000158 ),
    .R(sclr),
    .Q(\blk00000001/sig00000156 )
  );
  LUT2 #(
    .INIT ( 4'h8 ))
  \blk00000001/blk00000183  (
    .I0(\blk00000001/sig00000157 ),
    .I1(\blk00000001/sig0000004a ),
    .O(\blk00000001/sig00000158 )
  );
  FDRE #(
    .INIT ( 1'b0 ))
  \blk00000001/blk00000182  (
    .C(clk),
    .CE(ce),
    .D(\blk00000001/sig00000155 ),
    .R(sclr),
    .Q(\blk00000001/sig00000153 )
  );
  LUT2 #(
    .INIT ( 4'h8 ))
  \blk00000001/blk00000181  (
    .I0(\blk00000001/sig00000154 ),
    .I1(\blk00000001/sig0000004a ),
    .O(\blk00000001/sig00000155 )
  );
  FDRE #(
    .INIT ( 1'b0 ))
  \blk00000001/blk00000180  (
    .C(clk),
    .CE(ce),
    .D(\blk00000001/sig000001be ),
    .R(sclr),
    .Q(s[13])
  );
  LUT2 #(
    .INIT ( 4'h8 ))
  \blk00000001/blk0000017f  (
    .I0(\blk00000001/sig000001bd ),
    .I1(\blk00000001/sig00000049 ),
    .O(\blk00000001/sig000001be )
  );
  FDRE #(
    .INIT ( 1'b0 ))
  \blk00000001/blk0000017e  (
    .C(clk),
    .CE(ce),
    .D(\blk00000001/sig000001bb ),
    .R(sclr),
    .Q(s[12])
  );
  LUT2 #(
    .INIT ( 4'h8 ))
  \blk00000001/blk0000017d  (
    .I0(\blk00000001/sig000001ba ),
    .I1(\blk00000001/sig00000049 ),
    .O(\blk00000001/sig000001bb )
  );
  FDRE #(
    .INIT ( 1'b0 ))
  \blk00000001/blk0000017c  (
    .C(clk),
    .CE(ce),
    .D(\blk00000001/sig000001b4 ),
    .R(sclr),
    .Q(s[11])
  );
  LUT2 #(
    .INIT ( 4'h8 ))
  \blk00000001/blk0000017b  (
    .I0(\blk00000001/sig000001b3 ),
    .I1(\blk00000001/sig00000049 ),
    .O(\blk00000001/sig000001b4 )
  );
  FDRE #(
    .INIT ( 1'b0 ))
  \blk00000001/blk0000017a  (
    .C(clk),
    .CE(ce),
    .D(\blk00000001/sig000001b1 ),
    .R(sclr),
    .Q(s[10])
  );
  LUT2 #(
    .INIT ( 4'h8 ))
  \blk00000001/blk00000179  (
    .I0(\blk00000001/sig000001b0 ),
    .I1(\blk00000001/sig00000049 ),
    .O(\blk00000001/sig000001b1 )
  );
  FDRE #(
    .INIT ( 1'b0 ))
  \blk00000001/blk00000178  (
    .C(clk),
    .CE(ce),
    .D(\blk00000001/sig0000014e ),
    .R(sclr),
    .Q(\blk00000001/sig0000014c )
  );
  LUT2 #(
    .INIT ( 4'h8 ))
  \blk00000001/blk00000177  (
    .I0(\blk00000001/sig0000014d ),
    .I1(\blk00000001/sig00000049 ),
    .O(\blk00000001/sig0000014e )
  );
  FDRE #(
    .INIT ( 1'b0 ))
  \blk00000001/blk00000176  (
    .C(clk),
    .CE(ce),
    .D(\blk00000001/sig00000138 ),
    .R(sclr),
    .Q(\blk00000001/sig00000136 )
  );
  LUT2 #(
    .INIT ( 4'h8 ))
  \blk00000001/blk00000175  (
    .I0(\blk00000001/sig00000137 ),
    .I1(\blk00000001/sig00000049 ),
    .O(\blk00000001/sig00000138 )
  );
  FDRE #(
    .INIT ( 1'b0 ))
  \blk00000001/blk00000174  (
    .C(clk),
    .CE(ce),
    .D(\blk00000001/sig00000135 ),
    .R(sclr),
    .Q(\blk00000001/sig00000133 )
  );
  LUT2 #(
    .INIT ( 4'h8 ))
  \blk00000001/blk00000173  (
    .I0(\blk00000001/sig00000134 ),
    .I1(\blk00000001/sig00000049 ),
    .O(\blk00000001/sig00000135 )
  );
  FDRE #(
    .INIT ( 1'b0 ))
  \blk00000001/blk00000172  (
    .C(clk),
    .CE(ce),
    .D(\blk00000001/sig00000132 ),
    .R(sclr),
    .Q(\blk00000001/sig00000130 )
  );
  LUT2 #(
    .INIT ( 4'h8 ))
  \blk00000001/blk00000171  (
    .I0(\blk00000001/sig00000131 ),
    .I1(\blk00000001/sig00000049 ),
    .O(\blk00000001/sig00000132 )
  );
  FDRE #(
    .INIT ( 1'b0 ))
  \blk00000001/blk00000170  (
    .C(clk),
    .CE(ce),
    .D(\blk00000001/sig0000012f ),
    .R(sclr),
    .Q(\blk00000001/sig0000012d )
  );
  LUT2 #(
    .INIT ( 4'h8 ))
  \blk00000001/blk0000016f  (
    .I0(\blk00000001/sig0000012e ),
    .I1(\blk00000001/sig00000049 ),
    .O(\blk00000001/sig0000012f )
  );
  FDRE #(
    .INIT ( 1'b0 ))
  \blk00000001/blk0000016e  (
    .C(clk),
    .CE(ce),
    .D(\blk00000001/sig000001cc ),
    .R(sclr),
    .Q(s[17])
  );
  LUT2 #(
    .INIT ( 4'h8 ))
  \blk00000001/blk0000016d  (
    .I0(\blk00000001/sig000001cb ),
    .I1(\blk00000001/sig00000048 ),
    .O(\blk00000001/sig000001cc )
  );
  FDRE #(
    .INIT ( 1'b0 ))
  \blk00000001/blk0000016c  (
    .C(clk),
    .CE(ce),
    .D(\blk00000001/sig000001c9 ),
    .R(sclr),
    .Q(s[16])
  );
  LUT2 #(
    .INIT ( 4'h8 ))
  \blk00000001/blk0000016b  (
    .I0(\blk00000001/sig000001c8 ),
    .I1(\blk00000001/sig00000048 ),
    .O(\blk00000001/sig000001c9 )
  );
  FDRE #(
    .INIT ( 1'b0 ))
  \blk00000001/blk0000016a  (
    .C(clk),
    .CE(ce),
    .D(\blk00000001/sig000001c4 ),
    .R(sclr),
    .Q(s[15])
  );
  LUT2 #(
    .INIT ( 4'h8 ))
  \blk00000001/blk00000169  (
    .I0(\blk00000001/sig000001c3 ),
    .I1(\blk00000001/sig00000048 ),
    .O(\blk00000001/sig000001c4 )
  );
  FDRE #(
    .INIT ( 1'b0 ))
  \blk00000001/blk00000168  (
    .C(clk),
    .CE(ce),
    .D(\blk00000001/sig000001c1 ),
    .R(sclr),
    .Q(s[14])
  );
  LUT2 #(
    .INIT ( 4'h8 ))
  \blk00000001/blk00000167  (
    .I0(\blk00000001/sig000001c0 ),
    .I1(\blk00000001/sig00000048 ),
    .O(\blk00000001/sig000001c1 )
  );
  FDRE #(
    .INIT ( 1'b0 ))
  \blk00000001/blk00000166  (
    .C(clk),
    .CE(ce),
    .D(\blk00000001/sig00000128 ),
    .R(sclr),
    .Q(\blk00000001/sig00000126 )
  );
  LUT2 #(
    .INIT ( 4'h8 ))
  \blk00000001/blk00000165  (
    .I0(\blk00000001/sig00000127 ),
    .I1(\blk00000001/sig00000048 ),
    .O(\blk00000001/sig00000128 )
  );
  FDRE #(
    .INIT ( 1'b0 ))
  \blk00000001/blk00000164  (
    .C(clk),
    .CE(ce),
    .D(\blk00000001/sig00000116 ),
    .R(sclr),
    .Q(\blk00000001/sig00000114 )
  );
  LUT2 #(
    .INIT ( 4'h8 ))
  \blk00000001/blk00000163  (
    .I0(\blk00000001/sig00000115 ),
    .I1(\blk00000001/sig00000048 ),
    .O(\blk00000001/sig00000116 )
  );
  FDRE #(
    .INIT ( 1'b0 ))
  \blk00000001/blk00000162  (
    .C(clk),
    .CE(ce),
    .D(\blk00000001/sig00000113 ),
    .R(sclr),
    .Q(\blk00000001/sig00000111 )
  );
  LUT2 #(
    .INIT ( 4'h8 ))
  \blk00000001/blk00000161  (
    .I0(\blk00000001/sig00000112 ),
    .I1(\blk00000001/sig00000048 ),
    .O(\blk00000001/sig00000113 )
  );
  FDRE #(
    .INIT ( 1'b0 ))
  \blk00000001/blk00000160  (
    .C(clk),
    .CE(ce),
    .D(\blk00000001/sig00000110 ),
    .R(sclr),
    .Q(\blk00000001/sig0000010e )
  );
  LUT2 #(
    .INIT ( 4'h8 ))
  \blk00000001/blk0000015f  (
    .I0(\blk00000001/sig0000010f ),
    .I1(\blk00000001/sig00000048 ),
    .O(\blk00000001/sig00000110 )
  );
  FDRE #(
    .INIT ( 1'b0 ))
  \blk00000001/blk0000015e  (
    .C(clk),
    .CE(ce),
    .D(\blk00000001/sig0000010d ),
    .R(sclr),
    .Q(\blk00000001/sig0000010b )
  );
  LUT2 #(
    .INIT ( 4'h8 ))
  \blk00000001/blk0000015d  (
    .I0(\blk00000001/sig0000010c ),
    .I1(\blk00000001/sig00000048 ),
    .O(\blk00000001/sig0000010d )
  );
  FDRE   \blk00000001/blk0000015c  (
    .C(clk),
    .CE(ce),
    .D(\blk00000001/sig0000004b ),
    .R(sclr),
    .Q(\blk00000001/sig0000004c )
  );
  FDRE   \blk00000001/blk0000015b  (
    .C(clk),
    .CE(ce),
    .D(\blk00000001/sig0000004a ),
    .R(sclr),
    .Q(\blk00000001/sig0000004b )
  );
  FDRE   \blk00000001/blk0000015a  (
    .C(clk),
    .CE(ce),
    .D(\blk00000001/sig00000049 ),
    .R(sclr),
    .Q(\blk00000001/sig0000004a )
  );
  FDRE   \blk00000001/blk00000159  (
    .C(clk),
    .CE(ce),
    .D(\blk00000001/sig00000048 ),
    .R(sclr),
    .Q(\blk00000001/sig00000049 )
  );
  FDRE   \blk00000001/blk00000158  (
    .C(clk),
    .CE(ce),
    .D(\blk00000001/sig00000047 ),
    .R(sclr),
    .Q(\blk00000001/sig00000048 )
  );
  FDRE   \blk00000001/blk00000157  (
    .C(clk),
    .CE(ce),
    .D(\blk00000001/sig00000046 ),
    .R(sclr),
    .Q(\blk00000001/sig00000047 )
  );
  FDRE   \blk00000001/blk00000156  (
    .C(clk),
    .CE(ce),
    .D(\blk00000001/sig00000044 ),
    .R(sclr),
    .Q(\blk00000001/sig00000046 )
  );
  FDE #(
    .INIT ( 1'b0 ))
  \blk00000001/blk00000155  (
    .C(clk),
    .CE(ce),
    .D(\blk00000001/sig000001c5 ),
    .Q(\blk00000001/sig000001c8 )
  );
  SRL16E #(
    .INIT ( 16'h0000 ))
  \blk00000001/blk00000154  (
    .A0(\blk00000001/sig00000044 ),
    .A1(\blk00000001/sig00000043 ),
    .A2(\blk00000001/sig00000043 ),
    .A3(\blk00000001/sig00000043 ),
    .CE(ce),
    .CLK(clk),
    .D(\blk00000001/sig00000120 ),
    .Q(\blk00000001/sig000001c5 )
  );
  FDE #(
    .INIT ( 1'b0 ))
  \blk00000001/blk00000153  (
    .C(clk),
    .CE(ce),
    .D(\blk00000001/sig000001c6 ),
    .Q(\blk00000001/sig000001cb )
  );
  SRL16E #(
    .INIT ( 16'h0000 ))
  \blk00000001/blk00000152  (
    .A0(\blk00000001/sig00000044 ),
    .A1(\blk00000001/sig00000043 ),
    .A2(\blk00000001/sig00000043 ),
    .A3(\blk00000001/sig00000043 ),
    .CE(ce),
    .CLK(clk),
    .D(\blk00000001/sig00000121 ),
    .Q(\blk00000001/sig000001c6 )
  );
  FDE #(
    .INIT ( 1'b0 ))
  \blk00000001/blk00000151  (
    .C(clk),
    .CE(ce),
    .D(\blk00000001/sig000001b5 ),
    .Q(\blk00000001/sig000001ba )
  );
  SRL16E #(
    .INIT ( 16'h0000 ))
  \blk00000001/blk00000150  (
    .A0(\blk00000001/sig00000043 ),
    .A1(\blk00000001/sig00000044 ),
    .A2(\blk00000001/sig00000043 ),
    .A3(\blk00000001/sig00000043 ),
    .CE(ce),
    .CLK(clk),
    .D(\blk00000001/sig00000101 ),
    .Q(\blk00000001/sig000001b5 )
  );
  FDE #(
    .INIT ( 1'b0 ))
  \blk00000001/blk0000014f  (
    .C(clk),
    .CE(ce),
    .D(\blk00000001/sig000001b6 ),
    .Q(\blk00000001/sig000001bd )
  );
  SRL16E #(
    .INIT ( 16'h0000 ))
  \blk00000001/blk0000014e  (
    .A0(\blk00000001/sig00000043 ),
    .A1(\blk00000001/sig00000044 ),
    .A2(\blk00000001/sig00000043 ),
    .A3(\blk00000001/sig00000043 ),
    .CE(ce),
    .CLK(clk),
    .D(\blk00000001/sig00000102 ),
    .Q(\blk00000001/sig000001b6 )
  );
  FDE #(
    .INIT ( 1'b0 ))
  \blk00000001/blk0000014d  (
    .C(clk),
    .CE(ce),
    .D(\blk00000001/sig000001b7 ),
    .Q(\blk00000001/sig000001c0 )
  );
  SRL16E #(
    .INIT ( 16'h0000 ))
  \blk00000001/blk0000014c  (
    .A0(\blk00000001/sig00000044 ),
    .A1(\blk00000001/sig00000043 ),
    .A2(\blk00000001/sig00000043 ),
    .A3(\blk00000001/sig00000043 ),
    .CE(ce),
    .CLK(clk),
    .D(\blk00000001/sig000000ff ),
    .Q(\blk00000001/sig000001b7 )
  );
  FDE #(
    .INIT ( 1'b0 ))
  \blk00000001/blk0000014b  (
    .C(clk),
    .CE(ce),
    .D(\blk00000001/sig000001b8 ),
    .Q(\blk00000001/sig000001c3 )
  );
  SRL16E #(
    .INIT ( 16'h0000 ))
  \blk00000001/blk0000014a  (
    .A0(\blk00000001/sig00000044 ),
    .A1(\blk00000001/sig00000043 ),
    .A2(\blk00000001/sig00000043 ),
    .A3(\blk00000001/sig00000043 ),
    .CE(ce),
    .CLK(clk),
    .D(\blk00000001/sig00000100 ),
    .Q(\blk00000001/sig000001b8 )
  );
  FDE #(
    .INIT ( 1'b0 ))
  \blk00000001/blk00000149  (
    .C(clk),
    .CE(ce),
    .D(\blk00000001/sig000001a6 ),
    .Q(\blk00000001/sig000001ad )
  );
  SRL16E #(
    .INIT ( 16'h0000 ))
  \blk00000001/blk00000148  (
    .A0(\blk00000001/sig00000044 ),
    .A1(\blk00000001/sig00000044 ),
    .A2(\blk00000001/sig00000043 ),
    .A3(\blk00000001/sig00000043 ),
    .CE(ce),
    .CLK(clk),
    .D(\blk00000001/sig000000ee ),
    .Q(\blk00000001/sig000001a6 )
  );
  FDE #(
    .INIT ( 1'b0 ))
  \blk00000001/blk00000147  (
    .C(clk),
    .CE(ce),
    .D(\blk00000001/sig000001a7 ),
    .Q(\blk00000001/sig000001b0 )
  );
  SRL16E #(
    .INIT ( 16'h0000 ))
  \blk00000001/blk00000146  (
    .A0(\blk00000001/sig00000043 ),
    .A1(\blk00000001/sig00000044 ),
    .A2(\blk00000001/sig00000043 ),
    .A3(\blk00000001/sig00000043 ),
    .CE(ce),
    .CLK(clk),
    .D(\blk00000001/sig000000eb ),
    .Q(\blk00000001/sig000001a7 )
  );
  FDE #(
    .INIT ( 1'b0 ))
  \blk00000001/blk00000145  (
    .C(clk),
    .CE(ce),
    .D(\blk00000001/sig000001a5 ),
    .Q(\blk00000001/sig000001aa )
  );
  SRL16E #(
    .INIT ( 16'h0000 ))
  \blk00000001/blk00000144  (
    .A0(\blk00000001/sig00000044 ),
    .A1(\blk00000001/sig00000044 ),
    .A2(\blk00000001/sig00000043 ),
    .A3(\blk00000001/sig00000043 ),
    .CE(ce),
    .CLK(clk),
    .D(\blk00000001/sig000000ed ),
    .Q(\blk00000001/sig000001a5 )
  );
  FDE #(
    .INIT ( 1'b0 ))
  \blk00000001/blk00000143  (
    .C(clk),
    .CE(ce),
    .D(\blk00000001/sig000001a8 ),
    .Q(\blk00000001/sig000001b3 )
  );
  SRL16E #(
    .INIT ( 16'h0000 ))
  \blk00000001/blk00000142  (
    .A0(\blk00000001/sig00000043 ),
    .A1(\blk00000001/sig00000044 ),
    .A2(\blk00000001/sig00000043 ),
    .A3(\blk00000001/sig00000043 ),
    .CE(ce),
    .CLK(clk),
    .D(\blk00000001/sig000000ec ),
    .Q(\blk00000001/sig000001a8 )
  );
  FDE #(
    .INIT ( 1'b0 ))
  \blk00000001/blk00000141  (
    .C(clk),
    .CE(ce),
    .D(\blk00000001/sig00000195 ),
    .Q(\blk00000001/sig0000019a )
  );
  SRL16E #(
    .INIT ( 16'h0000 ))
  \blk00000001/blk00000140  (
    .A0(\blk00000001/sig00000043 ),
    .A1(\blk00000001/sig00000043 ),
    .A2(\blk00000001/sig00000044 ),
    .A3(\blk00000001/sig00000043 ),
    .CE(ce),
    .CLK(clk),
    .D(\blk00000001/sig000000d4 ),
    .Q(\blk00000001/sig00000195 )
  );
  FDE #(
    .INIT ( 1'b0 ))
  \blk00000001/blk0000013f  (
    .C(clk),
    .CE(ce),
    .D(\blk00000001/sig00000196 ),
    .Q(\blk00000001/sig0000019d )
  );
  SRL16E #(
    .INIT ( 16'h0000 ))
  \blk00000001/blk0000013e  (
    .A0(\blk00000001/sig00000043 ),
    .A1(\blk00000001/sig00000043 ),
    .A2(\blk00000001/sig00000044 ),
    .A3(\blk00000001/sig00000043 ),
    .CE(ce),
    .CLK(clk),
    .D(\blk00000001/sig000000d5 ),
    .Q(\blk00000001/sig00000196 )
  );
  FDE #(
    .INIT ( 1'b0 ))
  \blk00000001/blk0000013d  (
    .C(clk),
    .CE(ce),
    .D(\blk00000001/sig00000197 ),
    .Q(\blk00000001/sig000001a0 )
  );
  SRL16E #(
    .INIT ( 16'h0000 ))
  \blk00000001/blk0000013c  (
    .A0(\blk00000001/sig00000044 ),
    .A1(\blk00000001/sig00000044 ),
    .A2(\blk00000001/sig00000043 ),
    .A3(\blk00000001/sig00000043 ),
    .CE(ce),
    .CLK(clk),
    .D(\blk00000001/sig000000d2 ),
    .Q(\blk00000001/sig00000197 )
  );
  FDE #(
    .INIT ( 1'b0 ))
  \blk00000001/blk0000013b  (
    .C(clk),
    .CE(ce),
    .D(\blk00000001/sig00000198 ),
    .Q(\blk00000001/sig000001a3 )
  );
  SRL16E #(
    .INIT ( 16'h0000 ))
  \blk00000001/blk0000013a  (
    .A0(\blk00000001/sig00000044 ),
    .A1(\blk00000001/sig00000044 ),
    .A2(\blk00000001/sig00000043 ),
    .A3(\blk00000001/sig00000043 ),
    .CE(ce),
    .CLK(clk),
    .D(\blk00000001/sig000000d3 ),
    .Q(\blk00000001/sig00000198 )
  );
  FDE #(
    .INIT ( 1'b0 ))
  \blk00000001/blk00000139  (
    .C(clk),
    .CE(ce),
    .D(\blk00000001/sig00000185 ),
    .Q(\blk00000001/sig0000018a )
  );
  SRL16E #(
    .INIT ( 16'h0000 ))
  \blk00000001/blk00000138  (
    .A0(\blk00000001/sig00000044 ),
    .A1(\blk00000001/sig00000043 ),
    .A2(\blk00000001/sig00000044 ),
    .A3(\blk00000001/sig00000043 ),
    .CE(ce),
    .CLK(clk),
    .D(\blk00000001/sig00000062 ),
    .Q(\blk00000001/sig00000185 )
  );
  FDE #(
    .INIT ( 1'b0 ))
  \blk00000001/blk00000137  (
    .C(clk),
    .CE(ce),
    .D(\blk00000001/sig00000187 ),
    .Q(\blk00000001/sig00000190 )
  );
  SRL16E #(
    .INIT ( 16'h0000 ))
  \blk00000001/blk00000136  (
    .A0(\blk00000001/sig00000044 ),
    .A1(\blk00000001/sig00000043 ),
    .A2(\blk00000001/sig00000044 ),
    .A3(\blk00000001/sig00000043 ),
    .CE(ce),
    .CLK(clk),
    .D(\blk00000001/sig00000064 ),
    .Q(\blk00000001/sig00000187 )
  );
  FDE #(
    .INIT ( 1'b0 ))
  \blk00000001/blk00000135  (
    .C(clk),
    .CE(ce),
    .D(\blk00000001/sig00000188 ),
    .Q(\blk00000001/sig00000193 )
  );
  SRL16E #(
    .INIT ( 16'h0000 ))
  \blk00000001/blk00000134  (
    .A0(\blk00000001/sig00000044 ),
    .A1(\blk00000001/sig00000043 ),
    .A2(\blk00000001/sig00000044 ),
    .A3(\blk00000001/sig00000043 ),
    .CE(ce),
    .CLK(clk),
    .D(\blk00000001/sig00000065 ),
    .Q(\blk00000001/sig00000188 )
  );
  FDE #(
    .INIT ( 1'b0 ))
  \blk00000001/blk00000133  (
    .C(clk),
    .CE(ce),
    .D(\blk00000001/sig00000186 ),
    .Q(\blk00000001/sig0000018d )
  );
  SRL16E #(
    .INIT ( 16'h0000 ))
  \blk00000001/blk00000132  (
    .A0(\blk00000001/sig00000044 ),
    .A1(\blk00000001/sig00000043 ),
    .A2(\blk00000001/sig00000044 ),
    .A3(\blk00000001/sig00000043 ),
    .CE(ce),
    .CLK(clk),
    .D(\blk00000001/sig00000063 ),
    .Q(\blk00000001/sig00000186 )
  );
  FDE #(
    .INIT ( 1'b0 ))
  \blk00000001/blk00000131  (
    .C(clk),
    .CE(ce),
    .D(\blk00000001/sig00000175 ),
    .Q(\blk00000001/sig0000017a )
  );
  SRL16E #(
    .INIT ( 16'h0000 ))
  \blk00000001/blk00000130  (
    .A0(\blk00000001/sig00000043 ),
    .A1(\blk00000001/sig00000043 ),
    .A2(\blk00000001/sig00000044 ),
    .A3(\blk00000001/sig00000043 ),
    .CE(ce),
    .CLK(clk),
    .D(\blk00000001/sig000000c7 ),
    .Q(\blk00000001/sig00000175 )
  );
  FDE #(
    .INIT ( 1'b0 ))
  \blk00000001/blk0000012f  (
    .C(clk),
    .CE(ce),
    .D(\blk00000001/sig00000176 ),
    .Q(\blk00000001/sig0000017d )
  );
  SRL16E #(
    .INIT ( 16'h0000 ))
  \blk00000001/blk0000012e  (
    .A0(\blk00000001/sig00000043 ),
    .A1(\blk00000001/sig00000043 ),
    .A2(\blk00000001/sig00000044 ),
    .A3(\blk00000001/sig00000043 ),
    .CE(ce),
    .CLK(clk),
    .D(\blk00000001/sig000000c8 ),
    .Q(\blk00000001/sig00000176 )
  );
  FDE #(
    .INIT ( 1'b0 ))
  \blk00000001/blk0000012d  (
    .C(clk),
    .CE(ce),
    .D(\blk00000001/sig00000177 ),
    .Q(\blk00000001/sig00000180 )
  );
  SRL16E #(
    .INIT ( 16'h0000 ))
  \blk00000001/blk0000012c  (
    .A0(\blk00000001/sig00000043 ),
    .A1(\blk00000001/sig00000043 ),
    .A2(\blk00000001/sig00000044 ),
    .A3(\blk00000001/sig00000043 ),
    .CE(ce),
    .CLK(clk),
    .D(\blk00000001/sig000000c9 ),
    .Q(\blk00000001/sig00000177 )
  );
  FDE #(
    .INIT ( 1'b0 ))
  \blk00000001/blk0000012b  (
    .C(clk),
    .CE(ce),
    .D(\blk00000001/sig00000178 ),
    .Q(\blk00000001/sig00000183 )
  );
  SRL16E #(
    .INIT ( 16'h0000 ))
  \blk00000001/blk0000012a  (
    .A0(\blk00000001/sig00000043 ),
    .A1(\blk00000001/sig00000043 ),
    .A2(\blk00000001/sig00000044 ),
    .A3(\blk00000001/sig00000043 ),
    .CE(ce),
    .CLK(clk),
    .D(\blk00000001/sig000000ca ),
    .Q(\blk00000001/sig00000178 )
  );
  FDE #(
    .INIT ( 1'b0 ))
  \blk00000001/blk00000129  (
    .C(clk),
    .CE(ce),
    .D(\blk00000001/sig0000014f ),
    .Q(\blk00000001/sig00000154 )
  );
  SRL16E #(
    .INIT ( 16'h0000 ))
  \blk00000001/blk00000128  (
    .A0(\blk00000001/sig00000044 ),
    .A1(\blk00000001/sig00000044 ),
    .A2(\blk00000001/sig00000043 ),
    .A3(\blk00000001/sig00000043 ),
    .CE(ce),
    .CLK(clk),
    .D(\blk00000001/sig000000bc ),
    .Q(\blk00000001/sig0000014f )
  );
  FDE #(
    .INIT ( 1'b0 ))
  \blk00000001/blk00000127  (
    .C(clk),
    .CE(ce),
    .D(\blk00000001/sig00000150 ),
    .Q(\blk00000001/sig00000157 )
  );
  SRL16E #(
    .INIT ( 16'h0000 ))
  \blk00000001/blk00000126  (
    .A0(\blk00000001/sig00000044 ),
    .A1(\blk00000001/sig00000044 ),
    .A2(\blk00000001/sig00000043 ),
    .A3(\blk00000001/sig00000043 ),
    .CE(ce),
    .CLK(clk),
    .D(\blk00000001/sig000000bd ),
    .Q(\blk00000001/sig00000150 )
  );
  FDE #(
    .INIT ( 1'b0 ))
  \blk00000001/blk00000125  (
    .C(clk),
    .CE(ce),
    .D(\blk00000001/sig00000152 ),
    .Q(\blk00000001/sig0000015d )
  );
  SRL16E #(
    .INIT ( 16'h0000 ))
  \blk00000001/blk00000124  (
    .A0(\blk00000001/sig00000044 ),
    .A1(\blk00000001/sig00000044 ),
    .A2(\blk00000001/sig00000043 ),
    .A3(\blk00000001/sig00000043 ),
    .CE(ce),
    .CLK(clk),
    .D(\blk00000001/sig000000bf ),
    .Q(\blk00000001/sig00000152 )
  );
  FDE #(
    .INIT ( 1'b0 ))
  \blk00000001/blk00000123  (
    .C(clk),
    .CE(ce),
    .D(\blk00000001/sig00000129 ),
    .Q(\blk00000001/sig0000012e )
  );
  SRL16E #(
    .INIT ( 16'h0000 ))
  \blk00000001/blk00000122  (
    .A0(\blk00000001/sig00000043 ),
    .A1(\blk00000001/sig00000044 ),
    .A2(\blk00000001/sig00000043 ),
    .A3(\blk00000001/sig00000043 ),
    .CE(ce),
    .CLK(clk),
    .D(\blk00000001/sig000000af ),
    .Q(\blk00000001/sig00000129 )
  );
  FDE #(
    .INIT ( 1'b0 ))
  \blk00000001/blk00000121  (
    .C(clk),
    .CE(ce),
    .D(\blk00000001/sig00000151 ),
    .Q(\blk00000001/sig0000015a )
  );
  SRL16E #(
    .INIT ( 16'h0000 ))
  \blk00000001/blk00000120  (
    .A0(\blk00000001/sig00000044 ),
    .A1(\blk00000001/sig00000044 ),
    .A2(\blk00000001/sig00000043 ),
    .A3(\blk00000001/sig00000043 ),
    .CE(ce),
    .CLK(clk),
    .D(\blk00000001/sig000000be ),
    .Q(\blk00000001/sig00000151 )
  );
  FDE #(
    .INIT ( 1'b0 ))
  \blk00000001/blk0000011f  (
    .C(clk),
    .CE(ce),
    .D(\blk00000001/sig0000012a ),
    .Q(\blk00000001/sig00000131 )
  );
  SRL16E #(
    .INIT ( 16'h0000 ))
  \blk00000001/blk0000011e  (
    .A0(\blk00000001/sig00000043 ),
    .A1(\blk00000001/sig00000044 ),
    .A2(\blk00000001/sig00000043 ),
    .A3(\blk00000001/sig00000043 ),
    .CE(ce),
    .CLK(clk),
    .D(\blk00000001/sig000000b0 ),
    .Q(\blk00000001/sig0000012a )
  );
  FDE #(
    .INIT ( 1'b0 ))
  \blk00000001/blk0000011d  (
    .C(clk),
    .CE(ce),
    .D(\blk00000001/sig0000012b ),
    .Q(\blk00000001/sig00000134 )
  );
  SRL16E #(
    .INIT ( 16'h0000 ))
  \blk00000001/blk0000011c  (
    .A0(\blk00000001/sig00000043 ),
    .A1(\blk00000001/sig00000044 ),
    .A2(\blk00000001/sig00000043 ),
    .A3(\blk00000001/sig00000043 ),
    .CE(ce),
    .CLK(clk),
    .D(\blk00000001/sig000000b1 ),
    .Q(\blk00000001/sig0000012b )
  );
  FDE #(
    .INIT ( 1'b0 ))
  \blk00000001/blk0000011b  (
    .C(clk),
    .CE(ce),
    .D(\blk00000001/sig0000012c ),
    .Q(\blk00000001/sig00000137 )
  );
  SRL16E #(
    .INIT ( 16'h0000 ))
  \blk00000001/blk0000011a  (
    .A0(\blk00000001/sig00000043 ),
    .A1(\blk00000001/sig00000044 ),
    .A2(\blk00000001/sig00000043 ),
    .A3(\blk00000001/sig00000043 ),
    .CE(ce),
    .CLK(clk),
    .D(\blk00000001/sig000000b2 ),
    .Q(\blk00000001/sig0000012c )
  );
  FDE #(
    .INIT ( 1'b0 ))
  \blk00000001/blk00000119  (
    .C(clk),
    .CE(ce),
    .D(\blk00000001/sig00000107 ),
    .Q(\blk00000001/sig0000010c )
  );
  SRL16E #(
    .INIT ( 16'h0000 ))
  \blk00000001/blk00000118  (
    .A0(\blk00000001/sig00000044 ),
    .A1(\blk00000001/sig00000043 ),
    .A2(\blk00000001/sig00000043 ),
    .A3(\blk00000001/sig00000043 ),
    .CE(ce),
    .CLK(clk),
    .D(\blk00000001/sig000000a2 ),
    .Q(\blk00000001/sig00000107 )
  );
  FDE #(
    .INIT ( 1'b0 ))
  \blk00000001/blk00000117  (
    .C(clk),
    .CE(ce),
    .D(\blk00000001/sig00000108 ),
    .Q(\blk00000001/sig0000010f )
  );
  SRL16E #(
    .INIT ( 16'h0000 ))
  \blk00000001/blk00000116  (
    .A0(\blk00000001/sig00000044 ),
    .A1(\blk00000001/sig00000043 ),
    .A2(\blk00000001/sig00000043 ),
    .A3(\blk00000001/sig00000043 ),
    .CE(ce),
    .CLK(clk),
    .D(\blk00000001/sig000000a3 ),
    .Q(\blk00000001/sig00000108 )
  );
  FDE #(
    .INIT ( 1'b0 ))
  \blk00000001/blk00000115  (
    .C(clk),
    .CE(ce),
    .D(\blk00000001/sig00000109 ),
    .Q(\blk00000001/sig00000112 )
  );
  SRL16E #(
    .INIT ( 16'h0000 ))
  \blk00000001/blk00000114  (
    .A0(\blk00000001/sig00000044 ),
    .A1(\blk00000001/sig00000043 ),
    .A2(\blk00000001/sig00000043 ),
    .A3(\blk00000001/sig00000043 ),
    .CE(ce),
    .CLK(clk),
    .D(\blk00000001/sig000000a4 ),
    .Q(\blk00000001/sig00000109 )
  );
  FDE #(
    .INIT ( 1'b0 ))
  \blk00000001/blk00000113  (
    .C(clk),
    .CE(ce),
    .D(\blk00000001/sig0000014b ),
    .Q(\blk00000001/sig0000014d )
  );
  SRL16E #(
    .INIT ( 16'h0000 ))
  \blk00000001/blk00000112  (
    .A0(\blk00000001/sig00000043 ),
    .A1(\blk00000001/sig00000044 ),
    .A2(\blk00000001/sig00000043 ),
    .A3(\blk00000001/sig00000043 ),
    .CE(ce),
    .CLK(clk),
    .D(\blk00000001/sig000000bb ),
    .Q(\blk00000001/sig0000014b )
  );
  FDE #(
    .INIT ( 1'b0 ))
  \blk00000001/blk00000111  (
    .C(clk),
    .CE(ce),
    .D(\blk00000001/sig00000125 ),
    .Q(\blk00000001/sig00000127 )
  );
  SRL16E #(
    .INIT ( 16'h0000 ))
  \blk00000001/blk00000110  (
    .A0(\blk00000001/sig00000044 ),
    .A1(\blk00000001/sig00000043 ),
    .A2(\blk00000001/sig00000043 ),
    .A3(\blk00000001/sig00000043 ),
    .CE(ce),
    .CLK(clk),
    .D(\blk00000001/sig000000ae ),
    .Q(\blk00000001/sig00000125 )
  );
  FDE #(
    .INIT ( 1'b0 ))
  \blk00000001/blk0000010f  (
    .C(clk),
    .CE(ce),
    .D(\blk00000001/sig0000010a ),
    .Q(\blk00000001/sig00000115 )
  );
  SRL16E #(
    .INIT ( 16'h0000 ))
  \blk00000001/blk0000010e  (
    .A0(\blk00000001/sig00000044 ),
    .A1(\blk00000001/sig00000043 ),
    .A2(\blk00000001/sig00000043 ),
    .A3(\blk00000001/sig00000043 ),
    .CE(ce),
    .CLK(clk),
    .D(\blk00000001/sig000000a5 ),
    .Q(\blk00000001/sig0000010a )
  );
  INV   \blk00000001/blk0000010d  (
    .I(\blk00000001/sig0000006e ),
    .O(\blk00000001/sig000000ce )
  );
  INV   \blk00000001/blk0000010c  (
    .I(\blk00000001/sig000000da ),
    .O(\blk00000001/sig000000e7 )
  );
  INV   \blk00000001/blk0000010b  (
    .I(\blk00000001/sig000000f3 ),
    .O(\blk00000001/sig000000fb )
  );
  INV   \blk00000001/blk0000010a  (
    .I(\blk00000001/sig0000010b ),
    .O(\blk00000001/sig0000011a )
  );
  INV   \blk00000001/blk00000109  (
    .I(\blk00000001/sig0000012d ),
    .O(\blk00000001/sig0000013e )
  );
  INV   \blk00000001/blk00000108  (
    .I(\blk00000001/sig00000153 ),
    .O(\blk00000001/sig00000168 )
  );
  INV   \blk00000001/blk00000107  (
    .I(\blk00000001/sig00000179 ),
    .O(\blk00000001/sig000001da )
  );
  INV   \blk00000001/blk00000106  (
    .I(\blk00000001/sig0000006f ),
    .O(\blk00000001/sig000000cf )
  );
  INV   \blk00000001/blk00000105  (
    .I(\blk00000001/sig000000db ),
    .O(\blk00000001/sig000000e8 )
  );
  INV   \blk00000001/blk00000104  (
    .I(\blk00000001/sig000000f4 ),
    .O(\blk00000001/sig000000fc )
  );
  INV   \blk00000001/blk00000103  (
    .I(\blk00000001/sig0000010e ),
    .O(\blk00000001/sig0000011b )
  );
  INV   \blk00000001/blk00000102  (
    .I(\blk00000001/sig00000130 ),
    .O(\blk00000001/sig0000013f )
  );
  INV   \blk00000001/blk00000101  (
    .I(\blk00000001/sig00000156 ),
    .O(\blk00000001/sig00000169 )
  );
  INV   \blk00000001/blk00000100  (
    .I(\blk00000001/sig0000017c ),
    .O(\blk00000001/sig000001db )
  );
  INV   \blk00000001/blk000000ff  (
    .I(\blk00000001/sig00000070 ),
    .O(\blk00000001/sig000000d0 )
  );
  INV   \blk00000001/blk000000fe  (
    .I(\blk00000001/sig000000dc ),
    .O(\blk00000001/sig000000e9 )
  );
  INV   \blk00000001/blk000000fd  (
    .I(\blk00000001/sig000000f5 ),
    .O(\blk00000001/sig000000fd )
  );
  INV   \blk00000001/blk000000fc  (
    .I(\blk00000001/sig00000111 ),
    .O(\blk00000001/sig0000011c )
  );
  INV   \blk00000001/blk000000fb  (
    .I(\blk00000001/sig00000133 ),
    .O(\blk00000001/sig00000140 )
  );
  INV   \blk00000001/blk000000fa  (
    .I(\blk00000001/sig00000159 ),
    .O(\blk00000001/sig0000016a )
  );
  INV   \blk00000001/blk000000f9  (
    .I(\blk00000001/sig0000017f ),
    .O(\blk00000001/sig000001dc )
  );
  INV   \blk00000001/blk000000f8  (
    .I(\blk00000001/sig00000071 ),
    .O(\blk00000001/sig000000d1 )
  );
  INV   \blk00000001/blk000000f7  (
    .I(\blk00000001/sig000000dd ),
    .O(\blk00000001/sig000000ea )
  );
  INV   \blk00000001/blk000000f6  (
    .I(\blk00000001/sig000000f6 ),
    .O(\blk00000001/sig000000fe )
  );
  INV   \blk00000001/blk000000f5  (
    .I(\blk00000001/sig00000114 ),
    .O(\blk00000001/sig0000011d )
  );
  INV   \blk00000001/blk000000f4  (
    .I(\blk00000001/sig00000136 ),
    .O(\blk00000001/sig00000141 )
  );
  INV   \blk00000001/blk000000f3  (
    .I(\blk00000001/sig0000015c ),
    .O(\blk00000001/sig0000016b )
  );
  INV   \blk00000001/blk000000f2  (
    .I(\blk00000001/sig00000182 ),
    .O(\blk00000001/sig000001dd )
  );
  LUT2 #(
    .INIT ( 4'h8 ))
  \blk00000001/blk000000f1  (
    .I0(\blk00000001/sig00000072 ),
    .I1(\blk00000001/sig0000004d ),
    .O(\blk00000001/sig00000053 )
  );
  LUT2 #(
    .INIT ( 4'h8 ))
  \blk00000001/blk000000f0  (
    .I0(\blk00000001/sig000000d9 ),
    .I1(\blk00000001/sig0000004e ),
    .O(\blk00000001/sig00000054 )
  );
  LUT2 #(
    .INIT ( 4'h8 ))
  \blk00000001/blk000000ef  (
    .I0(\blk00000001/sig000000f2 ),
    .I1(\blk00000001/sig0000004f ),
    .O(\blk00000001/sig00000055 )
  );
  LUT2 #(
    .INIT ( 4'h8 ))
  \blk00000001/blk000000ee  (
    .I0(\blk00000001/sig00000106 ),
    .I1(\blk00000001/sig00000050 ),
    .O(\blk00000001/sig00000056 )
  );
  LUT2 #(
    .INIT ( 4'h8 ))
  \blk00000001/blk000000ed  (
    .I0(\blk00000001/sig00000126 ),
    .I1(\blk00000001/sig00000051 ),
    .O(\blk00000001/sig00000057 )
  );
  LUT2 #(
    .INIT ( 4'h8 ))
  \blk00000001/blk000000ec  (
    .I0(\blk00000001/sig0000014c ),
    .I1(\blk00000001/sig00000052 ),
    .O(\blk00000001/sig00000058 )
  );
  LUT2 #(
    .INIT ( 4'h9 ))
  \blk00000001/blk000000eb  (
    .I0(b[0]),
    .I1(a[0]),
    .O(\blk00000001/sig0000005d )
  );
  LUT2 #(
    .INIT ( 4'h9 ))
  \blk00000001/blk000000ea  (
    .I0(b[4]),
    .I1(a[4]),
    .O(\blk00000001/sig0000006a )
  );
  LUT2 #(
    .INIT ( 4'h9 ))
  \blk00000001/blk000000e9  (
    .I0(b[8]),
    .I1(a[8]),
    .O(\blk00000001/sig0000007b )
  );
  LUT2 #(
    .INIT ( 4'h9 ))
  \blk00000001/blk000000e8  (
    .I0(b[12]),
    .I1(a[12]),
    .O(\blk00000001/sig0000008c )
  );
  LUT2 #(
    .INIT ( 4'h9 ))
  \blk00000001/blk000000e7  (
    .I0(b[16]),
    .I1(a[16]),
    .O(\blk00000001/sig0000009d )
  );
  LUT2 #(
    .INIT ( 4'h9 ))
  \blk00000001/blk000000e6  (
    .I0(b[20]),
    .I1(a[20]),
    .O(\blk00000001/sig000000aa )
  );
  LUT2 #(
    .INIT ( 4'h9 ))
  \blk00000001/blk000000e5  (
    .I0(b[24]),
    .I1(a[24]),
    .O(\blk00000001/sig000000b7 )
  );
  LUT2 #(
    .INIT ( 4'h9 ))
  \blk00000001/blk000000e4  (
    .I0(b[28]),
    .I1(a[28]),
    .O(\blk00000001/sig000000c3 )
  );
  LUT2 #(
    .INIT ( 4'h9 ))
  \blk00000001/blk000000e3  (
    .I0(b[1]),
    .I1(a[1]),
    .O(\blk00000001/sig0000005e )
  );
  LUT2 #(
    .INIT ( 4'h9 ))
  \blk00000001/blk000000e2  (
    .I0(b[5]),
    .I1(a[5]),
    .O(\blk00000001/sig0000006b )
  );
  LUT2 #(
    .INIT ( 4'h9 ))
  \blk00000001/blk000000e1  (
    .I0(b[9]),
    .I1(a[9]),
    .O(\blk00000001/sig0000007c )
  );
  LUT2 #(
    .INIT ( 4'h9 ))
  \blk00000001/blk000000e0  (
    .I0(b[13]),
    .I1(a[13]),
    .O(\blk00000001/sig0000008d )
  );
  LUT2 #(
    .INIT ( 4'h9 ))
  \blk00000001/blk000000df  (
    .I0(b[17]),
    .I1(a[17]),
    .O(\blk00000001/sig0000009e )
  );
  LUT2 #(
    .INIT ( 4'h9 ))
  \blk00000001/blk000000de  (
    .I0(b[21]),
    .I1(a[21]),
    .O(\blk00000001/sig000000ab )
  );
  LUT2 #(
    .INIT ( 4'h9 ))
  \blk00000001/blk000000dd  (
    .I0(b[25]),
    .I1(a[25]),
    .O(\blk00000001/sig000000b8 )
  );
  LUT2 #(
    .INIT ( 4'h9 ))
  \blk00000001/blk000000dc  (
    .I0(b[29]),
    .I1(a[29]),
    .O(\blk00000001/sig000000c4 )
  );
  LUT2 #(
    .INIT ( 4'h9 ))
  \blk00000001/blk000000db  (
    .I0(b[2]),
    .I1(a[2]),
    .O(\blk00000001/sig0000005f )
  );
  LUT2 #(
    .INIT ( 4'h9 ))
  \blk00000001/blk000000da  (
    .I0(b[6]),
    .I1(a[6]),
    .O(\blk00000001/sig0000006c )
  );
  LUT2 #(
    .INIT ( 4'h9 ))
  \blk00000001/blk000000d9  (
    .I0(b[10]),
    .I1(a[10]),
    .O(\blk00000001/sig0000007d )
  );
  LUT2 #(
    .INIT ( 4'h9 ))
  \blk00000001/blk000000d8  (
    .I0(b[14]),
    .I1(a[14]),
    .O(\blk00000001/sig0000008e )
  );
  LUT2 #(
    .INIT ( 4'h9 ))
  \blk00000001/blk000000d7  (
    .I0(b[18]),
    .I1(a[18]),
    .O(\blk00000001/sig0000009f )
  );
  LUT2 #(
    .INIT ( 4'h9 ))
  \blk00000001/blk000000d6  (
    .I0(b[22]),
    .I1(a[22]),
    .O(\blk00000001/sig000000ac )
  );
  LUT2 #(
    .INIT ( 4'h9 ))
  \blk00000001/blk000000d5  (
    .I0(b[26]),
    .I1(a[26]),
    .O(\blk00000001/sig000000b9 )
  );
  LUT2 #(
    .INIT ( 4'h9 ))
  \blk00000001/blk000000d4  (
    .I0(b[30]),
    .I1(a[30]),
    .O(\blk00000001/sig000000c5 )
  );
  LUT2 #(
    .INIT ( 4'h9 ))
  \blk00000001/blk000000d3  (
    .I0(b[3]),
    .I1(a[3]),
    .O(\blk00000001/sig00000060 )
  );
  LUT2 #(
    .INIT ( 4'h9 ))
  \blk00000001/blk000000d2  (
    .I0(b[7]),
    .I1(a[7]),
    .O(\blk00000001/sig0000006d )
  );
  LUT2 #(
    .INIT ( 4'h9 ))
  \blk00000001/blk000000d1  (
    .I0(b[11]),
    .I1(a[11]),
    .O(\blk00000001/sig0000007e )
  );
  LUT2 #(
    .INIT ( 4'h9 ))
  \blk00000001/blk000000d0  (
    .I0(b[15]),
    .I1(a[15]),
    .O(\blk00000001/sig0000008f )
  );
  LUT2 #(
    .INIT ( 4'h9 ))
  \blk00000001/blk000000cf  (
    .I0(b[19]),
    .I1(a[19]),
    .O(\blk00000001/sig000000a0 )
  );
  LUT2 #(
    .INIT ( 4'h9 ))
  \blk00000001/blk000000ce  (
    .I0(b[23]),
    .I1(a[23]),
    .O(\blk00000001/sig000000ad )
  );
  LUT2 #(
    .INIT ( 4'h9 ))
  \blk00000001/blk000000cd  (
    .I0(b[27]),
    .I1(a[27]),
    .O(\blk00000001/sig000000ba )
  );
  LUT2 #(
    .INIT ( 4'h9 ))
  \blk00000001/blk000000cc  (
    .I0(b[31]),
    .I1(a[31]),
    .O(\blk00000001/sig000000c6 )
  );
  FDRE #(
    .INIT ( 1'b0 ))
  \blk00000001/blk000000cb  (
    .C(clk),
    .CE(ce),
    .D(\blk00000001/sig0000016c ),
    .R(sclr),
    .Q(s[24])
  );
  FDRE #(
    .INIT ( 1'b0 ))
  \blk00000001/blk000000ca  (
    .C(clk),
    .CE(ce),
    .D(\blk00000001/sig0000016d ),
    .R(sclr),
    .Q(s[25])
  );
  FDRE #(
    .INIT ( 1'b0 ))
  \blk00000001/blk000000c9  (
    .C(clk),
    .CE(ce),
    .D(\blk00000001/sig0000016e ),
    .R(sclr),
    .Q(s[26])
  );
  FDRE #(
    .INIT ( 1'b0 ))
  \blk00000001/blk000000c8  (
    .C(clk),
    .CE(ce),
    .D(\blk00000001/sig0000016f ),
    .R(sclr),
    .Q(s[27])
  );
  FDRE #(
    .INIT ( 1'b0 ))
  \blk00000001/blk000000c7  (
    .C(clk),
    .CE(ce),
    .D(\blk00000001/sig00000161 ),
    .R(sclr),
    .Q(s[20])
  );
  FDRE #(
    .INIT ( 1'b0 ))
  \blk00000001/blk000000c6  (
    .C(clk),
    .CE(ce),
    .D(\blk00000001/sig00000162 ),
    .R(sclr),
    .Q(s[21])
  );
  FDRE #(
    .INIT ( 1'b0 ))
  \blk00000001/blk000000c5  (
    .C(clk),
    .CE(ce),
    .D(\blk00000001/sig00000163 ),
    .R(sclr),
    .Q(s[22])
  );
  FDRE #(
    .INIT ( 1'b0 ))
  \blk00000001/blk000000c4  (
    .C(clk),
    .CE(ce),
    .D(\blk00000001/sig00000164 ),
    .R(sclr),
    .Q(s[23])
  );
  FDRE #(
    .INIT ( 1'b0 ))
  \blk00000001/blk000000c3  (
    .C(clk),
    .CE(ce),
    .D(\blk00000001/sig0000015f ),
    .R(sclr),
    .Q(s[18])
  );
  FDRE #(
    .INIT ( 1'b0 ))
  \blk00000001/blk000000c2  (
    .C(clk),
    .CE(ce),
    .D(\blk00000001/sig00000160 ),
    .R(sclr),
    .Q(s[19])
  );
  FDRE #(
    .INIT ( 1'b0 ))
  \blk00000001/blk000000c1  (
    .C(clk),
    .CE(ce),
    .D(\blk00000001/sig00000142 ),
    .R(sclr),
    .Q(\blk00000001/sig00000161 )
  );
  FDRE #(
    .INIT ( 1'b0 ))
  \blk00000001/blk000000c0  (
    .C(clk),
    .CE(ce),
    .D(\blk00000001/sig00000143 ),
    .R(sclr),
    .Q(\blk00000001/sig00000162 )
  );
  FDRE #(
    .INIT ( 1'b0 ))
  \blk00000001/blk000000bf  (
    .C(clk),
    .CE(ce),
    .D(\blk00000001/sig00000144 ),
    .R(sclr),
    .Q(\blk00000001/sig00000163 )
  );
  FDRE #(
    .INIT ( 1'b0 ))
  \blk00000001/blk000000be  (
    .C(clk),
    .CE(ce),
    .D(\blk00000001/sig00000145 ),
    .R(sclr),
    .Q(\blk00000001/sig00000164 )
  );
  FDRE #(
    .INIT ( 1'b0 ))
  \blk00000001/blk000000bd  (
    .C(clk),
    .CE(ce),
    .D(\blk00000001/sig00000139 ),
    .R(sclr),
    .Q(\blk00000001/sig0000015f )
  );
  FDRE #(
    .INIT ( 1'b0 ))
  \blk00000001/blk000000bc  (
    .C(clk),
    .CE(ce),
    .D(\blk00000001/sig0000013a ),
    .R(sclr),
    .Q(\blk00000001/sig00000160 )
  );
  FDRE #(
    .INIT ( 1'b0 ))
  \blk00000001/blk000000bb  (
    .C(clk),
    .CE(ce),
    .D(\blk00000001/sig0000011e ),
    .R(sclr),
    .Q(\blk00000001/sig00000139 )
  );
  FDRE #(
    .INIT ( 1'b0 ))
  \blk00000001/blk000000ba  (
    .C(clk),
    .CE(ce),
    .D(\blk00000001/sig0000011f ),
    .R(sclr),
    .Q(\blk00000001/sig0000013a )
  );
  FDRE #(
    .INIT ( 1'b0 ))
  \blk00000001/blk000000b9  (
    .C(clk),
    .CE(ce),
    .D(\blk00000001/sig000000df ),
    .R(sclr),
    .Q(\blk00000001/sig000000f3 )
  );
  FDRE #(
    .INIT ( 1'b0 ))
  \blk00000001/blk000000b8  (
    .C(clk),
    .CE(ce),
    .D(\blk00000001/sig000000e0 ),
    .R(sclr),
    .Q(\blk00000001/sig000000f4 )
  );
  FDRE #(
    .INIT ( 1'b0 ))
  \blk00000001/blk000000b7  (
    .C(clk),
    .CE(ce),
    .D(\blk00000001/sig000000e1 ),
    .R(sclr),
    .Q(\blk00000001/sig000000f5 )
  );
  FDRE #(
    .INIT ( 1'b0 ))
  \blk00000001/blk000000b6  (
    .C(clk),
    .CE(ce),
    .D(\blk00000001/sig000000e2 ),
    .R(sclr),
    .Q(\blk00000001/sig000000f6 )
  );
  FDRE #(
    .INIT ( 1'b0 ))
  \blk00000001/blk000000b5  (
    .C(clk),
    .CE(ce),
    .D(\blk00000001/sig00000090 ),
    .R(sclr),
    .Q(\blk00000001/sig000000df )
  );
  FDRE #(
    .INIT ( 1'b0 ))
  \blk00000001/blk000000b4  (
    .C(clk),
    .CE(ce),
    .D(\blk00000001/sig00000091 ),
    .R(sclr),
    .Q(\blk00000001/sig000000e0 )
  );
  FDRE #(
    .INIT ( 1'b0 ))
  \blk00000001/blk000000b3  (
    .C(clk),
    .CE(ce),
    .D(\blk00000001/sig00000092 ),
    .R(sclr),
    .Q(\blk00000001/sig000000e1 )
  );
  FDRE #(
    .INIT ( 1'b0 ))
  \blk00000001/blk000000b2  (
    .C(clk),
    .CE(ce),
    .D(\blk00000001/sig00000093 ),
    .R(sclr),
    .Q(\blk00000001/sig000000e2 )
  );
  FDRE #(
    .INIT ( 1'b0 ))
  \blk00000001/blk000000b1  (
    .C(clk),
    .CE(ce),
    .D(\blk00000001/sig0000007f ),
    .R(sclr),
    .Q(\blk00000001/sig000000da )
  );
  FDRE #(
    .INIT ( 1'b0 ))
  \blk00000001/blk000000b0  (
    .C(clk),
    .CE(ce),
    .D(\blk00000001/sig00000080 ),
    .R(sclr),
    .Q(\blk00000001/sig000000db )
  );
  FDRE #(
    .INIT ( 1'b0 ))
  \blk00000001/blk000000af  (
    .C(clk),
    .CE(ce),
    .D(\blk00000001/sig00000081 ),
    .R(sclr),
    .Q(\blk00000001/sig000000dc )
  );
  FDRE #(
    .INIT ( 1'b0 ))
  \blk00000001/blk000000ae  (
    .C(clk),
    .CE(ce),
    .D(\blk00000001/sig00000082 ),
    .R(sclr),
    .Q(\blk00000001/sig000000dd )
  );
  FDSE #(
    .INIT ( 1'b1 ))
  \blk00000001/blk000000ad  (
    .C(clk),
    .CE(ce),
    .D(\blk00000001/sig00000058 ),
    .S(\blk00000001/sig00000043 ),
    .Q(\blk00000001/sig00000174 )
  );
  FDSE #(
    .INIT ( 1'b1 ))
  \blk00000001/blk000000ac  (
    .C(clk),
    .CE(ce),
    .D(\blk00000001/sig00000057 ),
    .S(\blk00000001/sig00000043 ),
    .Q(\blk00000001/sig0000014a )
  );
  FDSE #(
    .INIT ( 1'b1 ))
  \blk00000001/blk000000ab  (
    .C(clk),
    .CE(ce),
    .D(\blk00000001/sig00000056 ),
    .S(\blk00000001/sig00000043 ),
    .Q(\blk00000001/sig00000124 )
  );
  FDSE #(
    .INIT ( 1'b1 ))
  \blk00000001/blk000000aa  (
    .C(clk),
    .CE(ce),
    .D(\blk00000001/sig00000055 ),
    .S(\blk00000001/sig00000043 ),
    .Q(\blk00000001/sig00000105 )
  );
  FDSE #(
    .INIT ( 1'b1 ))
  \blk00000001/blk000000a9  (
    .C(clk),
    .CE(ce),
    .D(\blk00000001/sig00000054 ),
    .S(\blk00000001/sig00000043 ),
    .Q(\blk00000001/sig000000f1 )
  );
  FDSE #(
    .INIT ( 1'b1 ))
  \blk00000001/blk000000a8  (
    .C(clk),
    .CE(ce),
    .D(\blk00000001/sig00000053 ),
    .S(\blk00000001/sig00000043 ),
    .Q(\blk00000001/sig000000d8 )
  );
  FDRE #(
    .INIT ( 1'b0 ))
  \blk00000001/blk000000a7  (
    .C(clk),
    .CE(ce),
    .D(\blk00000001/sig000000f7 ),
    .R(sclr),
    .Q(\blk00000001/sig00000106 )
  );
  FDRE #(
    .INIT ( 1'b0 ))
  \blk00000001/blk000000a6  (
    .C(clk),
    .CE(ce),
    .D(\blk00000001/sig000000e3 ),
    .R(sclr),
    .Q(\blk00000001/sig000000f7 )
  );
  FDRE #(
    .INIT ( 1'b0 ))
  \blk00000001/blk000000a5  (
    .C(clk),
    .CE(ce),
    .D(\blk00000001/sig000000de ),
    .R(sclr),
    .Q(\blk00000001/sig000000f2 )
  );
  FDRE #(
    .INIT ( 1'b0 ))
  \blk00000001/blk000000a4  (
    .C(clk),
    .CE(ce),
    .D(\blk00000001/sig000000a1 ),
    .R(sclr),
    .Q(\blk00000001/sig000000e3 )
  );
  FDRE #(
    .INIT ( 1'b0 ))
  \blk00000001/blk000000a3  (
    .C(clk),
    .CE(ce),
    .D(\blk00000001/sig00000094 ),
    .R(sclr),
    .Q(\blk00000001/sig000000de )
  );
  FDRE #(
    .INIT ( 1'b0 ))
  \blk00000001/blk000000a2  (
    .C(clk),
    .CE(ce),
    .D(\blk00000001/sig00000083 ),
    .R(sclr),
    .Q(\blk00000001/sig000000d9 )
  );
  MUXCY   \blk00000001/blk000000a1  (
    .CI(\blk00000001/sig00000044 ),
    .DI(a[24]),
    .S(\blk00000001/sig000000b7 ),
    .O(\blk00000001/sig000000b3 )
  );
  XORCY   \blk00000001/blk000000a0  (
    .CI(\blk00000001/sig00000044 ),
    .LI(\blk00000001/sig000000b7 ),
    .O(\blk00000001/sig000000bc )
  );
  XORCY   \blk00000001/blk0000009f  (
    .CI(\blk00000001/sig000000b5 ),
    .LI(\blk00000001/sig000000ba ),
    .O(\blk00000001/sig000000bf )
  );
  MUXCY   \blk00000001/blk0000009e  (
    .CI(\blk00000001/sig000000b5 ),
    .DI(a[27]),
    .S(\blk00000001/sig000000ba ),
    .O(\blk00000001/sig000000b6 )
  );
  MUXCY   \blk00000001/blk0000009d  (
    .CI(\blk00000001/sig000000b3 ),
    .DI(a[25]),
    .S(\blk00000001/sig000000b8 ),
    .O(\blk00000001/sig000000b4 )
  );
  XORCY   \blk00000001/blk0000009c  (
    .CI(\blk00000001/sig000000b3 ),
    .LI(\blk00000001/sig000000b8 ),
    .O(\blk00000001/sig000000bd )
  );
  MUXCY   \blk00000001/blk0000009b  (
    .CI(\blk00000001/sig000000b4 ),
    .DI(a[26]),
    .S(\blk00000001/sig000000b9 ),
    .O(\blk00000001/sig000000b5 )
  );
  XORCY   \blk00000001/blk0000009a  (
    .CI(\blk00000001/sig000000b4 ),
    .LI(\blk00000001/sig000000b9 ),
    .O(\blk00000001/sig000000be )
  );
  FDSE #(
    .INIT ( 1'b1 ))
  \blk00000001/blk00000099  (
    .C(clk),
    .CE(ce),
    .D(\blk00000001/sig000000b6 ),
    .S(sclr),
    .Q(\blk00000001/sig000000bb )
  );
  MUXCY   \blk00000001/blk00000098  (
    .CI(\blk00000001/sig00000044 ),
    .DI(a[20]),
    .S(\blk00000001/sig000000aa ),
    .O(\blk00000001/sig000000a6 )
  );
  XORCY   \blk00000001/blk00000097  (
    .CI(\blk00000001/sig00000044 ),
    .LI(\blk00000001/sig000000aa ),
    .O(\blk00000001/sig000000af )
  );
  XORCY   \blk00000001/blk00000096  (
    .CI(\blk00000001/sig000000a8 ),
    .LI(\blk00000001/sig000000ad ),
    .O(\blk00000001/sig000000b2 )
  );
  MUXCY   \blk00000001/blk00000095  (
    .CI(\blk00000001/sig000000a8 ),
    .DI(a[23]),
    .S(\blk00000001/sig000000ad ),
    .O(\blk00000001/sig000000a9 )
  );
  MUXCY   \blk00000001/blk00000094  (
    .CI(\blk00000001/sig000000a6 ),
    .DI(a[21]),
    .S(\blk00000001/sig000000ab ),
    .O(\blk00000001/sig000000a7 )
  );
  XORCY   \blk00000001/blk00000093  (
    .CI(\blk00000001/sig000000a6 ),
    .LI(\blk00000001/sig000000ab ),
    .O(\blk00000001/sig000000b0 )
  );
  MUXCY   \blk00000001/blk00000092  (
    .CI(\blk00000001/sig000000a7 ),
    .DI(a[22]),
    .S(\blk00000001/sig000000ac ),
    .O(\blk00000001/sig000000a8 )
  );
  XORCY   \blk00000001/blk00000091  (
    .CI(\blk00000001/sig000000a7 ),
    .LI(\blk00000001/sig000000ac ),
    .O(\blk00000001/sig000000b1 )
  );
  FDSE #(
    .INIT ( 1'b1 ))
  \blk00000001/blk00000090  (
    .C(clk),
    .CE(ce),
    .D(\blk00000001/sig000000a9 ),
    .S(sclr),
    .Q(\blk00000001/sig000000ae )
  );
  MUXCY   \blk00000001/blk0000008f  (
    .CI(\blk00000001/sig00000044 ),
    .DI(a[16]),
    .S(\blk00000001/sig0000009d ),
    .O(\blk00000001/sig00000099 )
  );
  XORCY   \blk00000001/blk0000008e  (
    .CI(\blk00000001/sig00000044 ),
    .LI(\blk00000001/sig0000009d ),
    .O(\blk00000001/sig000000a2 )
  );
  XORCY   \blk00000001/blk0000008d  (
    .CI(\blk00000001/sig0000009b ),
    .LI(\blk00000001/sig000000a0 ),
    .O(\blk00000001/sig000000a5 )
  );
  MUXCY   \blk00000001/blk0000008c  (
    .CI(\blk00000001/sig0000009b ),
    .DI(a[19]),
    .S(\blk00000001/sig000000a0 ),
    .O(\blk00000001/sig0000009c )
  );
  MUXCY   \blk00000001/blk0000008b  (
    .CI(\blk00000001/sig00000099 ),
    .DI(a[17]),
    .S(\blk00000001/sig0000009e ),
    .O(\blk00000001/sig0000009a )
  );
  XORCY   \blk00000001/blk0000008a  (
    .CI(\blk00000001/sig00000099 ),
    .LI(\blk00000001/sig0000009e ),
    .O(\blk00000001/sig000000a3 )
  );
  MUXCY   \blk00000001/blk00000089  (
    .CI(\blk00000001/sig0000009a ),
    .DI(a[18]),
    .S(\blk00000001/sig0000009f ),
    .O(\blk00000001/sig0000009b )
  );
  XORCY   \blk00000001/blk00000088  (
    .CI(\blk00000001/sig0000009a ),
    .LI(\blk00000001/sig0000009f ),
    .O(\blk00000001/sig000000a4 )
  );
  FDSE #(
    .INIT ( 1'b1 ))
  \blk00000001/blk00000087  (
    .C(clk),
    .CE(ce),
    .D(\blk00000001/sig0000009c ),
    .S(sclr),
    .Q(\blk00000001/sig000000a1 )
  );
  MUXCY   \blk00000001/blk00000086  (
    .CI(\blk00000001/sig00000044 ),
    .DI(a[12]),
    .S(\blk00000001/sig0000008c ),
    .O(\blk00000001/sig00000088 )
  );
  XORCY   \blk00000001/blk00000085  (
    .CI(\blk00000001/sig00000044 ),
    .LI(\blk00000001/sig0000008c ),
    .O(\blk00000001/sig00000095 )
  );
  XORCY   \blk00000001/blk00000084  (
    .CI(\blk00000001/sig0000008a ),
    .LI(\blk00000001/sig0000008f ),
    .O(\blk00000001/sig00000098 )
  );
  MUXCY   \blk00000001/blk00000083  (
    .CI(\blk00000001/sig0000008a ),
    .DI(a[15]),
    .S(\blk00000001/sig0000008f ),
    .O(\blk00000001/sig0000008b )
  );
  MUXCY   \blk00000001/blk00000082  (
    .CI(\blk00000001/sig00000088 ),
    .DI(a[13]),
    .S(\blk00000001/sig0000008d ),
    .O(\blk00000001/sig00000089 )
  );
  XORCY   \blk00000001/blk00000081  (
    .CI(\blk00000001/sig00000088 ),
    .LI(\blk00000001/sig0000008d ),
    .O(\blk00000001/sig00000096 )
  );
  MUXCY   \blk00000001/blk00000080  (
    .CI(\blk00000001/sig00000089 ),
    .DI(a[14]),
    .S(\blk00000001/sig0000008e ),
    .O(\blk00000001/sig0000008a )
  );
  XORCY   \blk00000001/blk0000007f  (
    .CI(\blk00000001/sig00000089 ),
    .LI(\blk00000001/sig0000008e ),
    .O(\blk00000001/sig00000097 )
  );
  FDSE #(
    .INIT ( 1'b1 ))
  \blk00000001/blk0000007e  (
    .C(clk),
    .CE(ce),
    .D(\blk00000001/sig0000008b ),
    .S(sclr),
    .Q(\blk00000001/sig00000094 )
  );
  FDRE #(
    .INIT ( 1'b0 ))
  \blk00000001/blk0000007d  (
    .C(clk),
    .CE(ce),
    .D(\blk00000001/sig00000095 ),
    .R(sclr),
    .Q(\blk00000001/sig00000090 )
  );
  FDRE #(
    .INIT ( 1'b0 ))
  \blk00000001/blk0000007c  (
    .C(clk),
    .CE(ce),
    .D(\blk00000001/sig00000096 ),
    .R(sclr),
    .Q(\blk00000001/sig00000091 )
  );
  FDRE #(
    .INIT ( 1'b0 ))
  \blk00000001/blk0000007b  (
    .C(clk),
    .CE(ce),
    .D(\blk00000001/sig00000097 ),
    .R(sclr),
    .Q(\blk00000001/sig00000092 )
  );
  FDRE #(
    .INIT ( 1'b0 ))
  \blk00000001/blk0000007a  (
    .C(clk),
    .CE(ce),
    .D(\blk00000001/sig00000098 ),
    .R(sclr),
    .Q(\blk00000001/sig00000093 )
  );
  MUXCY   \blk00000001/blk00000079  (
    .CI(\blk00000001/sig00000044 ),
    .DI(a[8]),
    .S(\blk00000001/sig0000007b ),
    .O(\blk00000001/sig00000077 )
  );
  XORCY   \blk00000001/blk00000078  (
    .CI(\blk00000001/sig00000044 ),
    .LI(\blk00000001/sig0000007b ),
    .O(\blk00000001/sig00000084 )
  );
  XORCY   \blk00000001/blk00000077  (
    .CI(\blk00000001/sig00000079 ),
    .LI(\blk00000001/sig0000007e ),
    .O(\blk00000001/sig00000087 )
  );
  MUXCY   \blk00000001/blk00000076  (
    .CI(\blk00000001/sig00000079 ),
    .DI(a[11]),
    .S(\blk00000001/sig0000007e ),
    .O(\blk00000001/sig0000007a )
  );
  MUXCY   \blk00000001/blk00000075  (
    .CI(\blk00000001/sig00000077 ),
    .DI(a[9]),
    .S(\blk00000001/sig0000007c ),
    .O(\blk00000001/sig00000078 )
  );
  XORCY   \blk00000001/blk00000074  (
    .CI(\blk00000001/sig00000077 ),
    .LI(\blk00000001/sig0000007c ),
    .O(\blk00000001/sig00000085 )
  );
  MUXCY   \blk00000001/blk00000073  (
    .CI(\blk00000001/sig00000078 ),
    .DI(a[10]),
    .S(\blk00000001/sig0000007d ),
    .O(\blk00000001/sig00000079 )
  );
  XORCY   \blk00000001/blk00000072  (
    .CI(\blk00000001/sig00000078 ),
    .LI(\blk00000001/sig0000007d ),
    .O(\blk00000001/sig00000086 )
  );
  FDSE #(
    .INIT ( 1'b1 ))
  \blk00000001/blk00000071  (
    .C(clk),
    .CE(ce),
    .D(\blk00000001/sig0000007a ),
    .S(sclr),
    .Q(\blk00000001/sig00000083 )
  );
  FDRE #(
    .INIT ( 1'b0 ))
  \blk00000001/blk00000070  (
    .C(clk),
    .CE(ce),
    .D(\blk00000001/sig00000084 ),
    .R(sclr),
    .Q(\blk00000001/sig0000007f )
  );
  FDRE #(
    .INIT ( 1'b0 ))
  \blk00000001/blk0000006f  (
    .C(clk),
    .CE(ce),
    .D(\blk00000001/sig00000085 ),
    .R(sclr),
    .Q(\blk00000001/sig00000080 )
  );
  FDRE #(
    .INIT ( 1'b0 ))
  \blk00000001/blk0000006e  (
    .C(clk),
    .CE(ce),
    .D(\blk00000001/sig00000086 ),
    .R(sclr),
    .Q(\blk00000001/sig00000081 )
  );
  FDRE #(
    .INIT ( 1'b0 ))
  \blk00000001/blk0000006d  (
    .C(clk),
    .CE(ce),
    .D(\blk00000001/sig00000087 ),
    .R(sclr),
    .Q(\blk00000001/sig00000082 )
  );
  MUXCY   \blk00000001/blk0000006c  (
    .CI(\blk00000001/sig00000044 ),
    .DI(a[4]),
    .S(\blk00000001/sig0000006a ),
    .O(\blk00000001/sig00000066 )
  );
  XORCY   \blk00000001/blk0000006b  (
    .CI(\blk00000001/sig00000044 ),
    .LI(\blk00000001/sig0000006a ),
    .O(\blk00000001/sig00000073 )
  );
  XORCY   \blk00000001/blk0000006a  (
    .CI(\blk00000001/sig00000068 ),
    .LI(\blk00000001/sig0000006d ),
    .O(\blk00000001/sig00000076 )
  );
  MUXCY   \blk00000001/blk00000069  (
    .CI(\blk00000001/sig00000068 ),
    .DI(a[7]),
    .S(\blk00000001/sig0000006d ),
    .O(\blk00000001/sig00000069 )
  );
  MUXCY   \blk00000001/blk00000068  (
    .CI(\blk00000001/sig00000066 ),
    .DI(a[5]),
    .S(\blk00000001/sig0000006b ),
    .O(\blk00000001/sig00000067 )
  );
  XORCY   \blk00000001/blk00000067  (
    .CI(\blk00000001/sig00000066 ),
    .LI(\blk00000001/sig0000006b ),
    .O(\blk00000001/sig00000074 )
  );
  MUXCY   \blk00000001/blk00000066  (
    .CI(\blk00000001/sig00000067 ),
    .DI(a[6]),
    .S(\blk00000001/sig0000006c ),
    .O(\blk00000001/sig00000068 )
  );
  XORCY   \blk00000001/blk00000065  (
    .CI(\blk00000001/sig00000067 ),
    .LI(\blk00000001/sig0000006c ),
    .O(\blk00000001/sig00000075 )
  );
  FDSE #(
    .INIT ( 1'b1 ))
  \blk00000001/blk00000064  (
    .C(clk),
    .CE(ce),
    .D(\blk00000001/sig00000069 ),
    .S(sclr),
    .Q(\blk00000001/sig00000072 )
  );
  FDRE #(
    .INIT ( 1'b0 ))
  \blk00000001/blk00000063  (
    .C(clk),
    .CE(ce),
    .D(\blk00000001/sig00000073 ),
    .R(sclr),
    .Q(\blk00000001/sig0000006e )
  );
  FDRE #(
    .INIT ( 1'b0 ))
  \blk00000001/blk00000062  (
    .C(clk),
    .CE(ce),
    .D(\blk00000001/sig00000074 ),
    .R(sclr),
    .Q(\blk00000001/sig0000006f )
  );
  FDRE #(
    .INIT ( 1'b0 ))
  \blk00000001/blk00000061  (
    .C(clk),
    .CE(ce),
    .D(\blk00000001/sig00000075 ),
    .R(sclr),
    .Q(\blk00000001/sig00000070 )
  );
  FDRE #(
    .INIT ( 1'b0 ))
  \blk00000001/blk00000060  (
    .C(clk),
    .CE(ce),
    .D(\blk00000001/sig00000076 ),
    .R(sclr),
    .Q(\blk00000001/sig00000071 )
  );
  MUXCY   \blk00000001/blk0000005f  (
    .CI(\blk00000001/sig00000044 ),
    .DI(a[0]),
    .S(\blk00000001/sig0000005d ),
    .O(\blk00000001/sig00000059 )
  );
  XORCY   \blk00000001/blk0000005e  (
    .CI(\blk00000001/sig00000044 ),
    .LI(\blk00000001/sig0000005d ),
    .O(\blk00000001/sig00000062 )
  );
  XORCY   \blk00000001/blk0000005d  (
    .CI(\blk00000001/sig0000005b ),
    .LI(\blk00000001/sig00000060 ),
    .O(\blk00000001/sig00000065 )
  );
  MUXCY   \blk00000001/blk0000005c  (
    .CI(\blk00000001/sig0000005b ),
    .DI(a[3]),
    .S(\blk00000001/sig00000060 ),
    .O(\blk00000001/sig0000005c )
  );
  MUXCY   \blk00000001/blk0000005b  (
    .CI(\blk00000001/sig00000059 ),
    .DI(a[1]),
    .S(\blk00000001/sig0000005e ),
    .O(\blk00000001/sig0000005a )
  );
  XORCY   \blk00000001/blk0000005a  (
    .CI(\blk00000001/sig00000059 ),
    .LI(\blk00000001/sig0000005e ),
    .O(\blk00000001/sig00000063 )
  );
  MUXCY   \blk00000001/blk00000059  (
    .CI(\blk00000001/sig0000005a ),
    .DI(a[2]),
    .S(\blk00000001/sig0000005f ),
    .O(\blk00000001/sig0000005b )
  );
  XORCY   \blk00000001/blk00000058  (
    .CI(\blk00000001/sig0000005a ),
    .LI(\blk00000001/sig0000005f ),
    .O(\blk00000001/sig00000064 )
  );
  FDSE #(
    .INIT ( 1'b1 ))
  \blk00000001/blk00000057  (
    .C(clk),
    .CE(ce),
    .D(\blk00000001/sig0000005c ),
    .S(sclr),
    .Q(\blk00000001/sig00000061 )
  );
  MUXCY   \blk00000001/blk00000056  (
    .CI(\blk00000001/sig00000044 ),
    .DI(a[28]),
    .S(\blk00000001/sig000000c3 ),
    .O(\blk00000001/sig000000c0 )
  );
  XORCY   \blk00000001/blk00000055  (
    .CI(\blk00000001/sig00000044 ),
    .LI(\blk00000001/sig000000c3 ),
    .O(\blk00000001/sig000000c7 )
  );
  XORCY   \blk00000001/blk00000054  (
    .CI(\blk00000001/sig000000c2 ),
    .LI(\blk00000001/sig000000c6 ),
    .O(\blk00000001/sig000000ca )
  );
  MUXCY   \blk00000001/blk00000053  (
    .CI(\blk00000001/sig000000c0 ),
    .DI(a[29]),
    .S(\blk00000001/sig000000c4 ),
    .O(\blk00000001/sig000000c1 )
  );
  XORCY   \blk00000001/blk00000052  (
    .CI(\blk00000001/sig000000c0 ),
    .LI(\blk00000001/sig000000c4 ),
    .O(\blk00000001/sig000000c8 )
  );
  MUXCY   \blk00000001/blk00000051  (
    .CI(\blk00000001/sig000000c1 ),
    .DI(a[30]),
    .S(\blk00000001/sig000000c5 ),
    .O(\blk00000001/sig000000c2 )
  );
  XORCY   \blk00000001/blk00000050  (
    .CI(\blk00000001/sig000000c1 ),
    .LI(\blk00000001/sig000000c5 ),
    .O(\blk00000001/sig000000c9 )
  );
  MUXCY   \blk00000001/blk0000004f  (
    .CI(\blk00000001/sig00000174 ),
    .DI(\blk00000001/sig00000044 ),
    .S(\blk00000001/sig000001da ),
    .O(\blk00000001/sig000001d7 )
  );
  XORCY   \blk00000001/blk0000004e  (
    .CI(\blk00000001/sig00000174 ),
    .LI(\blk00000001/sig000001da ),
    .O(\blk00000001/sig000001e2 )
  );
  XORCY   \blk00000001/blk0000004d  (
    .CI(\blk00000001/sig000001d9 ),
    .LI(\blk00000001/sig000001dd ),
    .O(\blk00000001/sig000001e5 )
  );
  MUXCY   \blk00000001/blk0000004c  (
    .CI(\blk00000001/sig000001d9 ),
    .DI(\blk00000001/sig00000044 ),
    .S(\blk00000001/sig000001dd ),
    .O(\NLW_blk00000001/blk0000004c_O_UNCONNECTED )
  );
  MUXCY   \blk00000001/blk0000004b  (
    .CI(\blk00000001/sig000001d7 ),
    .DI(\blk00000001/sig00000044 ),
    .S(\blk00000001/sig000001db ),
    .O(\blk00000001/sig000001d8 )
  );
  XORCY   \blk00000001/blk0000004a  (
    .CI(\blk00000001/sig000001d7 ),
    .LI(\blk00000001/sig000001db ),
    .O(\blk00000001/sig000001e3 )
  );
  MUXCY   \blk00000001/blk00000049  (
    .CI(\blk00000001/sig000001d8 ),
    .DI(\blk00000001/sig00000044 ),
    .S(\blk00000001/sig000001dc ),
    .O(\blk00000001/sig000001d9 )
  );
  XORCY   \blk00000001/blk00000048  (
    .CI(\blk00000001/sig000001d8 ),
    .LI(\blk00000001/sig000001dc ),
    .O(\blk00000001/sig000001e4 )
  );
  FDRE #(
    .INIT ( 1'b0 ))
  \blk00000001/blk00000047  (
    .C(clk),
    .CE(ce),
    .D(\blk00000001/sig000001e2 ),
    .R(sclr),
    .Q(s[28])
  );
  FDRE #(
    .INIT ( 1'b0 ))
  \blk00000001/blk00000046  (
    .C(clk),
    .CE(ce),
    .D(\blk00000001/sig000001e3 ),
    .R(sclr),
    .Q(s[29])
  );
  FDRE #(
    .INIT ( 1'b0 ))
  \blk00000001/blk00000045  (
    .C(clk),
    .CE(ce),
    .D(\blk00000001/sig000001e4 ),
    .R(sclr),
    .Q(s[30])
  );
  FDRE #(
    .INIT ( 1'b0 ))
  \blk00000001/blk00000044  (
    .C(clk),
    .CE(ce),
    .D(\blk00000001/sig000001e5 ),
    .R(sclr),
    .Q(s[31])
  );
  MUXCY   \blk00000001/blk00000043  (
    .CI(\blk00000001/sig0000014a ),
    .DI(\blk00000001/sig00000044 ),
    .S(\blk00000001/sig00000168 ),
    .O(\blk00000001/sig00000165 )
  );
  XORCY   \blk00000001/blk00000042  (
    .CI(\blk00000001/sig0000014a ),
    .LI(\blk00000001/sig00000168 ),
    .O(\blk00000001/sig00000170 )
  );
  XORCY   \blk00000001/blk00000041  (
    .CI(\blk00000001/sig00000167 ),
    .LI(\blk00000001/sig0000016b ),
    .O(\blk00000001/sig00000173 )
  );
  MUXCY   \blk00000001/blk00000040  (
    .CI(\blk00000001/sig00000167 ),
    .DI(\blk00000001/sig00000044 ),
    .S(\blk00000001/sig0000016b ),
    .O(\blk00000001/sig00000052 )
  );
  MUXCY   \blk00000001/blk0000003f  (
    .CI(\blk00000001/sig00000165 ),
    .DI(\blk00000001/sig00000044 ),
    .S(\blk00000001/sig00000169 ),
    .O(\blk00000001/sig00000166 )
  );
  XORCY   \blk00000001/blk0000003e  (
    .CI(\blk00000001/sig00000165 ),
    .LI(\blk00000001/sig00000169 ),
    .O(\blk00000001/sig00000171 )
  );
  MUXCY   \blk00000001/blk0000003d  (
    .CI(\blk00000001/sig00000166 ),
    .DI(\blk00000001/sig00000044 ),
    .S(\blk00000001/sig0000016a ),
    .O(\blk00000001/sig00000167 )
  );
  XORCY   \blk00000001/blk0000003c  (
    .CI(\blk00000001/sig00000166 ),
    .LI(\blk00000001/sig0000016a ),
    .O(\blk00000001/sig00000172 )
  );
  FDRE #(
    .INIT ( 1'b0 ))
  \blk00000001/blk0000003b  (
    .C(clk),
    .CE(ce),
    .D(\blk00000001/sig00000170 ),
    .R(sclr),
    .Q(\blk00000001/sig0000016c )
  );
  FDRE #(
    .INIT ( 1'b0 ))
  \blk00000001/blk0000003a  (
    .C(clk),
    .CE(ce),
    .D(\blk00000001/sig00000171 ),
    .R(sclr),
    .Q(\blk00000001/sig0000016d )
  );
  FDRE #(
    .INIT ( 1'b0 ))
  \blk00000001/blk00000039  (
    .C(clk),
    .CE(ce),
    .D(\blk00000001/sig00000172 ),
    .R(sclr),
    .Q(\blk00000001/sig0000016e )
  );
  FDRE #(
    .INIT ( 1'b0 ))
  \blk00000001/blk00000038  (
    .C(clk),
    .CE(ce),
    .D(\blk00000001/sig00000173 ),
    .R(sclr),
    .Q(\blk00000001/sig0000016f )
  );
  MUXCY   \blk00000001/blk00000037  (
    .CI(\blk00000001/sig00000124 ),
    .DI(\blk00000001/sig00000044 ),
    .S(\blk00000001/sig0000013e ),
    .O(\blk00000001/sig0000013b )
  );
  XORCY   \blk00000001/blk00000036  (
    .CI(\blk00000001/sig00000124 ),
    .LI(\blk00000001/sig0000013e ),
    .O(\blk00000001/sig00000146 )
  );
  XORCY   \blk00000001/blk00000035  (
    .CI(\blk00000001/sig0000013d ),
    .LI(\blk00000001/sig00000141 ),
    .O(\blk00000001/sig00000149 )
  );
  MUXCY   \blk00000001/blk00000034  (
    .CI(\blk00000001/sig0000013d ),
    .DI(\blk00000001/sig00000044 ),
    .S(\blk00000001/sig00000141 ),
    .O(\blk00000001/sig00000051 )
  );
  MUXCY   \blk00000001/blk00000033  (
    .CI(\blk00000001/sig0000013b ),
    .DI(\blk00000001/sig00000044 ),
    .S(\blk00000001/sig0000013f ),
    .O(\blk00000001/sig0000013c )
  );
  XORCY   \blk00000001/blk00000032  (
    .CI(\blk00000001/sig0000013b ),
    .LI(\blk00000001/sig0000013f ),
    .O(\blk00000001/sig00000147 )
  );
  MUXCY   \blk00000001/blk00000031  (
    .CI(\blk00000001/sig0000013c ),
    .DI(\blk00000001/sig00000044 ),
    .S(\blk00000001/sig00000140 ),
    .O(\blk00000001/sig0000013d )
  );
  XORCY   \blk00000001/blk00000030  (
    .CI(\blk00000001/sig0000013c ),
    .LI(\blk00000001/sig00000140 ),
    .O(\blk00000001/sig00000148 )
  );
  FDRE #(
    .INIT ( 1'b0 ))
  \blk00000001/blk0000002f  (
    .C(clk),
    .CE(ce),
    .D(\blk00000001/sig00000146 ),
    .R(sclr),
    .Q(\blk00000001/sig00000142 )
  );
  FDRE #(
    .INIT ( 1'b0 ))
  \blk00000001/blk0000002e  (
    .C(clk),
    .CE(ce),
    .D(\blk00000001/sig00000147 ),
    .R(sclr),
    .Q(\blk00000001/sig00000143 )
  );
  FDRE #(
    .INIT ( 1'b0 ))
  \blk00000001/blk0000002d  (
    .C(clk),
    .CE(ce),
    .D(\blk00000001/sig00000148 ),
    .R(sclr),
    .Q(\blk00000001/sig00000144 )
  );
  FDRE #(
    .INIT ( 1'b0 ))
  \blk00000001/blk0000002c  (
    .C(clk),
    .CE(ce),
    .D(\blk00000001/sig00000149 ),
    .R(sclr),
    .Q(\blk00000001/sig00000145 )
  );
  MUXCY   \blk00000001/blk0000002b  (
    .CI(\blk00000001/sig00000105 ),
    .DI(\blk00000001/sig00000044 ),
    .S(\blk00000001/sig0000011a ),
    .O(\blk00000001/sig00000117 )
  );
  XORCY   \blk00000001/blk0000002a  (
    .CI(\blk00000001/sig00000105 ),
    .LI(\blk00000001/sig0000011a ),
    .O(\blk00000001/sig00000120 )
  );
  XORCY   \blk00000001/blk00000029  (
    .CI(\blk00000001/sig00000119 ),
    .LI(\blk00000001/sig0000011d ),
    .O(\blk00000001/sig00000123 )
  );
  MUXCY   \blk00000001/blk00000028  (
    .CI(\blk00000001/sig00000119 ),
    .DI(\blk00000001/sig00000044 ),
    .S(\blk00000001/sig0000011d ),
    .O(\blk00000001/sig00000050 )
  );
  MUXCY   \blk00000001/blk00000027  (
    .CI(\blk00000001/sig00000117 ),
    .DI(\blk00000001/sig00000044 ),
    .S(\blk00000001/sig0000011b ),
    .O(\blk00000001/sig00000118 )
  );
  XORCY   \blk00000001/blk00000026  (
    .CI(\blk00000001/sig00000117 ),
    .LI(\blk00000001/sig0000011b ),
    .O(\blk00000001/sig00000121 )
  );
  MUXCY   \blk00000001/blk00000025  (
    .CI(\blk00000001/sig00000118 ),
    .DI(\blk00000001/sig00000044 ),
    .S(\blk00000001/sig0000011c ),
    .O(\blk00000001/sig00000119 )
  );
  XORCY   \blk00000001/blk00000024  (
    .CI(\blk00000001/sig00000118 ),
    .LI(\blk00000001/sig0000011c ),
    .O(\blk00000001/sig00000122 )
  );
  FDRE #(
    .INIT ( 1'b0 ))
  \blk00000001/blk00000023  (
    .C(clk),
    .CE(ce),
    .D(\blk00000001/sig00000122 ),
    .R(sclr),
    .Q(\blk00000001/sig0000011e )
  );
  FDRE #(
    .INIT ( 1'b0 ))
  \blk00000001/blk00000022  (
    .C(clk),
    .CE(ce),
    .D(\blk00000001/sig00000123 ),
    .R(sclr),
    .Q(\blk00000001/sig0000011f )
  );
  MUXCY   \blk00000001/blk00000021  (
    .CI(\blk00000001/sig000000f1 ),
    .DI(\blk00000001/sig00000044 ),
    .S(\blk00000001/sig000000fb ),
    .O(\blk00000001/sig000000f8 )
  );
  XORCY   \blk00000001/blk00000020  (
    .CI(\blk00000001/sig000000f1 ),
    .LI(\blk00000001/sig000000fb ),
    .O(\blk00000001/sig00000101 )
  );
  XORCY   \blk00000001/blk0000001f  (
    .CI(\blk00000001/sig000000fa ),
    .LI(\blk00000001/sig000000fe ),
    .O(\blk00000001/sig00000104 )
  );
  MUXCY   \blk00000001/blk0000001e  (
    .CI(\blk00000001/sig000000fa ),
    .DI(\blk00000001/sig00000044 ),
    .S(\blk00000001/sig000000fe ),
    .O(\blk00000001/sig0000004f )
  );
  MUXCY   \blk00000001/blk0000001d  (
    .CI(\blk00000001/sig000000f8 ),
    .DI(\blk00000001/sig00000044 ),
    .S(\blk00000001/sig000000fc ),
    .O(\blk00000001/sig000000f9 )
  );
  XORCY   \blk00000001/blk0000001c  (
    .CI(\blk00000001/sig000000f8 ),
    .LI(\blk00000001/sig000000fc ),
    .O(\blk00000001/sig00000102 )
  );
  MUXCY   \blk00000001/blk0000001b  (
    .CI(\blk00000001/sig000000f9 ),
    .DI(\blk00000001/sig00000044 ),
    .S(\blk00000001/sig000000fd ),
    .O(\blk00000001/sig000000fa )
  );
  XORCY   \blk00000001/blk0000001a  (
    .CI(\blk00000001/sig000000f9 ),
    .LI(\blk00000001/sig000000fd ),
    .O(\blk00000001/sig00000103 )
  );
  FDRE #(
    .INIT ( 1'b0 ))
  \blk00000001/blk00000019  (
    .C(clk),
    .CE(ce),
    .D(\blk00000001/sig00000103 ),
    .R(sclr),
    .Q(\blk00000001/sig000000ff )
  );
  FDRE #(
    .INIT ( 1'b0 ))
  \blk00000001/blk00000018  (
    .C(clk),
    .CE(ce),
    .D(\blk00000001/sig00000104 ),
    .R(sclr),
    .Q(\blk00000001/sig00000100 )
  );
  MUXCY   \blk00000001/blk00000017  (
    .CI(\blk00000001/sig000000d8 ),
    .DI(\blk00000001/sig00000044 ),
    .S(\blk00000001/sig000000e7 ),
    .O(\blk00000001/sig000000e4 )
  );
  XORCY   \blk00000001/blk00000016  (
    .CI(\blk00000001/sig000000d8 ),
    .LI(\blk00000001/sig000000e7 ),
    .O(\blk00000001/sig000000ed )
  );
  XORCY   \blk00000001/blk00000015  (
    .CI(\blk00000001/sig000000e6 ),
    .LI(\blk00000001/sig000000ea ),
    .O(\blk00000001/sig000000f0 )
  );
  MUXCY   \blk00000001/blk00000014  (
    .CI(\blk00000001/sig000000e6 ),
    .DI(\blk00000001/sig00000044 ),
    .S(\blk00000001/sig000000ea ),
    .O(\blk00000001/sig0000004e )
  );
  MUXCY   \blk00000001/blk00000013  (
    .CI(\blk00000001/sig000000e4 ),
    .DI(\blk00000001/sig00000044 ),
    .S(\blk00000001/sig000000e8 ),
    .O(\blk00000001/sig000000e5 )
  );
  XORCY   \blk00000001/blk00000012  (
    .CI(\blk00000001/sig000000e4 ),
    .LI(\blk00000001/sig000000e8 ),
    .O(\blk00000001/sig000000ee )
  );
  MUXCY   \blk00000001/blk00000011  (
    .CI(\blk00000001/sig000000e5 ),
    .DI(\blk00000001/sig00000044 ),
    .S(\blk00000001/sig000000e9 ),
    .O(\blk00000001/sig000000e6 )
  );
  XORCY   \blk00000001/blk00000010  (
    .CI(\blk00000001/sig000000e5 ),
    .LI(\blk00000001/sig000000e9 ),
    .O(\blk00000001/sig000000ef )
  );
  FDRE #(
    .INIT ( 1'b0 ))
  \blk00000001/blk0000000f  (
    .C(clk),
    .CE(ce),
    .D(\blk00000001/sig000000ef ),
    .R(sclr),
    .Q(\blk00000001/sig000000eb )
  );
  FDRE #(
    .INIT ( 1'b0 ))
  \blk00000001/blk0000000e  (
    .C(clk),
    .CE(ce),
    .D(\blk00000001/sig000000f0 ),
    .R(sclr),
    .Q(\blk00000001/sig000000ec )
  );
  MUXCY   \blk00000001/blk0000000d  (
    .CI(\blk00000001/sig00000061 ),
    .DI(\blk00000001/sig00000044 ),
    .S(\blk00000001/sig000000ce ),
    .O(\blk00000001/sig000000cb )
  );
  XORCY   \blk00000001/blk0000000c  (
    .CI(\blk00000001/sig00000061 ),
    .LI(\blk00000001/sig000000ce ),
    .O(\blk00000001/sig000000d4 )
  );
  XORCY   \blk00000001/blk0000000b  (
    .CI(\blk00000001/sig000000cd ),
    .LI(\blk00000001/sig000000d1 ),
    .O(\blk00000001/sig000000d7 )
  );
  MUXCY   \blk00000001/blk0000000a  (
    .CI(\blk00000001/sig000000cd ),
    .DI(\blk00000001/sig00000044 ),
    .S(\blk00000001/sig000000d1 ),
    .O(\blk00000001/sig0000004d )
  );
  MUXCY   \blk00000001/blk00000009  (
    .CI(\blk00000001/sig000000cb ),
    .DI(\blk00000001/sig00000044 ),
    .S(\blk00000001/sig000000cf ),
    .O(\blk00000001/sig000000cc )
  );
  XORCY   \blk00000001/blk00000008  (
    .CI(\blk00000001/sig000000cb ),
    .LI(\blk00000001/sig000000cf ),
    .O(\blk00000001/sig000000d5 )
  );
  MUXCY   \blk00000001/blk00000007  (
    .CI(\blk00000001/sig000000cc ),
    .DI(\blk00000001/sig00000044 ),
    .S(\blk00000001/sig000000d0 ),
    .O(\blk00000001/sig000000cd )
  );
  XORCY   \blk00000001/blk00000006  (
    .CI(\blk00000001/sig000000cc ),
    .LI(\blk00000001/sig000000d0 ),
    .O(\blk00000001/sig000000d6 )
  );
  FDRE #(
    .INIT ( 1'b0 ))
  \blk00000001/blk00000005  (
    .C(clk),
    .CE(ce),
    .D(\blk00000001/sig000000d6 ),
    .R(sclr),
    .Q(\blk00000001/sig000000d2 )
  );
  FDRE #(
    .INIT ( 1'b0 ))
  \blk00000001/blk00000004  (
    .C(clk),
    .CE(ce),
    .D(\blk00000001/sig000000d7 ),
    .R(sclr),
    .Q(\blk00000001/sig000000d3 )
  );
  VCC   \blk00000001/blk00000003  (
    .P(\blk00000001/sig00000044 )
  );
  GND   \blk00000001/blk00000002  (
    .G(\blk00000001/sig00000043 )
  );

// synthesis translate_on

endmodule

// synthesis translate_off

`ifndef GLBL
`define GLBL

`timescale  1 ps / 1 ps

module glbl ();

    parameter ROC_WIDTH = 100000;
    parameter TOC_WIDTH = 0;

//--------   STARTUP Globals --------------
    wire GSR;
    wire GTS;
    wire GWE;
    wire PRLD;
    tri1 p_up_tmp;
    tri (weak1, strong0) PLL_LOCKG = p_up_tmp;

    wire PROGB_GLBL;
    wire CCLKO_GLBL;

    reg GSR_int;
    reg GTS_int;
    reg PRLD_int;

//--------   JTAG Globals --------------
    wire JTAG_TDO_GLBL;
    wire JTAG_TCK_GLBL;
    wire JTAG_TDI_GLBL;
    wire JTAG_TMS_GLBL;
    wire JTAG_TRST_GLBL;

    reg JTAG_CAPTURE_GLBL;
    reg JTAG_RESET_GLBL;
    reg JTAG_SHIFT_GLBL;
    reg JTAG_UPDATE_GLBL;
    reg JTAG_RUNTEST_GLBL;

    reg JTAG_SEL1_GLBL = 0;
    reg JTAG_SEL2_GLBL = 0 ;
    reg JTAG_SEL3_GLBL = 0;
    reg JTAG_SEL4_GLBL = 0;

    reg JTAG_USER_TDO1_GLBL = 1'bz;
    reg JTAG_USER_TDO2_GLBL = 1'bz;
    reg JTAG_USER_TDO3_GLBL = 1'bz;
    reg JTAG_USER_TDO4_GLBL = 1'bz;

    assign (weak1, weak0) GSR = GSR_int;
    assign (weak1, weak0) GTS = GTS_int;
    assign (weak1, weak0) PRLD = PRLD_int;

    initial begin
	GSR_int = 1'b1;
	PRLD_int = 1'b1;
	#(ROC_WIDTH)
	GSR_int = 1'b0;
	PRLD_int = 1'b0;
    end

    initial begin
	GTS_int = 1'b1;
	#(TOC_WIDTH)
	GTS_int = 1'b0;
    end

endmodule

`endif

// synthesis translate_on
