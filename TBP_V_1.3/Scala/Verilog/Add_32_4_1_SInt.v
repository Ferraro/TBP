////////////////////////////////////////////////////////////////////////////////
// Copyright (c) 1995-2013 Xilinx, Inc.  All rights reserved.
////////////////////////////////////////////////////////////////////////////////
//   ____  ____
//  /   /\/   /
// /___/  \  /    Vendor: Xilinx
// \   \   \/     Version: P.20131013
//  \   \         Application: netgen
//  /   /         Filename: Add_32_4_1_SInt.v
// /___/   /\     Timestamp: Tue Aug  5 13:16:03 2014
// \   \  /  \ 
//  \___\/\___\
//             
// Command	: -w -sim -ofmt verilog /home/francesco/Xilinx/Projects/IPCOREs/ipcore_dir/tmp/_cg/Add_32_4_1_SInt.ngc /home/francesco/Xilinx/Projects/IPCOREs/ipcore_dir/tmp/_cg/Add_32_4_1_SInt.v 
// Device	: 3sd3400afg676-4
// Input file	: /home/francesco/Xilinx/Projects/IPCOREs/ipcore_dir/tmp/_cg/Add_32_4_1_SInt.ngc
// Output file	: /home/francesco/Xilinx/Projects/IPCOREs/ipcore_dir/tmp/_cg/Add_32_4_1_SInt.v
// # of Modules	: 1
// Design Name	: Add_32_4_1_SInt
// Xilinx        : /home/francesco/Xilinx/14.7/ISE_DS/ISE/
//             
// Purpose:    
//     This verilog netlist is a verification model and uses simulation 
//     primitives which may not represent the true implementation of the 
//     device, however the netlist is functionally correct and should not 
//     be modified. This file cannot be synthesized and should only be used 
//     with supported simulation tools.
//             
// Reference:  
//     Command Line Tools User Guide, Chapter 23 and Synthesis and Simulation Design Guide, Chapter 6
//             
////////////////////////////////////////////////////////////////////////////////

`timescale 1 ns/1 ps

module Add_32_4_1_SInt (
  clk, ce, sclr, s, a, b
)/* synthesis syn_black_box syn_noprune=1 */;
  input clk;
  input ce;
  input sclr;
  output [31 : 0] s;
  input [31 : 0] a;
  input [31 : 0] b;
  
  // synthesis translate_off
  
  wire \blk00000001/sig00000176 ;
  wire \blk00000001/sig00000175 ;
  wire \blk00000001/sig00000174 ;
  wire \blk00000001/sig00000173 ;
  wire \blk00000001/sig00000172 ;
  wire \blk00000001/sig00000171 ;
  wire \blk00000001/sig00000170 ;
  wire \blk00000001/sig0000016f ;
  wire \blk00000001/sig0000016e ;
  wire \blk00000001/sig0000016d ;
  wire \blk00000001/sig0000016c ;
  wire \blk00000001/sig0000016b ;
  wire \blk00000001/sig0000016a ;
  wire \blk00000001/sig00000169 ;
  wire \blk00000001/sig00000168 ;
  wire \blk00000001/sig00000167 ;
  wire \blk00000001/sig0000015e ;
  wire \blk00000001/sig0000015d ;
  wire \blk00000001/sig0000015c ;
  wire \blk00000001/sig0000015b ;
  wire \blk00000001/sig0000015a ;
  wire \blk00000001/sig00000159 ;
  wire \blk00000001/sig00000158 ;
  wire \blk00000001/sig00000147 ;
  wire \blk00000001/sig00000146 ;
  wire \blk00000001/sig00000144 ;
  wire \blk00000001/sig00000143 ;
  wire \blk00000001/sig00000141 ;
  wire \blk00000001/sig00000140 ;
  wire \blk00000001/sig0000013e ;
  wire \blk00000001/sig0000013d ;
  wire \blk00000001/sig0000013b ;
  wire \blk00000001/sig0000013a ;
  wire \blk00000001/sig00000138 ;
  wire \blk00000001/sig00000137 ;
  wire \blk00000001/sig00000135 ;
  wire \blk00000001/sig00000134 ;
  wire \blk00000001/sig00000132 ;
  wire \blk00000001/sig00000131 ;
  wire \blk00000001/sig0000012f ;
  wire \blk00000001/sig0000012e ;
  wire \blk00000001/sig0000012d ;
  wire \blk00000001/sig0000012c ;
  wire \blk00000001/sig0000012b ;
  wire \blk00000001/sig0000012a ;
  wire \blk00000001/sig00000129 ;
  wire \blk00000001/sig00000128 ;
  wire \blk00000001/sig00000127 ;
  wire \blk00000001/sig00000126 ;
  wire \blk00000001/sig00000125 ;
  wire \blk00000001/sig00000124 ;
  wire \blk00000001/sig00000123 ;
  wire \blk00000001/sig00000122 ;
  wire \blk00000001/sig00000121 ;
  wire \blk00000001/sig00000120 ;
  wire \blk00000001/sig0000011f ;
  wire \blk00000001/sig0000011e ;
  wire \blk00000001/sig0000011d ;
  wire \blk00000001/sig0000011c ;
  wire \blk00000001/sig0000011b ;
  wire \blk00000001/sig0000011a ;
  wire \blk00000001/sig00000119 ;
  wire \blk00000001/sig00000118 ;
  wire \blk00000001/sig00000117 ;
  wire \blk00000001/sig00000116 ;
  wire \blk00000001/sig00000115 ;
  wire \blk00000001/sig00000114 ;
  wire \blk00000001/sig00000113 ;
  wire \blk00000001/sig00000112 ;
  wire \blk00000001/sig00000111 ;
  wire \blk00000001/sig00000110 ;
  wire \blk00000001/sig0000010f ;
  wire \blk00000001/sig0000010e ;
  wire \blk00000001/sig0000010d ;
  wire \blk00000001/sig0000010c ;
  wire \blk00000001/sig0000010b ;
  wire \blk00000001/sig0000010a ;
  wire \blk00000001/sig00000109 ;
  wire \blk00000001/sig00000108 ;
  wire \blk00000001/sig00000107 ;
  wire \blk00000001/sig00000106 ;
  wire \blk00000001/sig00000105 ;
  wire \blk00000001/sig00000104 ;
  wire \blk00000001/sig00000103 ;
  wire \blk00000001/sig00000102 ;
  wire \blk00000001/sig00000101 ;
  wire \blk00000001/sig00000100 ;
  wire \blk00000001/sig000000ff ;
  wire \blk00000001/sig000000fe ;
  wire \blk00000001/sig000000fd ;
  wire \blk00000001/sig000000fc ;
  wire \blk00000001/sig000000fb ;
  wire \blk00000001/sig000000fa ;
  wire \blk00000001/sig000000f9 ;
  wire \blk00000001/sig000000f8 ;
  wire \blk00000001/sig000000f7 ;
  wire \blk00000001/sig000000f6 ;
  wire \blk00000001/sig000000f5 ;
  wire \blk00000001/sig000000f4 ;
  wire \blk00000001/sig000000f3 ;
  wire \blk00000001/sig000000f2 ;
  wire \blk00000001/sig000000f1 ;
  wire \blk00000001/sig000000f0 ;
  wire \blk00000001/sig000000ef ;
  wire \blk00000001/sig000000ee ;
  wire \blk00000001/sig000000ed ;
  wire \blk00000001/sig000000ec ;
  wire \blk00000001/sig000000eb ;
  wire \blk00000001/sig000000ea ;
  wire \blk00000001/sig000000e9 ;
  wire \blk00000001/sig000000e8 ;
  wire \blk00000001/sig000000e7 ;
  wire \blk00000001/sig000000e6 ;
  wire \blk00000001/sig000000e5 ;
  wire \blk00000001/sig000000e4 ;
  wire \blk00000001/sig000000e3 ;
  wire \blk00000001/sig000000e2 ;
  wire \blk00000001/sig000000e1 ;
  wire \blk00000001/sig000000e0 ;
  wire \blk00000001/sig000000df ;
  wire \blk00000001/sig000000de ;
  wire \blk00000001/sig000000dd ;
  wire \blk00000001/sig000000dc ;
  wire \blk00000001/sig000000db ;
  wire \blk00000001/sig000000da ;
  wire \blk00000001/sig000000d9 ;
  wire \blk00000001/sig000000d8 ;
  wire \blk00000001/sig000000d7 ;
  wire \blk00000001/sig000000d6 ;
  wire \blk00000001/sig000000d5 ;
  wire \blk00000001/sig000000d4 ;
  wire \blk00000001/sig000000d3 ;
  wire \blk00000001/sig000000d2 ;
  wire \blk00000001/sig000000d1 ;
  wire \blk00000001/sig000000d0 ;
  wire \blk00000001/sig000000cf ;
  wire \blk00000001/sig000000ce ;
  wire \blk00000001/sig000000cd ;
  wire \blk00000001/sig000000cc ;
  wire \blk00000001/sig000000cb ;
  wire \blk00000001/sig000000ca ;
  wire \blk00000001/sig000000c9 ;
  wire \blk00000001/sig000000c8 ;
  wire \blk00000001/sig000000c7 ;
  wire \blk00000001/sig000000c6 ;
  wire \blk00000001/sig000000c5 ;
  wire \blk00000001/sig000000c4 ;
  wire \blk00000001/sig000000c3 ;
  wire \blk00000001/sig000000c2 ;
  wire \blk00000001/sig000000c1 ;
  wire \blk00000001/sig000000c0 ;
  wire \blk00000001/sig000000bf ;
  wire \blk00000001/sig000000be ;
  wire \blk00000001/sig000000bd ;
  wire \blk00000001/sig000000bc ;
  wire \blk00000001/sig000000bb ;
  wire \blk00000001/sig000000ba ;
  wire \blk00000001/sig000000b9 ;
  wire \blk00000001/sig000000b8 ;
  wire \blk00000001/sig000000b7 ;
  wire \blk00000001/sig000000b6 ;
  wire \blk00000001/sig000000b5 ;
  wire \blk00000001/sig000000b4 ;
  wire \blk00000001/sig000000b3 ;
  wire \blk00000001/sig000000b2 ;
  wire \blk00000001/sig000000b1 ;
  wire \blk00000001/sig000000b0 ;
  wire \blk00000001/sig000000af ;
  wire \blk00000001/sig000000ae ;
  wire \blk00000001/sig000000ad ;
  wire \blk00000001/sig000000ac ;
  wire \blk00000001/sig000000ab ;
  wire \blk00000001/sig000000aa ;
  wire \blk00000001/sig000000a9 ;
  wire \blk00000001/sig000000a8 ;
  wire \blk00000001/sig000000a7 ;
  wire \blk00000001/sig000000a6 ;
  wire \blk00000001/sig000000a5 ;
  wire \blk00000001/sig000000a4 ;
  wire \blk00000001/sig000000a3 ;
  wire \blk00000001/sig000000a2 ;
  wire \blk00000001/sig000000a1 ;
  wire \blk00000001/sig000000a0 ;
  wire \blk00000001/sig0000009f ;
  wire \blk00000001/sig0000009e ;
  wire \blk00000001/sig0000009d ;
  wire \blk00000001/sig0000009c ;
  wire \blk00000001/sig0000009b ;
  wire \blk00000001/sig0000009a ;
  wire \blk00000001/sig00000099 ;
  wire \blk00000001/sig00000098 ;
  wire \blk00000001/sig00000097 ;
  wire \blk00000001/sig00000096 ;
  wire \blk00000001/sig00000095 ;
  wire \blk00000001/sig00000094 ;
  wire \blk00000001/sig00000093 ;
  wire \blk00000001/sig00000092 ;
  wire \blk00000001/sig00000091 ;
  wire \blk00000001/sig00000090 ;
  wire \blk00000001/sig0000008f ;
  wire \blk00000001/sig0000008e ;
  wire \blk00000001/sig0000008d ;
  wire \blk00000001/sig0000008c ;
  wire \blk00000001/sig0000008b ;
  wire \blk00000001/sig0000008a ;
  wire \blk00000001/sig00000089 ;
  wire \blk00000001/sig00000088 ;
  wire \blk00000001/sig00000087 ;
  wire \blk00000001/sig00000086 ;
  wire \blk00000001/sig00000085 ;
  wire \blk00000001/sig00000084 ;
  wire \blk00000001/sig00000083 ;
  wire \blk00000001/sig00000082 ;
  wire \blk00000001/sig00000081 ;
  wire \blk00000001/sig00000080 ;
  wire \blk00000001/sig0000007f ;
  wire \blk00000001/sig0000007e ;
  wire \blk00000001/sig0000007d ;
  wire \blk00000001/sig0000007c ;
  wire \blk00000001/sig0000007b ;
  wire \blk00000001/sig0000007a ;
  wire \blk00000001/sig00000079 ;
  wire \blk00000001/sig00000078 ;
  wire \blk00000001/sig00000077 ;
  wire \blk00000001/sig00000076 ;
  wire \blk00000001/sig00000075 ;
  wire \blk00000001/sig00000074 ;
  wire \blk00000001/sig00000073 ;
  wire \blk00000001/sig00000072 ;
  wire \blk00000001/sig00000071 ;
  wire \blk00000001/sig00000070 ;
  wire \blk00000001/sig0000006f ;
  wire \blk00000001/sig0000006e ;
  wire \blk00000001/sig0000006d ;
  wire \blk00000001/sig0000006c ;
  wire \blk00000001/sig0000006b ;
  wire \blk00000001/sig0000006a ;
  wire \blk00000001/sig00000069 ;
  wire \blk00000001/sig00000068 ;
  wire \blk00000001/sig00000067 ;
  wire \blk00000001/sig00000066 ;
  wire \blk00000001/sig00000065 ;
  wire \blk00000001/sig00000064 ;
  wire \blk00000001/sig00000063 ;
  wire \blk00000001/sig00000062 ;
  wire \blk00000001/sig00000061 ;
  wire \blk00000001/sig00000060 ;
  wire \blk00000001/sig0000005f ;
  wire \blk00000001/sig0000005e ;
  wire \blk00000001/sig0000005d ;
  wire \blk00000001/sig0000005c ;
  wire \blk00000001/sig0000005b ;
  wire \blk00000001/sig0000005a ;
  wire \blk00000001/sig00000059 ;
  wire \blk00000001/sig00000058 ;
  wire \blk00000001/sig00000057 ;
  wire \blk00000001/sig00000056 ;
  wire \blk00000001/sig00000055 ;
  wire \blk00000001/sig00000054 ;
  wire \blk00000001/sig00000053 ;
  wire \blk00000001/sig00000052 ;
  wire \blk00000001/sig00000051 ;
  wire \blk00000001/sig00000050 ;
  wire \blk00000001/sig0000004f ;
  wire \blk00000001/sig0000004e ;
  wire \blk00000001/sig0000004d ;
  wire \blk00000001/sig0000004c ;
  wire \blk00000001/sig0000004b ;
  wire \blk00000001/sig0000004a ;
  wire \blk00000001/sig00000049 ;
  wire \blk00000001/sig00000048 ;
  wire \blk00000001/sig00000047 ;
  wire \blk00000001/sig00000046 ;
  wire \blk00000001/sig00000044 ;
  wire \blk00000001/sig00000043 ;
  wire \NLW_blk00000001/blk00000047_O_UNCONNECTED ;
  FDRE #(
    .INIT ( 1'b0 ))
  \blk00000001/blk00000135  (
    .C(clk),
    .CE(ce),
    .D(\blk00000001/sig00000147 ),
    .R(sclr),
    .Q(s[7])
  );
  LUT2 #(
    .INIT ( 4'h8 ))
  \blk00000001/blk00000134  (
    .I0(\blk00000001/sig00000146 ),
    .I1(\blk00000001/sig00000048 ),
    .O(\blk00000001/sig00000147 )
  );
  FDRE #(
    .INIT ( 1'b0 ))
  \blk00000001/blk00000133  (
    .C(clk),
    .CE(ce),
    .D(\blk00000001/sig00000144 ),
    .R(sclr),
    .Q(s[6])
  );
  LUT2 #(
    .INIT ( 4'h8 ))
  \blk00000001/blk00000132  (
    .I0(\blk00000001/sig00000143 ),
    .I1(\blk00000001/sig00000048 ),
    .O(\blk00000001/sig00000144 )
  );
  FDRE #(
    .INIT ( 1'b0 ))
  \blk00000001/blk00000131  (
    .C(clk),
    .CE(ce),
    .D(\blk00000001/sig00000141 ),
    .R(sclr),
    .Q(s[5])
  );
  LUT2 #(
    .INIT ( 4'h8 ))
  \blk00000001/blk00000130  (
    .I0(\blk00000001/sig00000140 ),
    .I1(\blk00000001/sig00000048 ),
    .O(\blk00000001/sig00000141 )
  );
  FDRE #(
    .INIT ( 1'b0 ))
  \blk00000001/blk0000012f  (
    .C(clk),
    .CE(ce),
    .D(\blk00000001/sig0000013e ),
    .R(sclr),
    .Q(s[4])
  );
  LUT2 #(
    .INIT ( 4'h8 ))
  \blk00000001/blk0000012e  (
    .I0(\blk00000001/sig0000013d ),
    .I1(\blk00000001/sig00000048 ),
    .O(\blk00000001/sig0000013e )
  );
  FDRE #(
    .INIT ( 1'b0 ))
  \blk00000001/blk0000012d  (
    .C(clk),
    .CE(ce),
    .D(\blk00000001/sig0000013b ),
    .R(sclr),
    .Q(s[3])
  );
  LUT2 #(
    .INIT ( 4'h8 ))
  \blk00000001/blk0000012c  (
    .I0(\blk00000001/sig0000013a ),
    .I1(\blk00000001/sig00000048 ),
    .O(\blk00000001/sig0000013b )
  );
  FDRE #(
    .INIT ( 1'b0 ))
  \blk00000001/blk0000012b  (
    .C(clk),
    .CE(ce),
    .D(\blk00000001/sig00000138 ),
    .R(sclr),
    .Q(s[2])
  );
  LUT2 #(
    .INIT ( 4'h8 ))
  \blk00000001/blk0000012a  (
    .I0(\blk00000001/sig00000137 ),
    .I1(\blk00000001/sig00000048 ),
    .O(\blk00000001/sig00000138 )
  );
  FDRE #(
    .INIT ( 1'b0 ))
  \blk00000001/blk00000129  (
    .C(clk),
    .CE(ce),
    .D(\blk00000001/sig00000135 ),
    .R(sclr),
    .Q(s[1])
  );
  LUT2 #(
    .INIT ( 4'h8 ))
  \blk00000001/blk00000128  (
    .I0(\blk00000001/sig00000134 ),
    .I1(\blk00000001/sig00000048 ),
    .O(\blk00000001/sig00000135 )
  );
  FDRE #(
    .INIT ( 1'b0 ))
  \blk00000001/blk00000127  (
    .C(clk),
    .CE(ce),
    .D(\blk00000001/sig00000132 ),
    .R(sclr),
    .Q(s[0])
  );
  LUT2 #(
    .INIT ( 4'h8 ))
  \blk00000001/blk00000126  (
    .I0(\blk00000001/sig00000131 ),
    .I1(\blk00000001/sig00000048 ),
    .O(\blk00000001/sig00000132 )
  );
  FDRE   \blk00000001/blk00000125  (
    .C(clk),
    .CE(ce),
    .D(\blk00000001/sig00000047 ),
    .R(sclr),
    .Q(\blk00000001/sig00000048 )
  );
  FDRE   \blk00000001/blk00000124  (
    .C(clk),
    .CE(ce),
    .D(\blk00000001/sig00000046 ),
    .R(sclr),
    .Q(\blk00000001/sig00000047 )
  );
  FDRE   \blk00000001/blk00000123  (
    .C(clk),
    .CE(ce),
    .D(\blk00000001/sig00000044 ),
    .R(sclr),
    .Q(\blk00000001/sig00000046 )
  );
  FDE #(
    .INIT ( 1'b0 ))
  \blk00000001/blk00000122  (
    .C(clk),
    .CE(ce),
    .D(\blk00000001/sig00000128 ),
    .Q(\blk00000001/sig00000131 )
  );
  SRL16E #(
    .INIT ( 16'h0000 ))
  \blk00000001/blk00000121  (
    .A0(\blk00000001/sig00000044 ),
    .A1(\blk00000001/sig00000043 ),
    .A2(\blk00000001/sig00000043 ),
    .A3(\blk00000001/sig00000043 ),
    .CE(ce),
    .CLK(clk),
    .D(\blk00000001/sig0000005e ),
    .Q(\blk00000001/sig00000128 )
  );
  FDE #(
    .INIT ( 1'b0 ))
  \blk00000001/blk00000120  (
    .C(clk),
    .CE(ce),
    .D(\blk00000001/sig00000129 ),
    .Q(\blk00000001/sig00000134 )
  );
  SRL16E #(
    .INIT ( 16'h0000 ))
  \blk00000001/blk0000011f  (
    .A0(\blk00000001/sig00000044 ),
    .A1(\blk00000001/sig00000043 ),
    .A2(\blk00000001/sig00000043 ),
    .A3(\blk00000001/sig00000043 ),
    .CE(ce),
    .CLK(clk),
    .D(\blk00000001/sig0000005f ),
    .Q(\blk00000001/sig00000129 )
  );
  FDE #(
    .INIT ( 1'b0 ))
  \blk00000001/blk0000011e  (
    .C(clk),
    .CE(ce),
    .D(\blk00000001/sig0000012a ),
    .Q(\blk00000001/sig00000137 )
  );
  SRL16E #(
    .INIT ( 16'h0000 ))
  \blk00000001/blk0000011d  (
    .A0(\blk00000001/sig00000044 ),
    .A1(\blk00000001/sig00000043 ),
    .A2(\blk00000001/sig00000043 ),
    .A3(\blk00000001/sig00000043 ),
    .CE(ce),
    .CLK(clk),
    .D(\blk00000001/sig00000060 ),
    .Q(\blk00000001/sig0000012a )
  );
  FDE #(
    .INIT ( 1'b0 ))
  \blk00000001/blk0000011c  (
    .C(clk),
    .CE(ce),
    .D(\blk00000001/sig0000012b ),
    .Q(\blk00000001/sig0000013a )
  );
  SRL16E #(
    .INIT ( 16'h0000 ))
  \blk00000001/blk0000011b  (
    .A0(\blk00000001/sig00000044 ),
    .A1(\blk00000001/sig00000043 ),
    .A2(\blk00000001/sig00000043 ),
    .A3(\blk00000001/sig00000043 ),
    .CE(ce),
    .CLK(clk),
    .D(\blk00000001/sig00000061 ),
    .Q(\blk00000001/sig0000012b )
  );
  FDE #(
    .INIT ( 1'b0 ))
  \blk00000001/blk0000011a  (
    .C(clk),
    .CE(ce),
    .D(\blk00000001/sig0000012c ),
    .Q(\blk00000001/sig0000013d )
  );
  SRL16E #(
    .INIT ( 16'h0000 ))
  \blk00000001/blk00000119  (
    .A0(\blk00000001/sig00000044 ),
    .A1(\blk00000001/sig00000043 ),
    .A2(\blk00000001/sig00000043 ),
    .A3(\blk00000001/sig00000043 ),
    .CE(ce),
    .CLK(clk),
    .D(\blk00000001/sig00000062 ),
    .Q(\blk00000001/sig0000012c )
  );
  FDE #(
    .INIT ( 1'b0 ))
  \blk00000001/blk00000118  (
    .C(clk),
    .CE(ce),
    .D(\blk00000001/sig0000012d ),
    .Q(\blk00000001/sig00000140 )
  );
  SRL16E #(
    .INIT ( 16'h0000 ))
  \blk00000001/blk00000117  (
    .A0(\blk00000001/sig00000044 ),
    .A1(\blk00000001/sig00000043 ),
    .A2(\blk00000001/sig00000043 ),
    .A3(\blk00000001/sig00000043 ),
    .CE(ce),
    .CLK(clk),
    .D(\blk00000001/sig00000063 ),
    .Q(\blk00000001/sig0000012d )
  );
  FDE #(
    .INIT ( 1'b0 ))
  \blk00000001/blk00000116  (
    .C(clk),
    .CE(ce),
    .D(\blk00000001/sig0000012e ),
    .Q(\blk00000001/sig00000143 )
  );
  SRL16E #(
    .INIT ( 16'h0000 ))
  \blk00000001/blk00000115  (
    .A0(\blk00000001/sig00000044 ),
    .A1(\blk00000001/sig00000043 ),
    .A2(\blk00000001/sig00000043 ),
    .A3(\blk00000001/sig00000043 ),
    .CE(ce),
    .CLK(clk),
    .D(\blk00000001/sig00000064 ),
    .Q(\blk00000001/sig0000012e )
  );
  FDE #(
    .INIT ( 1'b0 ))
  \blk00000001/blk00000114  (
    .C(clk),
    .CE(ce),
    .D(\blk00000001/sig0000012f ),
    .Q(\blk00000001/sig00000146 )
  );
  SRL16E #(
    .INIT ( 16'h0000 ))
  \blk00000001/blk00000113  (
    .A0(\blk00000001/sig00000044 ),
    .A1(\blk00000001/sig00000043 ),
    .A2(\blk00000001/sig00000043 ),
    .A3(\blk00000001/sig00000043 ),
    .CE(ce),
    .CLK(clk),
    .D(\blk00000001/sig00000065 ),
    .Q(\blk00000001/sig0000012f )
  );
  VCC   \blk00000001/blk00000112  (
    .P(\blk00000001/sig00000044 )
  );
  LUT1 #(
    .INIT ( 2'h2 ))
  \blk00000001/blk00000111  (
    .I0(\blk00000001/sig00000120 ),
    .O(\blk00000001/sig0000016e )
  );
  LUT1 #(
    .INIT ( 2'h2 ))
  \blk00000001/blk00000110  (
    .I0(\blk00000001/sig00000127 ),
    .O(\blk00000001/sig0000016d )
  );
  LUT1 #(
    .INIT ( 2'h2 ))
  \blk00000001/blk0000010f  (
    .I0(\blk00000001/sig00000121 ),
    .O(\blk00000001/sig00000167 )
  );
  LUT1 #(
    .INIT ( 2'h2 ))
  \blk00000001/blk0000010e  (
    .I0(\blk00000001/sig00000122 ),
    .O(\blk00000001/sig00000168 )
  );
  LUT1 #(
    .INIT ( 2'h2 ))
  \blk00000001/blk0000010d  (
    .I0(\blk00000001/sig00000123 ),
    .O(\blk00000001/sig00000169 )
  );
  LUT1 #(
    .INIT ( 2'h2 ))
  \blk00000001/blk0000010c  (
    .I0(\blk00000001/sig00000124 ),
    .O(\blk00000001/sig0000016a )
  );
  LUT1 #(
    .INIT ( 2'h2 ))
  \blk00000001/blk0000010b  (
    .I0(\blk00000001/sig00000125 ),
    .O(\blk00000001/sig0000016b )
  );
  LUT1 #(
    .INIT ( 2'h2 ))
  \blk00000001/blk0000010a  (
    .I0(\blk00000001/sig00000126 ),
    .O(\blk00000001/sig0000016c )
  );
  LUT1 #(
    .INIT ( 2'h2 ))
  \blk00000001/blk00000109  (
    .I0(\blk00000001/sig000000e8 ),
    .O(\blk00000001/sig00000116 )
  );
  LUT1 #(
    .INIT ( 2'h2 ))
  \blk00000001/blk00000108  (
    .I0(\blk00000001/sig000000ef ),
    .O(\blk00000001/sig00000115 )
  );
  LUT1 #(
    .INIT ( 2'h2 ))
  \blk00000001/blk00000107  (
    .I0(\blk00000001/sig000000e9 ),
    .O(\blk00000001/sig0000010f )
  );
  LUT1 #(
    .INIT ( 2'h2 ))
  \blk00000001/blk00000106  (
    .I0(\blk00000001/sig000000ea ),
    .O(\blk00000001/sig00000110 )
  );
  LUT1 #(
    .INIT ( 2'h2 ))
  \blk00000001/blk00000105  (
    .I0(\blk00000001/sig000000eb ),
    .O(\blk00000001/sig00000111 )
  );
  LUT1 #(
    .INIT ( 2'h2 ))
  \blk00000001/blk00000104  (
    .I0(\blk00000001/sig000000ec ),
    .O(\blk00000001/sig00000112 )
  );
  LUT1 #(
    .INIT ( 2'h2 ))
  \blk00000001/blk00000103  (
    .I0(\blk00000001/sig000000ed ),
    .O(\blk00000001/sig00000113 )
  );
  LUT1 #(
    .INIT ( 2'h2 ))
  \blk00000001/blk00000102  (
    .I0(\blk00000001/sig000000ee ),
    .O(\blk00000001/sig00000114 )
  );
  LUT1 #(
    .INIT ( 2'h2 ))
  \blk00000001/blk00000101  (
    .I0(\blk00000001/sig00000076 ),
    .O(\blk00000001/sig000000dd )
  );
  LUT1 #(
    .INIT ( 2'h2 ))
  \blk00000001/blk00000100  (
    .I0(\blk00000001/sig0000007d ),
    .O(\blk00000001/sig000000dc )
  );
  LUT1 #(
    .INIT ( 2'h2 ))
  \blk00000001/blk000000ff  (
    .I0(\blk00000001/sig00000077 ),
    .O(\blk00000001/sig000000d6 )
  );
  LUT1 #(
    .INIT ( 2'h2 ))
  \blk00000001/blk000000fe  (
    .I0(\blk00000001/sig00000078 ),
    .O(\blk00000001/sig000000d7 )
  );
  LUT1 #(
    .INIT ( 2'h2 ))
  \blk00000001/blk000000fd  (
    .I0(\blk00000001/sig00000079 ),
    .O(\blk00000001/sig000000d8 )
  );
  LUT1 #(
    .INIT ( 2'h2 ))
  \blk00000001/blk000000fc  (
    .I0(\blk00000001/sig0000007a ),
    .O(\blk00000001/sig000000d9 )
  );
  LUT1 #(
    .INIT ( 2'h2 ))
  \blk00000001/blk000000fb  (
    .I0(\blk00000001/sig0000007b ),
    .O(\blk00000001/sig000000da )
  );
  LUT1 #(
    .INIT ( 2'h2 ))
  \blk00000001/blk000000fa  (
    .I0(\blk00000001/sig0000007c ),
    .O(\blk00000001/sig000000db )
  );
  LUT2 #(
    .INIT ( 4'h6 ))
  \blk00000001/blk000000f9  (
    .I0(b[0]),
    .I1(a[0]),
    .O(\blk00000001/sig00000055 )
  );
  LUT2 #(
    .INIT ( 4'h6 ))
  \blk00000001/blk000000f8  (
    .I0(b[8]),
    .I1(a[8]),
    .O(\blk00000001/sig0000006e )
  );
  LUT2 #(
    .INIT ( 4'h6 ))
  \blk00000001/blk000000f7  (
    .I0(b[16]),
    .I1(a[16]),
    .O(\blk00000001/sig0000008f )
  );
  LUT2 #(
    .INIT ( 4'h6 ))
  \blk00000001/blk000000f6  (
    .I0(b[24]),
    .I1(a[24]),
    .O(\blk00000001/sig000000af )
  );
  LUT2 #(
    .INIT ( 4'h6 ))
  \blk00000001/blk000000f5  (
    .I0(b[1]),
    .I1(a[1]),
    .O(\blk00000001/sig00000056 )
  );
  LUT2 #(
    .INIT ( 4'h6 ))
  \blk00000001/blk000000f4  (
    .I0(b[9]),
    .I1(a[9]),
    .O(\blk00000001/sig0000006f )
  );
  LUT2 #(
    .INIT ( 4'h6 ))
  \blk00000001/blk000000f3  (
    .I0(b[17]),
    .I1(a[17]),
    .O(\blk00000001/sig00000090 )
  );
  LUT2 #(
    .INIT ( 4'h6 ))
  \blk00000001/blk000000f2  (
    .I0(b[25]),
    .I1(a[25]),
    .O(\blk00000001/sig000000b0 )
  );
  LUT2 #(
    .INIT ( 4'h6 ))
  \blk00000001/blk000000f1  (
    .I0(b[2]),
    .I1(a[2]),
    .O(\blk00000001/sig00000057 )
  );
  LUT2 #(
    .INIT ( 4'h6 ))
  \blk00000001/blk000000f0  (
    .I0(b[10]),
    .I1(a[10]),
    .O(\blk00000001/sig00000070 )
  );
  LUT2 #(
    .INIT ( 4'h6 ))
  \blk00000001/blk000000ef  (
    .I0(b[18]),
    .I1(a[18]),
    .O(\blk00000001/sig00000091 )
  );
  LUT2 #(
    .INIT ( 4'h6 ))
  \blk00000001/blk000000ee  (
    .I0(b[26]),
    .I1(a[26]),
    .O(\blk00000001/sig000000b1 )
  );
  LUT2 #(
    .INIT ( 4'hE ))
  \blk00000001/blk000000ed  (
    .I0(\blk00000001/sig0000007e ),
    .I1(\blk00000001/sig00000049 ),
    .O(\blk00000001/sig0000004b )
  );
  LUT2 #(
    .INIT ( 4'hE ))
  \blk00000001/blk000000ec  (
    .I0(\blk00000001/sig000000e7 ),
    .I1(\blk00000001/sig0000004a ),
    .O(\blk00000001/sig0000004c )
  );
  LUT2 #(
    .INIT ( 4'h6 ))
  \blk00000001/blk000000eb  (
    .I0(b[3]),
    .I1(a[3]),
    .O(\blk00000001/sig00000058 )
  );
  LUT2 #(
    .INIT ( 4'h6 ))
  \blk00000001/blk000000ea  (
    .I0(b[11]),
    .I1(a[11]),
    .O(\blk00000001/sig00000071 )
  );
  LUT2 #(
    .INIT ( 4'h6 ))
  \blk00000001/blk000000e9  (
    .I0(b[19]),
    .I1(a[19]),
    .O(\blk00000001/sig00000092 )
  );
  LUT2 #(
    .INIT ( 4'h6 ))
  \blk00000001/blk000000e8  (
    .I0(b[27]),
    .I1(a[27]),
    .O(\blk00000001/sig000000b2 )
  );
  LUT2 #(
    .INIT ( 4'h6 ))
  \blk00000001/blk000000e7  (
    .I0(b[4]),
    .I1(a[4]),
    .O(\blk00000001/sig00000059 )
  );
  LUT2 #(
    .INIT ( 4'h6 ))
  \blk00000001/blk000000e6  (
    .I0(b[12]),
    .I1(a[12]),
    .O(\blk00000001/sig00000072 )
  );
  LUT2 #(
    .INIT ( 4'h6 ))
  \blk00000001/blk000000e5  (
    .I0(b[20]),
    .I1(a[20]),
    .O(\blk00000001/sig00000093 )
  );
  LUT2 #(
    .INIT ( 4'h6 ))
  \blk00000001/blk000000e4  (
    .I0(b[28]),
    .I1(a[28]),
    .O(\blk00000001/sig000000b3 )
  );
  LUT2 #(
    .INIT ( 4'h6 ))
  \blk00000001/blk000000e3  (
    .I0(b[5]),
    .I1(a[5]),
    .O(\blk00000001/sig0000005a )
  );
  LUT2 #(
    .INIT ( 4'h6 ))
  \blk00000001/blk000000e2  (
    .I0(b[13]),
    .I1(a[13]),
    .O(\blk00000001/sig00000073 )
  );
  LUT2 #(
    .INIT ( 4'h6 ))
  \blk00000001/blk000000e1  (
    .I0(b[21]),
    .I1(a[21]),
    .O(\blk00000001/sig00000094 )
  );
  LUT2 #(
    .INIT ( 4'h6 ))
  \blk00000001/blk000000e0  (
    .I0(b[29]),
    .I1(a[29]),
    .O(\blk00000001/sig000000b4 )
  );
  LUT2 #(
    .INIT ( 4'h6 ))
  \blk00000001/blk000000df  (
    .I0(b[6]),
    .I1(a[6]),
    .O(\blk00000001/sig0000005b )
  );
  LUT2 #(
    .INIT ( 4'h6 ))
  \blk00000001/blk000000de  (
    .I0(b[14]),
    .I1(a[14]),
    .O(\blk00000001/sig00000074 )
  );
  LUT2 #(
    .INIT ( 4'h6 ))
  \blk00000001/blk000000dd  (
    .I0(b[22]),
    .I1(a[22]),
    .O(\blk00000001/sig00000095 )
  );
  LUT2 #(
    .INIT ( 4'h6 ))
  \blk00000001/blk000000dc  (
    .I0(b[30]),
    .I1(a[30]),
    .O(\blk00000001/sig000000b5 )
  );
  LUT2 #(
    .INIT ( 4'h6 ))
  \blk00000001/blk000000db  (
    .I0(b[7]),
    .I1(a[7]),
    .O(\blk00000001/sig0000005c )
  );
  LUT2 #(
    .INIT ( 4'h6 ))
  \blk00000001/blk000000da  (
    .I0(b[15]),
    .I1(a[15]),
    .O(\blk00000001/sig00000075 )
  );
  LUT2 #(
    .INIT ( 4'h6 ))
  \blk00000001/blk000000d9  (
    .I0(b[23]),
    .I1(a[23]),
    .O(\blk00000001/sig00000096 )
  );
  LUT2 #(
    .INIT ( 4'h6 ))
  \blk00000001/blk000000d8  (
    .I0(b[31]),
    .I1(a[31]),
    .O(\blk00000001/sig000000b6 )
  );
  FDRE #(
    .INIT ( 1'b0 ))
  \blk00000001/blk000000d7  (
    .C(clk),
    .CE(ce),
    .D(\blk00000001/sig00000107 ),
    .R(sclr),
    .Q(s[16])
  );
  FDRE #(
    .INIT ( 1'b0 ))
  \blk00000001/blk000000d6  (
    .C(clk),
    .CE(ce),
    .D(\blk00000001/sig00000108 ),
    .R(sclr),
    .Q(s[17])
  );
  FDRE #(
    .INIT ( 1'b0 ))
  \blk00000001/blk000000d5  (
    .C(clk),
    .CE(ce),
    .D(\blk00000001/sig00000109 ),
    .R(sclr),
    .Q(s[18])
  );
  FDRE #(
    .INIT ( 1'b0 ))
  \blk00000001/blk000000d4  (
    .C(clk),
    .CE(ce),
    .D(\blk00000001/sig0000010a ),
    .R(sclr),
    .Q(s[19])
  );
  FDRE #(
    .INIT ( 1'b0 ))
  \blk00000001/blk000000d3  (
    .C(clk),
    .CE(ce),
    .D(\blk00000001/sig0000010b ),
    .R(sclr),
    .Q(s[20])
  );
  FDRE #(
    .INIT ( 1'b0 ))
  \blk00000001/blk000000d2  (
    .C(clk),
    .CE(ce),
    .D(\blk00000001/sig0000010c ),
    .R(sclr),
    .Q(s[21])
  );
  FDRE #(
    .INIT ( 1'b0 ))
  \blk00000001/blk000000d1  (
    .C(clk),
    .CE(ce),
    .D(\blk00000001/sig0000010d ),
    .R(sclr),
    .Q(s[22])
  );
  FDRE #(
    .INIT ( 1'b0 ))
  \blk00000001/blk000000d0  (
    .C(clk),
    .CE(ce),
    .D(\blk00000001/sig0000010e ),
    .R(sclr),
    .Q(s[23])
  );
  FDRE #(
    .INIT ( 1'b0 ))
  \blk00000001/blk000000cf  (
    .C(clk),
    .CE(ce),
    .D(\blk00000001/sig000000f8 ),
    .R(sclr),
    .Q(s[8])
  );
  FDRE #(
    .INIT ( 1'b0 ))
  \blk00000001/blk000000ce  (
    .C(clk),
    .CE(ce),
    .D(\blk00000001/sig000000f9 ),
    .R(sclr),
    .Q(s[9])
  );
  FDRE #(
    .INIT ( 1'b0 ))
  \blk00000001/blk000000cd  (
    .C(clk),
    .CE(ce),
    .D(\blk00000001/sig000000fa ),
    .R(sclr),
    .Q(s[10])
  );
  FDRE #(
    .INIT ( 1'b0 ))
  \blk00000001/blk000000cc  (
    .C(clk),
    .CE(ce),
    .D(\blk00000001/sig000000fb ),
    .R(sclr),
    .Q(s[11])
  );
  FDRE #(
    .INIT ( 1'b0 ))
  \blk00000001/blk000000cb  (
    .C(clk),
    .CE(ce),
    .D(\blk00000001/sig000000fc ),
    .R(sclr),
    .Q(s[12])
  );
  FDRE #(
    .INIT ( 1'b0 ))
  \blk00000001/blk000000ca  (
    .C(clk),
    .CE(ce),
    .D(\blk00000001/sig000000fd ),
    .R(sclr),
    .Q(s[13])
  );
  FDRE #(
    .INIT ( 1'b0 ))
  \blk00000001/blk000000c9  (
    .C(clk),
    .CE(ce),
    .D(\blk00000001/sig000000fe ),
    .R(sclr),
    .Q(s[14])
  );
  FDRE #(
    .INIT ( 1'b0 ))
  \blk00000001/blk000000c8  (
    .C(clk),
    .CE(ce),
    .D(\blk00000001/sig000000ff ),
    .R(sclr),
    .Q(s[15])
  );
  FDRE #(
    .INIT ( 1'b0 ))
  \blk00000001/blk000000c7  (
    .C(clk),
    .CE(ce),
    .D(\blk00000001/sig000000f0 ),
    .R(sclr),
    .Q(\blk00000001/sig00000120 )
  );
  FDRE #(
    .INIT ( 1'b0 ))
  \blk00000001/blk000000c6  (
    .C(clk),
    .CE(ce),
    .D(\blk00000001/sig000000f1 ),
    .R(sclr),
    .Q(\blk00000001/sig00000121 )
  );
  FDRE #(
    .INIT ( 1'b0 ))
  \blk00000001/blk000000c5  (
    .C(clk),
    .CE(ce),
    .D(\blk00000001/sig000000f2 ),
    .R(sclr),
    .Q(\blk00000001/sig00000122 )
  );
  FDRE #(
    .INIT ( 1'b0 ))
  \blk00000001/blk000000c4  (
    .C(clk),
    .CE(ce),
    .D(\blk00000001/sig000000f3 ),
    .R(sclr),
    .Q(\blk00000001/sig00000123 )
  );
  FDRE #(
    .INIT ( 1'b0 ))
  \blk00000001/blk000000c3  (
    .C(clk),
    .CE(ce),
    .D(\blk00000001/sig000000f4 ),
    .R(sclr),
    .Q(\blk00000001/sig00000124 )
  );
  FDRE #(
    .INIT ( 1'b0 ))
  \blk00000001/blk000000c2  (
    .C(clk),
    .CE(ce),
    .D(\blk00000001/sig000000f5 ),
    .R(sclr),
    .Q(\blk00000001/sig00000125 )
  );
  FDRE #(
    .INIT ( 1'b0 ))
  \blk00000001/blk000000c1  (
    .C(clk),
    .CE(ce),
    .D(\blk00000001/sig000000f6 ),
    .R(sclr),
    .Q(\blk00000001/sig00000126 )
  );
  FDRE #(
    .INIT ( 1'b0 ))
  \blk00000001/blk000000c0  (
    .C(clk),
    .CE(ce),
    .D(\blk00000001/sig000000f7 ),
    .R(sclr),
    .Q(\blk00000001/sig00000127 )
  );
  FDRE #(
    .INIT ( 1'b0 ))
  \blk00000001/blk000000bf  (
    .C(clk),
    .CE(ce),
    .D(\blk00000001/sig000000ce ),
    .R(sclr),
    .Q(\blk00000001/sig000000f8 )
  );
  FDRE #(
    .INIT ( 1'b0 ))
  \blk00000001/blk000000be  (
    .C(clk),
    .CE(ce),
    .D(\blk00000001/sig000000cf ),
    .R(sclr),
    .Q(\blk00000001/sig000000f9 )
  );
  FDRE #(
    .INIT ( 1'b0 ))
  \blk00000001/blk000000bd  (
    .C(clk),
    .CE(ce),
    .D(\blk00000001/sig000000d0 ),
    .R(sclr),
    .Q(\blk00000001/sig000000fa )
  );
  FDRE #(
    .INIT ( 1'b0 ))
  \blk00000001/blk000000bc  (
    .C(clk),
    .CE(ce),
    .D(\blk00000001/sig000000d1 ),
    .R(sclr),
    .Q(\blk00000001/sig000000fb )
  );
  FDRE #(
    .INIT ( 1'b0 ))
  \blk00000001/blk000000bb  (
    .C(clk),
    .CE(ce),
    .D(\blk00000001/sig000000d2 ),
    .R(sclr),
    .Q(\blk00000001/sig000000fc )
  );
  FDRE #(
    .INIT ( 1'b0 ))
  \blk00000001/blk000000ba  (
    .C(clk),
    .CE(ce),
    .D(\blk00000001/sig000000d3 ),
    .R(sclr),
    .Q(\blk00000001/sig000000fd )
  );
  FDRE #(
    .INIT ( 1'b0 ))
  \blk00000001/blk000000b9  (
    .C(clk),
    .CE(ce),
    .D(\blk00000001/sig000000d4 ),
    .R(sclr),
    .Q(\blk00000001/sig000000fe )
  );
  FDRE #(
    .INIT ( 1'b0 ))
  \blk00000001/blk000000b8  (
    .C(clk),
    .CE(ce),
    .D(\blk00000001/sig000000d5 ),
    .R(sclr),
    .Q(\blk00000001/sig000000ff )
  );
  FDRE #(
    .INIT ( 1'b0 ))
  \blk00000001/blk000000b7  (
    .C(clk),
    .CE(ce),
    .D(\blk00000001/sig000000b7 ),
    .R(sclr),
    .Q(\blk00000001/sig000000f0 )
  );
  FDRE #(
    .INIT ( 1'b0 ))
  \blk00000001/blk000000b6  (
    .C(clk),
    .CE(ce),
    .D(\blk00000001/sig000000b8 ),
    .R(sclr),
    .Q(\blk00000001/sig000000f1 )
  );
  FDRE #(
    .INIT ( 1'b0 ))
  \blk00000001/blk000000b5  (
    .C(clk),
    .CE(ce),
    .D(\blk00000001/sig000000b9 ),
    .R(sclr),
    .Q(\blk00000001/sig000000f2 )
  );
  FDRE #(
    .INIT ( 1'b0 ))
  \blk00000001/blk000000b4  (
    .C(clk),
    .CE(ce),
    .D(\blk00000001/sig000000ba ),
    .R(sclr),
    .Q(\blk00000001/sig000000f3 )
  );
  FDRE #(
    .INIT ( 1'b0 ))
  \blk00000001/blk000000b3  (
    .C(clk),
    .CE(ce),
    .D(\blk00000001/sig000000bb ),
    .R(sclr),
    .Q(\blk00000001/sig000000f4 )
  );
  FDRE #(
    .INIT ( 1'b0 ))
  \blk00000001/blk000000b2  (
    .C(clk),
    .CE(ce),
    .D(\blk00000001/sig000000bc ),
    .R(sclr),
    .Q(\blk00000001/sig000000f5 )
  );
  FDRE #(
    .INIT ( 1'b0 ))
  \blk00000001/blk000000b1  (
    .C(clk),
    .CE(ce),
    .D(\blk00000001/sig000000bd ),
    .R(sclr),
    .Q(\blk00000001/sig000000f6 )
  );
  FDRE #(
    .INIT ( 1'b0 ))
  \blk00000001/blk000000b0  (
    .C(clk),
    .CE(ce),
    .D(\blk00000001/sig000000be ),
    .R(sclr),
    .Q(\blk00000001/sig000000f7 )
  );
  FDRE #(
    .INIT ( 1'b0 ))
  \blk00000001/blk000000af  (
    .C(clk),
    .CE(ce),
    .D(\blk00000001/sig00000097 ),
    .R(sclr),
    .Q(\blk00000001/sig000000e8 )
  );
  FDRE #(
    .INIT ( 1'b0 ))
  \blk00000001/blk000000ae  (
    .C(clk),
    .CE(ce),
    .D(\blk00000001/sig00000098 ),
    .R(sclr),
    .Q(\blk00000001/sig000000e9 )
  );
  FDRE #(
    .INIT ( 1'b0 ))
  \blk00000001/blk000000ad  (
    .C(clk),
    .CE(ce),
    .D(\blk00000001/sig00000099 ),
    .R(sclr),
    .Q(\blk00000001/sig000000ea )
  );
  FDRE #(
    .INIT ( 1'b0 ))
  \blk00000001/blk000000ac  (
    .C(clk),
    .CE(ce),
    .D(\blk00000001/sig0000009a ),
    .R(sclr),
    .Q(\blk00000001/sig000000eb )
  );
  FDRE #(
    .INIT ( 1'b0 ))
  \blk00000001/blk000000ab  (
    .C(clk),
    .CE(ce),
    .D(\blk00000001/sig0000009b ),
    .R(sclr),
    .Q(\blk00000001/sig000000ec )
  );
  FDRE #(
    .INIT ( 1'b0 ))
  \blk00000001/blk000000aa  (
    .C(clk),
    .CE(ce),
    .D(\blk00000001/sig0000009c ),
    .R(sclr),
    .Q(\blk00000001/sig000000ed )
  );
  FDRE #(
    .INIT ( 1'b0 ))
  \blk00000001/blk000000a9  (
    .C(clk),
    .CE(ce),
    .D(\blk00000001/sig0000009d ),
    .R(sclr),
    .Q(\blk00000001/sig000000ee )
  );
  FDRE #(
    .INIT ( 1'b0 ))
  \blk00000001/blk000000a8  (
    .C(clk),
    .CE(ce),
    .D(\blk00000001/sig0000009e ),
    .R(sclr),
    .Q(\blk00000001/sig000000ef )
  );
  FDRE #(
    .INIT ( 1'b0 ))
  \blk00000001/blk000000a7  (
    .C(clk),
    .CE(ce),
    .D(\blk00000001/sig0000004c ),
    .R(sclr),
    .Q(\blk00000001/sig0000011f )
  );
  FDRE #(
    .INIT ( 1'b0 ))
  \blk00000001/blk000000a6  (
    .C(clk),
    .CE(ce),
    .D(\blk00000001/sig0000009f ),
    .R(sclr),
    .Q(\blk00000001/sig000000e7 )
  );
  FDRE #(
    .INIT ( 1'b0 ))
  \blk00000001/blk000000a5  (
    .C(clk),
    .CE(ce),
    .D(\blk00000001/sig0000004b ),
    .R(sclr),
    .Q(\blk00000001/sig000000e6 )
  );
  MUXCY   \blk00000001/blk000000a4  (
    .CI(\blk00000001/sig00000043 ),
    .DI(a[16]),
    .S(\blk00000001/sig0000008f ),
    .O(\blk00000001/sig00000087 )
  );
  XORCY   \blk00000001/blk000000a3  (
    .CI(\blk00000001/sig00000043 ),
    .LI(\blk00000001/sig0000008f ),
    .O(\blk00000001/sig000000a0 )
  );
  XORCY   \blk00000001/blk000000a2  (
    .CI(\blk00000001/sig0000008d ),
    .LI(\blk00000001/sig00000096 ),
    .O(\blk00000001/sig000000a7 )
  );
  MUXCY   \blk00000001/blk000000a1  (
    .CI(\blk00000001/sig0000008d ),
    .DI(a[23]),
    .S(\blk00000001/sig00000096 ),
    .O(\blk00000001/sig0000008e )
  );
  MUXCY   \blk00000001/blk000000a0  (
    .CI(\blk00000001/sig00000087 ),
    .DI(a[17]),
    .S(\blk00000001/sig00000090 ),
    .O(\blk00000001/sig00000088 )
  );
  XORCY   \blk00000001/blk0000009f  (
    .CI(\blk00000001/sig00000087 ),
    .LI(\blk00000001/sig00000090 ),
    .O(\blk00000001/sig000000a1 )
  );
  MUXCY   \blk00000001/blk0000009e  (
    .CI(\blk00000001/sig00000088 ),
    .DI(a[18]),
    .S(\blk00000001/sig00000091 ),
    .O(\blk00000001/sig00000089 )
  );
  XORCY   \blk00000001/blk0000009d  (
    .CI(\blk00000001/sig00000088 ),
    .LI(\blk00000001/sig00000091 ),
    .O(\blk00000001/sig000000a2 )
  );
  MUXCY   \blk00000001/blk0000009c  (
    .CI(\blk00000001/sig00000089 ),
    .DI(a[19]),
    .S(\blk00000001/sig00000092 ),
    .O(\blk00000001/sig0000008a )
  );
  XORCY   \blk00000001/blk0000009b  (
    .CI(\blk00000001/sig00000089 ),
    .LI(\blk00000001/sig00000092 ),
    .O(\blk00000001/sig000000a3 )
  );
  MUXCY   \blk00000001/blk0000009a  (
    .CI(\blk00000001/sig0000008a ),
    .DI(a[20]),
    .S(\blk00000001/sig00000093 ),
    .O(\blk00000001/sig0000008b )
  );
  XORCY   \blk00000001/blk00000099  (
    .CI(\blk00000001/sig0000008a ),
    .LI(\blk00000001/sig00000093 ),
    .O(\blk00000001/sig000000a4 )
  );
  MUXCY   \blk00000001/blk00000098  (
    .CI(\blk00000001/sig0000008b ),
    .DI(a[21]),
    .S(\blk00000001/sig00000094 ),
    .O(\blk00000001/sig0000008c )
  );
  XORCY   \blk00000001/blk00000097  (
    .CI(\blk00000001/sig0000008b ),
    .LI(\blk00000001/sig00000094 ),
    .O(\blk00000001/sig000000a5 )
  );
  MUXCY   \blk00000001/blk00000096  (
    .CI(\blk00000001/sig0000008c ),
    .DI(a[22]),
    .S(\blk00000001/sig00000095 ),
    .O(\blk00000001/sig0000008d )
  );
  XORCY   \blk00000001/blk00000095  (
    .CI(\blk00000001/sig0000008c ),
    .LI(\blk00000001/sig00000095 ),
    .O(\blk00000001/sig000000a6 )
  );
  FDRE #(
    .INIT ( 1'b0 ))
  \blk00000001/blk00000094  (
    .C(clk),
    .CE(ce),
    .D(\blk00000001/sig0000008e ),
    .R(sclr),
    .Q(\blk00000001/sig0000009f )
  );
  FDRE #(
    .INIT ( 1'b0 ))
  \blk00000001/blk00000093  (
    .C(clk),
    .CE(ce),
    .D(\blk00000001/sig000000a0 ),
    .R(sclr),
    .Q(\blk00000001/sig00000097 )
  );
  FDRE #(
    .INIT ( 1'b0 ))
  \blk00000001/blk00000092  (
    .C(clk),
    .CE(ce),
    .D(\blk00000001/sig000000a1 ),
    .R(sclr),
    .Q(\blk00000001/sig00000098 )
  );
  FDRE #(
    .INIT ( 1'b0 ))
  \blk00000001/blk00000091  (
    .C(clk),
    .CE(ce),
    .D(\blk00000001/sig000000a2 ),
    .R(sclr),
    .Q(\blk00000001/sig00000099 )
  );
  FDRE #(
    .INIT ( 1'b0 ))
  \blk00000001/blk00000090  (
    .C(clk),
    .CE(ce),
    .D(\blk00000001/sig000000a3 ),
    .R(sclr),
    .Q(\blk00000001/sig0000009a )
  );
  FDRE #(
    .INIT ( 1'b0 ))
  \blk00000001/blk0000008f  (
    .C(clk),
    .CE(ce),
    .D(\blk00000001/sig000000a4 ),
    .R(sclr),
    .Q(\blk00000001/sig0000009b )
  );
  FDRE #(
    .INIT ( 1'b0 ))
  \blk00000001/blk0000008e  (
    .C(clk),
    .CE(ce),
    .D(\blk00000001/sig000000a5 ),
    .R(sclr),
    .Q(\blk00000001/sig0000009c )
  );
  FDRE #(
    .INIT ( 1'b0 ))
  \blk00000001/blk0000008d  (
    .C(clk),
    .CE(ce),
    .D(\blk00000001/sig000000a6 ),
    .R(sclr),
    .Q(\blk00000001/sig0000009d )
  );
  FDRE #(
    .INIT ( 1'b0 ))
  \blk00000001/blk0000008c  (
    .C(clk),
    .CE(ce),
    .D(\blk00000001/sig000000a7 ),
    .R(sclr),
    .Q(\blk00000001/sig0000009e )
  );
  MUXCY   \blk00000001/blk0000008b  (
    .CI(\blk00000001/sig00000043 ),
    .DI(a[8]),
    .S(\blk00000001/sig0000006e ),
    .O(\blk00000001/sig00000066 )
  );
  XORCY   \blk00000001/blk0000008a  (
    .CI(\blk00000001/sig00000043 ),
    .LI(\blk00000001/sig0000006e ),
    .O(\blk00000001/sig0000007f )
  );
  XORCY   \blk00000001/blk00000089  (
    .CI(\blk00000001/sig0000006c ),
    .LI(\blk00000001/sig00000075 ),
    .O(\blk00000001/sig00000086 )
  );
  MUXCY   \blk00000001/blk00000088  (
    .CI(\blk00000001/sig0000006c ),
    .DI(a[15]),
    .S(\blk00000001/sig00000075 ),
    .O(\blk00000001/sig0000006d )
  );
  MUXCY   \blk00000001/blk00000087  (
    .CI(\blk00000001/sig00000066 ),
    .DI(a[9]),
    .S(\blk00000001/sig0000006f ),
    .O(\blk00000001/sig00000067 )
  );
  XORCY   \blk00000001/blk00000086  (
    .CI(\blk00000001/sig00000066 ),
    .LI(\blk00000001/sig0000006f ),
    .O(\blk00000001/sig00000080 )
  );
  MUXCY   \blk00000001/blk00000085  (
    .CI(\blk00000001/sig00000067 ),
    .DI(a[10]),
    .S(\blk00000001/sig00000070 ),
    .O(\blk00000001/sig00000068 )
  );
  XORCY   \blk00000001/blk00000084  (
    .CI(\blk00000001/sig00000067 ),
    .LI(\blk00000001/sig00000070 ),
    .O(\blk00000001/sig00000081 )
  );
  MUXCY   \blk00000001/blk00000083  (
    .CI(\blk00000001/sig00000068 ),
    .DI(a[11]),
    .S(\blk00000001/sig00000071 ),
    .O(\blk00000001/sig00000069 )
  );
  XORCY   \blk00000001/blk00000082  (
    .CI(\blk00000001/sig00000068 ),
    .LI(\blk00000001/sig00000071 ),
    .O(\blk00000001/sig00000082 )
  );
  MUXCY   \blk00000001/blk00000081  (
    .CI(\blk00000001/sig00000069 ),
    .DI(a[12]),
    .S(\blk00000001/sig00000072 ),
    .O(\blk00000001/sig0000006a )
  );
  XORCY   \blk00000001/blk00000080  (
    .CI(\blk00000001/sig00000069 ),
    .LI(\blk00000001/sig00000072 ),
    .O(\blk00000001/sig00000083 )
  );
  MUXCY   \blk00000001/blk0000007f  (
    .CI(\blk00000001/sig0000006a ),
    .DI(a[13]),
    .S(\blk00000001/sig00000073 ),
    .O(\blk00000001/sig0000006b )
  );
  XORCY   \blk00000001/blk0000007e  (
    .CI(\blk00000001/sig0000006a ),
    .LI(\blk00000001/sig00000073 ),
    .O(\blk00000001/sig00000084 )
  );
  MUXCY   \blk00000001/blk0000007d  (
    .CI(\blk00000001/sig0000006b ),
    .DI(a[14]),
    .S(\blk00000001/sig00000074 ),
    .O(\blk00000001/sig0000006c )
  );
  XORCY   \blk00000001/blk0000007c  (
    .CI(\blk00000001/sig0000006b ),
    .LI(\blk00000001/sig00000074 ),
    .O(\blk00000001/sig00000085 )
  );
  FDRE #(
    .INIT ( 1'b0 ))
  \blk00000001/blk0000007b  (
    .C(clk),
    .CE(ce),
    .D(\blk00000001/sig0000006d ),
    .R(sclr),
    .Q(\blk00000001/sig0000007e )
  );
  FDRE #(
    .INIT ( 1'b0 ))
  \blk00000001/blk0000007a  (
    .C(clk),
    .CE(ce),
    .D(\blk00000001/sig0000007f ),
    .R(sclr),
    .Q(\blk00000001/sig00000076 )
  );
  FDRE #(
    .INIT ( 1'b0 ))
  \blk00000001/blk00000079  (
    .C(clk),
    .CE(ce),
    .D(\blk00000001/sig00000080 ),
    .R(sclr),
    .Q(\blk00000001/sig00000077 )
  );
  FDRE #(
    .INIT ( 1'b0 ))
  \blk00000001/blk00000078  (
    .C(clk),
    .CE(ce),
    .D(\blk00000001/sig00000081 ),
    .R(sclr),
    .Q(\blk00000001/sig00000078 )
  );
  FDRE #(
    .INIT ( 1'b0 ))
  \blk00000001/blk00000077  (
    .C(clk),
    .CE(ce),
    .D(\blk00000001/sig00000082 ),
    .R(sclr),
    .Q(\blk00000001/sig00000079 )
  );
  FDRE #(
    .INIT ( 1'b0 ))
  \blk00000001/blk00000076  (
    .C(clk),
    .CE(ce),
    .D(\blk00000001/sig00000083 ),
    .R(sclr),
    .Q(\blk00000001/sig0000007a )
  );
  FDRE #(
    .INIT ( 1'b0 ))
  \blk00000001/blk00000075  (
    .C(clk),
    .CE(ce),
    .D(\blk00000001/sig00000084 ),
    .R(sclr),
    .Q(\blk00000001/sig0000007b )
  );
  FDRE #(
    .INIT ( 1'b0 ))
  \blk00000001/blk00000074  (
    .C(clk),
    .CE(ce),
    .D(\blk00000001/sig00000085 ),
    .R(sclr),
    .Q(\blk00000001/sig0000007c )
  );
  FDRE #(
    .INIT ( 1'b0 ))
  \blk00000001/blk00000073  (
    .C(clk),
    .CE(ce),
    .D(\blk00000001/sig00000086 ),
    .R(sclr),
    .Q(\blk00000001/sig0000007d )
  );
  MUXCY   \blk00000001/blk00000072  (
    .CI(\blk00000001/sig00000043 ),
    .DI(a[0]),
    .S(\blk00000001/sig00000055 ),
    .O(\blk00000001/sig0000004d )
  );
  XORCY   \blk00000001/blk00000071  (
    .CI(\blk00000001/sig00000043 ),
    .LI(\blk00000001/sig00000055 ),
    .O(\blk00000001/sig0000005e )
  );
  XORCY   \blk00000001/blk00000070  (
    .CI(\blk00000001/sig00000053 ),
    .LI(\blk00000001/sig0000005c ),
    .O(\blk00000001/sig00000065 )
  );
  MUXCY   \blk00000001/blk0000006f  (
    .CI(\blk00000001/sig00000053 ),
    .DI(a[7]),
    .S(\blk00000001/sig0000005c ),
    .O(\blk00000001/sig00000054 )
  );
  MUXCY   \blk00000001/blk0000006e  (
    .CI(\blk00000001/sig0000004d ),
    .DI(a[1]),
    .S(\blk00000001/sig00000056 ),
    .O(\blk00000001/sig0000004e )
  );
  XORCY   \blk00000001/blk0000006d  (
    .CI(\blk00000001/sig0000004d ),
    .LI(\blk00000001/sig00000056 ),
    .O(\blk00000001/sig0000005f )
  );
  MUXCY   \blk00000001/blk0000006c  (
    .CI(\blk00000001/sig0000004e ),
    .DI(a[2]),
    .S(\blk00000001/sig00000057 ),
    .O(\blk00000001/sig0000004f )
  );
  XORCY   \blk00000001/blk0000006b  (
    .CI(\blk00000001/sig0000004e ),
    .LI(\blk00000001/sig00000057 ),
    .O(\blk00000001/sig00000060 )
  );
  MUXCY   \blk00000001/blk0000006a  (
    .CI(\blk00000001/sig0000004f ),
    .DI(a[3]),
    .S(\blk00000001/sig00000058 ),
    .O(\blk00000001/sig00000050 )
  );
  XORCY   \blk00000001/blk00000069  (
    .CI(\blk00000001/sig0000004f ),
    .LI(\blk00000001/sig00000058 ),
    .O(\blk00000001/sig00000061 )
  );
  MUXCY   \blk00000001/blk00000068  (
    .CI(\blk00000001/sig00000050 ),
    .DI(a[4]),
    .S(\blk00000001/sig00000059 ),
    .O(\blk00000001/sig00000051 )
  );
  XORCY   \blk00000001/blk00000067  (
    .CI(\blk00000001/sig00000050 ),
    .LI(\blk00000001/sig00000059 ),
    .O(\blk00000001/sig00000062 )
  );
  MUXCY   \blk00000001/blk00000066  (
    .CI(\blk00000001/sig00000051 ),
    .DI(a[5]),
    .S(\blk00000001/sig0000005a ),
    .O(\blk00000001/sig00000052 )
  );
  XORCY   \blk00000001/blk00000065  (
    .CI(\blk00000001/sig00000051 ),
    .LI(\blk00000001/sig0000005a ),
    .O(\blk00000001/sig00000063 )
  );
  MUXCY   \blk00000001/blk00000064  (
    .CI(\blk00000001/sig00000052 ),
    .DI(a[6]),
    .S(\blk00000001/sig0000005b ),
    .O(\blk00000001/sig00000053 )
  );
  XORCY   \blk00000001/blk00000063  (
    .CI(\blk00000001/sig00000052 ),
    .LI(\blk00000001/sig0000005b ),
    .O(\blk00000001/sig00000064 )
  );
  FDRE #(
    .INIT ( 1'b0 ))
  \blk00000001/blk00000062  (
    .C(clk),
    .CE(ce),
    .D(\blk00000001/sig00000054 ),
    .R(sclr),
    .Q(\blk00000001/sig0000005d )
  );
  MUXCY   \blk00000001/blk00000061  (
    .CI(\blk00000001/sig00000043 ),
    .DI(a[24]),
    .S(\blk00000001/sig000000af ),
    .O(\blk00000001/sig000000a8 )
  );
  XORCY   \blk00000001/blk00000060  (
    .CI(\blk00000001/sig00000043 ),
    .LI(\blk00000001/sig000000af ),
    .O(\blk00000001/sig000000bf )
  );
  XORCY   \blk00000001/blk0000005f  (
    .CI(\blk00000001/sig000000ae ),
    .LI(\blk00000001/sig000000b6 ),
    .O(\blk00000001/sig000000c6 )
  );
  MUXCY   \blk00000001/blk0000005e  (
    .CI(\blk00000001/sig000000a8 ),
    .DI(a[25]),
    .S(\blk00000001/sig000000b0 ),
    .O(\blk00000001/sig000000a9 )
  );
  XORCY   \blk00000001/blk0000005d  (
    .CI(\blk00000001/sig000000a8 ),
    .LI(\blk00000001/sig000000b0 ),
    .O(\blk00000001/sig000000c0 )
  );
  MUXCY   \blk00000001/blk0000005c  (
    .CI(\blk00000001/sig000000a9 ),
    .DI(a[26]),
    .S(\blk00000001/sig000000b1 ),
    .O(\blk00000001/sig000000aa )
  );
  XORCY   \blk00000001/blk0000005b  (
    .CI(\blk00000001/sig000000a9 ),
    .LI(\blk00000001/sig000000b1 ),
    .O(\blk00000001/sig000000c1 )
  );
  MUXCY   \blk00000001/blk0000005a  (
    .CI(\blk00000001/sig000000aa ),
    .DI(a[27]),
    .S(\blk00000001/sig000000b2 ),
    .O(\blk00000001/sig000000ab )
  );
  XORCY   \blk00000001/blk00000059  (
    .CI(\blk00000001/sig000000aa ),
    .LI(\blk00000001/sig000000b2 ),
    .O(\blk00000001/sig000000c2 )
  );
  MUXCY   \blk00000001/blk00000058  (
    .CI(\blk00000001/sig000000ab ),
    .DI(a[28]),
    .S(\blk00000001/sig000000b3 ),
    .O(\blk00000001/sig000000ac )
  );
  XORCY   \blk00000001/blk00000057  (
    .CI(\blk00000001/sig000000ab ),
    .LI(\blk00000001/sig000000b3 ),
    .O(\blk00000001/sig000000c3 )
  );
  MUXCY   \blk00000001/blk00000056  (
    .CI(\blk00000001/sig000000ac ),
    .DI(a[29]),
    .S(\blk00000001/sig000000b4 ),
    .O(\blk00000001/sig000000ad )
  );
  XORCY   \blk00000001/blk00000055  (
    .CI(\blk00000001/sig000000ac ),
    .LI(\blk00000001/sig000000b4 ),
    .O(\blk00000001/sig000000c4 )
  );
  MUXCY   \blk00000001/blk00000054  (
    .CI(\blk00000001/sig000000ad ),
    .DI(a[30]),
    .S(\blk00000001/sig000000b5 ),
    .O(\blk00000001/sig000000ae )
  );
  XORCY   \blk00000001/blk00000053  (
    .CI(\blk00000001/sig000000ad ),
    .LI(\blk00000001/sig000000b5 ),
    .O(\blk00000001/sig000000c5 )
  );
  FDRE #(
    .INIT ( 1'b0 ))
  \blk00000001/blk00000052  (
    .C(clk),
    .CE(ce),
    .D(\blk00000001/sig000000bf ),
    .R(sclr),
    .Q(\blk00000001/sig000000b7 )
  );
  FDRE #(
    .INIT ( 1'b0 ))
  \blk00000001/blk00000051  (
    .C(clk),
    .CE(ce),
    .D(\blk00000001/sig000000c0 ),
    .R(sclr),
    .Q(\blk00000001/sig000000b8 )
  );
  FDRE #(
    .INIT ( 1'b0 ))
  \blk00000001/blk00000050  (
    .C(clk),
    .CE(ce),
    .D(\blk00000001/sig000000c1 ),
    .R(sclr),
    .Q(\blk00000001/sig000000b9 )
  );
  FDRE #(
    .INIT ( 1'b0 ))
  \blk00000001/blk0000004f  (
    .C(clk),
    .CE(ce),
    .D(\blk00000001/sig000000c2 ),
    .R(sclr),
    .Q(\blk00000001/sig000000ba )
  );
  FDRE #(
    .INIT ( 1'b0 ))
  \blk00000001/blk0000004e  (
    .C(clk),
    .CE(ce),
    .D(\blk00000001/sig000000c3 ),
    .R(sclr),
    .Q(\blk00000001/sig000000bb )
  );
  FDRE #(
    .INIT ( 1'b0 ))
  \blk00000001/blk0000004d  (
    .C(clk),
    .CE(ce),
    .D(\blk00000001/sig000000c4 ),
    .R(sclr),
    .Q(\blk00000001/sig000000bc )
  );
  FDRE #(
    .INIT ( 1'b0 ))
  \blk00000001/blk0000004c  (
    .C(clk),
    .CE(ce),
    .D(\blk00000001/sig000000c5 ),
    .R(sclr),
    .Q(\blk00000001/sig000000bd )
  );
  FDRE #(
    .INIT ( 1'b0 ))
  \blk00000001/blk0000004b  (
    .C(clk),
    .CE(ce),
    .D(\blk00000001/sig000000c6 ),
    .R(sclr),
    .Q(\blk00000001/sig000000be )
  );
  MUXCY   \blk00000001/blk0000004a  (
    .CI(\blk00000001/sig0000011f ),
    .DI(\blk00000001/sig00000043 ),
    .S(\blk00000001/sig0000016e ),
    .O(\blk00000001/sig00000158 )
  );
  XORCY   \blk00000001/blk00000049  (
    .CI(\blk00000001/sig0000011f ),
    .LI(\blk00000001/sig0000016e ),
    .O(\blk00000001/sig0000016f )
  );
  XORCY   \blk00000001/blk00000048  (
    .CI(\blk00000001/sig0000015e ),
    .LI(\blk00000001/sig0000016d ),
    .O(\blk00000001/sig00000176 )
  );
  MUXCY   \blk00000001/blk00000047  (
    .CI(\blk00000001/sig0000015e ),
    .DI(\blk00000001/sig00000043 ),
    .S(\blk00000001/sig0000016d ),
    .O(\NLW_blk00000001/blk00000047_O_UNCONNECTED )
  );
  MUXCY   \blk00000001/blk00000046  (
    .CI(\blk00000001/sig00000158 ),
    .DI(\blk00000001/sig00000043 ),
    .S(\blk00000001/sig00000167 ),
    .O(\blk00000001/sig00000159 )
  );
  XORCY   \blk00000001/blk00000045  (
    .CI(\blk00000001/sig00000158 ),
    .LI(\blk00000001/sig00000167 ),
    .O(\blk00000001/sig00000170 )
  );
  MUXCY   \blk00000001/blk00000044  (
    .CI(\blk00000001/sig00000159 ),
    .DI(\blk00000001/sig00000043 ),
    .S(\blk00000001/sig00000168 ),
    .O(\blk00000001/sig0000015a )
  );
  XORCY   \blk00000001/blk00000043  (
    .CI(\blk00000001/sig00000159 ),
    .LI(\blk00000001/sig00000168 ),
    .O(\blk00000001/sig00000171 )
  );
  MUXCY   \blk00000001/blk00000042  (
    .CI(\blk00000001/sig0000015a ),
    .DI(\blk00000001/sig00000043 ),
    .S(\blk00000001/sig00000169 ),
    .O(\blk00000001/sig0000015b )
  );
  XORCY   \blk00000001/blk00000041  (
    .CI(\blk00000001/sig0000015a ),
    .LI(\blk00000001/sig00000169 ),
    .O(\blk00000001/sig00000172 )
  );
  MUXCY   \blk00000001/blk00000040  (
    .CI(\blk00000001/sig0000015b ),
    .DI(\blk00000001/sig00000043 ),
    .S(\blk00000001/sig0000016a ),
    .O(\blk00000001/sig0000015c )
  );
  XORCY   \blk00000001/blk0000003f  (
    .CI(\blk00000001/sig0000015b ),
    .LI(\blk00000001/sig0000016a ),
    .O(\blk00000001/sig00000173 )
  );
  MUXCY   \blk00000001/blk0000003e  (
    .CI(\blk00000001/sig0000015c ),
    .DI(\blk00000001/sig00000043 ),
    .S(\blk00000001/sig0000016b ),
    .O(\blk00000001/sig0000015d )
  );
  XORCY   \blk00000001/blk0000003d  (
    .CI(\blk00000001/sig0000015c ),
    .LI(\blk00000001/sig0000016b ),
    .O(\blk00000001/sig00000174 )
  );
  MUXCY   \blk00000001/blk0000003c  (
    .CI(\blk00000001/sig0000015d ),
    .DI(\blk00000001/sig00000043 ),
    .S(\blk00000001/sig0000016c ),
    .O(\blk00000001/sig0000015e )
  );
  XORCY   \blk00000001/blk0000003b  (
    .CI(\blk00000001/sig0000015d ),
    .LI(\blk00000001/sig0000016c ),
    .O(\blk00000001/sig00000175 )
  );
  FDRE #(
    .INIT ( 1'b0 ))
  \blk00000001/blk0000003a  (
    .C(clk),
    .CE(ce),
    .D(\blk00000001/sig0000016f ),
    .R(sclr),
    .Q(s[24])
  );
  FDRE #(
    .INIT ( 1'b0 ))
  \blk00000001/blk00000039  (
    .C(clk),
    .CE(ce),
    .D(\blk00000001/sig00000170 ),
    .R(sclr),
    .Q(s[25])
  );
  FDRE #(
    .INIT ( 1'b0 ))
  \blk00000001/blk00000038  (
    .C(clk),
    .CE(ce),
    .D(\blk00000001/sig00000171 ),
    .R(sclr),
    .Q(s[26])
  );
  FDRE #(
    .INIT ( 1'b0 ))
  \blk00000001/blk00000037  (
    .C(clk),
    .CE(ce),
    .D(\blk00000001/sig00000172 ),
    .R(sclr),
    .Q(s[27])
  );
  FDRE #(
    .INIT ( 1'b0 ))
  \blk00000001/blk00000036  (
    .C(clk),
    .CE(ce),
    .D(\blk00000001/sig00000173 ),
    .R(sclr),
    .Q(s[28])
  );
  FDRE #(
    .INIT ( 1'b0 ))
  \blk00000001/blk00000035  (
    .C(clk),
    .CE(ce),
    .D(\blk00000001/sig00000174 ),
    .R(sclr),
    .Q(s[29])
  );
  FDRE #(
    .INIT ( 1'b0 ))
  \blk00000001/blk00000034  (
    .C(clk),
    .CE(ce),
    .D(\blk00000001/sig00000175 ),
    .R(sclr),
    .Q(s[30])
  );
  FDRE #(
    .INIT ( 1'b0 ))
  \blk00000001/blk00000033  (
    .C(clk),
    .CE(ce),
    .D(\blk00000001/sig00000176 ),
    .R(sclr),
    .Q(s[31])
  );
  MUXCY   \blk00000001/blk00000032  (
    .CI(\blk00000001/sig000000e6 ),
    .DI(\blk00000001/sig00000043 ),
    .S(\blk00000001/sig00000116 ),
    .O(\blk00000001/sig00000100 )
  );
  XORCY   \blk00000001/blk00000031  (
    .CI(\blk00000001/sig000000e6 ),
    .LI(\blk00000001/sig00000116 ),
    .O(\blk00000001/sig00000117 )
  );
  XORCY   \blk00000001/blk00000030  (
    .CI(\blk00000001/sig00000106 ),
    .LI(\blk00000001/sig00000115 ),
    .O(\blk00000001/sig0000011e )
  );
  MUXCY   \blk00000001/blk0000002f  (
    .CI(\blk00000001/sig00000106 ),
    .DI(\blk00000001/sig00000043 ),
    .S(\blk00000001/sig00000115 ),
    .O(\blk00000001/sig0000004a )
  );
  MUXCY   \blk00000001/blk0000002e  (
    .CI(\blk00000001/sig00000100 ),
    .DI(\blk00000001/sig00000043 ),
    .S(\blk00000001/sig0000010f ),
    .O(\blk00000001/sig00000101 )
  );
  XORCY   \blk00000001/blk0000002d  (
    .CI(\blk00000001/sig00000100 ),
    .LI(\blk00000001/sig0000010f ),
    .O(\blk00000001/sig00000118 )
  );
  MUXCY   \blk00000001/blk0000002c  (
    .CI(\blk00000001/sig00000101 ),
    .DI(\blk00000001/sig00000043 ),
    .S(\blk00000001/sig00000110 ),
    .O(\blk00000001/sig00000102 )
  );
  XORCY   \blk00000001/blk0000002b  (
    .CI(\blk00000001/sig00000101 ),
    .LI(\blk00000001/sig00000110 ),
    .O(\blk00000001/sig00000119 )
  );
  MUXCY   \blk00000001/blk0000002a  (
    .CI(\blk00000001/sig00000102 ),
    .DI(\blk00000001/sig00000043 ),
    .S(\blk00000001/sig00000111 ),
    .O(\blk00000001/sig00000103 )
  );
  XORCY   \blk00000001/blk00000029  (
    .CI(\blk00000001/sig00000102 ),
    .LI(\blk00000001/sig00000111 ),
    .O(\blk00000001/sig0000011a )
  );
  MUXCY   \blk00000001/blk00000028  (
    .CI(\blk00000001/sig00000103 ),
    .DI(\blk00000001/sig00000043 ),
    .S(\blk00000001/sig00000112 ),
    .O(\blk00000001/sig00000104 )
  );
  XORCY   \blk00000001/blk00000027  (
    .CI(\blk00000001/sig00000103 ),
    .LI(\blk00000001/sig00000112 ),
    .O(\blk00000001/sig0000011b )
  );
  MUXCY   \blk00000001/blk00000026  (
    .CI(\blk00000001/sig00000104 ),
    .DI(\blk00000001/sig00000043 ),
    .S(\blk00000001/sig00000113 ),
    .O(\blk00000001/sig00000105 )
  );
  XORCY   \blk00000001/blk00000025  (
    .CI(\blk00000001/sig00000104 ),
    .LI(\blk00000001/sig00000113 ),
    .O(\blk00000001/sig0000011c )
  );
  MUXCY   \blk00000001/blk00000024  (
    .CI(\blk00000001/sig00000105 ),
    .DI(\blk00000001/sig00000043 ),
    .S(\blk00000001/sig00000114 ),
    .O(\blk00000001/sig00000106 )
  );
  XORCY   \blk00000001/blk00000023  (
    .CI(\blk00000001/sig00000105 ),
    .LI(\blk00000001/sig00000114 ),
    .O(\blk00000001/sig0000011d )
  );
  FDRE #(
    .INIT ( 1'b0 ))
  \blk00000001/blk00000022  (
    .C(clk),
    .CE(ce),
    .D(\blk00000001/sig00000117 ),
    .R(sclr),
    .Q(\blk00000001/sig00000107 )
  );
  FDRE #(
    .INIT ( 1'b0 ))
  \blk00000001/blk00000021  (
    .C(clk),
    .CE(ce),
    .D(\blk00000001/sig00000118 ),
    .R(sclr),
    .Q(\blk00000001/sig00000108 )
  );
  FDRE #(
    .INIT ( 1'b0 ))
  \blk00000001/blk00000020  (
    .C(clk),
    .CE(ce),
    .D(\blk00000001/sig00000119 ),
    .R(sclr),
    .Q(\blk00000001/sig00000109 )
  );
  FDRE #(
    .INIT ( 1'b0 ))
  \blk00000001/blk0000001f  (
    .C(clk),
    .CE(ce),
    .D(\blk00000001/sig0000011a ),
    .R(sclr),
    .Q(\blk00000001/sig0000010a )
  );
  FDRE #(
    .INIT ( 1'b0 ))
  \blk00000001/blk0000001e  (
    .C(clk),
    .CE(ce),
    .D(\blk00000001/sig0000011b ),
    .R(sclr),
    .Q(\blk00000001/sig0000010b )
  );
  FDRE #(
    .INIT ( 1'b0 ))
  \blk00000001/blk0000001d  (
    .C(clk),
    .CE(ce),
    .D(\blk00000001/sig0000011c ),
    .R(sclr),
    .Q(\blk00000001/sig0000010c )
  );
  FDRE #(
    .INIT ( 1'b0 ))
  \blk00000001/blk0000001c  (
    .C(clk),
    .CE(ce),
    .D(\blk00000001/sig0000011d ),
    .R(sclr),
    .Q(\blk00000001/sig0000010d )
  );
  FDRE #(
    .INIT ( 1'b0 ))
  \blk00000001/blk0000001b  (
    .C(clk),
    .CE(ce),
    .D(\blk00000001/sig0000011e ),
    .R(sclr),
    .Q(\blk00000001/sig0000010e )
  );
  MUXCY   \blk00000001/blk0000001a  (
    .CI(\blk00000001/sig0000005d ),
    .DI(\blk00000001/sig00000043 ),
    .S(\blk00000001/sig000000dd ),
    .O(\blk00000001/sig000000c7 )
  );
  XORCY   \blk00000001/blk00000019  (
    .CI(\blk00000001/sig0000005d ),
    .LI(\blk00000001/sig000000dd ),
    .O(\blk00000001/sig000000de )
  );
  XORCY   \blk00000001/blk00000018  (
    .CI(\blk00000001/sig000000cd ),
    .LI(\blk00000001/sig000000dc ),
    .O(\blk00000001/sig000000e5 )
  );
  MUXCY   \blk00000001/blk00000017  (
    .CI(\blk00000001/sig000000cd ),
    .DI(\blk00000001/sig00000043 ),
    .S(\blk00000001/sig000000dc ),
    .O(\blk00000001/sig00000049 )
  );
  MUXCY   \blk00000001/blk00000016  (
    .CI(\blk00000001/sig000000c7 ),
    .DI(\blk00000001/sig00000043 ),
    .S(\blk00000001/sig000000d6 ),
    .O(\blk00000001/sig000000c8 )
  );
  XORCY   \blk00000001/blk00000015  (
    .CI(\blk00000001/sig000000c7 ),
    .LI(\blk00000001/sig000000d6 ),
    .O(\blk00000001/sig000000df )
  );
  MUXCY   \blk00000001/blk00000014  (
    .CI(\blk00000001/sig000000c8 ),
    .DI(\blk00000001/sig00000043 ),
    .S(\blk00000001/sig000000d7 ),
    .O(\blk00000001/sig000000c9 )
  );
  XORCY   \blk00000001/blk00000013  (
    .CI(\blk00000001/sig000000c8 ),
    .LI(\blk00000001/sig000000d7 ),
    .O(\blk00000001/sig000000e0 )
  );
  MUXCY   \blk00000001/blk00000012  (
    .CI(\blk00000001/sig000000c9 ),
    .DI(\blk00000001/sig00000043 ),
    .S(\blk00000001/sig000000d8 ),
    .O(\blk00000001/sig000000ca )
  );
  XORCY   \blk00000001/blk00000011  (
    .CI(\blk00000001/sig000000c9 ),
    .LI(\blk00000001/sig000000d8 ),
    .O(\blk00000001/sig000000e1 )
  );
  MUXCY   \blk00000001/blk00000010  (
    .CI(\blk00000001/sig000000ca ),
    .DI(\blk00000001/sig00000043 ),
    .S(\blk00000001/sig000000d9 ),
    .O(\blk00000001/sig000000cb )
  );
  XORCY   \blk00000001/blk0000000f  (
    .CI(\blk00000001/sig000000ca ),
    .LI(\blk00000001/sig000000d9 ),
    .O(\blk00000001/sig000000e2 )
  );
  MUXCY   \blk00000001/blk0000000e  (
    .CI(\blk00000001/sig000000cb ),
    .DI(\blk00000001/sig00000043 ),
    .S(\blk00000001/sig000000da ),
    .O(\blk00000001/sig000000cc )
  );
  XORCY   \blk00000001/blk0000000d  (
    .CI(\blk00000001/sig000000cb ),
    .LI(\blk00000001/sig000000da ),
    .O(\blk00000001/sig000000e3 )
  );
  MUXCY   \blk00000001/blk0000000c  (
    .CI(\blk00000001/sig000000cc ),
    .DI(\blk00000001/sig00000043 ),
    .S(\blk00000001/sig000000db ),
    .O(\blk00000001/sig000000cd )
  );
  XORCY   \blk00000001/blk0000000b  (
    .CI(\blk00000001/sig000000cc ),
    .LI(\blk00000001/sig000000db ),
    .O(\blk00000001/sig000000e4 )
  );
  FDRE #(
    .INIT ( 1'b0 ))
  \blk00000001/blk0000000a  (
    .C(clk),
    .CE(ce),
    .D(\blk00000001/sig000000de ),
    .R(sclr),
    .Q(\blk00000001/sig000000ce )
  );
  FDRE #(
    .INIT ( 1'b0 ))
  \blk00000001/blk00000009  (
    .C(clk),
    .CE(ce),
    .D(\blk00000001/sig000000df ),
    .R(sclr),
    .Q(\blk00000001/sig000000cf )
  );
  FDRE #(
    .INIT ( 1'b0 ))
  \blk00000001/blk00000008  (
    .C(clk),
    .CE(ce),
    .D(\blk00000001/sig000000e0 ),
    .R(sclr),
    .Q(\blk00000001/sig000000d0 )
  );
  FDRE #(
    .INIT ( 1'b0 ))
  \blk00000001/blk00000007  (
    .C(clk),
    .CE(ce),
    .D(\blk00000001/sig000000e1 ),
    .R(sclr),
    .Q(\blk00000001/sig000000d1 )
  );
  FDRE #(
    .INIT ( 1'b0 ))
  \blk00000001/blk00000006  (
    .C(clk),
    .CE(ce),
    .D(\blk00000001/sig000000e2 ),
    .R(sclr),
    .Q(\blk00000001/sig000000d2 )
  );
  FDRE #(
    .INIT ( 1'b0 ))
  \blk00000001/blk00000005  (
    .C(clk),
    .CE(ce),
    .D(\blk00000001/sig000000e3 ),
    .R(sclr),
    .Q(\blk00000001/sig000000d3 )
  );
  FDRE #(
    .INIT ( 1'b0 ))
  \blk00000001/blk00000004  (
    .C(clk),
    .CE(ce),
    .D(\blk00000001/sig000000e4 ),
    .R(sclr),
    .Q(\blk00000001/sig000000d4 )
  );
  FDRE #(
    .INIT ( 1'b0 ))
  \blk00000001/blk00000003  (
    .C(clk),
    .CE(ce),
    .D(\blk00000001/sig000000e5 ),
    .R(sclr),
    .Q(\blk00000001/sig000000d5 )
  );
  GND   \blk00000001/blk00000002  (
    .G(\blk00000001/sig00000043 )
  );

// synthesis translate_on

endmodule

// synthesis translate_off

`ifndef GLBL
`define GLBL

`timescale  1 ps / 1 ps

module glbl ();

    parameter ROC_WIDTH = 100000;
    parameter TOC_WIDTH = 0;

//--------   STARTUP Globals --------------
    wire GSR;
    wire GTS;
    wire GWE;
    wire PRLD;
    tri1 p_up_tmp;
    tri (weak1, strong0) PLL_LOCKG = p_up_tmp;

    wire PROGB_GLBL;
    wire CCLKO_GLBL;

    reg GSR_int;
    reg GTS_int;
    reg PRLD_int;

//--------   JTAG Globals --------------
    wire JTAG_TDO_GLBL;
    wire JTAG_TCK_GLBL;
    wire JTAG_TDI_GLBL;
    wire JTAG_TMS_GLBL;
    wire JTAG_TRST_GLBL;

    reg JTAG_CAPTURE_GLBL;
    reg JTAG_RESET_GLBL;
    reg JTAG_SHIFT_GLBL;
    reg JTAG_UPDATE_GLBL;
    reg JTAG_RUNTEST_GLBL;

    reg JTAG_SEL1_GLBL = 0;
    reg JTAG_SEL2_GLBL = 0 ;
    reg JTAG_SEL3_GLBL = 0;
    reg JTAG_SEL4_GLBL = 0;

    reg JTAG_USER_TDO1_GLBL = 1'bz;
    reg JTAG_USER_TDO2_GLBL = 1'bz;
    reg JTAG_USER_TDO3_GLBL = 1'bz;
    reg JTAG_USER_TDO4_GLBL = 1'bz;

    assign (weak1, weak0) GSR = GSR_int;
    assign (weak1, weak0) GTS = GTS_int;
    assign (weak1, weak0) PRLD = PRLD_int;

    initial begin
	GSR_int = 1'b1;
	PRLD_int = 1'b1;
	#(ROC_WIDTH)
	GSR_int = 1'b0;
	PRLD_int = 1'b0;
    end

    initial begin
	GTS_int = 1'b1;
	#(TOC_WIDTH)
	GTS_int = 1'b0;
    end

endmodule

`endif

// synthesis translate_on
