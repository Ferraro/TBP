module WAdd_32_1_1_Float(input clk,
    input [31:0] io_A,
    input [31:0] io_B,
    input  io_CE,
    input  io_SCLR,
    output[31:0] io_C,
    output io_RDY,
    output io_OVERFLOW,
    output io_UNDERFLOW
);

  wire logic_unit_io_underflow;
  wire logic_unit_io_overflow;
  wire logic_unit_io_rdy;
  wire[31:0] logic_unit_io_result;


  assign io_UNDERFLOW = logic_unit_io_underflow;
  assign io_OVERFLOW = logic_unit_io_overflow;
  assign io_RDY = logic_unit_io_rdy;
  assign io_C = logic_unit_io_result;
  Add_32_1_1_Float logic_unit(.clk(clk),
       .a( io_A ),
       .b( io_B ),
       .ce( io_CE ),
       .sclr( io_SCLR ),
       .rdy( logic_unit_io_rdy ),
       .overflow( logic_unit_io_overflow ),
       .underflow( logic_unit_io_underflow ),
       .result( logic_unit_io_result )
  );
endmodule
