module WSqt_32_1_1_Float(input clk,
    input [31:0] io_A,
    input [31:0] io_B,
    input  io_CE,
    input  io_SCLR,
    output[31:0] io_C,
    output io_RDY,
    output io_OVERFLOW,
    output io_UNDERFLOW
);

  wire logic_unit_io_invalid_op;
  wire logic_unit_io_rdy;
  wire[31:0] logic_unit_io_result;


  assign io_UNDERFLOW = 1'h0;
  assign io_OVERFLOW = logic_unit_io_invalid_op;
  assign io_RDY = logic_unit_io_rdy;
  assign io_C = logic_unit_io_result;
  Sqt_32_1_1_Float logic_unit(.clk(clk),
       .a( io_A ),
       .ce( io_CE ),
       .sclr( io_SCLR ),
       .result( logic_unit_io_result ),
       .rdy( logic_unit_io_rdy ),
       .invalid_op( logic_unit_io_invalid_op )
  );
endmodule
