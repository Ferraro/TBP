////////////////////////////////////////////////////////////////////////////////
// Copyright (c) 1995-2013 Xilinx, Inc.  All rights reserved.
////////////////////////////////////////////////////////////////////////////////
//   ____  ____
//  /   /\/   /
// /___/  \  /    Vendor: Xilinx
// \   \   \/     Version: P.20131013
//  \   \         Application: netgen
//  /   /         Filename: Sub_64_4_1_UInt.v
// /___/   /\     Timestamp: Tue Aug  5 14:33:23 2014
// \   \  /  \ 
//  \___\/\___\
//             
// Command	: -w -sim -ofmt verilog /home/francesco/Xilinx/Projects/IPCOREs/ipcore_dir/tmp/_cg/Sub_64_4_1_UInt.ngc /home/francesco/Xilinx/Projects/IPCOREs/ipcore_dir/tmp/_cg/Sub_64_4_1_UInt.v 
// Device	: 3sd3400afg676-4
// Input file	: /home/francesco/Xilinx/Projects/IPCOREs/ipcore_dir/tmp/_cg/Sub_64_4_1_UInt.ngc
// Output file	: /home/francesco/Xilinx/Projects/IPCOREs/ipcore_dir/tmp/_cg/Sub_64_4_1_UInt.v
// # of Modules	: 1
// Design Name	: Sub_64_4_1_UInt
// Xilinx        : /home/francesco/Xilinx/14.7/ISE_DS/ISE/
//             
// Purpose:    
//     This verilog netlist is a verification model and uses simulation 
//     primitives which may not represent the true implementation of the 
//     device, however the netlist is functionally correct and should not 
//     be modified. This file cannot be synthesized and should only be used 
//     with supported simulation tools.
//             
// Reference:  
//     Command Line Tools User Guide, Chapter 23 and Synthesis and Simulation Design Guide, Chapter 6
//             
////////////////////////////////////////////////////////////////////////////////

`timescale 1 ns/1 ps

module Sub_64_4_1_UInt (
  clk, ce, sclr, s, a, b
)/* synthesis syn_black_box syn_noprune=1 */;
  input clk;
  input ce;
  input sclr;
  output [63 : 0] s;
  input [63 : 0] a;
  input [63 : 0] b;
  
  // synthesis translate_off
  
  wire \blk00000001/sig000002de ;
  wire \blk00000001/sig000002dd ;
  wire \blk00000001/sig000002dc ;
  wire \blk00000001/sig000002db ;
  wire \blk00000001/sig000002da ;
  wire \blk00000001/sig000002d9 ;
  wire \blk00000001/sig000002d8 ;
  wire \blk00000001/sig000002d7 ;
  wire \blk00000001/sig000002d6 ;
  wire \blk00000001/sig000002d5 ;
  wire \blk00000001/sig000002d4 ;
  wire \blk00000001/sig000002d3 ;
  wire \blk00000001/sig000002d2 ;
  wire \blk00000001/sig000002d1 ;
  wire \blk00000001/sig000002d0 ;
  wire \blk00000001/sig000002cf ;
  wire \blk00000001/sig000002be ;
  wire \blk00000001/sig000002bd ;
  wire \blk00000001/sig000002bc ;
  wire \blk00000001/sig000002bb ;
  wire \blk00000001/sig000002ba ;
  wire \blk00000001/sig000002b9 ;
  wire \blk00000001/sig000002b8 ;
  wire \blk00000001/sig000002b7 ;
  wire \blk00000001/sig000002b6 ;
  wire \blk00000001/sig000002b5 ;
  wire \blk00000001/sig000002b4 ;
  wire \blk00000001/sig000002b3 ;
  wire \blk00000001/sig000002b2 ;
  wire \blk00000001/sig000002b1 ;
  wire \blk00000001/sig000002b0 ;
  wire \blk00000001/sig000002af ;
  wire \blk00000001/sig000002ae ;
  wire \blk00000001/sig000002ad ;
  wire \blk00000001/sig000002ac ;
  wire \blk00000001/sig000002ab ;
  wire \blk00000001/sig000002aa ;
  wire \blk00000001/sig000002a9 ;
  wire \blk00000001/sig000002a8 ;
  wire \blk00000001/sig000002a7 ;
  wire \blk00000001/sig000002a6 ;
  wire \blk00000001/sig000002a5 ;
  wire \blk00000001/sig000002a4 ;
  wire \blk00000001/sig000002a3 ;
  wire \blk00000001/sig000002a2 ;
  wire \blk00000001/sig000002a1 ;
  wire \blk00000001/sig000002a0 ;
  wire \blk00000001/sig0000027f ;
  wire \blk00000001/sig0000027e ;
  wire \blk00000001/sig0000027c ;
  wire \blk00000001/sig0000027b ;
  wire \blk00000001/sig00000279 ;
  wire \blk00000001/sig00000278 ;
  wire \blk00000001/sig00000276 ;
  wire \blk00000001/sig00000275 ;
  wire \blk00000001/sig00000273 ;
  wire \blk00000001/sig00000272 ;
  wire \blk00000001/sig00000270 ;
  wire \blk00000001/sig0000026f ;
  wire \blk00000001/sig0000026d ;
  wire \blk00000001/sig0000026c ;
  wire \blk00000001/sig0000026a ;
  wire \blk00000001/sig00000269 ;
  wire \blk00000001/sig00000267 ;
  wire \blk00000001/sig00000266 ;
  wire \blk00000001/sig00000264 ;
  wire \blk00000001/sig00000263 ;
  wire \blk00000001/sig00000261 ;
  wire \blk00000001/sig00000260 ;
  wire \blk00000001/sig0000025e ;
  wire \blk00000001/sig0000025d ;
  wire \blk00000001/sig0000025b ;
  wire \blk00000001/sig0000025a ;
  wire \blk00000001/sig00000258 ;
  wire \blk00000001/sig00000257 ;
  wire \blk00000001/sig00000256 ;
  wire \blk00000001/sig00000255 ;
  wire \blk00000001/sig00000253 ;
  wire \blk00000001/sig00000252 ;
  wire \blk00000001/sig0000024f ;
  wire \blk00000001/sig0000024e ;
  wire \blk00000001/sig0000024d ;
  wire \blk00000001/sig0000024c ;
  wire \blk00000001/sig0000024b ;
  wire \blk00000001/sig0000024a ;
  wire \blk00000001/sig00000249 ;
  wire \blk00000001/sig00000248 ;
  wire \blk00000001/sig00000247 ;
  wire \blk00000001/sig00000246 ;
  wire \blk00000001/sig00000245 ;
  wire \blk00000001/sig00000244 ;
  wire \blk00000001/sig00000243 ;
  wire \blk00000001/sig00000242 ;
  wire \blk00000001/sig00000241 ;
  wire \blk00000001/sig00000240 ;
  wire \blk00000001/sig0000023f ;
  wire \blk00000001/sig0000023e ;
  wire \blk00000001/sig0000023d ;
  wire \blk00000001/sig0000023c ;
  wire \blk00000001/sig0000023b ;
  wire \blk00000001/sig0000023a ;
  wire \blk00000001/sig00000239 ;
  wire \blk00000001/sig00000238 ;
  wire \blk00000001/sig00000237 ;
  wire \blk00000001/sig00000236 ;
  wire \blk00000001/sig00000235 ;
  wire \blk00000001/sig00000234 ;
  wire \blk00000001/sig00000233 ;
  wire \blk00000001/sig00000232 ;
  wire \blk00000001/sig00000231 ;
  wire \blk00000001/sig00000230 ;
  wire \blk00000001/sig0000022f ;
  wire \blk00000001/sig0000022e ;
  wire \blk00000001/sig0000022d ;
  wire \blk00000001/sig0000022c ;
  wire \blk00000001/sig0000022b ;
  wire \blk00000001/sig0000022a ;
  wire \blk00000001/sig00000229 ;
  wire \blk00000001/sig00000228 ;
  wire \blk00000001/sig00000227 ;
  wire \blk00000001/sig00000226 ;
  wire \blk00000001/sig00000225 ;
  wire \blk00000001/sig00000224 ;
  wire \blk00000001/sig00000223 ;
  wire \blk00000001/sig00000222 ;
  wire \blk00000001/sig00000221 ;
  wire \blk00000001/sig00000220 ;
  wire \blk00000001/sig0000021f ;
  wire \blk00000001/sig0000021e ;
  wire \blk00000001/sig0000021d ;
  wire \blk00000001/sig0000021c ;
  wire \blk00000001/sig0000021b ;
  wire \blk00000001/sig0000021a ;
  wire \blk00000001/sig00000219 ;
  wire \blk00000001/sig00000218 ;
  wire \blk00000001/sig00000217 ;
  wire \blk00000001/sig00000216 ;
  wire \blk00000001/sig00000215 ;
  wire \blk00000001/sig00000214 ;
  wire \blk00000001/sig00000213 ;
  wire \blk00000001/sig00000212 ;
  wire \blk00000001/sig00000211 ;
  wire \blk00000001/sig00000210 ;
  wire \blk00000001/sig0000020f ;
  wire \blk00000001/sig0000020e ;
  wire \blk00000001/sig0000020d ;
  wire \blk00000001/sig0000020c ;
  wire \blk00000001/sig0000020b ;
  wire \blk00000001/sig0000020a ;
  wire \blk00000001/sig00000209 ;
  wire \blk00000001/sig00000208 ;
  wire \blk00000001/sig00000207 ;
  wire \blk00000001/sig00000206 ;
  wire \blk00000001/sig00000205 ;
  wire \blk00000001/sig00000204 ;
  wire \blk00000001/sig00000203 ;
  wire \blk00000001/sig00000202 ;
  wire \blk00000001/sig00000201 ;
  wire \blk00000001/sig00000200 ;
  wire \blk00000001/sig000001ff ;
  wire \blk00000001/sig000001fe ;
  wire \blk00000001/sig000001fd ;
  wire \blk00000001/sig000001fc ;
  wire \blk00000001/sig000001fb ;
  wire \blk00000001/sig000001fa ;
  wire \blk00000001/sig000001f9 ;
  wire \blk00000001/sig000001f8 ;
  wire \blk00000001/sig000001f7 ;
  wire \blk00000001/sig000001f6 ;
  wire \blk00000001/sig000001f5 ;
  wire \blk00000001/sig000001f4 ;
  wire \blk00000001/sig000001f3 ;
  wire \blk00000001/sig000001f2 ;
  wire \blk00000001/sig000001f1 ;
  wire \blk00000001/sig000001f0 ;
  wire \blk00000001/sig000001ef ;
  wire \blk00000001/sig000001ee ;
  wire \blk00000001/sig000001ed ;
  wire \blk00000001/sig000001ec ;
  wire \blk00000001/sig000001eb ;
  wire \blk00000001/sig000001ea ;
  wire \blk00000001/sig000001e9 ;
  wire \blk00000001/sig000001e8 ;
  wire \blk00000001/sig000001e7 ;
  wire \blk00000001/sig000001e6 ;
  wire \blk00000001/sig000001e5 ;
  wire \blk00000001/sig000001e4 ;
  wire \blk00000001/sig000001e3 ;
  wire \blk00000001/sig000001e2 ;
  wire \blk00000001/sig000001e1 ;
  wire \blk00000001/sig000001e0 ;
  wire \blk00000001/sig000001df ;
  wire \blk00000001/sig000001de ;
  wire \blk00000001/sig000001dd ;
  wire \blk00000001/sig000001dc ;
  wire \blk00000001/sig000001db ;
  wire \blk00000001/sig000001da ;
  wire \blk00000001/sig000001d9 ;
  wire \blk00000001/sig000001d8 ;
  wire \blk00000001/sig000001d7 ;
  wire \blk00000001/sig000001d6 ;
  wire \blk00000001/sig000001d5 ;
  wire \blk00000001/sig000001d4 ;
  wire \blk00000001/sig000001d3 ;
  wire \blk00000001/sig000001d2 ;
  wire \blk00000001/sig000001d1 ;
  wire \blk00000001/sig000001d0 ;
  wire \blk00000001/sig000001cf ;
  wire \blk00000001/sig000001ce ;
  wire \blk00000001/sig000001cd ;
  wire \blk00000001/sig000001cc ;
  wire \blk00000001/sig000001cb ;
  wire \blk00000001/sig000001ca ;
  wire \blk00000001/sig000001c9 ;
  wire \blk00000001/sig000001c8 ;
  wire \blk00000001/sig000001c7 ;
  wire \blk00000001/sig000001c6 ;
  wire \blk00000001/sig000001c5 ;
  wire \blk00000001/sig000001c4 ;
  wire \blk00000001/sig000001c3 ;
  wire \blk00000001/sig000001c2 ;
  wire \blk00000001/sig000001c1 ;
  wire \blk00000001/sig000001c0 ;
  wire \blk00000001/sig000001bf ;
  wire \blk00000001/sig000001be ;
  wire \blk00000001/sig000001bd ;
  wire \blk00000001/sig000001bc ;
  wire \blk00000001/sig000001bb ;
  wire \blk00000001/sig000001ba ;
  wire \blk00000001/sig000001b9 ;
  wire \blk00000001/sig000001b8 ;
  wire \blk00000001/sig000001b7 ;
  wire \blk00000001/sig000001b6 ;
  wire \blk00000001/sig000001b5 ;
  wire \blk00000001/sig000001b4 ;
  wire \blk00000001/sig000001b3 ;
  wire \blk00000001/sig000001b2 ;
  wire \blk00000001/sig000001b1 ;
  wire \blk00000001/sig000001b0 ;
  wire \blk00000001/sig000001af ;
  wire \blk00000001/sig000001ae ;
  wire \blk00000001/sig000001ad ;
  wire \blk00000001/sig000001ac ;
  wire \blk00000001/sig000001ab ;
  wire \blk00000001/sig000001aa ;
  wire \blk00000001/sig000001a9 ;
  wire \blk00000001/sig000001a8 ;
  wire \blk00000001/sig000001a7 ;
  wire \blk00000001/sig000001a6 ;
  wire \blk00000001/sig000001a5 ;
  wire \blk00000001/sig000001a4 ;
  wire \blk00000001/sig000001a3 ;
  wire \blk00000001/sig000001a2 ;
  wire \blk00000001/sig000001a1 ;
  wire \blk00000001/sig000001a0 ;
  wire \blk00000001/sig0000019f ;
  wire \blk00000001/sig0000019e ;
  wire \blk00000001/sig0000019d ;
  wire \blk00000001/sig0000019c ;
  wire \blk00000001/sig0000019b ;
  wire \blk00000001/sig0000019a ;
  wire \blk00000001/sig00000199 ;
  wire \blk00000001/sig00000198 ;
  wire \blk00000001/sig00000197 ;
  wire \blk00000001/sig00000196 ;
  wire \blk00000001/sig00000195 ;
  wire \blk00000001/sig00000194 ;
  wire \blk00000001/sig00000193 ;
  wire \blk00000001/sig00000192 ;
  wire \blk00000001/sig00000191 ;
  wire \blk00000001/sig00000190 ;
  wire \blk00000001/sig0000018f ;
  wire \blk00000001/sig0000018e ;
  wire \blk00000001/sig0000018d ;
  wire \blk00000001/sig0000018c ;
  wire \blk00000001/sig0000018b ;
  wire \blk00000001/sig0000018a ;
  wire \blk00000001/sig00000189 ;
  wire \blk00000001/sig00000188 ;
  wire \blk00000001/sig00000187 ;
  wire \blk00000001/sig00000186 ;
  wire \blk00000001/sig00000185 ;
  wire \blk00000001/sig00000184 ;
  wire \blk00000001/sig00000183 ;
  wire \blk00000001/sig00000182 ;
  wire \blk00000001/sig00000181 ;
  wire \blk00000001/sig00000180 ;
  wire \blk00000001/sig0000017f ;
  wire \blk00000001/sig0000017e ;
  wire \blk00000001/sig0000017d ;
  wire \blk00000001/sig0000017c ;
  wire \blk00000001/sig0000017b ;
  wire \blk00000001/sig0000017a ;
  wire \blk00000001/sig00000179 ;
  wire \blk00000001/sig00000178 ;
  wire \blk00000001/sig00000177 ;
  wire \blk00000001/sig00000176 ;
  wire \blk00000001/sig00000175 ;
  wire \blk00000001/sig00000174 ;
  wire \blk00000001/sig00000173 ;
  wire \blk00000001/sig00000172 ;
  wire \blk00000001/sig00000171 ;
  wire \blk00000001/sig00000170 ;
  wire \blk00000001/sig0000016f ;
  wire \blk00000001/sig0000016e ;
  wire \blk00000001/sig0000016d ;
  wire \blk00000001/sig0000016c ;
  wire \blk00000001/sig0000016b ;
  wire \blk00000001/sig0000016a ;
  wire \blk00000001/sig00000169 ;
  wire \blk00000001/sig00000168 ;
  wire \blk00000001/sig00000167 ;
  wire \blk00000001/sig00000166 ;
  wire \blk00000001/sig00000165 ;
  wire \blk00000001/sig00000164 ;
  wire \blk00000001/sig00000163 ;
  wire \blk00000001/sig00000162 ;
  wire \blk00000001/sig00000161 ;
  wire \blk00000001/sig00000160 ;
  wire \blk00000001/sig0000015f ;
  wire \blk00000001/sig0000015e ;
  wire \blk00000001/sig0000015d ;
  wire \blk00000001/sig0000015c ;
  wire \blk00000001/sig0000015b ;
  wire \blk00000001/sig0000015a ;
  wire \blk00000001/sig00000159 ;
  wire \blk00000001/sig00000158 ;
  wire \blk00000001/sig00000157 ;
  wire \blk00000001/sig00000156 ;
  wire \blk00000001/sig00000155 ;
  wire \blk00000001/sig00000154 ;
  wire \blk00000001/sig00000153 ;
  wire \blk00000001/sig00000152 ;
  wire \blk00000001/sig00000151 ;
  wire \blk00000001/sig00000150 ;
  wire \blk00000001/sig0000014f ;
  wire \blk00000001/sig0000014e ;
  wire \blk00000001/sig0000014d ;
  wire \blk00000001/sig0000014c ;
  wire \blk00000001/sig0000014b ;
  wire \blk00000001/sig0000014a ;
  wire \blk00000001/sig00000149 ;
  wire \blk00000001/sig00000148 ;
  wire \blk00000001/sig00000147 ;
  wire \blk00000001/sig00000146 ;
  wire \blk00000001/sig00000145 ;
  wire \blk00000001/sig00000144 ;
  wire \blk00000001/sig00000143 ;
  wire \blk00000001/sig00000142 ;
  wire \blk00000001/sig00000141 ;
  wire \blk00000001/sig00000140 ;
  wire \blk00000001/sig0000013f ;
  wire \blk00000001/sig0000013e ;
  wire \blk00000001/sig0000013d ;
  wire \blk00000001/sig0000013c ;
  wire \blk00000001/sig0000013b ;
  wire \blk00000001/sig0000013a ;
  wire \blk00000001/sig00000139 ;
  wire \blk00000001/sig00000138 ;
  wire \blk00000001/sig00000137 ;
  wire \blk00000001/sig00000136 ;
  wire \blk00000001/sig00000135 ;
  wire \blk00000001/sig00000134 ;
  wire \blk00000001/sig00000133 ;
  wire \blk00000001/sig00000132 ;
  wire \blk00000001/sig00000131 ;
  wire \blk00000001/sig00000130 ;
  wire \blk00000001/sig0000012f ;
  wire \blk00000001/sig0000012e ;
  wire \blk00000001/sig0000012d ;
  wire \blk00000001/sig0000012c ;
  wire \blk00000001/sig0000012b ;
  wire \blk00000001/sig0000012a ;
  wire \blk00000001/sig00000129 ;
  wire \blk00000001/sig00000128 ;
  wire \blk00000001/sig00000127 ;
  wire \blk00000001/sig00000126 ;
  wire \blk00000001/sig00000125 ;
  wire \blk00000001/sig00000124 ;
  wire \blk00000001/sig00000123 ;
  wire \blk00000001/sig00000122 ;
  wire \blk00000001/sig00000121 ;
  wire \blk00000001/sig00000120 ;
  wire \blk00000001/sig0000011f ;
  wire \blk00000001/sig0000011e ;
  wire \blk00000001/sig0000011d ;
  wire \blk00000001/sig0000011c ;
  wire \blk00000001/sig0000011b ;
  wire \blk00000001/sig0000011a ;
  wire \blk00000001/sig00000119 ;
  wire \blk00000001/sig00000118 ;
  wire \blk00000001/sig00000117 ;
  wire \blk00000001/sig00000116 ;
  wire \blk00000001/sig00000115 ;
  wire \blk00000001/sig00000114 ;
  wire \blk00000001/sig00000113 ;
  wire \blk00000001/sig00000112 ;
  wire \blk00000001/sig00000111 ;
  wire \blk00000001/sig00000110 ;
  wire \blk00000001/sig0000010f ;
  wire \blk00000001/sig0000010e ;
  wire \blk00000001/sig0000010d ;
  wire \blk00000001/sig0000010c ;
  wire \blk00000001/sig0000010b ;
  wire \blk00000001/sig0000010a ;
  wire \blk00000001/sig00000109 ;
  wire \blk00000001/sig00000108 ;
  wire \blk00000001/sig00000107 ;
  wire \blk00000001/sig00000106 ;
  wire \blk00000001/sig00000105 ;
  wire \blk00000001/sig00000104 ;
  wire \blk00000001/sig00000103 ;
  wire \blk00000001/sig00000102 ;
  wire \blk00000001/sig00000101 ;
  wire \blk00000001/sig00000100 ;
  wire \blk00000001/sig000000ff ;
  wire \blk00000001/sig000000fe ;
  wire \blk00000001/sig000000fd ;
  wire \blk00000001/sig000000fc ;
  wire \blk00000001/sig000000fb ;
  wire \blk00000001/sig000000fa ;
  wire \blk00000001/sig000000f9 ;
  wire \blk00000001/sig000000f8 ;
  wire \blk00000001/sig000000f7 ;
  wire \blk00000001/sig000000f6 ;
  wire \blk00000001/sig000000f5 ;
  wire \blk00000001/sig000000f4 ;
  wire \blk00000001/sig000000f3 ;
  wire \blk00000001/sig000000f2 ;
  wire \blk00000001/sig000000f1 ;
  wire \blk00000001/sig000000f0 ;
  wire \blk00000001/sig000000ef ;
  wire \blk00000001/sig000000ee ;
  wire \blk00000001/sig000000ed ;
  wire \blk00000001/sig000000ec ;
  wire \blk00000001/sig000000eb ;
  wire \blk00000001/sig000000ea ;
  wire \blk00000001/sig000000e9 ;
  wire \blk00000001/sig000000e8 ;
  wire \blk00000001/sig000000e7 ;
  wire \blk00000001/sig000000e6 ;
  wire \blk00000001/sig000000e5 ;
  wire \blk00000001/sig000000e4 ;
  wire \blk00000001/sig000000e3 ;
  wire \blk00000001/sig000000e2 ;
  wire \blk00000001/sig000000e1 ;
  wire \blk00000001/sig000000e0 ;
  wire \blk00000001/sig000000df ;
  wire \blk00000001/sig000000de ;
  wire \blk00000001/sig000000dd ;
  wire \blk00000001/sig000000dc ;
  wire \blk00000001/sig000000db ;
  wire \blk00000001/sig000000da ;
  wire \blk00000001/sig000000d9 ;
  wire \blk00000001/sig000000d8 ;
  wire \blk00000001/sig000000d7 ;
  wire \blk00000001/sig000000d6 ;
  wire \blk00000001/sig000000d5 ;
  wire \blk00000001/sig000000d4 ;
  wire \blk00000001/sig000000d3 ;
  wire \blk00000001/sig000000d2 ;
  wire \blk00000001/sig000000d1 ;
  wire \blk00000001/sig000000d0 ;
  wire \blk00000001/sig000000cf ;
  wire \blk00000001/sig000000ce ;
  wire \blk00000001/sig000000cd ;
  wire \blk00000001/sig000000cc ;
  wire \blk00000001/sig000000cb ;
  wire \blk00000001/sig000000ca ;
  wire \blk00000001/sig000000c9 ;
  wire \blk00000001/sig000000c8 ;
  wire \blk00000001/sig000000c7 ;
  wire \blk00000001/sig000000c6 ;
  wire \blk00000001/sig000000c5 ;
  wire \blk00000001/sig000000c4 ;
  wire \blk00000001/sig000000c3 ;
  wire \blk00000001/sig000000c2 ;
  wire \blk00000001/sig000000c1 ;
  wire \blk00000001/sig000000c0 ;
  wire \blk00000001/sig000000bf ;
  wire \blk00000001/sig000000be ;
  wire \blk00000001/sig000000bd ;
  wire \blk00000001/sig000000bc ;
  wire \blk00000001/sig000000bb ;
  wire \blk00000001/sig000000ba ;
  wire \blk00000001/sig000000b9 ;
  wire \blk00000001/sig000000b8 ;
  wire \blk00000001/sig000000b7 ;
  wire \blk00000001/sig000000b6 ;
  wire \blk00000001/sig000000b5 ;
  wire \blk00000001/sig000000b4 ;
  wire \blk00000001/sig000000b3 ;
  wire \blk00000001/sig000000b2 ;
  wire \blk00000001/sig000000b1 ;
  wire \blk00000001/sig000000b0 ;
  wire \blk00000001/sig000000af ;
  wire \blk00000001/sig000000ae ;
  wire \blk00000001/sig000000ad ;
  wire \blk00000001/sig000000ac ;
  wire \blk00000001/sig000000ab ;
  wire \blk00000001/sig000000aa ;
  wire \blk00000001/sig000000a9 ;
  wire \blk00000001/sig000000a8 ;
  wire \blk00000001/sig000000a7 ;
  wire \blk00000001/sig000000a6 ;
  wire \blk00000001/sig000000a5 ;
  wire \blk00000001/sig000000a4 ;
  wire \blk00000001/sig000000a3 ;
  wire \blk00000001/sig000000a2 ;
  wire \blk00000001/sig000000a1 ;
  wire \blk00000001/sig000000a0 ;
  wire \blk00000001/sig0000009f ;
  wire \blk00000001/sig0000009e ;
  wire \blk00000001/sig0000009d ;
  wire \blk00000001/sig0000009c ;
  wire \blk00000001/sig0000009b ;
  wire \blk00000001/sig0000009a ;
  wire \blk00000001/sig00000099 ;
  wire \blk00000001/sig00000098 ;
  wire \blk00000001/sig00000097 ;
  wire \blk00000001/sig00000096 ;
  wire \blk00000001/sig00000095 ;
  wire \blk00000001/sig00000094 ;
  wire \blk00000001/sig00000093 ;
  wire \blk00000001/sig00000092 ;
  wire \blk00000001/sig00000091 ;
  wire \blk00000001/sig00000090 ;
  wire \blk00000001/sig0000008f ;
  wire \blk00000001/sig0000008e ;
  wire \blk00000001/sig0000008d ;
  wire \blk00000001/sig0000008c ;
  wire \blk00000001/sig0000008b ;
  wire \blk00000001/sig0000008a ;
  wire \blk00000001/sig00000089 ;
  wire \blk00000001/sig00000088 ;
  wire \blk00000001/sig00000087 ;
  wire \blk00000001/sig00000086 ;
  wire \blk00000001/sig00000084 ;
  wire \blk00000001/sig00000083 ;
  wire \NLW_blk00000001/blk00000090_O_UNCONNECTED ;
  FDRE #(
    .INIT ( 1'b0 ))
  \blk00000001/blk0000025d  (
    .C(clk),
    .CE(ce),
    .D(\blk00000001/sig0000027f ),
    .R(sclr),
    .Q(s[8])
  );
  LUT2 #(
    .INIT ( 4'h8 ))
  \blk00000001/blk0000025c  (
    .I0(\blk00000001/sig0000027e ),
    .I1(\blk00000001/sig00000088 ),
    .O(\blk00000001/sig0000027f )
  );
  FDRE #(
    .INIT ( 1'b0 ))
  \blk00000001/blk0000025b  (
    .C(clk),
    .CE(ce),
    .D(\blk00000001/sig0000027c ),
    .R(sclr),
    .Q(s[7])
  );
  LUT2 #(
    .INIT ( 4'h8 ))
  \blk00000001/blk0000025a  (
    .I0(\blk00000001/sig0000027b ),
    .I1(\blk00000001/sig00000088 ),
    .O(\blk00000001/sig0000027c )
  );
  FDRE #(
    .INIT ( 1'b0 ))
  \blk00000001/blk00000259  (
    .C(clk),
    .CE(ce),
    .D(\blk00000001/sig00000279 ),
    .R(sclr),
    .Q(s[6])
  );
  LUT2 #(
    .INIT ( 4'h8 ))
  \blk00000001/blk00000258  (
    .I0(\blk00000001/sig00000278 ),
    .I1(\blk00000001/sig00000088 ),
    .O(\blk00000001/sig00000279 )
  );
  FDRE #(
    .INIT ( 1'b0 ))
  \blk00000001/blk00000257  (
    .C(clk),
    .CE(ce),
    .D(\blk00000001/sig00000276 ),
    .R(sclr),
    .Q(s[5])
  );
  LUT2 #(
    .INIT ( 4'h8 ))
  \blk00000001/blk00000256  (
    .I0(\blk00000001/sig00000275 ),
    .I1(\blk00000001/sig00000088 ),
    .O(\blk00000001/sig00000276 )
  );
  FDRE #(
    .INIT ( 1'b0 ))
  \blk00000001/blk00000255  (
    .C(clk),
    .CE(ce),
    .D(\blk00000001/sig00000273 ),
    .R(sclr),
    .Q(s[4])
  );
  LUT2 #(
    .INIT ( 4'h8 ))
  \blk00000001/blk00000254  (
    .I0(\blk00000001/sig00000272 ),
    .I1(\blk00000001/sig00000088 ),
    .O(\blk00000001/sig00000273 )
  );
  FDRE #(
    .INIT ( 1'b0 ))
  \blk00000001/blk00000253  (
    .C(clk),
    .CE(ce),
    .D(\blk00000001/sig00000270 ),
    .R(sclr),
    .Q(s[3])
  );
  LUT2 #(
    .INIT ( 4'h8 ))
  \blk00000001/blk00000252  (
    .I0(\blk00000001/sig0000026f ),
    .I1(\blk00000001/sig00000088 ),
    .O(\blk00000001/sig00000270 )
  );
  FDRE #(
    .INIT ( 1'b0 ))
  \blk00000001/blk00000251  (
    .C(clk),
    .CE(ce),
    .D(\blk00000001/sig0000026d ),
    .R(sclr),
    .Q(s[2])
  );
  LUT2 #(
    .INIT ( 4'h8 ))
  \blk00000001/blk00000250  (
    .I0(\blk00000001/sig0000026c ),
    .I1(\blk00000001/sig00000088 ),
    .O(\blk00000001/sig0000026d )
  );
  FDRE #(
    .INIT ( 1'b0 ))
  \blk00000001/blk0000024f  (
    .C(clk),
    .CE(ce),
    .D(\blk00000001/sig0000026a ),
    .R(sclr),
    .Q(s[1])
  );
  LUT2 #(
    .INIT ( 4'h8 ))
  \blk00000001/blk0000024e  (
    .I0(\blk00000001/sig00000269 ),
    .I1(\blk00000001/sig00000088 ),
    .O(\blk00000001/sig0000026a )
  );
  FDRE #(
    .INIT ( 1'b0 ))
  \blk00000001/blk0000024d  (
    .C(clk),
    .CE(ce),
    .D(\blk00000001/sig00000258 ),
    .R(sclr),
    .Q(s[0])
  );
  LUT2 #(
    .INIT ( 4'h8 ))
  \blk00000001/blk0000024c  (
    .I0(\blk00000001/sig00000257 ),
    .I1(\blk00000001/sig00000088 ),
    .O(\blk00000001/sig00000258 )
  );
  FDRE #(
    .INIT ( 1'b0 ))
  \blk00000001/blk0000024b  (
    .C(clk),
    .CE(ce),
    .D(\blk00000001/sig00000267 ),
    .R(sclr),
    .Q(s[15])
  );
  LUT2 #(
    .INIT ( 4'h8 ))
  \blk00000001/blk0000024a  (
    .I0(\blk00000001/sig00000266 ),
    .I1(\blk00000001/sig00000088 ),
    .O(\blk00000001/sig00000267 )
  );
  FDRE #(
    .INIT ( 1'b0 ))
  \blk00000001/blk00000249  (
    .C(clk),
    .CE(ce),
    .D(\blk00000001/sig00000264 ),
    .R(sclr),
    .Q(s[14])
  );
  LUT2 #(
    .INIT ( 4'h8 ))
  \blk00000001/blk00000248  (
    .I0(\blk00000001/sig00000263 ),
    .I1(\blk00000001/sig00000088 ),
    .O(\blk00000001/sig00000264 )
  );
  FDRE #(
    .INIT ( 1'b0 ))
  \blk00000001/blk00000247  (
    .C(clk),
    .CE(ce),
    .D(\blk00000001/sig00000261 ),
    .R(sclr),
    .Q(s[13])
  );
  LUT2 #(
    .INIT ( 4'h8 ))
  \blk00000001/blk00000246  (
    .I0(\blk00000001/sig00000260 ),
    .I1(\blk00000001/sig00000088 ),
    .O(\blk00000001/sig00000261 )
  );
  FDRE #(
    .INIT ( 1'b0 ))
  \blk00000001/blk00000245  (
    .C(clk),
    .CE(ce),
    .D(\blk00000001/sig0000025e ),
    .R(sclr),
    .Q(s[12])
  );
  LUT2 #(
    .INIT ( 4'h8 ))
  \blk00000001/blk00000244  (
    .I0(\blk00000001/sig0000025d ),
    .I1(\blk00000001/sig00000088 ),
    .O(\blk00000001/sig0000025e )
  );
  FDRE #(
    .INIT ( 1'b0 ))
  \blk00000001/blk00000243  (
    .C(clk),
    .CE(ce),
    .D(\blk00000001/sig0000025b ),
    .R(sclr),
    .Q(s[11])
  );
  LUT2 #(
    .INIT ( 4'h8 ))
  \blk00000001/blk00000242  (
    .I0(\blk00000001/sig0000025a ),
    .I1(\blk00000001/sig00000088 ),
    .O(\blk00000001/sig0000025b )
  );
  FDRE #(
    .INIT ( 1'b0 ))
  \blk00000001/blk00000241  (
    .C(clk),
    .CE(ce),
    .D(\blk00000001/sig00000256 ),
    .R(sclr),
    .Q(s[10])
  );
  LUT2 #(
    .INIT ( 4'h8 ))
  \blk00000001/blk00000240  (
    .I0(\blk00000001/sig00000255 ),
    .I1(\blk00000001/sig00000088 ),
    .O(\blk00000001/sig00000256 )
  );
  FDRE #(
    .INIT ( 1'b0 ))
  \blk00000001/blk0000023f  (
    .C(clk),
    .CE(ce),
    .D(\blk00000001/sig00000253 ),
    .R(sclr),
    .Q(s[9])
  );
  LUT2 #(
    .INIT ( 4'h8 ))
  \blk00000001/blk0000023e  (
    .I0(\blk00000001/sig00000252 ),
    .I1(\blk00000001/sig00000088 ),
    .O(\blk00000001/sig00000253 )
  );
  FDRE   \blk00000001/blk0000023d  (
    .C(clk),
    .CE(ce),
    .D(\blk00000001/sig00000087 ),
    .R(sclr),
    .Q(\blk00000001/sig00000088 )
  );
  FDRE   \blk00000001/blk0000023c  (
    .C(clk),
    .CE(ce),
    .D(\blk00000001/sig00000086 ),
    .R(sclr),
    .Q(\blk00000001/sig00000087 )
  );
  FDRE   \blk00000001/blk0000023b  (
    .C(clk),
    .CE(ce),
    .D(\blk00000001/sig00000084 ),
    .R(sclr),
    .Q(\blk00000001/sig00000086 )
  );
  FDE #(
    .INIT ( 1'b0 ))
  \blk00000001/blk0000023a  (
    .C(clk),
    .CE(ce),
    .D(\blk00000001/sig00000240 ),
    .Q(\blk00000001/sig00000257 )
  );
  SRL16E #(
    .INIT ( 16'h0000 ))
  \blk00000001/blk00000239  (
    .A0(\blk00000001/sig00000084 ),
    .A1(\blk00000001/sig00000083 ),
    .A2(\blk00000001/sig00000083 ),
    .A3(\blk00000001/sig00000083 ),
    .CE(ce),
    .CLK(clk),
    .D(\blk00000001/sig000000ae ),
    .Q(\blk00000001/sig00000240 )
  );
  FDE #(
    .INIT ( 1'b0 ))
  \blk00000001/blk00000238  (
    .C(clk),
    .CE(ce),
    .D(\blk00000001/sig00000248 ),
    .Q(\blk00000001/sig00000269 )
  );
  SRL16E #(
    .INIT ( 16'h0000 ))
  \blk00000001/blk00000237  (
    .A0(\blk00000001/sig00000084 ),
    .A1(\blk00000001/sig00000083 ),
    .A2(\blk00000001/sig00000083 ),
    .A3(\blk00000001/sig00000083 ),
    .CE(ce),
    .CLK(clk),
    .D(\blk00000001/sig000000b5 ),
    .Q(\blk00000001/sig00000248 )
  );
  FDE #(
    .INIT ( 1'b0 ))
  \blk00000001/blk00000236  (
    .C(clk),
    .CE(ce),
    .D(\blk00000001/sig00000249 ),
    .Q(\blk00000001/sig0000026c )
  );
  SRL16E #(
    .INIT ( 16'h0000 ))
  \blk00000001/blk00000235  (
    .A0(\blk00000001/sig00000084 ),
    .A1(\blk00000001/sig00000083 ),
    .A2(\blk00000001/sig00000083 ),
    .A3(\blk00000001/sig00000083 ),
    .CE(ce),
    .CLK(clk),
    .D(\blk00000001/sig000000b6 ),
    .Q(\blk00000001/sig00000249 )
  );
  FDE #(
    .INIT ( 1'b0 ))
  \blk00000001/blk00000234  (
    .C(clk),
    .CE(ce),
    .D(\blk00000001/sig0000024a ),
    .Q(\blk00000001/sig0000026f )
  );
  SRL16E #(
    .INIT ( 16'h0000 ))
  \blk00000001/blk00000233  (
    .A0(\blk00000001/sig00000084 ),
    .A1(\blk00000001/sig00000083 ),
    .A2(\blk00000001/sig00000083 ),
    .A3(\blk00000001/sig00000083 ),
    .CE(ce),
    .CLK(clk),
    .D(\blk00000001/sig000000b7 ),
    .Q(\blk00000001/sig0000024a )
  );
  FDE #(
    .INIT ( 1'b0 ))
  \blk00000001/blk00000232  (
    .C(clk),
    .CE(ce),
    .D(\blk00000001/sig0000024b ),
    .Q(\blk00000001/sig00000272 )
  );
  SRL16E #(
    .INIT ( 16'h0000 ))
  \blk00000001/blk00000231  (
    .A0(\blk00000001/sig00000084 ),
    .A1(\blk00000001/sig00000083 ),
    .A2(\blk00000001/sig00000083 ),
    .A3(\blk00000001/sig00000083 ),
    .CE(ce),
    .CLK(clk),
    .D(\blk00000001/sig000000b8 ),
    .Q(\blk00000001/sig0000024b )
  );
  FDE #(
    .INIT ( 1'b0 ))
  \blk00000001/blk00000230  (
    .C(clk),
    .CE(ce),
    .D(\blk00000001/sig0000024c ),
    .Q(\blk00000001/sig00000275 )
  );
  SRL16E #(
    .INIT ( 16'h0000 ))
  \blk00000001/blk0000022f  (
    .A0(\blk00000001/sig00000084 ),
    .A1(\blk00000001/sig00000083 ),
    .A2(\blk00000001/sig00000083 ),
    .A3(\blk00000001/sig00000083 ),
    .CE(ce),
    .CLK(clk),
    .D(\blk00000001/sig000000b9 ),
    .Q(\blk00000001/sig0000024c )
  );
  FDE #(
    .INIT ( 1'b0 ))
  \blk00000001/blk0000022e  (
    .C(clk),
    .CE(ce),
    .D(\blk00000001/sig0000024d ),
    .Q(\blk00000001/sig00000278 )
  );
  SRL16E #(
    .INIT ( 16'h0000 ))
  \blk00000001/blk0000022d  (
    .A0(\blk00000001/sig00000084 ),
    .A1(\blk00000001/sig00000083 ),
    .A2(\blk00000001/sig00000083 ),
    .A3(\blk00000001/sig00000083 ),
    .CE(ce),
    .CLK(clk),
    .D(\blk00000001/sig000000ba ),
    .Q(\blk00000001/sig0000024d )
  );
  FDE #(
    .INIT ( 1'b0 ))
  \blk00000001/blk0000022c  (
    .C(clk),
    .CE(ce),
    .D(\blk00000001/sig0000024e ),
    .Q(\blk00000001/sig0000027b )
  );
  SRL16E #(
    .INIT ( 16'h0000 ))
  \blk00000001/blk0000022b  (
    .A0(\blk00000001/sig00000084 ),
    .A1(\blk00000001/sig00000083 ),
    .A2(\blk00000001/sig00000083 ),
    .A3(\blk00000001/sig00000083 ),
    .CE(ce),
    .CLK(clk),
    .D(\blk00000001/sig000000bb ),
    .Q(\blk00000001/sig0000024e )
  );
  FDE #(
    .INIT ( 1'b0 ))
  \blk00000001/blk0000022a  (
    .C(clk),
    .CE(ce),
    .D(\blk00000001/sig0000024f ),
    .Q(\blk00000001/sig0000027e )
  );
  SRL16E #(
    .INIT ( 16'h0000 ))
  \blk00000001/blk00000229  (
    .A0(\blk00000001/sig00000084 ),
    .A1(\blk00000001/sig00000083 ),
    .A2(\blk00000001/sig00000083 ),
    .A3(\blk00000001/sig00000083 ),
    .CE(ce),
    .CLK(clk),
    .D(\blk00000001/sig000000bc ),
    .Q(\blk00000001/sig0000024f )
  );
  FDE #(
    .INIT ( 1'b0 ))
  \blk00000001/blk00000228  (
    .C(clk),
    .CE(ce),
    .D(\blk00000001/sig00000241 ),
    .Q(\blk00000001/sig00000252 )
  );
  SRL16E #(
    .INIT ( 16'h0000 ))
  \blk00000001/blk00000227  (
    .A0(\blk00000001/sig00000084 ),
    .A1(\blk00000001/sig00000083 ),
    .A2(\blk00000001/sig00000083 ),
    .A3(\blk00000001/sig00000083 ),
    .CE(ce),
    .CLK(clk),
    .D(\blk00000001/sig000000bd ),
    .Q(\blk00000001/sig00000241 )
  );
  FDE #(
    .INIT ( 1'b0 ))
  \blk00000001/blk00000226  (
    .C(clk),
    .CE(ce),
    .D(\blk00000001/sig00000242 ),
    .Q(\blk00000001/sig00000255 )
  );
  SRL16E #(
    .INIT ( 16'h0000 ))
  \blk00000001/blk00000225  (
    .A0(\blk00000001/sig00000084 ),
    .A1(\blk00000001/sig00000083 ),
    .A2(\blk00000001/sig00000083 ),
    .A3(\blk00000001/sig00000083 ),
    .CE(ce),
    .CLK(clk),
    .D(\blk00000001/sig000000af ),
    .Q(\blk00000001/sig00000242 )
  );
  FDE #(
    .INIT ( 1'b0 ))
  \blk00000001/blk00000224  (
    .C(clk),
    .CE(ce),
    .D(\blk00000001/sig00000243 ),
    .Q(\blk00000001/sig0000025a )
  );
  SRL16E #(
    .INIT ( 16'h0000 ))
  \blk00000001/blk00000223  (
    .A0(\blk00000001/sig00000084 ),
    .A1(\blk00000001/sig00000083 ),
    .A2(\blk00000001/sig00000083 ),
    .A3(\blk00000001/sig00000083 ),
    .CE(ce),
    .CLK(clk),
    .D(\blk00000001/sig000000b0 ),
    .Q(\blk00000001/sig00000243 )
  );
  FDE #(
    .INIT ( 1'b0 ))
  \blk00000001/blk00000222  (
    .C(clk),
    .CE(ce),
    .D(\blk00000001/sig00000244 ),
    .Q(\blk00000001/sig0000025d )
  );
  SRL16E #(
    .INIT ( 16'h0000 ))
  \blk00000001/blk00000221  (
    .A0(\blk00000001/sig00000084 ),
    .A1(\blk00000001/sig00000083 ),
    .A2(\blk00000001/sig00000083 ),
    .A3(\blk00000001/sig00000083 ),
    .CE(ce),
    .CLK(clk),
    .D(\blk00000001/sig000000b1 ),
    .Q(\blk00000001/sig00000244 )
  );
  FDE #(
    .INIT ( 1'b0 ))
  \blk00000001/blk00000220  (
    .C(clk),
    .CE(ce),
    .D(\blk00000001/sig00000245 ),
    .Q(\blk00000001/sig00000260 )
  );
  SRL16E #(
    .INIT ( 16'h0000 ))
  \blk00000001/blk0000021f  (
    .A0(\blk00000001/sig00000084 ),
    .A1(\blk00000001/sig00000083 ),
    .A2(\blk00000001/sig00000083 ),
    .A3(\blk00000001/sig00000083 ),
    .CE(ce),
    .CLK(clk),
    .D(\blk00000001/sig000000b2 ),
    .Q(\blk00000001/sig00000245 )
  );
  FDE #(
    .INIT ( 1'b0 ))
  \blk00000001/blk0000021e  (
    .C(clk),
    .CE(ce),
    .D(\blk00000001/sig00000246 ),
    .Q(\blk00000001/sig00000263 )
  );
  SRL16E #(
    .INIT ( 16'h0000 ))
  \blk00000001/blk0000021d  (
    .A0(\blk00000001/sig00000084 ),
    .A1(\blk00000001/sig00000083 ),
    .A2(\blk00000001/sig00000083 ),
    .A3(\blk00000001/sig00000083 ),
    .CE(ce),
    .CLK(clk),
    .D(\blk00000001/sig000000b3 ),
    .Q(\blk00000001/sig00000246 )
  );
  FDE #(
    .INIT ( 1'b0 ))
  \blk00000001/blk0000021c  (
    .C(clk),
    .CE(ce),
    .D(\blk00000001/sig00000247 ),
    .Q(\blk00000001/sig00000266 )
  );
  SRL16E #(
    .INIT ( 16'h0000 ))
  \blk00000001/blk0000021b  (
    .A0(\blk00000001/sig00000084 ),
    .A1(\blk00000001/sig00000083 ),
    .A2(\blk00000001/sig00000083 ),
    .A3(\blk00000001/sig00000083 ),
    .CE(ce),
    .CLK(clk),
    .D(\blk00000001/sig000000b4 ),
    .Q(\blk00000001/sig00000247 )
  );
  INV   \blk00000001/blk0000021a  (
    .I(\blk00000001/sig000000de ),
    .O(\blk00000001/sig0000018e )
  );
  INV   \blk00000001/blk00000219  (
    .I(\blk00000001/sig000001c0 ),
    .O(\blk00000001/sig000001ff )
  );
  INV   \blk00000001/blk00000218  (
    .I(\blk00000001/sig00000230 ),
    .O(\blk00000001/sig000002af )
  );
  INV   \blk00000001/blk00000217  (
    .I(\blk00000001/sig000000e6 ),
    .O(\blk00000001/sig00000195 )
  );
  INV   \blk00000001/blk00000216  (
    .I(\blk00000001/sig000001c8 ),
    .O(\blk00000001/sig00000206 )
  );
  INV   \blk00000001/blk00000215  (
    .I(\blk00000001/sig00000238 ),
    .O(\blk00000001/sig000002b6 )
  );
  INV   \blk00000001/blk00000214  (
    .I(\blk00000001/sig000000e7 ),
    .O(\blk00000001/sig00000196 )
  );
  INV   \blk00000001/blk00000213  (
    .I(\blk00000001/sig000001c9 ),
    .O(\blk00000001/sig00000207 )
  );
  INV   \blk00000001/blk00000212  (
    .I(\blk00000001/sig00000239 ),
    .O(\blk00000001/sig000002b7 )
  );
  INV   \blk00000001/blk00000211  (
    .I(\blk00000001/sig000000e8 ),
    .O(\blk00000001/sig00000197 )
  );
  INV   \blk00000001/blk00000210  (
    .I(\blk00000001/sig000001ca ),
    .O(\blk00000001/sig00000208 )
  );
  INV   \blk00000001/blk0000020f  (
    .I(\blk00000001/sig0000023a ),
    .O(\blk00000001/sig000002b8 )
  );
  INV   \blk00000001/blk0000020e  (
    .I(\blk00000001/sig000000e9 ),
    .O(\blk00000001/sig00000198 )
  );
  INV   \blk00000001/blk0000020d  (
    .I(\blk00000001/sig000001cb ),
    .O(\blk00000001/sig00000209 )
  );
  INV   \blk00000001/blk0000020c  (
    .I(\blk00000001/sig0000023b ),
    .O(\blk00000001/sig000002b9 )
  );
  INV   \blk00000001/blk0000020b  (
    .I(\blk00000001/sig000000ea ),
    .O(\blk00000001/sig00000199 )
  );
  INV   \blk00000001/blk0000020a  (
    .I(\blk00000001/sig000001cc ),
    .O(\blk00000001/sig0000020a )
  );
  INV   \blk00000001/blk00000209  (
    .I(\blk00000001/sig0000023c ),
    .O(\blk00000001/sig000002ba )
  );
  INV   \blk00000001/blk00000208  (
    .I(\blk00000001/sig000000eb ),
    .O(\blk00000001/sig0000019a )
  );
  INV   \blk00000001/blk00000207  (
    .I(\blk00000001/sig000001cd ),
    .O(\blk00000001/sig0000020b )
  );
  INV   \blk00000001/blk00000206  (
    .I(\blk00000001/sig0000023d ),
    .O(\blk00000001/sig000002bb )
  );
  INV   \blk00000001/blk00000205  (
    .I(\blk00000001/sig000000ec ),
    .O(\blk00000001/sig0000019b )
  );
  INV   \blk00000001/blk00000204  (
    .I(\blk00000001/sig000001ce ),
    .O(\blk00000001/sig0000020c )
  );
  INV   \blk00000001/blk00000203  (
    .I(\blk00000001/sig0000023e ),
    .O(\blk00000001/sig000002bc )
  );
  INV   \blk00000001/blk00000202  (
    .I(\blk00000001/sig000000ed ),
    .O(\blk00000001/sig0000019c )
  );
  INV   \blk00000001/blk00000201  (
    .I(\blk00000001/sig000001cf ),
    .O(\blk00000001/sig0000020d )
  );
  INV   \blk00000001/blk00000200  (
    .I(\blk00000001/sig0000023f ),
    .O(\blk00000001/sig000002bd )
  );
  INV   \blk00000001/blk000001ff  (
    .I(\blk00000001/sig000000df ),
    .O(\blk00000001/sig0000019d )
  );
  INV   \blk00000001/blk000001fe  (
    .I(\blk00000001/sig000001c1 ),
    .O(\blk00000001/sig0000020e )
  );
  INV   \blk00000001/blk000001fd  (
    .I(\blk00000001/sig00000231 ),
    .O(\blk00000001/sig000002be )
  );
  INV   \blk00000001/blk000001fc  (
    .I(\blk00000001/sig000000e0 ),
    .O(\blk00000001/sig0000018f )
  );
  INV   \blk00000001/blk000001fb  (
    .I(\blk00000001/sig000001c2 ),
    .O(\blk00000001/sig00000200 )
  );
  INV   \blk00000001/blk000001fa  (
    .I(\blk00000001/sig00000232 ),
    .O(\blk00000001/sig000002b0 )
  );
  INV   \blk00000001/blk000001f9  (
    .I(\blk00000001/sig000000e1 ),
    .O(\blk00000001/sig00000190 )
  );
  INV   \blk00000001/blk000001f8  (
    .I(\blk00000001/sig000001c3 ),
    .O(\blk00000001/sig00000201 )
  );
  INV   \blk00000001/blk000001f7  (
    .I(\blk00000001/sig00000233 ),
    .O(\blk00000001/sig000002b1 )
  );
  INV   \blk00000001/blk000001f6  (
    .I(\blk00000001/sig000000e2 ),
    .O(\blk00000001/sig00000191 )
  );
  INV   \blk00000001/blk000001f5  (
    .I(\blk00000001/sig000001c4 ),
    .O(\blk00000001/sig00000202 )
  );
  INV   \blk00000001/blk000001f4  (
    .I(\blk00000001/sig00000234 ),
    .O(\blk00000001/sig000002b2 )
  );
  INV   \blk00000001/blk000001f3  (
    .I(\blk00000001/sig000000e3 ),
    .O(\blk00000001/sig00000192 )
  );
  INV   \blk00000001/blk000001f2  (
    .I(\blk00000001/sig000001c5 ),
    .O(\blk00000001/sig00000203 )
  );
  INV   \blk00000001/blk000001f1  (
    .I(\blk00000001/sig00000235 ),
    .O(\blk00000001/sig000002b3 )
  );
  INV   \blk00000001/blk000001f0  (
    .I(\blk00000001/sig000000e4 ),
    .O(\blk00000001/sig00000193 )
  );
  INV   \blk00000001/blk000001ef  (
    .I(\blk00000001/sig000001c6 ),
    .O(\blk00000001/sig00000204 )
  );
  INV   \blk00000001/blk000001ee  (
    .I(\blk00000001/sig00000236 ),
    .O(\blk00000001/sig000002b4 )
  );
  INV   \blk00000001/blk000001ed  (
    .I(\blk00000001/sig000000e5 ),
    .O(\blk00000001/sig00000194 )
  );
  INV   \blk00000001/blk000001ec  (
    .I(\blk00000001/sig000001c7 ),
    .O(\blk00000001/sig00000205 )
  );
  INV   \blk00000001/blk000001eb  (
    .I(\blk00000001/sig00000237 ),
    .O(\blk00000001/sig000002b5 )
  );
  LUT2 #(
    .INIT ( 4'h8 ))
  \blk00000001/blk000001ea  (
    .I0(\blk00000001/sig000000ee ),
    .I1(\blk00000001/sig00000089 ),
    .O(\blk00000001/sig0000008b )
  );
  LUT2 #(
    .INIT ( 4'h8 ))
  \blk00000001/blk000001e9  (
    .I0(\blk00000001/sig000001bf ),
    .I1(\blk00000001/sig0000008a ),
    .O(\blk00000001/sig0000008c )
  );
  LUT2 #(
    .INIT ( 4'h9 ))
  \blk00000001/blk000001e8  (
    .I0(b[0]),
    .I1(a[0]),
    .O(\blk00000001/sig0000009d )
  );
  LUT2 #(
    .INIT ( 4'h9 ))
  \blk00000001/blk000001e7  (
    .I0(b[16]),
    .I1(a[16]),
    .O(\blk00000001/sig000000ce )
  );
  LUT2 #(
    .INIT ( 4'h9 ))
  \blk00000001/blk000001e6  (
    .I0(b[32]),
    .I1(a[32]),
    .O(\blk00000001/sig0000010f )
  );
  LUT2 #(
    .INIT ( 4'h9 ))
  \blk00000001/blk000001e5  (
    .I0(b[48]),
    .I1(a[48]),
    .O(\blk00000001/sig0000014f )
  );
  LUT2 #(
    .INIT ( 4'h9 ))
  \blk00000001/blk000001e4  (
    .I0(b[1]),
    .I1(a[1]),
    .O(\blk00000001/sig000000a4 )
  );
  LUT2 #(
    .INIT ( 4'h9 ))
  \blk00000001/blk000001e3  (
    .I0(b[17]),
    .I1(a[17]),
    .O(\blk00000001/sig000000d5 )
  );
  LUT2 #(
    .INIT ( 4'h9 ))
  \blk00000001/blk000001e2  (
    .I0(b[33]),
    .I1(a[33]),
    .O(\blk00000001/sig00000116 )
  );
  LUT2 #(
    .INIT ( 4'h9 ))
  \blk00000001/blk000001e1  (
    .I0(b[49]),
    .I1(a[49]),
    .O(\blk00000001/sig00000156 )
  );
  LUT2 #(
    .INIT ( 4'h9 ))
  \blk00000001/blk000001e0  (
    .I0(b[2]),
    .I1(a[2]),
    .O(\blk00000001/sig000000a5 )
  );
  LUT2 #(
    .INIT ( 4'h9 ))
  \blk00000001/blk000001df  (
    .I0(b[18]),
    .I1(a[18]),
    .O(\blk00000001/sig000000d6 )
  );
  LUT2 #(
    .INIT ( 4'h9 ))
  \blk00000001/blk000001de  (
    .I0(b[34]),
    .I1(a[34]),
    .O(\blk00000001/sig00000117 )
  );
  LUT2 #(
    .INIT ( 4'h9 ))
  \blk00000001/blk000001dd  (
    .I0(b[50]),
    .I1(a[50]),
    .O(\blk00000001/sig00000157 )
  );
  LUT2 #(
    .INIT ( 4'h9 ))
  \blk00000001/blk000001dc  (
    .I0(b[3]),
    .I1(a[3]),
    .O(\blk00000001/sig000000a6 )
  );
  LUT2 #(
    .INIT ( 4'h9 ))
  \blk00000001/blk000001db  (
    .I0(b[19]),
    .I1(a[19]),
    .O(\blk00000001/sig000000d7 )
  );
  LUT2 #(
    .INIT ( 4'h9 ))
  \blk00000001/blk000001da  (
    .I0(b[35]),
    .I1(a[35]),
    .O(\blk00000001/sig00000118 )
  );
  LUT2 #(
    .INIT ( 4'h9 ))
  \blk00000001/blk000001d9  (
    .I0(b[51]),
    .I1(a[51]),
    .O(\blk00000001/sig00000158 )
  );
  LUT2 #(
    .INIT ( 4'h9 ))
  \blk00000001/blk000001d8  (
    .I0(b[4]),
    .I1(a[4]),
    .O(\blk00000001/sig000000a7 )
  );
  LUT2 #(
    .INIT ( 4'h9 ))
  \blk00000001/blk000001d7  (
    .I0(b[20]),
    .I1(a[20]),
    .O(\blk00000001/sig000000d8 )
  );
  LUT2 #(
    .INIT ( 4'h9 ))
  \blk00000001/blk000001d6  (
    .I0(b[36]),
    .I1(a[36]),
    .O(\blk00000001/sig00000119 )
  );
  LUT2 #(
    .INIT ( 4'h9 ))
  \blk00000001/blk000001d5  (
    .I0(b[52]),
    .I1(a[52]),
    .O(\blk00000001/sig00000159 )
  );
  LUT2 #(
    .INIT ( 4'h9 ))
  \blk00000001/blk000001d4  (
    .I0(b[5]),
    .I1(a[5]),
    .O(\blk00000001/sig000000a8 )
  );
  LUT2 #(
    .INIT ( 4'h9 ))
  \blk00000001/blk000001d3  (
    .I0(b[21]),
    .I1(a[21]),
    .O(\blk00000001/sig000000d9 )
  );
  LUT2 #(
    .INIT ( 4'h9 ))
  \blk00000001/blk000001d2  (
    .I0(b[37]),
    .I1(a[37]),
    .O(\blk00000001/sig0000011a )
  );
  LUT2 #(
    .INIT ( 4'h9 ))
  \blk00000001/blk000001d1  (
    .I0(b[53]),
    .I1(a[53]),
    .O(\blk00000001/sig0000015a )
  );
  LUT2 #(
    .INIT ( 4'h9 ))
  \blk00000001/blk000001d0  (
    .I0(b[6]),
    .I1(a[6]),
    .O(\blk00000001/sig000000a9 )
  );
  LUT2 #(
    .INIT ( 4'h9 ))
  \blk00000001/blk000001cf  (
    .I0(b[22]),
    .I1(a[22]),
    .O(\blk00000001/sig000000da )
  );
  LUT2 #(
    .INIT ( 4'h9 ))
  \blk00000001/blk000001ce  (
    .I0(b[38]),
    .I1(a[38]),
    .O(\blk00000001/sig0000011b )
  );
  LUT2 #(
    .INIT ( 4'h9 ))
  \blk00000001/blk000001cd  (
    .I0(b[54]),
    .I1(a[54]),
    .O(\blk00000001/sig0000015b )
  );
  LUT2 #(
    .INIT ( 4'h9 ))
  \blk00000001/blk000001cc  (
    .I0(b[7]),
    .I1(a[7]),
    .O(\blk00000001/sig000000aa )
  );
  LUT2 #(
    .INIT ( 4'h9 ))
  \blk00000001/blk000001cb  (
    .I0(b[23]),
    .I1(a[23]),
    .O(\blk00000001/sig000000db )
  );
  LUT2 #(
    .INIT ( 4'h9 ))
  \blk00000001/blk000001ca  (
    .I0(b[39]),
    .I1(a[39]),
    .O(\blk00000001/sig0000011c )
  );
  LUT2 #(
    .INIT ( 4'h9 ))
  \blk00000001/blk000001c9  (
    .I0(b[55]),
    .I1(a[55]),
    .O(\blk00000001/sig0000015c )
  );
  LUT2 #(
    .INIT ( 4'h9 ))
  \blk00000001/blk000001c8  (
    .I0(b[8]),
    .I1(a[8]),
    .O(\blk00000001/sig000000ab )
  );
  LUT2 #(
    .INIT ( 4'h9 ))
  \blk00000001/blk000001c7  (
    .I0(b[24]),
    .I1(a[24]),
    .O(\blk00000001/sig000000dc )
  );
  LUT2 #(
    .INIT ( 4'h9 ))
  \blk00000001/blk000001c6  (
    .I0(b[40]),
    .I1(a[40]),
    .O(\blk00000001/sig0000011d )
  );
  LUT2 #(
    .INIT ( 4'h9 ))
  \blk00000001/blk000001c5  (
    .I0(b[56]),
    .I1(a[56]),
    .O(\blk00000001/sig0000015d )
  );
  LUT2 #(
    .INIT ( 4'h9 ))
  \blk00000001/blk000001c4  (
    .I0(b[9]),
    .I1(a[9]),
    .O(\blk00000001/sig000000ac )
  );
  LUT2 #(
    .INIT ( 4'h9 ))
  \blk00000001/blk000001c3  (
    .I0(b[25]),
    .I1(a[25]),
    .O(\blk00000001/sig000000dd )
  );
  LUT2 #(
    .INIT ( 4'h9 ))
  \blk00000001/blk000001c2  (
    .I0(b[41]),
    .I1(a[41]),
    .O(\blk00000001/sig0000011e )
  );
  LUT2 #(
    .INIT ( 4'h9 ))
  \blk00000001/blk000001c1  (
    .I0(b[57]),
    .I1(a[57]),
    .O(\blk00000001/sig0000015e )
  );
  LUT2 #(
    .INIT ( 4'h9 ))
  \blk00000001/blk000001c0  (
    .I0(b[10]),
    .I1(a[10]),
    .O(\blk00000001/sig0000009e )
  );
  LUT2 #(
    .INIT ( 4'h9 ))
  \blk00000001/blk000001bf  (
    .I0(b[26]),
    .I1(a[26]),
    .O(\blk00000001/sig000000cf )
  );
  LUT2 #(
    .INIT ( 4'h9 ))
  \blk00000001/blk000001be  (
    .I0(b[42]),
    .I1(a[42]),
    .O(\blk00000001/sig00000110 )
  );
  LUT2 #(
    .INIT ( 4'h9 ))
  \blk00000001/blk000001bd  (
    .I0(b[58]),
    .I1(a[58]),
    .O(\blk00000001/sig00000150 )
  );
  LUT2 #(
    .INIT ( 4'h9 ))
  \blk00000001/blk000001bc  (
    .I0(b[11]),
    .I1(a[11]),
    .O(\blk00000001/sig0000009f )
  );
  LUT2 #(
    .INIT ( 4'h9 ))
  \blk00000001/blk000001bb  (
    .I0(b[27]),
    .I1(a[27]),
    .O(\blk00000001/sig000000d0 )
  );
  LUT2 #(
    .INIT ( 4'h9 ))
  \blk00000001/blk000001ba  (
    .I0(b[43]),
    .I1(a[43]),
    .O(\blk00000001/sig00000111 )
  );
  LUT2 #(
    .INIT ( 4'h9 ))
  \blk00000001/blk000001b9  (
    .I0(b[59]),
    .I1(a[59]),
    .O(\blk00000001/sig00000151 )
  );
  LUT2 #(
    .INIT ( 4'h9 ))
  \blk00000001/blk000001b8  (
    .I0(b[12]),
    .I1(a[12]),
    .O(\blk00000001/sig000000a0 )
  );
  LUT2 #(
    .INIT ( 4'h9 ))
  \blk00000001/blk000001b7  (
    .I0(b[28]),
    .I1(a[28]),
    .O(\blk00000001/sig000000d1 )
  );
  LUT2 #(
    .INIT ( 4'h9 ))
  \blk00000001/blk000001b6  (
    .I0(b[44]),
    .I1(a[44]),
    .O(\blk00000001/sig00000112 )
  );
  LUT2 #(
    .INIT ( 4'h9 ))
  \blk00000001/blk000001b5  (
    .I0(b[60]),
    .I1(a[60]),
    .O(\blk00000001/sig00000152 )
  );
  LUT2 #(
    .INIT ( 4'h9 ))
  \blk00000001/blk000001b4  (
    .I0(b[13]),
    .I1(a[13]),
    .O(\blk00000001/sig000000a1 )
  );
  LUT2 #(
    .INIT ( 4'h9 ))
  \blk00000001/blk000001b3  (
    .I0(b[29]),
    .I1(a[29]),
    .O(\blk00000001/sig000000d2 )
  );
  LUT2 #(
    .INIT ( 4'h9 ))
  \blk00000001/blk000001b2  (
    .I0(b[45]),
    .I1(a[45]),
    .O(\blk00000001/sig00000113 )
  );
  LUT2 #(
    .INIT ( 4'h9 ))
  \blk00000001/blk000001b1  (
    .I0(b[61]),
    .I1(a[61]),
    .O(\blk00000001/sig00000153 )
  );
  LUT2 #(
    .INIT ( 4'h9 ))
  \blk00000001/blk000001b0  (
    .I0(b[14]),
    .I1(a[14]),
    .O(\blk00000001/sig000000a2 )
  );
  LUT2 #(
    .INIT ( 4'h9 ))
  \blk00000001/blk000001af  (
    .I0(b[30]),
    .I1(a[30]),
    .O(\blk00000001/sig000000d3 )
  );
  LUT2 #(
    .INIT ( 4'h9 ))
  \blk00000001/blk000001ae  (
    .I0(b[46]),
    .I1(a[46]),
    .O(\blk00000001/sig00000114 )
  );
  LUT2 #(
    .INIT ( 4'h9 ))
  \blk00000001/blk000001ad  (
    .I0(b[62]),
    .I1(a[62]),
    .O(\blk00000001/sig00000154 )
  );
  LUT2 #(
    .INIT ( 4'h9 ))
  \blk00000001/blk000001ac  (
    .I0(b[15]),
    .I1(a[15]),
    .O(\blk00000001/sig000000a3 )
  );
  LUT2 #(
    .INIT ( 4'h9 ))
  \blk00000001/blk000001ab  (
    .I0(b[31]),
    .I1(a[31]),
    .O(\blk00000001/sig000000d4 )
  );
  LUT2 #(
    .INIT ( 4'h9 ))
  \blk00000001/blk000001aa  (
    .I0(b[47]),
    .I1(a[47]),
    .O(\blk00000001/sig00000115 )
  );
  LUT2 #(
    .INIT ( 4'h9 ))
  \blk00000001/blk000001a9  (
    .I0(b[63]),
    .I1(a[63]),
    .O(\blk00000001/sig00000155 )
  );
  FDRE #(
    .INIT ( 1'b0 ))
  \blk00000001/blk000001a8  (
    .C(clk),
    .CE(ce),
    .D(\blk00000001/sig0000020f ),
    .R(sclr),
    .Q(s[32])
  );
  FDRE #(
    .INIT ( 1'b0 ))
  \blk00000001/blk000001a7  (
    .C(clk),
    .CE(ce),
    .D(\blk00000001/sig00000217 ),
    .R(sclr),
    .Q(s[33])
  );
  FDRE #(
    .INIT ( 1'b0 ))
  \blk00000001/blk000001a6  (
    .C(clk),
    .CE(ce),
    .D(\blk00000001/sig00000218 ),
    .R(sclr),
    .Q(s[34])
  );
  FDRE #(
    .INIT ( 1'b0 ))
  \blk00000001/blk000001a5  (
    .C(clk),
    .CE(ce),
    .D(\blk00000001/sig00000219 ),
    .R(sclr),
    .Q(s[35])
  );
  FDRE #(
    .INIT ( 1'b0 ))
  \blk00000001/blk000001a4  (
    .C(clk),
    .CE(ce),
    .D(\blk00000001/sig0000021a ),
    .R(sclr),
    .Q(s[36])
  );
  FDRE #(
    .INIT ( 1'b0 ))
  \blk00000001/blk000001a3  (
    .C(clk),
    .CE(ce),
    .D(\blk00000001/sig0000021b ),
    .R(sclr),
    .Q(s[37])
  );
  FDRE #(
    .INIT ( 1'b0 ))
  \blk00000001/blk000001a2  (
    .C(clk),
    .CE(ce),
    .D(\blk00000001/sig0000021c ),
    .R(sclr),
    .Q(s[38])
  );
  FDRE #(
    .INIT ( 1'b0 ))
  \blk00000001/blk000001a1  (
    .C(clk),
    .CE(ce),
    .D(\blk00000001/sig0000021d ),
    .R(sclr),
    .Q(s[39])
  );
  FDRE #(
    .INIT ( 1'b0 ))
  \blk00000001/blk000001a0  (
    .C(clk),
    .CE(ce),
    .D(\blk00000001/sig0000021e ),
    .R(sclr),
    .Q(s[40])
  );
  FDRE #(
    .INIT ( 1'b0 ))
  \blk00000001/blk0000019f  (
    .C(clk),
    .CE(ce),
    .D(\blk00000001/sig00000210 ),
    .R(sclr),
    .Q(s[41])
  );
  FDRE #(
    .INIT ( 1'b0 ))
  \blk00000001/blk0000019e  (
    .C(clk),
    .CE(ce),
    .D(\blk00000001/sig00000211 ),
    .R(sclr),
    .Q(s[42])
  );
  FDRE #(
    .INIT ( 1'b0 ))
  \blk00000001/blk0000019d  (
    .C(clk),
    .CE(ce),
    .D(\blk00000001/sig00000212 ),
    .R(sclr),
    .Q(s[43])
  );
  FDRE #(
    .INIT ( 1'b0 ))
  \blk00000001/blk0000019c  (
    .C(clk),
    .CE(ce),
    .D(\blk00000001/sig00000213 ),
    .R(sclr),
    .Q(s[44])
  );
  FDRE #(
    .INIT ( 1'b0 ))
  \blk00000001/blk0000019b  (
    .C(clk),
    .CE(ce),
    .D(\blk00000001/sig00000214 ),
    .R(sclr),
    .Q(s[45])
  );
  FDRE #(
    .INIT ( 1'b0 ))
  \blk00000001/blk0000019a  (
    .C(clk),
    .CE(ce),
    .D(\blk00000001/sig00000215 ),
    .R(sclr),
    .Q(s[46])
  );
  FDRE #(
    .INIT ( 1'b0 ))
  \blk00000001/blk00000199  (
    .C(clk),
    .CE(ce),
    .D(\blk00000001/sig00000216 ),
    .R(sclr),
    .Q(s[47])
  );
  FDRE #(
    .INIT ( 1'b0 ))
  \blk00000001/blk00000198  (
    .C(clk),
    .CE(ce),
    .D(\blk00000001/sig000001e0 ),
    .R(sclr),
    .Q(s[16])
  );
  FDRE #(
    .INIT ( 1'b0 ))
  \blk00000001/blk00000197  (
    .C(clk),
    .CE(ce),
    .D(\blk00000001/sig000001e8 ),
    .R(sclr),
    .Q(s[17])
  );
  FDRE #(
    .INIT ( 1'b0 ))
  \blk00000001/blk00000196  (
    .C(clk),
    .CE(ce),
    .D(\blk00000001/sig000001e9 ),
    .R(sclr),
    .Q(s[18])
  );
  FDRE #(
    .INIT ( 1'b0 ))
  \blk00000001/blk00000195  (
    .C(clk),
    .CE(ce),
    .D(\blk00000001/sig000001ea ),
    .R(sclr),
    .Q(s[19])
  );
  FDRE #(
    .INIT ( 1'b0 ))
  \blk00000001/blk00000194  (
    .C(clk),
    .CE(ce),
    .D(\blk00000001/sig000001eb ),
    .R(sclr),
    .Q(s[20])
  );
  FDRE #(
    .INIT ( 1'b0 ))
  \blk00000001/blk00000193  (
    .C(clk),
    .CE(ce),
    .D(\blk00000001/sig000001ec ),
    .R(sclr),
    .Q(s[21])
  );
  FDRE #(
    .INIT ( 1'b0 ))
  \blk00000001/blk00000192  (
    .C(clk),
    .CE(ce),
    .D(\blk00000001/sig000001ed ),
    .R(sclr),
    .Q(s[22])
  );
  FDRE #(
    .INIT ( 1'b0 ))
  \blk00000001/blk00000191  (
    .C(clk),
    .CE(ce),
    .D(\blk00000001/sig000001ee ),
    .R(sclr),
    .Q(s[23])
  );
  FDRE #(
    .INIT ( 1'b0 ))
  \blk00000001/blk00000190  (
    .C(clk),
    .CE(ce),
    .D(\blk00000001/sig000001ef ),
    .R(sclr),
    .Q(s[24])
  );
  FDRE #(
    .INIT ( 1'b0 ))
  \blk00000001/blk0000018f  (
    .C(clk),
    .CE(ce),
    .D(\blk00000001/sig000001e1 ),
    .R(sclr),
    .Q(s[25])
  );
  FDRE #(
    .INIT ( 1'b0 ))
  \blk00000001/blk0000018e  (
    .C(clk),
    .CE(ce),
    .D(\blk00000001/sig000001e2 ),
    .R(sclr),
    .Q(s[26])
  );
  FDRE #(
    .INIT ( 1'b0 ))
  \blk00000001/blk0000018d  (
    .C(clk),
    .CE(ce),
    .D(\blk00000001/sig000001e3 ),
    .R(sclr),
    .Q(s[27])
  );
  FDRE #(
    .INIT ( 1'b0 ))
  \blk00000001/blk0000018c  (
    .C(clk),
    .CE(ce),
    .D(\blk00000001/sig000001e4 ),
    .R(sclr),
    .Q(s[28])
  );
  FDRE #(
    .INIT ( 1'b0 ))
  \blk00000001/blk0000018b  (
    .C(clk),
    .CE(ce),
    .D(\blk00000001/sig000001e5 ),
    .R(sclr),
    .Q(s[29])
  );
  FDRE #(
    .INIT ( 1'b0 ))
  \blk00000001/blk0000018a  (
    .C(clk),
    .CE(ce),
    .D(\blk00000001/sig000001e6 ),
    .R(sclr),
    .Q(s[30])
  );
  FDRE #(
    .INIT ( 1'b0 ))
  \blk00000001/blk00000189  (
    .C(clk),
    .CE(ce),
    .D(\blk00000001/sig000001e7 ),
    .R(sclr),
    .Q(s[31])
  );
  FDRE #(
    .INIT ( 1'b0 ))
  \blk00000001/blk00000188  (
    .C(clk),
    .CE(ce),
    .D(\blk00000001/sig000001d0 ),
    .R(sclr),
    .Q(\blk00000001/sig00000230 )
  );
  FDRE #(
    .INIT ( 1'b0 ))
  \blk00000001/blk00000187  (
    .C(clk),
    .CE(ce),
    .D(\blk00000001/sig000001d8 ),
    .R(sclr),
    .Q(\blk00000001/sig00000238 )
  );
  FDRE #(
    .INIT ( 1'b0 ))
  \blk00000001/blk00000186  (
    .C(clk),
    .CE(ce),
    .D(\blk00000001/sig000001d9 ),
    .R(sclr),
    .Q(\blk00000001/sig00000239 )
  );
  FDRE #(
    .INIT ( 1'b0 ))
  \blk00000001/blk00000185  (
    .C(clk),
    .CE(ce),
    .D(\blk00000001/sig000001da ),
    .R(sclr),
    .Q(\blk00000001/sig0000023a )
  );
  FDRE #(
    .INIT ( 1'b0 ))
  \blk00000001/blk00000184  (
    .C(clk),
    .CE(ce),
    .D(\blk00000001/sig000001db ),
    .R(sclr),
    .Q(\blk00000001/sig0000023b )
  );
  FDRE #(
    .INIT ( 1'b0 ))
  \blk00000001/blk00000183  (
    .C(clk),
    .CE(ce),
    .D(\blk00000001/sig000001dc ),
    .R(sclr),
    .Q(\blk00000001/sig0000023c )
  );
  FDRE #(
    .INIT ( 1'b0 ))
  \blk00000001/blk00000182  (
    .C(clk),
    .CE(ce),
    .D(\blk00000001/sig000001dd ),
    .R(sclr),
    .Q(\blk00000001/sig0000023d )
  );
  FDRE #(
    .INIT ( 1'b0 ))
  \blk00000001/blk00000181  (
    .C(clk),
    .CE(ce),
    .D(\blk00000001/sig000001de ),
    .R(sclr),
    .Q(\blk00000001/sig0000023e )
  );
  FDRE #(
    .INIT ( 1'b0 ))
  \blk00000001/blk00000180  (
    .C(clk),
    .CE(ce),
    .D(\blk00000001/sig000001df ),
    .R(sclr),
    .Q(\blk00000001/sig0000023f )
  );
  FDRE #(
    .INIT ( 1'b0 ))
  \blk00000001/blk0000017f  (
    .C(clk),
    .CE(ce),
    .D(\blk00000001/sig000001d1 ),
    .R(sclr),
    .Q(\blk00000001/sig00000231 )
  );
  FDRE #(
    .INIT ( 1'b0 ))
  \blk00000001/blk0000017e  (
    .C(clk),
    .CE(ce),
    .D(\blk00000001/sig000001d2 ),
    .R(sclr),
    .Q(\blk00000001/sig00000232 )
  );
  FDRE #(
    .INIT ( 1'b0 ))
  \blk00000001/blk0000017d  (
    .C(clk),
    .CE(ce),
    .D(\blk00000001/sig000001d3 ),
    .R(sclr),
    .Q(\blk00000001/sig00000233 )
  );
  FDRE #(
    .INIT ( 1'b0 ))
  \blk00000001/blk0000017c  (
    .C(clk),
    .CE(ce),
    .D(\blk00000001/sig000001d4 ),
    .R(sclr),
    .Q(\blk00000001/sig00000234 )
  );
  FDRE #(
    .INIT ( 1'b0 ))
  \blk00000001/blk0000017b  (
    .C(clk),
    .CE(ce),
    .D(\blk00000001/sig000001d5 ),
    .R(sclr),
    .Q(\blk00000001/sig00000235 )
  );
  FDRE #(
    .INIT ( 1'b0 ))
  \blk00000001/blk0000017a  (
    .C(clk),
    .CE(ce),
    .D(\blk00000001/sig000001d6 ),
    .R(sclr),
    .Q(\blk00000001/sig00000236 )
  );
  FDRE #(
    .INIT ( 1'b0 ))
  \blk00000001/blk00000179  (
    .C(clk),
    .CE(ce),
    .D(\blk00000001/sig000001d7 ),
    .R(sclr),
    .Q(\blk00000001/sig00000237 )
  );
  FDRE #(
    .INIT ( 1'b0 ))
  \blk00000001/blk00000178  (
    .C(clk),
    .CE(ce),
    .D(\blk00000001/sig0000019e ),
    .R(sclr),
    .Q(\blk00000001/sig000001e0 )
  );
  FDRE #(
    .INIT ( 1'b0 ))
  \blk00000001/blk00000177  (
    .C(clk),
    .CE(ce),
    .D(\blk00000001/sig000001a6 ),
    .R(sclr),
    .Q(\blk00000001/sig000001e8 )
  );
  FDRE #(
    .INIT ( 1'b0 ))
  \blk00000001/blk00000176  (
    .C(clk),
    .CE(ce),
    .D(\blk00000001/sig000001a7 ),
    .R(sclr),
    .Q(\blk00000001/sig000001e9 )
  );
  FDRE #(
    .INIT ( 1'b0 ))
  \blk00000001/blk00000175  (
    .C(clk),
    .CE(ce),
    .D(\blk00000001/sig000001a8 ),
    .R(sclr),
    .Q(\blk00000001/sig000001ea )
  );
  FDRE #(
    .INIT ( 1'b0 ))
  \blk00000001/blk00000174  (
    .C(clk),
    .CE(ce),
    .D(\blk00000001/sig000001a9 ),
    .R(sclr),
    .Q(\blk00000001/sig000001eb )
  );
  FDRE #(
    .INIT ( 1'b0 ))
  \blk00000001/blk00000173  (
    .C(clk),
    .CE(ce),
    .D(\blk00000001/sig000001aa ),
    .R(sclr),
    .Q(\blk00000001/sig000001ec )
  );
  FDRE #(
    .INIT ( 1'b0 ))
  \blk00000001/blk00000172  (
    .C(clk),
    .CE(ce),
    .D(\blk00000001/sig000001ab ),
    .R(sclr),
    .Q(\blk00000001/sig000001ed )
  );
  FDRE #(
    .INIT ( 1'b0 ))
  \blk00000001/blk00000171  (
    .C(clk),
    .CE(ce),
    .D(\blk00000001/sig000001ac ),
    .R(sclr),
    .Q(\blk00000001/sig000001ee )
  );
  FDRE #(
    .INIT ( 1'b0 ))
  \blk00000001/blk00000170  (
    .C(clk),
    .CE(ce),
    .D(\blk00000001/sig000001ad ),
    .R(sclr),
    .Q(\blk00000001/sig000001ef )
  );
  FDRE #(
    .INIT ( 1'b0 ))
  \blk00000001/blk0000016f  (
    .C(clk),
    .CE(ce),
    .D(\blk00000001/sig0000019f ),
    .R(sclr),
    .Q(\blk00000001/sig000001e1 )
  );
  FDRE #(
    .INIT ( 1'b0 ))
  \blk00000001/blk0000016e  (
    .C(clk),
    .CE(ce),
    .D(\blk00000001/sig000001a0 ),
    .R(sclr),
    .Q(\blk00000001/sig000001e2 )
  );
  FDRE #(
    .INIT ( 1'b0 ))
  \blk00000001/blk0000016d  (
    .C(clk),
    .CE(ce),
    .D(\blk00000001/sig000001a1 ),
    .R(sclr),
    .Q(\blk00000001/sig000001e3 )
  );
  FDRE #(
    .INIT ( 1'b0 ))
  \blk00000001/blk0000016c  (
    .C(clk),
    .CE(ce),
    .D(\blk00000001/sig000001a2 ),
    .R(sclr),
    .Q(\blk00000001/sig000001e4 )
  );
  FDRE #(
    .INIT ( 1'b0 ))
  \blk00000001/blk0000016b  (
    .C(clk),
    .CE(ce),
    .D(\blk00000001/sig000001a3 ),
    .R(sclr),
    .Q(\blk00000001/sig000001e5 )
  );
  FDRE #(
    .INIT ( 1'b0 ))
  \blk00000001/blk0000016a  (
    .C(clk),
    .CE(ce),
    .D(\blk00000001/sig000001a4 ),
    .R(sclr),
    .Q(\blk00000001/sig000001e6 )
  );
  FDRE #(
    .INIT ( 1'b0 ))
  \blk00000001/blk00000169  (
    .C(clk),
    .CE(ce),
    .D(\blk00000001/sig000001a5 ),
    .R(sclr),
    .Q(\blk00000001/sig000001e7 )
  );
  FDRE #(
    .INIT ( 1'b0 ))
  \blk00000001/blk00000168  (
    .C(clk),
    .CE(ce),
    .D(\blk00000001/sig0000015f ),
    .R(sclr),
    .Q(\blk00000001/sig000001d0 )
  );
  FDRE #(
    .INIT ( 1'b0 ))
  \blk00000001/blk00000167  (
    .C(clk),
    .CE(ce),
    .D(\blk00000001/sig00000167 ),
    .R(sclr),
    .Q(\blk00000001/sig000001d8 )
  );
  FDRE #(
    .INIT ( 1'b0 ))
  \blk00000001/blk00000166  (
    .C(clk),
    .CE(ce),
    .D(\blk00000001/sig00000168 ),
    .R(sclr),
    .Q(\blk00000001/sig000001d9 )
  );
  FDRE #(
    .INIT ( 1'b0 ))
  \blk00000001/blk00000165  (
    .C(clk),
    .CE(ce),
    .D(\blk00000001/sig00000169 ),
    .R(sclr),
    .Q(\blk00000001/sig000001da )
  );
  FDRE #(
    .INIT ( 1'b0 ))
  \blk00000001/blk00000164  (
    .C(clk),
    .CE(ce),
    .D(\blk00000001/sig0000016a ),
    .R(sclr),
    .Q(\blk00000001/sig000001db )
  );
  FDRE #(
    .INIT ( 1'b0 ))
  \blk00000001/blk00000163  (
    .C(clk),
    .CE(ce),
    .D(\blk00000001/sig0000016b ),
    .R(sclr),
    .Q(\blk00000001/sig000001dc )
  );
  FDRE #(
    .INIT ( 1'b0 ))
  \blk00000001/blk00000162  (
    .C(clk),
    .CE(ce),
    .D(\blk00000001/sig0000016c ),
    .R(sclr),
    .Q(\blk00000001/sig000001dd )
  );
  FDRE #(
    .INIT ( 1'b0 ))
  \blk00000001/blk00000161  (
    .C(clk),
    .CE(ce),
    .D(\blk00000001/sig0000016d ),
    .R(sclr),
    .Q(\blk00000001/sig000001de )
  );
  FDRE #(
    .INIT ( 1'b0 ))
  \blk00000001/blk00000160  (
    .C(clk),
    .CE(ce),
    .D(\blk00000001/sig0000016e ),
    .R(sclr),
    .Q(\blk00000001/sig000001df )
  );
  FDRE #(
    .INIT ( 1'b0 ))
  \blk00000001/blk0000015f  (
    .C(clk),
    .CE(ce),
    .D(\blk00000001/sig00000160 ),
    .R(sclr),
    .Q(\blk00000001/sig000001d1 )
  );
  FDRE #(
    .INIT ( 1'b0 ))
  \blk00000001/blk0000015e  (
    .C(clk),
    .CE(ce),
    .D(\blk00000001/sig00000161 ),
    .R(sclr),
    .Q(\blk00000001/sig000001d2 )
  );
  FDRE #(
    .INIT ( 1'b0 ))
  \blk00000001/blk0000015d  (
    .C(clk),
    .CE(ce),
    .D(\blk00000001/sig00000162 ),
    .R(sclr),
    .Q(\blk00000001/sig000001d3 )
  );
  FDRE #(
    .INIT ( 1'b0 ))
  \blk00000001/blk0000015c  (
    .C(clk),
    .CE(ce),
    .D(\blk00000001/sig00000163 ),
    .R(sclr),
    .Q(\blk00000001/sig000001d4 )
  );
  FDRE #(
    .INIT ( 1'b0 ))
  \blk00000001/blk0000015b  (
    .C(clk),
    .CE(ce),
    .D(\blk00000001/sig00000164 ),
    .R(sclr),
    .Q(\blk00000001/sig000001d5 )
  );
  FDRE #(
    .INIT ( 1'b0 ))
  \blk00000001/blk0000015a  (
    .C(clk),
    .CE(ce),
    .D(\blk00000001/sig00000165 ),
    .R(sclr),
    .Q(\blk00000001/sig000001d6 )
  );
  FDRE #(
    .INIT ( 1'b0 ))
  \blk00000001/blk00000159  (
    .C(clk),
    .CE(ce),
    .D(\blk00000001/sig00000166 ),
    .R(sclr),
    .Q(\blk00000001/sig000001d7 )
  );
  FDRE #(
    .INIT ( 1'b0 ))
  \blk00000001/blk00000158  (
    .C(clk),
    .CE(ce),
    .D(\blk00000001/sig0000011f ),
    .R(sclr),
    .Q(\blk00000001/sig000001c0 )
  );
  FDRE #(
    .INIT ( 1'b0 ))
  \blk00000001/blk00000157  (
    .C(clk),
    .CE(ce),
    .D(\blk00000001/sig00000127 ),
    .R(sclr),
    .Q(\blk00000001/sig000001c8 )
  );
  FDRE #(
    .INIT ( 1'b0 ))
  \blk00000001/blk00000156  (
    .C(clk),
    .CE(ce),
    .D(\blk00000001/sig00000128 ),
    .R(sclr),
    .Q(\blk00000001/sig000001c9 )
  );
  FDRE #(
    .INIT ( 1'b0 ))
  \blk00000001/blk00000155  (
    .C(clk),
    .CE(ce),
    .D(\blk00000001/sig00000129 ),
    .R(sclr),
    .Q(\blk00000001/sig000001ca )
  );
  FDRE #(
    .INIT ( 1'b0 ))
  \blk00000001/blk00000154  (
    .C(clk),
    .CE(ce),
    .D(\blk00000001/sig0000012a ),
    .R(sclr),
    .Q(\blk00000001/sig000001cb )
  );
  FDRE #(
    .INIT ( 1'b0 ))
  \blk00000001/blk00000153  (
    .C(clk),
    .CE(ce),
    .D(\blk00000001/sig0000012b ),
    .R(sclr),
    .Q(\blk00000001/sig000001cc )
  );
  FDRE #(
    .INIT ( 1'b0 ))
  \blk00000001/blk00000152  (
    .C(clk),
    .CE(ce),
    .D(\blk00000001/sig0000012c ),
    .R(sclr),
    .Q(\blk00000001/sig000001cd )
  );
  FDRE #(
    .INIT ( 1'b0 ))
  \blk00000001/blk00000151  (
    .C(clk),
    .CE(ce),
    .D(\blk00000001/sig0000012d ),
    .R(sclr),
    .Q(\blk00000001/sig000001ce )
  );
  FDRE #(
    .INIT ( 1'b0 ))
  \blk00000001/blk00000150  (
    .C(clk),
    .CE(ce),
    .D(\blk00000001/sig0000012e ),
    .R(sclr),
    .Q(\blk00000001/sig000001cf )
  );
  FDRE #(
    .INIT ( 1'b0 ))
  \blk00000001/blk0000014f  (
    .C(clk),
    .CE(ce),
    .D(\blk00000001/sig00000120 ),
    .R(sclr),
    .Q(\blk00000001/sig000001c1 )
  );
  FDRE #(
    .INIT ( 1'b0 ))
  \blk00000001/blk0000014e  (
    .C(clk),
    .CE(ce),
    .D(\blk00000001/sig00000121 ),
    .R(sclr),
    .Q(\blk00000001/sig000001c2 )
  );
  FDRE #(
    .INIT ( 1'b0 ))
  \blk00000001/blk0000014d  (
    .C(clk),
    .CE(ce),
    .D(\blk00000001/sig00000122 ),
    .R(sclr),
    .Q(\blk00000001/sig000001c3 )
  );
  FDRE #(
    .INIT ( 1'b0 ))
  \blk00000001/blk0000014c  (
    .C(clk),
    .CE(ce),
    .D(\blk00000001/sig00000123 ),
    .R(sclr),
    .Q(\blk00000001/sig000001c4 )
  );
  FDRE #(
    .INIT ( 1'b0 ))
  \blk00000001/blk0000014b  (
    .C(clk),
    .CE(ce),
    .D(\blk00000001/sig00000124 ),
    .R(sclr),
    .Q(\blk00000001/sig000001c5 )
  );
  FDRE #(
    .INIT ( 1'b0 ))
  \blk00000001/blk0000014a  (
    .C(clk),
    .CE(ce),
    .D(\blk00000001/sig00000125 ),
    .R(sclr),
    .Q(\blk00000001/sig000001c6 )
  );
  FDRE #(
    .INIT ( 1'b0 ))
  \blk00000001/blk00000149  (
    .C(clk),
    .CE(ce),
    .D(\blk00000001/sig00000126 ),
    .R(sclr),
    .Q(\blk00000001/sig000001c7 )
  );
  FDSE #(
    .INIT ( 1'b1 ))
  \blk00000001/blk00000148  (
    .C(clk),
    .CE(ce),
    .D(\blk00000001/sig0000008c ),
    .S(\blk00000001/sig00000083 ),
    .Q(\blk00000001/sig0000022f )
  );
  FDSE #(
    .INIT ( 1'b1 ))
  \blk00000001/blk00000147  (
    .C(clk),
    .CE(ce),
    .D(\blk00000001/sig0000008b ),
    .S(\blk00000001/sig00000083 ),
    .Q(\blk00000001/sig000001be )
  );
  FDRE #(
    .INIT ( 1'b0 ))
  \blk00000001/blk00000146  (
    .C(clk),
    .CE(ce),
    .D(\blk00000001/sig0000012f ),
    .R(sclr),
    .Q(\blk00000001/sig000001bf )
  );
  MUXCY   \blk00000001/blk00000145  (
    .CI(\blk00000001/sig00000084 ),
    .DI(a[32]),
    .S(\blk00000001/sig0000010f ),
    .O(\blk00000001/sig000000ff )
  );
  XORCY   \blk00000001/blk00000144  (
    .CI(\blk00000001/sig00000084 ),
    .LI(\blk00000001/sig0000010f ),
    .O(\blk00000001/sig00000130 )
  );
  XORCY   \blk00000001/blk00000143  (
    .CI(\blk00000001/sig00000104 ),
    .LI(\blk00000001/sig00000115 ),
    .O(\blk00000001/sig00000136 )
  );
  MUXCY   \blk00000001/blk00000142  (
    .CI(\blk00000001/sig00000104 ),
    .DI(a[47]),
    .S(\blk00000001/sig00000115 ),
    .O(\blk00000001/sig00000105 )
  );
  MUXCY   \blk00000001/blk00000141  (
    .CI(\blk00000001/sig000000ff ),
    .DI(a[33]),
    .S(\blk00000001/sig00000116 ),
    .O(\blk00000001/sig00000106 )
  );
  XORCY   \blk00000001/blk00000140  (
    .CI(\blk00000001/sig000000ff ),
    .LI(\blk00000001/sig00000116 ),
    .O(\blk00000001/sig00000137 )
  );
  MUXCY   \blk00000001/blk0000013f  (
    .CI(\blk00000001/sig00000106 ),
    .DI(a[34]),
    .S(\blk00000001/sig00000117 ),
    .O(\blk00000001/sig00000107 )
  );
  XORCY   \blk00000001/blk0000013e  (
    .CI(\blk00000001/sig00000106 ),
    .LI(\blk00000001/sig00000117 ),
    .O(\blk00000001/sig00000138 )
  );
  MUXCY   \blk00000001/blk0000013d  (
    .CI(\blk00000001/sig00000107 ),
    .DI(a[35]),
    .S(\blk00000001/sig00000118 ),
    .O(\blk00000001/sig00000108 )
  );
  XORCY   \blk00000001/blk0000013c  (
    .CI(\blk00000001/sig00000107 ),
    .LI(\blk00000001/sig00000118 ),
    .O(\blk00000001/sig00000139 )
  );
  MUXCY   \blk00000001/blk0000013b  (
    .CI(\blk00000001/sig00000108 ),
    .DI(a[36]),
    .S(\blk00000001/sig00000119 ),
    .O(\blk00000001/sig00000109 )
  );
  XORCY   \blk00000001/blk0000013a  (
    .CI(\blk00000001/sig00000108 ),
    .LI(\blk00000001/sig00000119 ),
    .O(\blk00000001/sig0000013a )
  );
  MUXCY   \blk00000001/blk00000139  (
    .CI(\blk00000001/sig00000109 ),
    .DI(a[37]),
    .S(\blk00000001/sig0000011a ),
    .O(\blk00000001/sig0000010a )
  );
  XORCY   \blk00000001/blk00000138  (
    .CI(\blk00000001/sig00000109 ),
    .LI(\blk00000001/sig0000011a ),
    .O(\blk00000001/sig0000013b )
  );
  MUXCY   \blk00000001/blk00000137  (
    .CI(\blk00000001/sig0000010a ),
    .DI(a[38]),
    .S(\blk00000001/sig0000011b ),
    .O(\blk00000001/sig0000010b )
  );
  XORCY   \blk00000001/blk00000136  (
    .CI(\blk00000001/sig0000010a ),
    .LI(\blk00000001/sig0000011b ),
    .O(\blk00000001/sig0000013c )
  );
  MUXCY   \blk00000001/blk00000135  (
    .CI(\blk00000001/sig0000010b ),
    .DI(a[39]),
    .S(\blk00000001/sig0000011c ),
    .O(\blk00000001/sig0000010c )
  );
  XORCY   \blk00000001/blk00000134  (
    .CI(\blk00000001/sig0000010b ),
    .LI(\blk00000001/sig0000011c ),
    .O(\blk00000001/sig0000013d )
  );
  MUXCY   \blk00000001/blk00000133  (
    .CI(\blk00000001/sig0000010c ),
    .DI(a[40]),
    .S(\blk00000001/sig0000011d ),
    .O(\blk00000001/sig0000010d )
  );
  XORCY   \blk00000001/blk00000132  (
    .CI(\blk00000001/sig0000010c ),
    .LI(\blk00000001/sig0000011d ),
    .O(\blk00000001/sig0000013e )
  );
  MUXCY   \blk00000001/blk00000131  (
    .CI(\blk00000001/sig0000010d ),
    .DI(a[41]),
    .S(\blk00000001/sig0000011e ),
    .O(\blk00000001/sig0000010e )
  );
  XORCY   \blk00000001/blk00000130  (
    .CI(\blk00000001/sig0000010d ),
    .LI(\blk00000001/sig0000011e ),
    .O(\blk00000001/sig0000013f )
  );
  MUXCY   \blk00000001/blk0000012f  (
    .CI(\blk00000001/sig0000010e ),
    .DI(a[42]),
    .S(\blk00000001/sig00000110 ),
    .O(\blk00000001/sig00000100 )
  );
  XORCY   \blk00000001/blk0000012e  (
    .CI(\blk00000001/sig0000010e ),
    .LI(\blk00000001/sig00000110 ),
    .O(\blk00000001/sig00000131 )
  );
  MUXCY   \blk00000001/blk0000012d  (
    .CI(\blk00000001/sig00000100 ),
    .DI(a[43]),
    .S(\blk00000001/sig00000111 ),
    .O(\blk00000001/sig00000101 )
  );
  XORCY   \blk00000001/blk0000012c  (
    .CI(\blk00000001/sig00000100 ),
    .LI(\blk00000001/sig00000111 ),
    .O(\blk00000001/sig00000132 )
  );
  MUXCY   \blk00000001/blk0000012b  (
    .CI(\blk00000001/sig00000101 ),
    .DI(a[44]),
    .S(\blk00000001/sig00000112 ),
    .O(\blk00000001/sig00000102 )
  );
  XORCY   \blk00000001/blk0000012a  (
    .CI(\blk00000001/sig00000101 ),
    .LI(\blk00000001/sig00000112 ),
    .O(\blk00000001/sig00000133 )
  );
  MUXCY   \blk00000001/blk00000129  (
    .CI(\blk00000001/sig00000102 ),
    .DI(a[45]),
    .S(\blk00000001/sig00000113 ),
    .O(\blk00000001/sig00000103 )
  );
  XORCY   \blk00000001/blk00000128  (
    .CI(\blk00000001/sig00000102 ),
    .LI(\blk00000001/sig00000113 ),
    .O(\blk00000001/sig00000134 )
  );
  MUXCY   \blk00000001/blk00000127  (
    .CI(\blk00000001/sig00000103 ),
    .DI(a[46]),
    .S(\blk00000001/sig00000114 ),
    .O(\blk00000001/sig00000104 )
  );
  XORCY   \blk00000001/blk00000126  (
    .CI(\blk00000001/sig00000103 ),
    .LI(\blk00000001/sig00000114 ),
    .O(\blk00000001/sig00000135 )
  );
  FDSE #(
    .INIT ( 1'b1 ))
  \blk00000001/blk00000125  (
    .C(clk),
    .CE(ce),
    .D(\blk00000001/sig00000105 ),
    .S(sclr),
    .Q(\blk00000001/sig0000012f )
  );
  FDRE #(
    .INIT ( 1'b0 ))
  \blk00000001/blk00000124  (
    .C(clk),
    .CE(ce),
    .D(\blk00000001/sig00000130 ),
    .R(sclr),
    .Q(\blk00000001/sig0000011f )
  );
  FDRE #(
    .INIT ( 1'b0 ))
  \blk00000001/blk00000123  (
    .C(clk),
    .CE(ce),
    .D(\blk00000001/sig00000137 ),
    .R(sclr),
    .Q(\blk00000001/sig00000127 )
  );
  FDRE #(
    .INIT ( 1'b0 ))
  \blk00000001/blk00000122  (
    .C(clk),
    .CE(ce),
    .D(\blk00000001/sig00000138 ),
    .R(sclr),
    .Q(\blk00000001/sig00000128 )
  );
  FDRE #(
    .INIT ( 1'b0 ))
  \blk00000001/blk00000121  (
    .C(clk),
    .CE(ce),
    .D(\blk00000001/sig00000139 ),
    .R(sclr),
    .Q(\blk00000001/sig00000129 )
  );
  FDRE #(
    .INIT ( 1'b0 ))
  \blk00000001/blk00000120  (
    .C(clk),
    .CE(ce),
    .D(\blk00000001/sig0000013a ),
    .R(sclr),
    .Q(\blk00000001/sig0000012a )
  );
  FDRE #(
    .INIT ( 1'b0 ))
  \blk00000001/blk0000011f  (
    .C(clk),
    .CE(ce),
    .D(\blk00000001/sig0000013b ),
    .R(sclr),
    .Q(\blk00000001/sig0000012b )
  );
  FDRE #(
    .INIT ( 1'b0 ))
  \blk00000001/blk0000011e  (
    .C(clk),
    .CE(ce),
    .D(\blk00000001/sig0000013c ),
    .R(sclr),
    .Q(\blk00000001/sig0000012c )
  );
  FDRE #(
    .INIT ( 1'b0 ))
  \blk00000001/blk0000011d  (
    .C(clk),
    .CE(ce),
    .D(\blk00000001/sig0000013d ),
    .R(sclr),
    .Q(\blk00000001/sig0000012d )
  );
  FDRE #(
    .INIT ( 1'b0 ))
  \blk00000001/blk0000011c  (
    .C(clk),
    .CE(ce),
    .D(\blk00000001/sig0000013e ),
    .R(sclr),
    .Q(\blk00000001/sig0000012e )
  );
  FDRE #(
    .INIT ( 1'b0 ))
  \blk00000001/blk0000011b  (
    .C(clk),
    .CE(ce),
    .D(\blk00000001/sig0000013f ),
    .R(sclr),
    .Q(\blk00000001/sig00000120 )
  );
  FDRE #(
    .INIT ( 1'b0 ))
  \blk00000001/blk0000011a  (
    .C(clk),
    .CE(ce),
    .D(\blk00000001/sig00000131 ),
    .R(sclr),
    .Q(\blk00000001/sig00000121 )
  );
  FDRE #(
    .INIT ( 1'b0 ))
  \blk00000001/blk00000119  (
    .C(clk),
    .CE(ce),
    .D(\blk00000001/sig00000132 ),
    .R(sclr),
    .Q(\blk00000001/sig00000122 )
  );
  FDRE #(
    .INIT ( 1'b0 ))
  \blk00000001/blk00000118  (
    .C(clk),
    .CE(ce),
    .D(\blk00000001/sig00000133 ),
    .R(sclr),
    .Q(\blk00000001/sig00000123 )
  );
  FDRE #(
    .INIT ( 1'b0 ))
  \blk00000001/blk00000117  (
    .C(clk),
    .CE(ce),
    .D(\blk00000001/sig00000134 ),
    .R(sclr),
    .Q(\blk00000001/sig00000124 )
  );
  FDRE #(
    .INIT ( 1'b0 ))
  \blk00000001/blk00000116  (
    .C(clk),
    .CE(ce),
    .D(\blk00000001/sig00000135 ),
    .R(sclr),
    .Q(\blk00000001/sig00000125 )
  );
  FDRE #(
    .INIT ( 1'b0 ))
  \blk00000001/blk00000115  (
    .C(clk),
    .CE(ce),
    .D(\blk00000001/sig00000136 ),
    .R(sclr),
    .Q(\blk00000001/sig00000126 )
  );
  MUXCY   \blk00000001/blk00000114  (
    .CI(\blk00000001/sig00000084 ),
    .DI(a[16]),
    .S(\blk00000001/sig000000ce ),
    .O(\blk00000001/sig000000be )
  );
  XORCY   \blk00000001/blk00000113  (
    .CI(\blk00000001/sig00000084 ),
    .LI(\blk00000001/sig000000ce ),
    .O(\blk00000001/sig000000ef )
  );
  XORCY   \blk00000001/blk00000112  (
    .CI(\blk00000001/sig000000c3 ),
    .LI(\blk00000001/sig000000d4 ),
    .O(\blk00000001/sig000000f5 )
  );
  MUXCY   \blk00000001/blk00000111  (
    .CI(\blk00000001/sig000000c3 ),
    .DI(a[31]),
    .S(\blk00000001/sig000000d4 ),
    .O(\blk00000001/sig000000c4 )
  );
  MUXCY   \blk00000001/blk00000110  (
    .CI(\blk00000001/sig000000be ),
    .DI(a[17]),
    .S(\blk00000001/sig000000d5 ),
    .O(\blk00000001/sig000000c5 )
  );
  XORCY   \blk00000001/blk0000010f  (
    .CI(\blk00000001/sig000000be ),
    .LI(\blk00000001/sig000000d5 ),
    .O(\blk00000001/sig000000f6 )
  );
  MUXCY   \blk00000001/blk0000010e  (
    .CI(\blk00000001/sig000000c5 ),
    .DI(a[18]),
    .S(\blk00000001/sig000000d6 ),
    .O(\blk00000001/sig000000c6 )
  );
  XORCY   \blk00000001/blk0000010d  (
    .CI(\blk00000001/sig000000c5 ),
    .LI(\blk00000001/sig000000d6 ),
    .O(\blk00000001/sig000000f7 )
  );
  MUXCY   \blk00000001/blk0000010c  (
    .CI(\blk00000001/sig000000c6 ),
    .DI(a[19]),
    .S(\blk00000001/sig000000d7 ),
    .O(\blk00000001/sig000000c7 )
  );
  XORCY   \blk00000001/blk0000010b  (
    .CI(\blk00000001/sig000000c6 ),
    .LI(\blk00000001/sig000000d7 ),
    .O(\blk00000001/sig000000f8 )
  );
  MUXCY   \blk00000001/blk0000010a  (
    .CI(\blk00000001/sig000000c7 ),
    .DI(a[20]),
    .S(\blk00000001/sig000000d8 ),
    .O(\blk00000001/sig000000c8 )
  );
  XORCY   \blk00000001/blk00000109  (
    .CI(\blk00000001/sig000000c7 ),
    .LI(\blk00000001/sig000000d8 ),
    .O(\blk00000001/sig000000f9 )
  );
  MUXCY   \blk00000001/blk00000108  (
    .CI(\blk00000001/sig000000c8 ),
    .DI(a[21]),
    .S(\blk00000001/sig000000d9 ),
    .O(\blk00000001/sig000000c9 )
  );
  XORCY   \blk00000001/blk00000107  (
    .CI(\blk00000001/sig000000c8 ),
    .LI(\blk00000001/sig000000d9 ),
    .O(\blk00000001/sig000000fa )
  );
  MUXCY   \blk00000001/blk00000106  (
    .CI(\blk00000001/sig000000c9 ),
    .DI(a[22]),
    .S(\blk00000001/sig000000da ),
    .O(\blk00000001/sig000000ca )
  );
  XORCY   \blk00000001/blk00000105  (
    .CI(\blk00000001/sig000000c9 ),
    .LI(\blk00000001/sig000000da ),
    .O(\blk00000001/sig000000fb )
  );
  MUXCY   \blk00000001/blk00000104  (
    .CI(\blk00000001/sig000000ca ),
    .DI(a[23]),
    .S(\blk00000001/sig000000db ),
    .O(\blk00000001/sig000000cb )
  );
  XORCY   \blk00000001/blk00000103  (
    .CI(\blk00000001/sig000000ca ),
    .LI(\blk00000001/sig000000db ),
    .O(\blk00000001/sig000000fc )
  );
  MUXCY   \blk00000001/blk00000102  (
    .CI(\blk00000001/sig000000cb ),
    .DI(a[24]),
    .S(\blk00000001/sig000000dc ),
    .O(\blk00000001/sig000000cc )
  );
  XORCY   \blk00000001/blk00000101  (
    .CI(\blk00000001/sig000000cb ),
    .LI(\blk00000001/sig000000dc ),
    .O(\blk00000001/sig000000fd )
  );
  MUXCY   \blk00000001/blk00000100  (
    .CI(\blk00000001/sig000000cc ),
    .DI(a[25]),
    .S(\blk00000001/sig000000dd ),
    .O(\blk00000001/sig000000cd )
  );
  XORCY   \blk00000001/blk000000ff  (
    .CI(\blk00000001/sig000000cc ),
    .LI(\blk00000001/sig000000dd ),
    .O(\blk00000001/sig000000fe )
  );
  MUXCY   \blk00000001/blk000000fe  (
    .CI(\blk00000001/sig000000cd ),
    .DI(a[26]),
    .S(\blk00000001/sig000000cf ),
    .O(\blk00000001/sig000000bf )
  );
  XORCY   \blk00000001/blk000000fd  (
    .CI(\blk00000001/sig000000cd ),
    .LI(\blk00000001/sig000000cf ),
    .O(\blk00000001/sig000000f0 )
  );
  MUXCY   \blk00000001/blk000000fc  (
    .CI(\blk00000001/sig000000bf ),
    .DI(a[27]),
    .S(\blk00000001/sig000000d0 ),
    .O(\blk00000001/sig000000c0 )
  );
  XORCY   \blk00000001/blk000000fb  (
    .CI(\blk00000001/sig000000bf ),
    .LI(\blk00000001/sig000000d0 ),
    .O(\blk00000001/sig000000f1 )
  );
  MUXCY   \blk00000001/blk000000fa  (
    .CI(\blk00000001/sig000000c0 ),
    .DI(a[28]),
    .S(\blk00000001/sig000000d1 ),
    .O(\blk00000001/sig000000c1 )
  );
  XORCY   \blk00000001/blk000000f9  (
    .CI(\blk00000001/sig000000c0 ),
    .LI(\blk00000001/sig000000d1 ),
    .O(\blk00000001/sig000000f2 )
  );
  MUXCY   \blk00000001/blk000000f8  (
    .CI(\blk00000001/sig000000c1 ),
    .DI(a[29]),
    .S(\blk00000001/sig000000d2 ),
    .O(\blk00000001/sig000000c2 )
  );
  XORCY   \blk00000001/blk000000f7  (
    .CI(\blk00000001/sig000000c1 ),
    .LI(\blk00000001/sig000000d2 ),
    .O(\blk00000001/sig000000f3 )
  );
  MUXCY   \blk00000001/blk000000f6  (
    .CI(\blk00000001/sig000000c2 ),
    .DI(a[30]),
    .S(\blk00000001/sig000000d3 ),
    .O(\blk00000001/sig000000c3 )
  );
  XORCY   \blk00000001/blk000000f5  (
    .CI(\blk00000001/sig000000c2 ),
    .LI(\blk00000001/sig000000d3 ),
    .O(\blk00000001/sig000000f4 )
  );
  FDSE #(
    .INIT ( 1'b1 ))
  \blk00000001/blk000000f4  (
    .C(clk),
    .CE(ce),
    .D(\blk00000001/sig000000c4 ),
    .S(sclr),
    .Q(\blk00000001/sig000000ee )
  );
  FDRE #(
    .INIT ( 1'b0 ))
  \blk00000001/blk000000f3  (
    .C(clk),
    .CE(ce),
    .D(\blk00000001/sig000000ef ),
    .R(sclr),
    .Q(\blk00000001/sig000000de )
  );
  FDRE #(
    .INIT ( 1'b0 ))
  \blk00000001/blk000000f2  (
    .C(clk),
    .CE(ce),
    .D(\blk00000001/sig000000f6 ),
    .R(sclr),
    .Q(\blk00000001/sig000000e6 )
  );
  FDRE #(
    .INIT ( 1'b0 ))
  \blk00000001/blk000000f1  (
    .C(clk),
    .CE(ce),
    .D(\blk00000001/sig000000f7 ),
    .R(sclr),
    .Q(\blk00000001/sig000000e7 )
  );
  FDRE #(
    .INIT ( 1'b0 ))
  \blk00000001/blk000000f0  (
    .C(clk),
    .CE(ce),
    .D(\blk00000001/sig000000f8 ),
    .R(sclr),
    .Q(\blk00000001/sig000000e8 )
  );
  FDRE #(
    .INIT ( 1'b0 ))
  \blk00000001/blk000000ef  (
    .C(clk),
    .CE(ce),
    .D(\blk00000001/sig000000f9 ),
    .R(sclr),
    .Q(\blk00000001/sig000000e9 )
  );
  FDRE #(
    .INIT ( 1'b0 ))
  \blk00000001/blk000000ee  (
    .C(clk),
    .CE(ce),
    .D(\blk00000001/sig000000fa ),
    .R(sclr),
    .Q(\blk00000001/sig000000ea )
  );
  FDRE #(
    .INIT ( 1'b0 ))
  \blk00000001/blk000000ed  (
    .C(clk),
    .CE(ce),
    .D(\blk00000001/sig000000fb ),
    .R(sclr),
    .Q(\blk00000001/sig000000eb )
  );
  FDRE #(
    .INIT ( 1'b0 ))
  \blk00000001/blk000000ec  (
    .C(clk),
    .CE(ce),
    .D(\blk00000001/sig000000fc ),
    .R(sclr),
    .Q(\blk00000001/sig000000ec )
  );
  FDRE #(
    .INIT ( 1'b0 ))
  \blk00000001/blk000000eb  (
    .C(clk),
    .CE(ce),
    .D(\blk00000001/sig000000fd ),
    .R(sclr),
    .Q(\blk00000001/sig000000ed )
  );
  FDRE #(
    .INIT ( 1'b0 ))
  \blk00000001/blk000000ea  (
    .C(clk),
    .CE(ce),
    .D(\blk00000001/sig000000fe ),
    .R(sclr),
    .Q(\blk00000001/sig000000df )
  );
  FDRE #(
    .INIT ( 1'b0 ))
  \blk00000001/blk000000e9  (
    .C(clk),
    .CE(ce),
    .D(\blk00000001/sig000000f0 ),
    .R(sclr),
    .Q(\blk00000001/sig000000e0 )
  );
  FDRE #(
    .INIT ( 1'b0 ))
  \blk00000001/blk000000e8  (
    .C(clk),
    .CE(ce),
    .D(\blk00000001/sig000000f1 ),
    .R(sclr),
    .Q(\blk00000001/sig000000e1 )
  );
  FDRE #(
    .INIT ( 1'b0 ))
  \blk00000001/blk000000e7  (
    .C(clk),
    .CE(ce),
    .D(\blk00000001/sig000000f2 ),
    .R(sclr),
    .Q(\blk00000001/sig000000e2 )
  );
  FDRE #(
    .INIT ( 1'b0 ))
  \blk00000001/blk000000e6  (
    .C(clk),
    .CE(ce),
    .D(\blk00000001/sig000000f3 ),
    .R(sclr),
    .Q(\blk00000001/sig000000e3 )
  );
  FDRE #(
    .INIT ( 1'b0 ))
  \blk00000001/blk000000e5  (
    .C(clk),
    .CE(ce),
    .D(\blk00000001/sig000000f4 ),
    .R(sclr),
    .Q(\blk00000001/sig000000e4 )
  );
  FDRE #(
    .INIT ( 1'b0 ))
  \blk00000001/blk000000e4  (
    .C(clk),
    .CE(ce),
    .D(\blk00000001/sig000000f5 ),
    .R(sclr),
    .Q(\blk00000001/sig000000e5 )
  );
  MUXCY   \blk00000001/blk000000e3  (
    .CI(\blk00000001/sig00000084 ),
    .DI(a[0]),
    .S(\blk00000001/sig0000009d ),
    .O(\blk00000001/sig0000008d )
  );
  XORCY   \blk00000001/blk000000e2  (
    .CI(\blk00000001/sig00000084 ),
    .LI(\blk00000001/sig0000009d ),
    .O(\blk00000001/sig000000ae )
  );
  XORCY   \blk00000001/blk000000e1  (
    .CI(\blk00000001/sig00000092 ),
    .LI(\blk00000001/sig000000a3 ),
    .O(\blk00000001/sig000000b4 )
  );
  MUXCY   \blk00000001/blk000000e0  (
    .CI(\blk00000001/sig00000092 ),
    .DI(a[15]),
    .S(\blk00000001/sig000000a3 ),
    .O(\blk00000001/sig00000093 )
  );
  MUXCY   \blk00000001/blk000000df  (
    .CI(\blk00000001/sig0000008d ),
    .DI(a[1]),
    .S(\blk00000001/sig000000a4 ),
    .O(\blk00000001/sig00000094 )
  );
  XORCY   \blk00000001/blk000000de  (
    .CI(\blk00000001/sig0000008d ),
    .LI(\blk00000001/sig000000a4 ),
    .O(\blk00000001/sig000000b5 )
  );
  MUXCY   \blk00000001/blk000000dd  (
    .CI(\blk00000001/sig00000094 ),
    .DI(a[2]),
    .S(\blk00000001/sig000000a5 ),
    .O(\blk00000001/sig00000095 )
  );
  XORCY   \blk00000001/blk000000dc  (
    .CI(\blk00000001/sig00000094 ),
    .LI(\blk00000001/sig000000a5 ),
    .O(\blk00000001/sig000000b6 )
  );
  MUXCY   \blk00000001/blk000000db  (
    .CI(\blk00000001/sig00000095 ),
    .DI(a[3]),
    .S(\blk00000001/sig000000a6 ),
    .O(\blk00000001/sig00000096 )
  );
  XORCY   \blk00000001/blk000000da  (
    .CI(\blk00000001/sig00000095 ),
    .LI(\blk00000001/sig000000a6 ),
    .O(\blk00000001/sig000000b7 )
  );
  MUXCY   \blk00000001/blk000000d9  (
    .CI(\blk00000001/sig00000096 ),
    .DI(a[4]),
    .S(\blk00000001/sig000000a7 ),
    .O(\blk00000001/sig00000097 )
  );
  XORCY   \blk00000001/blk000000d8  (
    .CI(\blk00000001/sig00000096 ),
    .LI(\blk00000001/sig000000a7 ),
    .O(\blk00000001/sig000000b8 )
  );
  MUXCY   \blk00000001/blk000000d7  (
    .CI(\blk00000001/sig00000097 ),
    .DI(a[5]),
    .S(\blk00000001/sig000000a8 ),
    .O(\blk00000001/sig00000098 )
  );
  XORCY   \blk00000001/blk000000d6  (
    .CI(\blk00000001/sig00000097 ),
    .LI(\blk00000001/sig000000a8 ),
    .O(\blk00000001/sig000000b9 )
  );
  MUXCY   \blk00000001/blk000000d5  (
    .CI(\blk00000001/sig00000098 ),
    .DI(a[6]),
    .S(\blk00000001/sig000000a9 ),
    .O(\blk00000001/sig00000099 )
  );
  XORCY   \blk00000001/blk000000d4  (
    .CI(\blk00000001/sig00000098 ),
    .LI(\blk00000001/sig000000a9 ),
    .O(\blk00000001/sig000000ba )
  );
  MUXCY   \blk00000001/blk000000d3  (
    .CI(\blk00000001/sig00000099 ),
    .DI(a[7]),
    .S(\blk00000001/sig000000aa ),
    .O(\blk00000001/sig0000009a )
  );
  XORCY   \blk00000001/blk000000d2  (
    .CI(\blk00000001/sig00000099 ),
    .LI(\blk00000001/sig000000aa ),
    .O(\blk00000001/sig000000bb )
  );
  MUXCY   \blk00000001/blk000000d1  (
    .CI(\blk00000001/sig0000009a ),
    .DI(a[8]),
    .S(\blk00000001/sig000000ab ),
    .O(\blk00000001/sig0000009b )
  );
  XORCY   \blk00000001/blk000000d0  (
    .CI(\blk00000001/sig0000009a ),
    .LI(\blk00000001/sig000000ab ),
    .O(\blk00000001/sig000000bc )
  );
  MUXCY   \blk00000001/blk000000cf  (
    .CI(\blk00000001/sig0000009b ),
    .DI(a[9]),
    .S(\blk00000001/sig000000ac ),
    .O(\blk00000001/sig0000009c )
  );
  XORCY   \blk00000001/blk000000ce  (
    .CI(\blk00000001/sig0000009b ),
    .LI(\blk00000001/sig000000ac ),
    .O(\blk00000001/sig000000bd )
  );
  MUXCY   \blk00000001/blk000000cd  (
    .CI(\blk00000001/sig0000009c ),
    .DI(a[10]),
    .S(\blk00000001/sig0000009e ),
    .O(\blk00000001/sig0000008e )
  );
  XORCY   \blk00000001/blk000000cc  (
    .CI(\blk00000001/sig0000009c ),
    .LI(\blk00000001/sig0000009e ),
    .O(\blk00000001/sig000000af )
  );
  MUXCY   \blk00000001/blk000000cb  (
    .CI(\blk00000001/sig0000008e ),
    .DI(a[11]),
    .S(\blk00000001/sig0000009f ),
    .O(\blk00000001/sig0000008f )
  );
  XORCY   \blk00000001/blk000000ca  (
    .CI(\blk00000001/sig0000008e ),
    .LI(\blk00000001/sig0000009f ),
    .O(\blk00000001/sig000000b0 )
  );
  MUXCY   \blk00000001/blk000000c9  (
    .CI(\blk00000001/sig0000008f ),
    .DI(a[12]),
    .S(\blk00000001/sig000000a0 ),
    .O(\blk00000001/sig00000090 )
  );
  XORCY   \blk00000001/blk000000c8  (
    .CI(\blk00000001/sig0000008f ),
    .LI(\blk00000001/sig000000a0 ),
    .O(\blk00000001/sig000000b1 )
  );
  MUXCY   \blk00000001/blk000000c7  (
    .CI(\blk00000001/sig00000090 ),
    .DI(a[13]),
    .S(\blk00000001/sig000000a1 ),
    .O(\blk00000001/sig00000091 )
  );
  XORCY   \blk00000001/blk000000c6  (
    .CI(\blk00000001/sig00000090 ),
    .LI(\blk00000001/sig000000a1 ),
    .O(\blk00000001/sig000000b2 )
  );
  MUXCY   \blk00000001/blk000000c5  (
    .CI(\blk00000001/sig00000091 ),
    .DI(a[14]),
    .S(\blk00000001/sig000000a2 ),
    .O(\blk00000001/sig00000092 )
  );
  XORCY   \blk00000001/blk000000c4  (
    .CI(\blk00000001/sig00000091 ),
    .LI(\blk00000001/sig000000a2 ),
    .O(\blk00000001/sig000000b3 )
  );
  FDSE #(
    .INIT ( 1'b1 ))
  \blk00000001/blk000000c3  (
    .C(clk),
    .CE(ce),
    .D(\blk00000001/sig00000093 ),
    .S(sclr),
    .Q(\blk00000001/sig000000ad )
  );
  MUXCY   \blk00000001/blk000000c2  (
    .CI(\blk00000001/sig00000084 ),
    .DI(a[48]),
    .S(\blk00000001/sig0000014f ),
    .O(\blk00000001/sig00000140 )
  );
  XORCY   \blk00000001/blk000000c1  (
    .CI(\blk00000001/sig00000084 ),
    .LI(\blk00000001/sig0000014f ),
    .O(\blk00000001/sig0000016f )
  );
  XORCY   \blk00000001/blk000000c0  (
    .CI(\blk00000001/sig00000145 ),
    .LI(\blk00000001/sig00000155 ),
    .O(\blk00000001/sig00000175 )
  );
  MUXCY   \blk00000001/blk000000bf  (
    .CI(\blk00000001/sig00000140 ),
    .DI(a[49]),
    .S(\blk00000001/sig00000156 ),
    .O(\blk00000001/sig00000146 )
  );
  XORCY   \blk00000001/blk000000be  (
    .CI(\blk00000001/sig00000140 ),
    .LI(\blk00000001/sig00000156 ),
    .O(\blk00000001/sig00000176 )
  );
  MUXCY   \blk00000001/blk000000bd  (
    .CI(\blk00000001/sig00000146 ),
    .DI(a[50]),
    .S(\blk00000001/sig00000157 ),
    .O(\blk00000001/sig00000147 )
  );
  XORCY   \blk00000001/blk000000bc  (
    .CI(\blk00000001/sig00000146 ),
    .LI(\blk00000001/sig00000157 ),
    .O(\blk00000001/sig00000177 )
  );
  MUXCY   \blk00000001/blk000000bb  (
    .CI(\blk00000001/sig00000147 ),
    .DI(a[51]),
    .S(\blk00000001/sig00000158 ),
    .O(\blk00000001/sig00000148 )
  );
  XORCY   \blk00000001/blk000000ba  (
    .CI(\blk00000001/sig00000147 ),
    .LI(\blk00000001/sig00000158 ),
    .O(\blk00000001/sig00000178 )
  );
  MUXCY   \blk00000001/blk000000b9  (
    .CI(\blk00000001/sig00000148 ),
    .DI(a[52]),
    .S(\blk00000001/sig00000159 ),
    .O(\blk00000001/sig00000149 )
  );
  XORCY   \blk00000001/blk000000b8  (
    .CI(\blk00000001/sig00000148 ),
    .LI(\blk00000001/sig00000159 ),
    .O(\blk00000001/sig00000179 )
  );
  MUXCY   \blk00000001/blk000000b7  (
    .CI(\blk00000001/sig00000149 ),
    .DI(a[53]),
    .S(\blk00000001/sig0000015a ),
    .O(\blk00000001/sig0000014a )
  );
  XORCY   \blk00000001/blk000000b6  (
    .CI(\blk00000001/sig00000149 ),
    .LI(\blk00000001/sig0000015a ),
    .O(\blk00000001/sig0000017a )
  );
  MUXCY   \blk00000001/blk000000b5  (
    .CI(\blk00000001/sig0000014a ),
    .DI(a[54]),
    .S(\blk00000001/sig0000015b ),
    .O(\blk00000001/sig0000014b )
  );
  XORCY   \blk00000001/blk000000b4  (
    .CI(\blk00000001/sig0000014a ),
    .LI(\blk00000001/sig0000015b ),
    .O(\blk00000001/sig0000017b )
  );
  MUXCY   \blk00000001/blk000000b3  (
    .CI(\blk00000001/sig0000014b ),
    .DI(a[55]),
    .S(\blk00000001/sig0000015c ),
    .O(\blk00000001/sig0000014c )
  );
  XORCY   \blk00000001/blk000000b2  (
    .CI(\blk00000001/sig0000014b ),
    .LI(\blk00000001/sig0000015c ),
    .O(\blk00000001/sig0000017c )
  );
  MUXCY   \blk00000001/blk000000b1  (
    .CI(\blk00000001/sig0000014c ),
    .DI(a[56]),
    .S(\blk00000001/sig0000015d ),
    .O(\blk00000001/sig0000014d )
  );
  XORCY   \blk00000001/blk000000b0  (
    .CI(\blk00000001/sig0000014c ),
    .LI(\blk00000001/sig0000015d ),
    .O(\blk00000001/sig0000017d )
  );
  MUXCY   \blk00000001/blk000000af  (
    .CI(\blk00000001/sig0000014d ),
    .DI(a[57]),
    .S(\blk00000001/sig0000015e ),
    .O(\blk00000001/sig0000014e )
  );
  XORCY   \blk00000001/blk000000ae  (
    .CI(\blk00000001/sig0000014d ),
    .LI(\blk00000001/sig0000015e ),
    .O(\blk00000001/sig0000017e )
  );
  MUXCY   \blk00000001/blk000000ad  (
    .CI(\blk00000001/sig0000014e ),
    .DI(a[58]),
    .S(\blk00000001/sig00000150 ),
    .O(\blk00000001/sig00000141 )
  );
  XORCY   \blk00000001/blk000000ac  (
    .CI(\blk00000001/sig0000014e ),
    .LI(\blk00000001/sig00000150 ),
    .O(\blk00000001/sig00000170 )
  );
  MUXCY   \blk00000001/blk000000ab  (
    .CI(\blk00000001/sig00000141 ),
    .DI(a[59]),
    .S(\blk00000001/sig00000151 ),
    .O(\blk00000001/sig00000142 )
  );
  XORCY   \blk00000001/blk000000aa  (
    .CI(\blk00000001/sig00000141 ),
    .LI(\blk00000001/sig00000151 ),
    .O(\blk00000001/sig00000171 )
  );
  MUXCY   \blk00000001/blk000000a9  (
    .CI(\blk00000001/sig00000142 ),
    .DI(a[60]),
    .S(\blk00000001/sig00000152 ),
    .O(\blk00000001/sig00000143 )
  );
  XORCY   \blk00000001/blk000000a8  (
    .CI(\blk00000001/sig00000142 ),
    .LI(\blk00000001/sig00000152 ),
    .O(\blk00000001/sig00000172 )
  );
  MUXCY   \blk00000001/blk000000a7  (
    .CI(\blk00000001/sig00000143 ),
    .DI(a[61]),
    .S(\blk00000001/sig00000153 ),
    .O(\blk00000001/sig00000144 )
  );
  XORCY   \blk00000001/blk000000a6  (
    .CI(\blk00000001/sig00000143 ),
    .LI(\blk00000001/sig00000153 ),
    .O(\blk00000001/sig00000173 )
  );
  MUXCY   \blk00000001/blk000000a5  (
    .CI(\blk00000001/sig00000144 ),
    .DI(a[62]),
    .S(\blk00000001/sig00000154 ),
    .O(\blk00000001/sig00000145 )
  );
  XORCY   \blk00000001/blk000000a4  (
    .CI(\blk00000001/sig00000144 ),
    .LI(\blk00000001/sig00000154 ),
    .O(\blk00000001/sig00000174 )
  );
  FDRE #(
    .INIT ( 1'b0 ))
  \blk00000001/blk000000a3  (
    .C(clk),
    .CE(ce),
    .D(\blk00000001/sig0000016f ),
    .R(sclr),
    .Q(\blk00000001/sig0000015f )
  );
  FDRE #(
    .INIT ( 1'b0 ))
  \blk00000001/blk000000a2  (
    .C(clk),
    .CE(ce),
    .D(\blk00000001/sig00000176 ),
    .R(sclr),
    .Q(\blk00000001/sig00000167 )
  );
  FDRE #(
    .INIT ( 1'b0 ))
  \blk00000001/blk000000a1  (
    .C(clk),
    .CE(ce),
    .D(\blk00000001/sig00000177 ),
    .R(sclr),
    .Q(\blk00000001/sig00000168 )
  );
  FDRE #(
    .INIT ( 1'b0 ))
  \blk00000001/blk000000a0  (
    .C(clk),
    .CE(ce),
    .D(\blk00000001/sig00000178 ),
    .R(sclr),
    .Q(\blk00000001/sig00000169 )
  );
  FDRE #(
    .INIT ( 1'b0 ))
  \blk00000001/blk0000009f  (
    .C(clk),
    .CE(ce),
    .D(\blk00000001/sig00000179 ),
    .R(sclr),
    .Q(\blk00000001/sig0000016a )
  );
  FDRE #(
    .INIT ( 1'b0 ))
  \blk00000001/blk0000009e  (
    .C(clk),
    .CE(ce),
    .D(\blk00000001/sig0000017a ),
    .R(sclr),
    .Q(\blk00000001/sig0000016b )
  );
  FDRE #(
    .INIT ( 1'b0 ))
  \blk00000001/blk0000009d  (
    .C(clk),
    .CE(ce),
    .D(\blk00000001/sig0000017b ),
    .R(sclr),
    .Q(\blk00000001/sig0000016c )
  );
  FDRE #(
    .INIT ( 1'b0 ))
  \blk00000001/blk0000009c  (
    .C(clk),
    .CE(ce),
    .D(\blk00000001/sig0000017c ),
    .R(sclr),
    .Q(\blk00000001/sig0000016d )
  );
  FDRE #(
    .INIT ( 1'b0 ))
  \blk00000001/blk0000009b  (
    .C(clk),
    .CE(ce),
    .D(\blk00000001/sig0000017d ),
    .R(sclr),
    .Q(\blk00000001/sig0000016e )
  );
  FDRE #(
    .INIT ( 1'b0 ))
  \blk00000001/blk0000009a  (
    .C(clk),
    .CE(ce),
    .D(\blk00000001/sig0000017e ),
    .R(sclr),
    .Q(\blk00000001/sig00000160 )
  );
  FDRE #(
    .INIT ( 1'b0 ))
  \blk00000001/blk00000099  (
    .C(clk),
    .CE(ce),
    .D(\blk00000001/sig00000170 ),
    .R(sclr),
    .Q(\blk00000001/sig00000161 )
  );
  FDRE #(
    .INIT ( 1'b0 ))
  \blk00000001/blk00000098  (
    .C(clk),
    .CE(ce),
    .D(\blk00000001/sig00000171 ),
    .R(sclr),
    .Q(\blk00000001/sig00000162 )
  );
  FDRE #(
    .INIT ( 1'b0 ))
  \blk00000001/blk00000097  (
    .C(clk),
    .CE(ce),
    .D(\blk00000001/sig00000172 ),
    .R(sclr),
    .Q(\blk00000001/sig00000163 )
  );
  FDRE #(
    .INIT ( 1'b0 ))
  \blk00000001/blk00000096  (
    .C(clk),
    .CE(ce),
    .D(\blk00000001/sig00000173 ),
    .R(sclr),
    .Q(\blk00000001/sig00000164 )
  );
  FDRE #(
    .INIT ( 1'b0 ))
  \blk00000001/blk00000095  (
    .C(clk),
    .CE(ce),
    .D(\blk00000001/sig00000174 ),
    .R(sclr),
    .Q(\blk00000001/sig00000165 )
  );
  FDRE #(
    .INIT ( 1'b0 ))
  \blk00000001/blk00000094  (
    .C(clk),
    .CE(ce),
    .D(\blk00000001/sig00000175 ),
    .R(sclr),
    .Q(\blk00000001/sig00000166 )
  );
  MUXCY   \blk00000001/blk00000093  (
    .CI(\blk00000001/sig0000022f ),
    .DI(\blk00000001/sig00000084 ),
    .S(\blk00000001/sig000002af ),
    .O(\blk00000001/sig000002a0 )
  );
  XORCY   \blk00000001/blk00000092  (
    .CI(\blk00000001/sig0000022f ),
    .LI(\blk00000001/sig000002af ),
    .O(\blk00000001/sig000002cf )
  );
  XORCY   \blk00000001/blk00000091  (
    .CI(\blk00000001/sig000002a5 ),
    .LI(\blk00000001/sig000002b5 ),
    .O(\blk00000001/sig000002d5 )
  );
  MUXCY   \blk00000001/blk00000090  (
    .CI(\blk00000001/sig000002a5 ),
    .DI(\blk00000001/sig00000084 ),
    .S(\blk00000001/sig000002b5 ),
    .O(\NLW_blk00000001/blk00000090_O_UNCONNECTED )
  );
  MUXCY   \blk00000001/blk0000008f  (
    .CI(\blk00000001/sig000002a0 ),
    .DI(\blk00000001/sig00000084 ),
    .S(\blk00000001/sig000002b6 ),
    .O(\blk00000001/sig000002a6 )
  );
  XORCY   \blk00000001/blk0000008e  (
    .CI(\blk00000001/sig000002a0 ),
    .LI(\blk00000001/sig000002b6 ),
    .O(\blk00000001/sig000002d6 )
  );
  MUXCY   \blk00000001/blk0000008d  (
    .CI(\blk00000001/sig000002a6 ),
    .DI(\blk00000001/sig00000084 ),
    .S(\blk00000001/sig000002b7 ),
    .O(\blk00000001/sig000002a7 )
  );
  XORCY   \blk00000001/blk0000008c  (
    .CI(\blk00000001/sig000002a6 ),
    .LI(\blk00000001/sig000002b7 ),
    .O(\blk00000001/sig000002d7 )
  );
  MUXCY   \blk00000001/blk0000008b  (
    .CI(\blk00000001/sig000002a7 ),
    .DI(\blk00000001/sig00000084 ),
    .S(\blk00000001/sig000002b8 ),
    .O(\blk00000001/sig000002a8 )
  );
  XORCY   \blk00000001/blk0000008a  (
    .CI(\blk00000001/sig000002a7 ),
    .LI(\blk00000001/sig000002b8 ),
    .O(\blk00000001/sig000002d8 )
  );
  MUXCY   \blk00000001/blk00000089  (
    .CI(\blk00000001/sig000002a8 ),
    .DI(\blk00000001/sig00000084 ),
    .S(\blk00000001/sig000002b9 ),
    .O(\blk00000001/sig000002a9 )
  );
  XORCY   \blk00000001/blk00000088  (
    .CI(\blk00000001/sig000002a8 ),
    .LI(\blk00000001/sig000002b9 ),
    .O(\blk00000001/sig000002d9 )
  );
  MUXCY   \blk00000001/blk00000087  (
    .CI(\blk00000001/sig000002a9 ),
    .DI(\blk00000001/sig00000084 ),
    .S(\blk00000001/sig000002ba ),
    .O(\blk00000001/sig000002aa )
  );
  XORCY   \blk00000001/blk00000086  (
    .CI(\blk00000001/sig000002a9 ),
    .LI(\blk00000001/sig000002ba ),
    .O(\blk00000001/sig000002da )
  );
  MUXCY   \blk00000001/blk00000085  (
    .CI(\blk00000001/sig000002aa ),
    .DI(\blk00000001/sig00000084 ),
    .S(\blk00000001/sig000002bb ),
    .O(\blk00000001/sig000002ab )
  );
  XORCY   \blk00000001/blk00000084  (
    .CI(\blk00000001/sig000002aa ),
    .LI(\blk00000001/sig000002bb ),
    .O(\blk00000001/sig000002db )
  );
  MUXCY   \blk00000001/blk00000083  (
    .CI(\blk00000001/sig000002ab ),
    .DI(\blk00000001/sig00000084 ),
    .S(\blk00000001/sig000002bc ),
    .O(\blk00000001/sig000002ac )
  );
  XORCY   \blk00000001/blk00000082  (
    .CI(\blk00000001/sig000002ab ),
    .LI(\blk00000001/sig000002bc ),
    .O(\blk00000001/sig000002dc )
  );
  MUXCY   \blk00000001/blk00000081  (
    .CI(\blk00000001/sig000002ac ),
    .DI(\blk00000001/sig00000084 ),
    .S(\blk00000001/sig000002bd ),
    .O(\blk00000001/sig000002ad )
  );
  XORCY   \blk00000001/blk00000080  (
    .CI(\blk00000001/sig000002ac ),
    .LI(\blk00000001/sig000002bd ),
    .O(\blk00000001/sig000002dd )
  );
  MUXCY   \blk00000001/blk0000007f  (
    .CI(\blk00000001/sig000002ad ),
    .DI(\blk00000001/sig00000084 ),
    .S(\blk00000001/sig000002be ),
    .O(\blk00000001/sig000002ae )
  );
  XORCY   \blk00000001/blk0000007e  (
    .CI(\blk00000001/sig000002ad ),
    .LI(\blk00000001/sig000002be ),
    .O(\blk00000001/sig000002de )
  );
  MUXCY   \blk00000001/blk0000007d  (
    .CI(\blk00000001/sig000002ae ),
    .DI(\blk00000001/sig00000084 ),
    .S(\blk00000001/sig000002b0 ),
    .O(\blk00000001/sig000002a1 )
  );
  XORCY   \blk00000001/blk0000007c  (
    .CI(\blk00000001/sig000002ae ),
    .LI(\blk00000001/sig000002b0 ),
    .O(\blk00000001/sig000002d0 )
  );
  MUXCY   \blk00000001/blk0000007b  (
    .CI(\blk00000001/sig000002a1 ),
    .DI(\blk00000001/sig00000084 ),
    .S(\blk00000001/sig000002b1 ),
    .O(\blk00000001/sig000002a2 )
  );
  XORCY   \blk00000001/blk0000007a  (
    .CI(\blk00000001/sig000002a1 ),
    .LI(\blk00000001/sig000002b1 ),
    .O(\blk00000001/sig000002d1 )
  );
  MUXCY   \blk00000001/blk00000079  (
    .CI(\blk00000001/sig000002a2 ),
    .DI(\blk00000001/sig00000084 ),
    .S(\blk00000001/sig000002b2 ),
    .O(\blk00000001/sig000002a3 )
  );
  XORCY   \blk00000001/blk00000078  (
    .CI(\blk00000001/sig000002a2 ),
    .LI(\blk00000001/sig000002b2 ),
    .O(\blk00000001/sig000002d2 )
  );
  MUXCY   \blk00000001/blk00000077  (
    .CI(\blk00000001/sig000002a3 ),
    .DI(\blk00000001/sig00000084 ),
    .S(\blk00000001/sig000002b3 ),
    .O(\blk00000001/sig000002a4 )
  );
  XORCY   \blk00000001/blk00000076  (
    .CI(\blk00000001/sig000002a3 ),
    .LI(\blk00000001/sig000002b3 ),
    .O(\blk00000001/sig000002d3 )
  );
  MUXCY   \blk00000001/blk00000075  (
    .CI(\blk00000001/sig000002a4 ),
    .DI(\blk00000001/sig00000084 ),
    .S(\blk00000001/sig000002b4 ),
    .O(\blk00000001/sig000002a5 )
  );
  XORCY   \blk00000001/blk00000074  (
    .CI(\blk00000001/sig000002a4 ),
    .LI(\blk00000001/sig000002b4 ),
    .O(\blk00000001/sig000002d4 )
  );
  FDRE #(
    .INIT ( 1'b0 ))
  \blk00000001/blk00000073  (
    .C(clk),
    .CE(ce),
    .D(\blk00000001/sig000002cf ),
    .R(sclr),
    .Q(s[48])
  );
  FDRE #(
    .INIT ( 1'b0 ))
  \blk00000001/blk00000072  (
    .C(clk),
    .CE(ce),
    .D(\blk00000001/sig000002d6 ),
    .R(sclr),
    .Q(s[49])
  );
  FDRE #(
    .INIT ( 1'b0 ))
  \blk00000001/blk00000071  (
    .C(clk),
    .CE(ce),
    .D(\blk00000001/sig000002d7 ),
    .R(sclr),
    .Q(s[50])
  );
  FDRE #(
    .INIT ( 1'b0 ))
  \blk00000001/blk00000070  (
    .C(clk),
    .CE(ce),
    .D(\blk00000001/sig000002d8 ),
    .R(sclr),
    .Q(s[51])
  );
  FDRE #(
    .INIT ( 1'b0 ))
  \blk00000001/blk0000006f  (
    .C(clk),
    .CE(ce),
    .D(\blk00000001/sig000002d9 ),
    .R(sclr),
    .Q(s[52])
  );
  FDRE #(
    .INIT ( 1'b0 ))
  \blk00000001/blk0000006e  (
    .C(clk),
    .CE(ce),
    .D(\blk00000001/sig000002da ),
    .R(sclr),
    .Q(s[53])
  );
  FDRE #(
    .INIT ( 1'b0 ))
  \blk00000001/blk0000006d  (
    .C(clk),
    .CE(ce),
    .D(\blk00000001/sig000002db ),
    .R(sclr),
    .Q(s[54])
  );
  FDRE #(
    .INIT ( 1'b0 ))
  \blk00000001/blk0000006c  (
    .C(clk),
    .CE(ce),
    .D(\blk00000001/sig000002dc ),
    .R(sclr),
    .Q(s[55])
  );
  FDRE #(
    .INIT ( 1'b0 ))
  \blk00000001/blk0000006b  (
    .C(clk),
    .CE(ce),
    .D(\blk00000001/sig000002dd ),
    .R(sclr),
    .Q(s[56])
  );
  FDRE #(
    .INIT ( 1'b0 ))
  \blk00000001/blk0000006a  (
    .C(clk),
    .CE(ce),
    .D(\blk00000001/sig000002de ),
    .R(sclr),
    .Q(s[57])
  );
  FDRE #(
    .INIT ( 1'b0 ))
  \blk00000001/blk00000069  (
    .C(clk),
    .CE(ce),
    .D(\blk00000001/sig000002d0 ),
    .R(sclr),
    .Q(s[58])
  );
  FDRE #(
    .INIT ( 1'b0 ))
  \blk00000001/blk00000068  (
    .C(clk),
    .CE(ce),
    .D(\blk00000001/sig000002d1 ),
    .R(sclr),
    .Q(s[59])
  );
  FDRE #(
    .INIT ( 1'b0 ))
  \blk00000001/blk00000067  (
    .C(clk),
    .CE(ce),
    .D(\blk00000001/sig000002d2 ),
    .R(sclr),
    .Q(s[60])
  );
  FDRE #(
    .INIT ( 1'b0 ))
  \blk00000001/blk00000066  (
    .C(clk),
    .CE(ce),
    .D(\blk00000001/sig000002d3 ),
    .R(sclr),
    .Q(s[61])
  );
  FDRE #(
    .INIT ( 1'b0 ))
  \blk00000001/blk00000065  (
    .C(clk),
    .CE(ce),
    .D(\blk00000001/sig000002d4 ),
    .R(sclr),
    .Q(s[62])
  );
  FDRE #(
    .INIT ( 1'b0 ))
  \blk00000001/blk00000064  (
    .C(clk),
    .CE(ce),
    .D(\blk00000001/sig000002d5 ),
    .R(sclr),
    .Q(s[63])
  );
  MUXCY   \blk00000001/blk00000063  (
    .CI(\blk00000001/sig000001be ),
    .DI(\blk00000001/sig00000084 ),
    .S(\blk00000001/sig000001ff ),
    .O(\blk00000001/sig000001f0 )
  );
  XORCY   \blk00000001/blk00000062  (
    .CI(\blk00000001/sig000001be ),
    .LI(\blk00000001/sig000001ff ),
    .O(\blk00000001/sig0000021f )
  );
  XORCY   \blk00000001/blk00000061  (
    .CI(\blk00000001/sig000001f5 ),
    .LI(\blk00000001/sig00000205 ),
    .O(\blk00000001/sig00000225 )
  );
  MUXCY   \blk00000001/blk00000060  (
    .CI(\blk00000001/sig000001f5 ),
    .DI(\blk00000001/sig00000084 ),
    .S(\blk00000001/sig00000205 ),
    .O(\blk00000001/sig0000008a )
  );
  MUXCY   \blk00000001/blk0000005f  (
    .CI(\blk00000001/sig000001f0 ),
    .DI(\blk00000001/sig00000084 ),
    .S(\blk00000001/sig00000206 ),
    .O(\blk00000001/sig000001f6 )
  );
  XORCY   \blk00000001/blk0000005e  (
    .CI(\blk00000001/sig000001f0 ),
    .LI(\blk00000001/sig00000206 ),
    .O(\blk00000001/sig00000226 )
  );
  MUXCY   \blk00000001/blk0000005d  (
    .CI(\blk00000001/sig000001f6 ),
    .DI(\blk00000001/sig00000084 ),
    .S(\blk00000001/sig00000207 ),
    .O(\blk00000001/sig000001f7 )
  );
  XORCY   \blk00000001/blk0000005c  (
    .CI(\blk00000001/sig000001f6 ),
    .LI(\blk00000001/sig00000207 ),
    .O(\blk00000001/sig00000227 )
  );
  MUXCY   \blk00000001/blk0000005b  (
    .CI(\blk00000001/sig000001f7 ),
    .DI(\blk00000001/sig00000084 ),
    .S(\blk00000001/sig00000208 ),
    .O(\blk00000001/sig000001f8 )
  );
  XORCY   \blk00000001/blk0000005a  (
    .CI(\blk00000001/sig000001f7 ),
    .LI(\blk00000001/sig00000208 ),
    .O(\blk00000001/sig00000228 )
  );
  MUXCY   \blk00000001/blk00000059  (
    .CI(\blk00000001/sig000001f8 ),
    .DI(\blk00000001/sig00000084 ),
    .S(\blk00000001/sig00000209 ),
    .O(\blk00000001/sig000001f9 )
  );
  XORCY   \blk00000001/blk00000058  (
    .CI(\blk00000001/sig000001f8 ),
    .LI(\blk00000001/sig00000209 ),
    .O(\blk00000001/sig00000229 )
  );
  MUXCY   \blk00000001/blk00000057  (
    .CI(\blk00000001/sig000001f9 ),
    .DI(\blk00000001/sig00000084 ),
    .S(\blk00000001/sig0000020a ),
    .O(\blk00000001/sig000001fa )
  );
  XORCY   \blk00000001/blk00000056  (
    .CI(\blk00000001/sig000001f9 ),
    .LI(\blk00000001/sig0000020a ),
    .O(\blk00000001/sig0000022a )
  );
  MUXCY   \blk00000001/blk00000055  (
    .CI(\blk00000001/sig000001fa ),
    .DI(\blk00000001/sig00000084 ),
    .S(\blk00000001/sig0000020b ),
    .O(\blk00000001/sig000001fb )
  );
  XORCY   \blk00000001/blk00000054  (
    .CI(\blk00000001/sig000001fa ),
    .LI(\blk00000001/sig0000020b ),
    .O(\blk00000001/sig0000022b )
  );
  MUXCY   \blk00000001/blk00000053  (
    .CI(\blk00000001/sig000001fb ),
    .DI(\blk00000001/sig00000084 ),
    .S(\blk00000001/sig0000020c ),
    .O(\blk00000001/sig000001fc )
  );
  XORCY   \blk00000001/blk00000052  (
    .CI(\blk00000001/sig000001fb ),
    .LI(\blk00000001/sig0000020c ),
    .O(\blk00000001/sig0000022c )
  );
  MUXCY   \blk00000001/blk00000051  (
    .CI(\blk00000001/sig000001fc ),
    .DI(\blk00000001/sig00000084 ),
    .S(\blk00000001/sig0000020d ),
    .O(\blk00000001/sig000001fd )
  );
  XORCY   \blk00000001/blk00000050  (
    .CI(\blk00000001/sig000001fc ),
    .LI(\blk00000001/sig0000020d ),
    .O(\blk00000001/sig0000022d )
  );
  MUXCY   \blk00000001/blk0000004f  (
    .CI(\blk00000001/sig000001fd ),
    .DI(\blk00000001/sig00000084 ),
    .S(\blk00000001/sig0000020e ),
    .O(\blk00000001/sig000001fe )
  );
  XORCY   \blk00000001/blk0000004e  (
    .CI(\blk00000001/sig000001fd ),
    .LI(\blk00000001/sig0000020e ),
    .O(\blk00000001/sig0000022e )
  );
  MUXCY   \blk00000001/blk0000004d  (
    .CI(\blk00000001/sig000001fe ),
    .DI(\blk00000001/sig00000084 ),
    .S(\blk00000001/sig00000200 ),
    .O(\blk00000001/sig000001f1 )
  );
  XORCY   \blk00000001/blk0000004c  (
    .CI(\blk00000001/sig000001fe ),
    .LI(\blk00000001/sig00000200 ),
    .O(\blk00000001/sig00000220 )
  );
  MUXCY   \blk00000001/blk0000004b  (
    .CI(\blk00000001/sig000001f1 ),
    .DI(\blk00000001/sig00000084 ),
    .S(\blk00000001/sig00000201 ),
    .O(\blk00000001/sig000001f2 )
  );
  XORCY   \blk00000001/blk0000004a  (
    .CI(\blk00000001/sig000001f1 ),
    .LI(\blk00000001/sig00000201 ),
    .O(\blk00000001/sig00000221 )
  );
  MUXCY   \blk00000001/blk00000049  (
    .CI(\blk00000001/sig000001f2 ),
    .DI(\blk00000001/sig00000084 ),
    .S(\blk00000001/sig00000202 ),
    .O(\blk00000001/sig000001f3 )
  );
  XORCY   \blk00000001/blk00000048  (
    .CI(\blk00000001/sig000001f2 ),
    .LI(\blk00000001/sig00000202 ),
    .O(\blk00000001/sig00000222 )
  );
  MUXCY   \blk00000001/blk00000047  (
    .CI(\blk00000001/sig000001f3 ),
    .DI(\blk00000001/sig00000084 ),
    .S(\blk00000001/sig00000203 ),
    .O(\blk00000001/sig000001f4 )
  );
  XORCY   \blk00000001/blk00000046  (
    .CI(\blk00000001/sig000001f3 ),
    .LI(\blk00000001/sig00000203 ),
    .O(\blk00000001/sig00000223 )
  );
  MUXCY   \blk00000001/blk00000045  (
    .CI(\blk00000001/sig000001f4 ),
    .DI(\blk00000001/sig00000084 ),
    .S(\blk00000001/sig00000204 ),
    .O(\blk00000001/sig000001f5 )
  );
  XORCY   \blk00000001/blk00000044  (
    .CI(\blk00000001/sig000001f4 ),
    .LI(\blk00000001/sig00000204 ),
    .O(\blk00000001/sig00000224 )
  );
  FDRE #(
    .INIT ( 1'b0 ))
  \blk00000001/blk00000043  (
    .C(clk),
    .CE(ce),
    .D(\blk00000001/sig0000021f ),
    .R(sclr),
    .Q(\blk00000001/sig0000020f )
  );
  FDRE #(
    .INIT ( 1'b0 ))
  \blk00000001/blk00000042  (
    .C(clk),
    .CE(ce),
    .D(\blk00000001/sig00000226 ),
    .R(sclr),
    .Q(\blk00000001/sig00000217 )
  );
  FDRE #(
    .INIT ( 1'b0 ))
  \blk00000001/blk00000041  (
    .C(clk),
    .CE(ce),
    .D(\blk00000001/sig00000227 ),
    .R(sclr),
    .Q(\blk00000001/sig00000218 )
  );
  FDRE #(
    .INIT ( 1'b0 ))
  \blk00000001/blk00000040  (
    .C(clk),
    .CE(ce),
    .D(\blk00000001/sig00000228 ),
    .R(sclr),
    .Q(\blk00000001/sig00000219 )
  );
  FDRE #(
    .INIT ( 1'b0 ))
  \blk00000001/blk0000003f  (
    .C(clk),
    .CE(ce),
    .D(\blk00000001/sig00000229 ),
    .R(sclr),
    .Q(\blk00000001/sig0000021a )
  );
  FDRE #(
    .INIT ( 1'b0 ))
  \blk00000001/blk0000003e  (
    .C(clk),
    .CE(ce),
    .D(\blk00000001/sig0000022a ),
    .R(sclr),
    .Q(\blk00000001/sig0000021b )
  );
  FDRE #(
    .INIT ( 1'b0 ))
  \blk00000001/blk0000003d  (
    .C(clk),
    .CE(ce),
    .D(\blk00000001/sig0000022b ),
    .R(sclr),
    .Q(\blk00000001/sig0000021c )
  );
  FDRE #(
    .INIT ( 1'b0 ))
  \blk00000001/blk0000003c  (
    .C(clk),
    .CE(ce),
    .D(\blk00000001/sig0000022c ),
    .R(sclr),
    .Q(\blk00000001/sig0000021d )
  );
  FDRE #(
    .INIT ( 1'b0 ))
  \blk00000001/blk0000003b  (
    .C(clk),
    .CE(ce),
    .D(\blk00000001/sig0000022d ),
    .R(sclr),
    .Q(\blk00000001/sig0000021e )
  );
  FDRE #(
    .INIT ( 1'b0 ))
  \blk00000001/blk0000003a  (
    .C(clk),
    .CE(ce),
    .D(\blk00000001/sig0000022e ),
    .R(sclr),
    .Q(\blk00000001/sig00000210 )
  );
  FDRE #(
    .INIT ( 1'b0 ))
  \blk00000001/blk00000039  (
    .C(clk),
    .CE(ce),
    .D(\blk00000001/sig00000220 ),
    .R(sclr),
    .Q(\blk00000001/sig00000211 )
  );
  FDRE #(
    .INIT ( 1'b0 ))
  \blk00000001/blk00000038  (
    .C(clk),
    .CE(ce),
    .D(\blk00000001/sig00000221 ),
    .R(sclr),
    .Q(\blk00000001/sig00000212 )
  );
  FDRE #(
    .INIT ( 1'b0 ))
  \blk00000001/blk00000037  (
    .C(clk),
    .CE(ce),
    .D(\blk00000001/sig00000222 ),
    .R(sclr),
    .Q(\blk00000001/sig00000213 )
  );
  FDRE #(
    .INIT ( 1'b0 ))
  \blk00000001/blk00000036  (
    .C(clk),
    .CE(ce),
    .D(\blk00000001/sig00000223 ),
    .R(sclr),
    .Q(\blk00000001/sig00000214 )
  );
  FDRE #(
    .INIT ( 1'b0 ))
  \blk00000001/blk00000035  (
    .C(clk),
    .CE(ce),
    .D(\blk00000001/sig00000224 ),
    .R(sclr),
    .Q(\blk00000001/sig00000215 )
  );
  FDRE #(
    .INIT ( 1'b0 ))
  \blk00000001/blk00000034  (
    .C(clk),
    .CE(ce),
    .D(\blk00000001/sig00000225 ),
    .R(sclr),
    .Q(\blk00000001/sig00000216 )
  );
  MUXCY   \blk00000001/blk00000033  (
    .CI(\blk00000001/sig000000ad ),
    .DI(\blk00000001/sig00000084 ),
    .S(\blk00000001/sig0000018e ),
    .O(\blk00000001/sig0000017f )
  );
  XORCY   \blk00000001/blk00000032  (
    .CI(\blk00000001/sig000000ad ),
    .LI(\blk00000001/sig0000018e ),
    .O(\blk00000001/sig000001ae )
  );
  XORCY   \blk00000001/blk00000031  (
    .CI(\blk00000001/sig00000184 ),
    .LI(\blk00000001/sig00000194 ),
    .O(\blk00000001/sig000001b4 )
  );
  MUXCY   \blk00000001/blk00000030  (
    .CI(\blk00000001/sig00000184 ),
    .DI(\blk00000001/sig00000084 ),
    .S(\blk00000001/sig00000194 ),
    .O(\blk00000001/sig00000089 )
  );
  MUXCY   \blk00000001/blk0000002f  (
    .CI(\blk00000001/sig0000017f ),
    .DI(\blk00000001/sig00000084 ),
    .S(\blk00000001/sig00000195 ),
    .O(\blk00000001/sig00000185 )
  );
  XORCY   \blk00000001/blk0000002e  (
    .CI(\blk00000001/sig0000017f ),
    .LI(\blk00000001/sig00000195 ),
    .O(\blk00000001/sig000001b5 )
  );
  MUXCY   \blk00000001/blk0000002d  (
    .CI(\blk00000001/sig00000185 ),
    .DI(\blk00000001/sig00000084 ),
    .S(\blk00000001/sig00000196 ),
    .O(\blk00000001/sig00000186 )
  );
  XORCY   \blk00000001/blk0000002c  (
    .CI(\blk00000001/sig00000185 ),
    .LI(\blk00000001/sig00000196 ),
    .O(\blk00000001/sig000001b6 )
  );
  MUXCY   \blk00000001/blk0000002b  (
    .CI(\blk00000001/sig00000186 ),
    .DI(\blk00000001/sig00000084 ),
    .S(\blk00000001/sig00000197 ),
    .O(\blk00000001/sig00000187 )
  );
  XORCY   \blk00000001/blk0000002a  (
    .CI(\blk00000001/sig00000186 ),
    .LI(\blk00000001/sig00000197 ),
    .O(\blk00000001/sig000001b7 )
  );
  MUXCY   \blk00000001/blk00000029  (
    .CI(\blk00000001/sig00000187 ),
    .DI(\blk00000001/sig00000084 ),
    .S(\blk00000001/sig00000198 ),
    .O(\blk00000001/sig00000188 )
  );
  XORCY   \blk00000001/blk00000028  (
    .CI(\blk00000001/sig00000187 ),
    .LI(\blk00000001/sig00000198 ),
    .O(\blk00000001/sig000001b8 )
  );
  MUXCY   \blk00000001/blk00000027  (
    .CI(\blk00000001/sig00000188 ),
    .DI(\blk00000001/sig00000084 ),
    .S(\blk00000001/sig00000199 ),
    .O(\blk00000001/sig00000189 )
  );
  XORCY   \blk00000001/blk00000026  (
    .CI(\blk00000001/sig00000188 ),
    .LI(\blk00000001/sig00000199 ),
    .O(\blk00000001/sig000001b9 )
  );
  MUXCY   \blk00000001/blk00000025  (
    .CI(\blk00000001/sig00000189 ),
    .DI(\blk00000001/sig00000084 ),
    .S(\blk00000001/sig0000019a ),
    .O(\blk00000001/sig0000018a )
  );
  XORCY   \blk00000001/blk00000024  (
    .CI(\blk00000001/sig00000189 ),
    .LI(\blk00000001/sig0000019a ),
    .O(\blk00000001/sig000001ba )
  );
  MUXCY   \blk00000001/blk00000023  (
    .CI(\blk00000001/sig0000018a ),
    .DI(\blk00000001/sig00000084 ),
    .S(\blk00000001/sig0000019b ),
    .O(\blk00000001/sig0000018b )
  );
  XORCY   \blk00000001/blk00000022  (
    .CI(\blk00000001/sig0000018a ),
    .LI(\blk00000001/sig0000019b ),
    .O(\blk00000001/sig000001bb )
  );
  MUXCY   \blk00000001/blk00000021  (
    .CI(\blk00000001/sig0000018b ),
    .DI(\blk00000001/sig00000084 ),
    .S(\blk00000001/sig0000019c ),
    .O(\blk00000001/sig0000018c )
  );
  XORCY   \blk00000001/blk00000020  (
    .CI(\blk00000001/sig0000018b ),
    .LI(\blk00000001/sig0000019c ),
    .O(\blk00000001/sig000001bc )
  );
  MUXCY   \blk00000001/blk0000001f  (
    .CI(\blk00000001/sig0000018c ),
    .DI(\blk00000001/sig00000084 ),
    .S(\blk00000001/sig0000019d ),
    .O(\blk00000001/sig0000018d )
  );
  XORCY   \blk00000001/blk0000001e  (
    .CI(\blk00000001/sig0000018c ),
    .LI(\blk00000001/sig0000019d ),
    .O(\blk00000001/sig000001bd )
  );
  MUXCY   \blk00000001/blk0000001d  (
    .CI(\blk00000001/sig0000018d ),
    .DI(\blk00000001/sig00000084 ),
    .S(\blk00000001/sig0000018f ),
    .O(\blk00000001/sig00000180 )
  );
  XORCY   \blk00000001/blk0000001c  (
    .CI(\blk00000001/sig0000018d ),
    .LI(\blk00000001/sig0000018f ),
    .O(\blk00000001/sig000001af )
  );
  MUXCY   \blk00000001/blk0000001b  (
    .CI(\blk00000001/sig00000180 ),
    .DI(\blk00000001/sig00000084 ),
    .S(\blk00000001/sig00000190 ),
    .O(\blk00000001/sig00000181 )
  );
  XORCY   \blk00000001/blk0000001a  (
    .CI(\blk00000001/sig00000180 ),
    .LI(\blk00000001/sig00000190 ),
    .O(\blk00000001/sig000001b0 )
  );
  MUXCY   \blk00000001/blk00000019  (
    .CI(\blk00000001/sig00000181 ),
    .DI(\blk00000001/sig00000084 ),
    .S(\blk00000001/sig00000191 ),
    .O(\blk00000001/sig00000182 )
  );
  XORCY   \blk00000001/blk00000018  (
    .CI(\blk00000001/sig00000181 ),
    .LI(\blk00000001/sig00000191 ),
    .O(\blk00000001/sig000001b1 )
  );
  MUXCY   \blk00000001/blk00000017  (
    .CI(\blk00000001/sig00000182 ),
    .DI(\blk00000001/sig00000084 ),
    .S(\blk00000001/sig00000192 ),
    .O(\blk00000001/sig00000183 )
  );
  XORCY   \blk00000001/blk00000016  (
    .CI(\blk00000001/sig00000182 ),
    .LI(\blk00000001/sig00000192 ),
    .O(\blk00000001/sig000001b2 )
  );
  MUXCY   \blk00000001/blk00000015  (
    .CI(\blk00000001/sig00000183 ),
    .DI(\blk00000001/sig00000084 ),
    .S(\blk00000001/sig00000193 ),
    .O(\blk00000001/sig00000184 )
  );
  XORCY   \blk00000001/blk00000014  (
    .CI(\blk00000001/sig00000183 ),
    .LI(\blk00000001/sig00000193 ),
    .O(\blk00000001/sig000001b3 )
  );
  FDRE #(
    .INIT ( 1'b0 ))
  \blk00000001/blk00000013  (
    .C(clk),
    .CE(ce),
    .D(\blk00000001/sig000001ae ),
    .R(sclr),
    .Q(\blk00000001/sig0000019e )
  );
  FDRE #(
    .INIT ( 1'b0 ))
  \blk00000001/blk00000012  (
    .C(clk),
    .CE(ce),
    .D(\blk00000001/sig000001b5 ),
    .R(sclr),
    .Q(\blk00000001/sig000001a6 )
  );
  FDRE #(
    .INIT ( 1'b0 ))
  \blk00000001/blk00000011  (
    .C(clk),
    .CE(ce),
    .D(\blk00000001/sig000001b6 ),
    .R(sclr),
    .Q(\blk00000001/sig000001a7 )
  );
  FDRE #(
    .INIT ( 1'b0 ))
  \blk00000001/blk00000010  (
    .C(clk),
    .CE(ce),
    .D(\blk00000001/sig000001b7 ),
    .R(sclr),
    .Q(\blk00000001/sig000001a8 )
  );
  FDRE #(
    .INIT ( 1'b0 ))
  \blk00000001/blk0000000f  (
    .C(clk),
    .CE(ce),
    .D(\blk00000001/sig000001b8 ),
    .R(sclr),
    .Q(\blk00000001/sig000001a9 )
  );
  FDRE #(
    .INIT ( 1'b0 ))
  \blk00000001/blk0000000e  (
    .C(clk),
    .CE(ce),
    .D(\blk00000001/sig000001b9 ),
    .R(sclr),
    .Q(\blk00000001/sig000001aa )
  );
  FDRE #(
    .INIT ( 1'b0 ))
  \blk00000001/blk0000000d  (
    .C(clk),
    .CE(ce),
    .D(\blk00000001/sig000001ba ),
    .R(sclr),
    .Q(\blk00000001/sig000001ab )
  );
  FDRE #(
    .INIT ( 1'b0 ))
  \blk00000001/blk0000000c  (
    .C(clk),
    .CE(ce),
    .D(\blk00000001/sig000001bb ),
    .R(sclr),
    .Q(\blk00000001/sig000001ac )
  );
  FDRE #(
    .INIT ( 1'b0 ))
  \blk00000001/blk0000000b  (
    .C(clk),
    .CE(ce),
    .D(\blk00000001/sig000001bc ),
    .R(sclr),
    .Q(\blk00000001/sig000001ad )
  );
  FDRE #(
    .INIT ( 1'b0 ))
  \blk00000001/blk0000000a  (
    .C(clk),
    .CE(ce),
    .D(\blk00000001/sig000001bd ),
    .R(sclr),
    .Q(\blk00000001/sig0000019f )
  );
  FDRE #(
    .INIT ( 1'b0 ))
  \blk00000001/blk00000009  (
    .C(clk),
    .CE(ce),
    .D(\blk00000001/sig000001af ),
    .R(sclr),
    .Q(\blk00000001/sig000001a0 )
  );
  FDRE #(
    .INIT ( 1'b0 ))
  \blk00000001/blk00000008  (
    .C(clk),
    .CE(ce),
    .D(\blk00000001/sig000001b0 ),
    .R(sclr),
    .Q(\blk00000001/sig000001a1 )
  );
  FDRE #(
    .INIT ( 1'b0 ))
  \blk00000001/blk00000007  (
    .C(clk),
    .CE(ce),
    .D(\blk00000001/sig000001b1 ),
    .R(sclr),
    .Q(\blk00000001/sig000001a2 )
  );
  FDRE #(
    .INIT ( 1'b0 ))
  \blk00000001/blk00000006  (
    .C(clk),
    .CE(ce),
    .D(\blk00000001/sig000001b2 ),
    .R(sclr),
    .Q(\blk00000001/sig000001a3 )
  );
  FDRE #(
    .INIT ( 1'b0 ))
  \blk00000001/blk00000005  (
    .C(clk),
    .CE(ce),
    .D(\blk00000001/sig000001b3 ),
    .R(sclr),
    .Q(\blk00000001/sig000001a4 )
  );
  FDRE #(
    .INIT ( 1'b0 ))
  \blk00000001/blk00000004  (
    .C(clk),
    .CE(ce),
    .D(\blk00000001/sig000001b4 ),
    .R(sclr),
    .Q(\blk00000001/sig000001a5 )
  );
  VCC   \blk00000001/blk00000003  (
    .P(\blk00000001/sig00000084 )
  );
  GND   \blk00000001/blk00000002  (
    .G(\blk00000001/sig00000083 )
  );

// synthesis translate_on

endmodule

// synthesis translate_off

`ifndef GLBL
`define GLBL

`timescale  1 ps / 1 ps

module glbl ();

    parameter ROC_WIDTH = 100000;
    parameter TOC_WIDTH = 0;

//--------   STARTUP Globals --------------
    wire GSR;
    wire GTS;
    wire GWE;
    wire PRLD;
    tri1 p_up_tmp;
    tri (weak1, strong0) PLL_LOCKG = p_up_tmp;

    wire PROGB_GLBL;
    wire CCLKO_GLBL;

    reg GSR_int;
    reg GTS_int;
    reg PRLD_int;

//--------   JTAG Globals --------------
    wire JTAG_TDO_GLBL;
    wire JTAG_TCK_GLBL;
    wire JTAG_TDI_GLBL;
    wire JTAG_TMS_GLBL;
    wire JTAG_TRST_GLBL;

    reg JTAG_CAPTURE_GLBL;
    reg JTAG_RESET_GLBL;
    reg JTAG_SHIFT_GLBL;
    reg JTAG_UPDATE_GLBL;
    reg JTAG_RUNTEST_GLBL;

    reg JTAG_SEL1_GLBL = 0;
    reg JTAG_SEL2_GLBL = 0 ;
    reg JTAG_SEL3_GLBL = 0;
    reg JTAG_SEL4_GLBL = 0;

    reg JTAG_USER_TDO1_GLBL = 1'bz;
    reg JTAG_USER_TDO2_GLBL = 1'bz;
    reg JTAG_USER_TDO3_GLBL = 1'bz;
    reg JTAG_USER_TDO4_GLBL = 1'bz;

    assign (weak1, weak0) GSR = GSR_int;
    assign (weak1, weak0) GTS = GTS_int;
    assign (weak1, weak0) PRLD = PRLD_int;

    initial begin
	GSR_int = 1'b1;
	PRLD_int = 1'b1;
	#(ROC_WIDTH)
	GSR_int = 1'b0;
	PRLD_int = 1'b0;
    end

    initial begin
	GTS_int = 1'b1;
	#(TOC_WIDTH)
	GTS_int = 1'b0;
    end

endmodule

`endif

// synthesis translate_on
