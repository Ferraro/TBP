module WBlkMem_64_PS_64_0(

  input io_clka,
  input io_RSTa,
  input io_ENa,
  input io_WEa,
  input [5:0] io_ADDRa,
  input [63:0] io_DINa,
  output[63:0] io_DOUTa
);

wire[63:0] bbox_io_douta;

assign io_DOUTa = bbox_io_douta;

BlkMem_64_PS_64_0 bbox(

 .clka( io_clka ),
 .rsta( io_RSTa ),
 .ena( io_ENa ),
 .wea( io_WEa ),
 .addra( io_ADDRa ),
 .dina( io_DINa ),
 .douta( bbox_io_douta )
);

endmodule


