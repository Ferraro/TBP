module WBlkMem_32_PS_32_0(

  input io_clka,
  input io_RSTa,
  input io_ENa,
  input io_WEa,
  input [4:0] io_ADDRa,
  input [31:0] io_DINa,
  output[31:0] io_DOUTa
);

wire[31:0] bbox_io_douta;

assign io_DOUTa = bbox_io_douta;

BlkMem_32_PS_32_0 bbox(

 .clka( io_clka ),
 .rsta( io_RSTa ),
 .ena( io_ENa ),
 .wea( io_WEa ),
 .addra( io_ADDRa ),
 .dina( io_DINa ),
 .douta( bbox_io_douta )
);

endmodule

