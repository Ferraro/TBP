module CLK(input clk,
    output io_CLK
);

  assign io_CLK = clk;
endmodule
