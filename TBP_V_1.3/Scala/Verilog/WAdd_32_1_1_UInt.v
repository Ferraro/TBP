module WAdd_32_1_1_UInt(input clk,
    input [31:0] io_A,
    input [31:0] io_B,
    input  io_CE,
    input  io_SCLR,
    output[31:0] io_C,
    output io_RDY,
    output io_OVERFLOW,
    output io_UNDERFLOW
);

  wire[31:0] logic_unit_io_s;


  assign io_UNDERFLOW = 1'h0;
  assign io_OVERFLOW = 1'h0;
  assign io_RDY = 1'h0;
  assign io_C = logic_unit_io_s;
  Add_32_1_1_UInt logic_unit(.clk(clk),
       .a( io_A ),
       .b( io_B ),
       .ce( io_CE ),
       .sclr( io_SCLR ),
       .s( logic_unit_io_s )
  );
endmodule
