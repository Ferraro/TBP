////////////////////////////////////////////////////////////////////////////////
// Copyright (c) 1995-2013 Xilinx, Inc.  All rights reserved.
////////////////////////////////////////////////////////////////////////////////
//   ____  ____
//  /   /\/   /
// /___/  \  /    Vendor: Xilinx
// \   \   \/     Version: P.20131013
//  \   \         Application: netgen
//  /   /         Filename: Add_32_8_1_SInt.v
// /___/   /\     Timestamp: Tue Aug  5 13:20:12 2014
// \   \  /  \ 
//  \___\/\___\
//             
// Command	: -w -sim -ofmt verilog /home/francesco/Xilinx/Projects/IPCOREs/ipcore_dir/tmp/_cg/Add_32_8_1_SInt.ngc /home/francesco/Xilinx/Projects/IPCOREs/ipcore_dir/tmp/_cg/Add_32_8_1_SInt.v 
// Device	: 3sd3400afg676-4
// Input file	: /home/francesco/Xilinx/Projects/IPCOREs/ipcore_dir/tmp/_cg/Add_32_8_1_SInt.ngc
// Output file	: /home/francesco/Xilinx/Projects/IPCOREs/ipcore_dir/tmp/_cg/Add_32_8_1_SInt.v
// # of Modules	: 1
// Design Name	: Add_32_8_1_SInt
// Xilinx        : /home/francesco/Xilinx/14.7/ISE_DS/ISE/
//             
// Purpose:    
//     This verilog netlist is a verification model and uses simulation 
//     primitives which may not represent the true implementation of the 
//     device, however the netlist is functionally correct and should not 
//     be modified. This file cannot be synthesized and should only be used 
//     with supported simulation tools.
//             
// Reference:  
//     Command Line Tools User Guide, Chapter 23 and Synthesis and Simulation Design Guide, Chapter 6
//             
////////////////////////////////////////////////////////////////////////////////

`timescale 1 ns/1 ps

module Add_32_8_1_SInt (
  clk, ce, sclr, s, a, b
)/* synthesis syn_black_box syn_noprune=1 */;
  input clk;
  input ce;
  input sclr;
  output [31 : 0] s;
  input [31 : 0] a;
  input [31 : 0] b;
  
  // synthesis translate_off
  
  wire \blk00000001/sig000001e3 ;
  wire \blk00000001/sig000001e2 ;
  wire \blk00000001/sig000001e1 ;
  wire \blk00000001/sig000001e0 ;
  wire \blk00000001/sig000001df ;
  wire \blk00000001/sig000001de ;
  wire \blk00000001/sig000001dd ;
  wire \blk00000001/sig000001dc ;
  wire \blk00000001/sig000001d7 ;
  wire \blk00000001/sig000001d6 ;
  wire \blk00000001/sig000001d5 ;
  wire \blk00000001/sig000001ca ;
  wire \blk00000001/sig000001c9 ;
  wire \blk00000001/sig000001c7 ;
  wire \blk00000001/sig000001c6 ;
  wire \blk00000001/sig000001c4 ;
  wire \blk00000001/sig000001c3 ;
  wire \blk00000001/sig000001c2 ;
  wire \blk00000001/sig000001c1 ;
  wire \blk00000001/sig000001bf ;
  wire \blk00000001/sig000001be ;
  wire \blk00000001/sig000001bc ;
  wire \blk00000001/sig000001bb ;
  wire \blk00000001/sig000001b9 ;
  wire \blk00000001/sig000001b8 ;
  wire \blk00000001/sig000001b6 ;
  wire \blk00000001/sig000001b5 ;
  wire \blk00000001/sig000001b4 ;
  wire \blk00000001/sig000001b3 ;
  wire \blk00000001/sig000001b2 ;
  wire \blk00000001/sig000001b1 ;
  wire \blk00000001/sig000001af ;
  wire \blk00000001/sig000001ae ;
  wire \blk00000001/sig000001ac ;
  wire \blk00000001/sig000001ab ;
  wire \blk00000001/sig000001a9 ;
  wire \blk00000001/sig000001a8 ;
  wire \blk00000001/sig000001a6 ;
  wire \blk00000001/sig000001a5 ;
  wire \blk00000001/sig000001a4 ;
  wire \blk00000001/sig000001a3 ;
  wire \blk00000001/sig000001a2 ;
  wire \blk00000001/sig000001a1 ;
  wire \blk00000001/sig0000019f ;
  wire \blk00000001/sig0000019e ;
  wire \blk00000001/sig0000019c ;
  wire \blk00000001/sig0000019b ;
  wire \blk00000001/sig00000199 ;
  wire \blk00000001/sig00000198 ;
  wire \blk00000001/sig00000196 ;
  wire \blk00000001/sig00000195 ;
  wire \blk00000001/sig00000194 ;
  wire \blk00000001/sig00000193 ;
  wire \blk00000001/sig00000192 ;
  wire \blk00000001/sig00000191 ;
  wire \blk00000001/sig0000018f ;
  wire \blk00000001/sig0000018e ;
  wire \blk00000001/sig0000018c ;
  wire \blk00000001/sig0000018b ;
  wire \blk00000001/sig00000189 ;
  wire \blk00000001/sig00000188 ;
  wire \blk00000001/sig00000186 ;
  wire \blk00000001/sig00000185 ;
  wire \blk00000001/sig00000184 ;
  wire \blk00000001/sig00000183 ;
  wire \blk00000001/sig00000182 ;
  wire \blk00000001/sig00000181 ;
  wire \blk00000001/sig00000180 ;
  wire \blk00000001/sig0000017f ;
  wire \blk00000001/sig0000017e ;
  wire \blk00000001/sig0000017d ;
  wire \blk00000001/sig0000017c ;
  wire \blk00000001/sig0000017b ;
  wire \blk00000001/sig0000017a ;
  wire \blk00000001/sig00000179 ;
  wire \blk00000001/sig00000178 ;
  wire \blk00000001/sig00000177 ;
  wire \blk00000001/sig00000176 ;
  wire \blk00000001/sig00000175 ;
  wire \blk00000001/sig00000174 ;
  wire \blk00000001/sig00000173 ;
  wire \blk00000001/sig00000172 ;
  wire \blk00000001/sig00000171 ;
  wire \blk00000001/sig00000170 ;
  wire \blk00000001/sig0000016f ;
  wire \blk00000001/sig0000016e ;
  wire \blk00000001/sig0000016d ;
  wire \blk00000001/sig0000016c ;
  wire \blk00000001/sig0000016b ;
  wire \blk00000001/sig0000016a ;
  wire \blk00000001/sig00000169 ;
  wire \blk00000001/sig00000168 ;
  wire \blk00000001/sig00000167 ;
  wire \blk00000001/sig00000166 ;
  wire \blk00000001/sig00000165 ;
  wire \blk00000001/sig00000164 ;
  wire \blk00000001/sig00000163 ;
  wire \blk00000001/sig00000162 ;
  wire \blk00000001/sig00000161 ;
  wire \blk00000001/sig00000160 ;
  wire \blk00000001/sig0000015f ;
  wire \blk00000001/sig0000015e ;
  wire \blk00000001/sig0000015d ;
  wire \blk00000001/sig0000015c ;
  wire \blk00000001/sig0000015b ;
  wire \blk00000001/sig0000015a ;
  wire \blk00000001/sig00000159 ;
  wire \blk00000001/sig00000158 ;
  wire \blk00000001/sig00000157 ;
  wire \blk00000001/sig00000156 ;
  wire \blk00000001/sig00000155 ;
  wire \blk00000001/sig00000154 ;
  wire \blk00000001/sig00000153 ;
  wire \blk00000001/sig00000152 ;
  wire \blk00000001/sig00000151 ;
  wire \blk00000001/sig00000150 ;
  wire \blk00000001/sig0000014f ;
  wire \blk00000001/sig0000014e ;
  wire \blk00000001/sig0000014d ;
  wire \blk00000001/sig0000014c ;
  wire \blk00000001/sig0000014b ;
  wire \blk00000001/sig0000014a ;
  wire \blk00000001/sig00000149 ;
  wire \blk00000001/sig00000148 ;
  wire \blk00000001/sig00000147 ;
  wire \blk00000001/sig00000146 ;
  wire \blk00000001/sig00000145 ;
  wire \blk00000001/sig00000144 ;
  wire \blk00000001/sig00000143 ;
  wire \blk00000001/sig00000142 ;
  wire \blk00000001/sig00000141 ;
  wire \blk00000001/sig00000140 ;
  wire \blk00000001/sig0000013f ;
  wire \blk00000001/sig0000013e ;
  wire \blk00000001/sig0000013d ;
  wire \blk00000001/sig0000013c ;
  wire \blk00000001/sig0000013b ;
  wire \blk00000001/sig0000013a ;
  wire \blk00000001/sig00000139 ;
  wire \blk00000001/sig00000138 ;
  wire \blk00000001/sig00000137 ;
  wire \blk00000001/sig00000136 ;
  wire \blk00000001/sig00000135 ;
  wire \blk00000001/sig00000134 ;
  wire \blk00000001/sig00000133 ;
  wire \blk00000001/sig00000132 ;
  wire \blk00000001/sig00000131 ;
  wire \blk00000001/sig00000130 ;
  wire \blk00000001/sig0000012f ;
  wire \blk00000001/sig0000012e ;
  wire \blk00000001/sig0000012d ;
  wire \blk00000001/sig0000012c ;
  wire \blk00000001/sig0000012b ;
  wire \blk00000001/sig0000012a ;
  wire \blk00000001/sig00000129 ;
  wire \blk00000001/sig00000128 ;
  wire \blk00000001/sig00000127 ;
  wire \blk00000001/sig00000126 ;
  wire \blk00000001/sig00000125 ;
  wire \blk00000001/sig00000124 ;
  wire \blk00000001/sig00000123 ;
  wire \blk00000001/sig00000122 ;
  wire \blk00000001/sig00000121 ;
  wire \blk00000001/sig00000120 ;
  wire \blk00000001/sig0000011f ;
  wire \blk00000001/sig0000011e ;
  wire \blk00000001/sig0000011d ;
  wire \blk00000001/sig0000011c ;
  wire \blk00000001/sig0000011b ;
  wire \blk00000001/sig0000011a ;
  wire \blk00000001/sig00000119 ;
  wire \blk00000001/sig00000118 ;
  wire \blk00000001/sig00000117 ;
  wire \blk00000001/sig00000116 ;
  wire \blk00000001/sig00000115 ;
  wire \blk00000001/sig00000114 ;
  wire \blk00000001/sig00000113 ;
  wire \blk00000001/sig00000112 ;
  wire \blk00000001/sig00000111 ;
  wire \blk00000001/sig00000110 ;
  wire \blk00000001/sig0000010f ;
  wire \blk00000001/sig0000010e ;
  wire \blk00000001/sig0000010d ;
  wire \blk00000001/sig0000010c ;
  wire \blk00000001/sig0000010b ;
  wire \blk00000001/sig0000010a ;
  wire \blk00000001/sig00000109 ;
  wire \blk00000001/sig00000108 ;
  wire \blk00000001/sig00000107 ;
  wire \blk00000001/sig00000106 ;
  wire \blk00000001/sig00000105 ;
  wire \blk00000001/sig00000104 ;
  wire \blk00000001/sig00000103 ;
  wire \blk00000001/sig00000102 ;
  wire \blk00000001/sig00000101 ;
  wire \blk00000001/sig00000100 ;
  wire \blk00000001/sig000000ff ;
  wire \blk00000001/sig000000fe ;
  wire \blk00000001/sig000000fd ;
  wire \blk00000001/sig000000fc ;
  wire \blk00000001/sig000000fb ;
  wire \blk00000001/sig000000fa ;
  wire \blk00000001/sig000000f9 ;
  wire \blk00000001/sig000000f8 ;
  wire \blk00000001/sig000000f7 ;
  wire \blk00000001/sig000000f6 ;
  wire \blk00000001/sig000000f5 ;
  wire \blk00000001/sig000000f4 ;
  wire \blk00000001/sig000000f3 ;
  wire \blk00000001/sig000000f2 ;
  wire \blk00000001/sig000000f1 ;
  wire \blk00000001/sig000000f0 ;
  wire \blk00000001/sig000000ef ;
  wire \blk00000001/sig000000ee ;
  wire \blk00000001/sig000000ed ;
  wire \blk00000001/sig000000ec ;
  wire \blk00000001/sig000000eb ;
  wire \blk00000001/sig000000ea ;
  wire \blk00000001/sig000000e9 ;
  wire \blk00000001/sig000000e8 ;
  wire \blk00000001/sig000000e7 ;
  wire \blk00000001/sig000000e6 ;
  wire \blk00000001/sig000000e5 ;
  wire \blk00000001/sig000000e4 ;
  wire \blk00000001/sig000000e3 ;
  wire \blk00000001/sig000000e2 ;
  wire \blk00000001/sig000000e1 ;
  wire \blk00000001/sig000000e0 ;
  wire \blk00000001/sig000000df ;
  wire \blk00000001/sig000000de ;
  wire \blk00000001/sig000000dd ;
  wire \blk00000001/sig000000dc ;
  wire \blk00000001/sig000000db ;
  wire \blk00000001/sig000000da ;
  wire \blk00000001/sig000000d9 ;
  wire \blk00000001/sig000000d8 ;
  wire \blk00000001/sig000000d7 ;
  wire \blk00000001/sig000000d6 ;
  wire \blk00000001/sig000000d5 ;
  wire \blk00000001/sig000000d4 ;
  wire \blk00000001/sig000000d3 ;
  wire \blk00000001/sig000000d2 ;
  wire \blk00000001/sig000000d1 ;
  wire \blk00000001/sig000000d0 ;
  wire \blk00000001/sig000000cf ;
  wire \blk00000001/sig000000ce ;
  wire \blk00000001/sig000000cd ;
  wire \blk00000001/sig000000cc ;
  wire \blk00000001/sig000000cb ;
  wire \blk00000001/sig000000ca ;
  wire \blk00000001/sig000000c9 ;
  wire \blk00000001/sig000000c8 ;
  wire \blk00000001/sig000000c7 ;
  wire \blk00000001/sig000000c6 ;
  wire \blk00000001/sig000000c5 ;
  wire \blk00000001/sig000000c4 ;
  wire \blk00000001/sig000000c3 ;
  wire \blk00000001/sig000000c2 ;
  wire \blk00000001/sig000000c1 ;
  wire \blk00000001/sig000000c0 ;
  wire \blk00000001/sig000000bf ;
  wire \blk00000001/sig000000be ;
  wire \blk00000001/sig000000bd ;
  wire \blk00000001/sig000000bc ;
  wire \blk00000001/sig000000bb ;
  wire \blk00000001/sig000000ba ;
  wire \blk00000001/sig000000b9 ;
  wire \blk00000001/sig000000b8 ;
  wire \blk00000001/sig000000b7 ;
  wire \blk00000001/sig000000b6 ;
  wire \blk00000001/sig000000b5 ;
  wire \blk00000001/sig000000b4 ;
  wire \blk00000001/sig000000b3 ;
  wire \blk00000001/sig000000b2 ;
  wire \blk00000001/sig000000b1 ;
  wire \blk00000001/sig000000b0 ;
  wire \blk00000001/sig000000af ;
  wire \blk00000001/sig000000ae ;
  wire \blk00000001/sig000000ad ;
  wire \blk00000001/sig000000ac ;
  wire \blk00000001/sig000000ab ;
  wire \blk00000001/sig000000aa ;
  wire \blk00000001/sig000000a9 ;
  wire \blk00000001/sig000000a8 ;
  wire \blk00000001/sig000000a7 ;
  wire \blk00000001/sig000000a6 ;
  wire \blk00000001/sig000000a5 ;
  wire \blk00000001/sig000000a4 ;
  wire \blk00000001/sig000000a3 ;
  wire \blk00000001/sig000000a2 ;
  wire \blk00000001/sig000000a1 ;
  wire \blk00000001/sig000000a0 ;
  wire \blk00000001/sig0000009f ;
  wire \blk00000001/sig0000009e ;
  wire \blk00000001/sig0000009d ;
  wire \blk00000001/sig0000009c ;
  wire \blk00000001/sig0000009b ;
  wire \blk00000001/sig0000009a ;
  wire \blk00000001/sig00000099 ;
  wire \blk00000001/sig00000098 ;
  wire \blk00000001/sig00000097 ;
  wire \blk00000001/sig00000096 ;
  wire \blk00000001/sig00000095 ;
  wire \blk00000001/sig00000094 ;
  wire \blk00000001/sig00000093 ;
  wire \blk00000001/sig00000092 ;
  wire \blk00000001/sig00000091 ;
  wire \blk00000001/sig00000090 ;
  wire \blk00000001/sig0000008f ;
  wire \blk00000001/sig0000008e ;
  wire \blk00000001/sig0000008d ;
  wire \blk00000001/sig0000008c ;
  wire \blk00000001/sig0000008b ;
  wire \blk00000001/sig0000008a ;
  wire \blk00000001/sig00000089 ;
  wire \blk00000001/sig00000088 ;
  wire \blk00000001/sig00000087 ;
  wire \blk00000001/sig00000086 ;
  wire \blk00000001/sig00000085 ;
  wire \blk00000001/sig00000084 ;
  wire \blk00000001/sig00000083 ;
  wire \blk00000001/sig00000082 ;
  wire \blk00000001/sig00000081 ;
  wire \blk00000001/sig00000080 ;
  wire \blk00000001/sig0000007f ;
  wire \blk00000001/sig0000007e ;
  wire \blk00000001/sig0000007d ;
  wire \blk00000001/sig0000007c ;
  wire \blk00000001/sig0000007b ;
  wire \blk00000001/sig0000007a ;
  wire \blk00000001/sig00000079 ;
  wire \blk00000001/sig00000078 ;
  wire \blk00000001/sig00000077 ;
  wire \blk00000001/sig00000076 ;
  wire \blk00000001/sig00000075 ;
  wire \blk00000001/sig00000074 ;
  wire \blk00000001/sig00000073 ;
  wire \blk00000001/sig00000072 ;
  wire \blk00000001/sig00000071 ;
  wire \blk00000001/sig00000070 ;
  wire \blk00000001/sig0000006f ;
  wire \blk00000001/sig0000006e ;
  wire \blk00000001/sig0000006d ;
  wire \blk00000001/sig0000006c ;
  wire \blk00000001/sig0000006b ;
  wire \blk00000001/sig0000006a ;
  wire \blk00000001/sig00000069 ;
  wire \blk00000001/sig00000068 ;
  wire \blk00000001/sig00000067 ;
  wire \blk00000001/sig00000066 ;
  wire \blk00000001/sig00000065 ;
  wire \blk00000001/sig00000064 ;
  wire \blk00000001/sig00000063 ;
  wire \blk00000001/sig00000062 ;
  wire \blk00000001/sig00000061 ;
  wire \blk00000001/sig00000060 ;
  wire \blk00000001/sig0000005f ;
  wire \blk00000001/sig0000005e ;
  wire \blk00000001/sig0000005d ;
  wire \blk00000001/sig0000005c ;
  wire \blk00000001/sig0000005b ;
  wire \blk00000001/sig0000005a ;
  wire \blk00000001/sig00000059 ;
  wire \blk00000001/sig00000058 ;
  wire \blk00000001/sig00000057 ;
  wire \blk00000001/sig00000056 ;
  wire \blk00000001/sig00000055 ;
  wire \blk00000001/sig00000054 ;
  wire \blk00000001/sig00000053 ;
  wire \blk00000001/sig00000052 ;
  wire \blk00000001/sig00000051 ;
  wire \blk00000001/sig00000050 ;
  wire \blk00000001/sig0000004f ;
  wire \blk00000001/sig0000004e ;
  wire \blk00000001/sig0000004d ;
  wire \blk00000001/sig0000004c ;
  wire \blk00000001/sig0000004b ;
  wire \blk00000001/sig0000004a ;
  wire \blk00000001/sig00000049 ;
  wire \blk00000001/sig00000048 ;
  wire \blk00000001/sig00000047 ;
  wire \blk00000001/sig00000046 ;
  wire \blk00000001/sig00000044 ;
  wire \blk00000001/sig00000043 ;
  wire \NLW_blk00000001/blk0000004b_O_UNCONNECTED ;
  FDRE #(
    .INIT ( 1'b0 ))
  \blk00000001/blk000001a2  (
    .C(clk),
    .CE(ce),
    .D(\blk00000001/sig00000192 ),
    .R(sclr),
    .Q(s[3])
  );
  LUT2 #(
    .INIT ( 4'h8 ))
  \blk00000001/blk000001a1  (
    .I0(\blk00000001/sig00000191 ),
    .I1(\blk00000001/sig0000004c ),
    .O(\blk00000001/sig00000192 )
  );
  FDRE #(
    .INIT ( 1'b0 ))
  \blk00000001/blk000001a0  (
    .C(clk),
    .CE(ce),
    .D(\blk00000001/sig0000018f ),
    .R(sclr),
    .Q(s[2])
  );
  LUT2 #(
    .INIT ( 4'h8 ))
  \blk00000001/blk0000019f  (
    .I0(\blk00000001/sig0000018e ),
    .I1(\blk00000001/sig0000004c ),
    .O(\blk00000001/sig0000018f )
  );
  FDRE #(
    .INIT ( 1'b0 ))
  \blk00000001/blk0000019e  (
    .C(clk),
    .CE(ce),
    .D(\blk00000001/sig0000018c ),
    .R(sclr),
    .Q(s[1])
  );
  LUT2 #(
    .INIT ( 4'h8 ))
  \blk00000001/blk0000019d  (
    .I0(\blk00000001/sig0000018b ),
    .I1(\blk00000001/sig0000004c ),
    .O(\blk00000001/sig0000018c )
  );
  FDRE #(
    .INIT ( 1'b0 ))
  \blk00000001/blk0000019c  (
    .C(clk),
    .CE(ce),
    .D(\blk00000001/sig00000189 ),
    .R(sclr),
    .Q(s[0])
  );
  LUT2 #(
    .INIT ( 4'h8 ))
  \blk00000001/blk0000019b  (
    .I0(\blk00000001/sig00000188 ),
    .I1(\blk00000001/sig0000004c ),
    .O(\blk00000001/sig00000189 )
  );
  FDRE #(
    .INIT ( 1'b0 ))
  \blk00000001/blk0000019a  (
    .C(clk),
    .CE(ce),
    .D(\blk00000001/sig0000019c ),
    .R(sclr),
    .Q(s[5])
  );
  LUT2 #(
    .INIT ( 4'h8 ))
  \blk00000001/blk00000199  (
    .I0(\blk00000001/sig0000019b ),
    .I1(\blk00000001/sig0000004b ),
    .O(\blk00000001/sig0000019c )
  );
  FDRE #(
    .INIT ( 1'b0 ))
  \blk00000001/blk00000198  (
    .C(clk),
    .CE(ce),
    .D(\blk00000001/sig00000199 ),
    .R(sclr),
    .Q(s[4])
  );
  LUT2 #(
    .INIT ( 4'h8 ))
  \blk00000001/blk00000197  (
    .I0(\blk00000001/sig00000198 ),
    .I1(\blk00000001/sig0000004b ),
    .O(\blk00000001/sig00000199 )
  );
  FDRE #(
    .INIT ( 1'b0 ))
  \blk00000001/blk00000196  (
    .C(clk),
    .CE(ce),
    .D(\blk00000001/sig00000182 ),
    .R(sclr),
    .Q(\blk00000001/sig00000180 )
  );
  LUT2 #(
    .INIT ( 4'h8 ))
  \blk00000001/blk00000195  (
    .I0(\blk00000001/sig00000181 ),
    .I1(\blk00000001/sig0000004b ),
    .O(\blk00000001/sig00000182 )
  );
  FDRE #(
    .INIT ( 1'b0 ))
  \blk00000001/blk00000194  (
    .C(clk),
    .CE(ce),
    .D(\blk00000001/sig0000017f ),
    .R(sclr),
    .Q(\blk00000001/sig0000017d )
  );
  LUT2 #(
    .INIT ( 4'h8 ))
  \blk00000001/blk00000193  (
    .I0(\blk00000001/sig0000017e ),
    .I1(\blk00000001/sig0000004b ),
    .O(\blk00000001/sig0000017f )
  );
  FDRE #(
    .INIT ( 1'b0 ))
  \blk00000001/blk00000192  (
    .C(clk),
    .CE(ce),
    .D(\blk00000001/sig0000017c ),
    .R(sclr),
    .Q(\blk00000001/sig0000017a )
  );
  LUT2 #(
    .INIT ( 4'h8 ))
  \blk00000001/blk00000191  (
    .I0(\blk00000001/sig0000017b ),
    .I1(\blk00000001/sig0000004b ),
    .O(\blk00000001/sig0000017c )
  );
  FDRE #(
    .INIT ( 1'b0 ))
  \blk00000001/blk00000190  (
    .C(clk),
    .CE(ce),
    .D(\blk00000001/sig00000179 ),
    .R(sclr),
    .Q(\blk00000001/sig00000177 )
  );
  LUT2 #(
    .INIT ( 4'h8 ))
  \blk00000001/blk0000018f  (
    .I0(\blk00000001/sig00000178 ),
    .I1(\blk00000001/sig0000004b ),
    .O(\blk00000001/sig00000179 )
  );
  FDRE #(
    .INIT ( 1'b0 ))
  \blk00000001/blk0000018e  (
    .C(clk),
    .CE(ce),
    .D(\blk00000001/sig000001ac ),
    .R(sclr),
    .Q(s[9])
  );
  LUT2 #(
    .INIT ( 4'h8 ))
  \blk00000001/blk0000018d  (
    .I0(\blk00000001/sig000001ab ),
    .I1(\blk00000001/sig0000004a ),
    .O(\blk00000001/sig000001ac )
  );
  FDRE #(
    .INIT ( 1'b0 ))
  \blk00000001/blk0000018c  (
    .C(clk),
    .CE(ce),
    .D(\blk00000001/sig000001a9 ),
    .R(sclr),
    .Q(s[8])
  );
  LUT2 #(
    .INIT ( 4'h8 ))
  \blk00000001/blk0000018b  (
    .I0(\blk00000001/sig000001a8 ),
    .I1(\blk00000001/sig0000004a ),
    .O(\blk00000001/sig000001a9 )
  );
  FDRE #(
    .INIT ( 1'b0 ))
  \blk00000001/blk0000018a  (
    .C(clk),
    .CE(ce),
    .D(\blk00000001/sig000001a2 ),
    .R(sclr),
    .Q(s[7])
  );
  LUT2 #(
    .INIT ( 4'h8 ))
  \blk00000001/blk00000189  (
    .I0(\blk00000001/sig000001a1 ),
    .I1(\blk00000001/sig0000004a ),
    .O(\blk00000001/sig000001a2 )
  );
  FDRE #(
    .INIT ( 1'b0 ))
  \blk00000001/blk00000188  (
    .C(clk),
    .CE(ce),
    .D(\blk00000001/sig0000019f ),
    .R(sclr),
    .Q(s[6])
  );
  LUT2 #(
    .INIT ( 4'h8 ))
  \blk00000001/blk00000187  (
    .I0(\blk00000001/sig0000019e ),
    .I1(\blk00000001/sig0000004a ),
    .O(\blk00000001/sig0000019f )
  );
  FDRE #(
    .INIT ( 1'b0 ))
  \blk00000001/blk00000186  (
    .C(clk),
    .CE(ce),
    .D(\blk00000001/sig0000015c ),
    .R(sclr),
    .Q(\blk00000001/sig0000015a )
  );
  LUT2 #(
    .INIT ( 4'h8 ))
  \blk00000001/blk00000185  (
    .I0(\blk00000001/sig0000015b ),
    .I1(\blk00000001/sig0000004a ),
    .O(\blk00000001/sig0000015c )
  );
  FDRE #(
    .INIT ( 1'b0 ))
  \blk00000001/blk00000184  (
    .C(clk),
    .CE(ce),
    .D(\blk00000001/sig00000159 ),
    .R(sclr),
    .Q(\blk00000001/sig00000157 )
  );
  LUT2 #(
    .INIT ( 4'h8 ))
  \blk00000001/blk00000183  (
    .I0(\blk00000001/sig00000158 ),
    .I1(\blk00000001/sig0000004a ),
    .O(\blk00000001/sig00000159 )
  );
  FDRE #(
    .INIT ( 1'b0 ))
  \blk00000001/blk00000182  (
    .C(clk),
    .CE(ce),
    .D(\blk00000001/sig00000156 ),
    .R(sclr),
    .Q(\blk00000001/sig00000154 )
  );
  LUT2 #(
    .INIT ( 4'h8 ))
  \blk00000001/blk00000181  (
    .I0(\blk00000001/sig00000155 ),
    .I1(\blk00000001/sig0000004a ),
    .O(\blk00000001/sig00000156 )
  );
  FDRE #(
    .INIT ( 1'b0 ))
  \blk00000001/blk00000180  (
    .C(clk),
    .CE(ce),
    .D(\blk00000001/sig00000153 ),
    .R(sclr),
    .Q(\blk00000001/sig00000151 )
  );
  LUT2 #(
    .INIT ( 4'h8 ))
  \blk00000001/blk0000017f  (
    .I0(\blk00000001/sig00000152 ),
    .I1(\blk00000001/sig0000004a ),
    .O(\blk00000001/sig00000153 )
  );
  FDRE #(
    .INIT ( 1'b0 ))
  \blk00000001/blk0000017e  (
    .C(clk),
    .CE(ce),
    .D(\blk00000001/sig0000014c ),
    .R(sclr),
    .Q(\blk00000001/sig0000014a )
  );
  LUT2 #(
    .INIT ( 4'h8 ))
  \blk00000001/blk0000017d  (
    .I0(\blk00000001/sig0000014b ),
    .I1(\blk00000001/sig0000004a ),
    .O(\blk00000001/sig0000014c )
  );
  FDRE #(
    .INIT ( 1'b0 ))
  \blk00000001/blk0000017c  (
    .C(clk),
    .CE(ce),
    .D(\blk00000001/sig000001bc ),
    .R(sclr),
    .Q(s[13])
  );
  LUT2 #(
    .INIT ( 4'h8 ))
  \blk00000001/blk0000017b  (
    .I0(\blk00000001/sig000001bb ),
    .I1(\blk00000001/sig00000049 ),
    .O(\blk00000001/sig000001bc )
  );
  FDRE #(
    .INIT ( 1'b0 ))
  \blk00000001/blk0000017a  (
    .C(clk),
    .CE(ce),
    .D(\blk00000001/sig000001b9 ),
    .R(sclr),
    .Q(s[12])
  );
  LUT2 #(
    .INIT ( 4'h8 ))
  \blk00000001/blk00000179  (
    .I0(\blk00000001/sig000001b8 ),
    .I1(\blk00000001/sig00000049 ),
    .O(\blk00000001/sig000001b9 )
  );
  FDRE #(
    .INIT ( 1'b0 ))
  \blk00000001/blk00000178  (
    .C(clk),
    .CE(ce),
    .D(\blk00000001/sig000001b2 ),
    .R(sclr),
    .Q(s[11])
  );
  LUT2 #(
    .INIT ( 4'h8 ))
  \blk00000001/blk00000177  (
    .I0(\blk00000001/sig000001b1 ),
    .I1(\blk00000001/sig00000049 ),
    .O(\blk00000001/sig000001b2 )
  );
  FDRE #(
    .INIT ( 1'b0 ))
  \blk00000001/blk00000176  (
    .C(clk),
    .CE(ce),
    .D(\blk00000001/sig000001af ),
    .R(sclr),
    .Q(s[10])
  );
  LUT2 #(
    .INIT ( 4'h8 ))
  \blk00000001/blk00000175  (
    .I0(\blk00000001/sig000001ae ),
    .I1(\blk00000001/sig00000049 ),
    .O(\blk00000001/sig000001af )
  );
  FDRE #(
    .INIT ( 1'b0 ))
  \blk00000001/blk00000174  (
    .C(clk),
    .CE(ce),
    .D(\blk00000001/sig00000136 ),
    .R(sclr),
    .Q(\blk00000001/sig00000134 )
  );
  LUT2 #(
    .INIT ( 4'h8 ))
  \blk00000001/blk00000173  (
    .I0(\blk00000001/sig00000135 ),
    .I1(\blk00000001/sig00000049 ),
    .O(\blk00000001/sig00000136 )
  );
  FDRE #(
    .INIT ( 1'b0 ))
  \blk00000001/blk00000172  (
    .C(clk),
    .CE(ce),
    .D(\blk00000001/sig00000133 ),
    .R(sclr),
    .Q(\blk00000001/sig00000131 )
  );
  LUT2 #(
    .INIT ( 4'h8 ))
  \blk00000001/blk00000171  (
    .I0(\blk00000001/sig00000132 ),
    .I1(\blk00000001/sig00000049 ),
    .O(\blk00000001/sig00000133 )
  );
  FDRE #(
    .INIT ( 1'b0 ))
  \blk00000001/blk00000170  (
    .C(clk),
    .CE(ce),
    .D(\blk00000001/sig00000130 ),
    .R(sclr),
    .Q(\blk00000001/sig0000012e )
  );
  LUT2 #(
    .INIT ( 4'h8 ))
  \blk00000001/blk0000016f  (
    .I0(\blk00000001/sig0000012f ),
    .I1(\blk00000001/sig00000049 ),
    .O(\blk00000001/sig00000130 )
  );
  FDRE #(
    .INIT ( 1'b0 ))
  \blk00000001/blk0000016e  (
    .C(clk),
    .CE(ce),
    .D(\blk00000001/sig0000012d ),
    .R(sclr),
    .Q(\blk00000001/sig0000012b )
  );
  LUT2 #(
    .INIT ( 4'h8 ))
  \blk00000001/blk0000016d  (
    .I0(\blk00000001/sig0000012c ),
    .I1(\blk00000001/sig00000049 ),
    .O(\blk00000001/sig0000012d )
  );
  FDRE #(
    .INIT ( 1'b0 ))
  \blk00000001/blk0000016c  (
    .C(clk),
    .CE(ce),
    .D(\blk00000001/sig00000126 ),
    .R(sclr),
    .Q(\blk00000001/sig00000124 )
  );
  LUT2 #(
    .INIT ( 4'h8 ))
  \blk00000001/blk0000016b  (
    .I0(\blk00000001/sig00000125 ),
    .I1(\blk00000001/sig00000049 ),
    .O(\blk00000001/sig00000126 )
  );
  FDRE #(
    .INIT ( 1'b0 ))
  \blk00000001/blk0000016a  (
    .C(clk),
    .CE(ce),
    .D(\blk00000001/sig000001ca ),
    .R(sclr),
    .Q(s[17])
  );
  LUT2 #(
    .INIT ( 4'h8 ))
  \blk00000001/blk00000169  (
    .I0(\blk00000001/sig000001c9 ),
    .I1(\blk00000001/sig00000048 ),
    .O(\blk00000001/sig000001ca )
  );
  FDRE #(
    .INIT ( 1'b0 ))
  \blk00000001/blk00000168  (
    .C(clk),
    .CE(ce),
    .D(\blk00000001/sig000001c7 ),
    .R(sclr),
    .Q(s[16])
  );
  LUT2 #(
    .INIT ( 4'h8 ))
  \blk00000001/blk00000167  (
    .I0(\blk00000001/sig000001c6 ),
    .I1(\blk00000001/sig00000048 ),
    .O(\blk00000001/sig000001c7 )
  );
  FDRE #(
    .INIT ( 1'b0 ))
  \blk00000001/blk00000166  (
    .C(clk),
    .CE(ce),
    .D(\blk00000001/sig000001c2 ),
    .R(sclr),
    .Q(s[15])
  );
  LUT2 #(
    .INIT ( 4'h8 ))
  \blk00000001/blk00000165  (
    .I0(\blk00000001/sig000001c1 ),
    .I1(\blk00000001/sig00000048 ),
    .O(\blk00000001/sig000001c2 )
  );
  FDRE #(
    .INIT ( 1'b0 ))
  \blk00000001/blk00000164  (
    .C(clk),
    .CE(ce),
    .D(\blk00000001/sig000001bf ),
    .R(sclr),
    .Q(s[14])
  );
  LUT2 #(
    .INIT ( 4'h8 ))
  \blk00000001/blk00000163  (
    .I0(\blk00000001/sig000001be ),
    .I1(\blk00000001/sig00000048 ),
    .O(\blk00000001/sig000001bf )
  );
  FDRE #(
    .INIT ( 1'b0 ))
  \blk00000001/blk00000162  (
    .C(clk),
    .CE(ce),
    .D(\blk00000001/sig00000114 ),
    .R(sclr),
    .Q(\blk00000001/sig00000112 )
  );
  LUT2 #(
    .INIT ( 4'h8 ))
  \blk00000001/blk00000161  (
    .I0(\blk00000001/sig00000113 ),
    .I1(\blk00000001/sig00000048 ),
    .O(\blk00000001/sig00000114 )
  );
  FDRE #(
    .INIT ( 1'b0 ))
  \blk00000001/blk00000160  (
    .C(clk),
    .CE(ce),
    .D(\blk00000001/sig00000111 ),
    .R(sclr),
    .Q(\blk00000001/sig0000010f )
  );
  LUT2 #(
    .INIT ( 4'h8 ))
  \blk00000001/blk0000015f  (
    .I0(\blk00000001/sig00000110 ),
    .I1(\blk00000001/sig00000048 ),
    .O(\blk00000001/sig00000111 )
  );
  FDRE #(
    .INIT ( 1'b0 ))
  \blk00000001/blk0000015e  (
    .C(clk),
    .CE(ce),
    .D(\blk00000001/sig0000010e ),
    .R(sclr),
    .Q(\blk00000001/sig0000010c )
  );
  LUT2 #(
    .INIT ( 4'h8 ))
  \blk00000001/blk0000015d  (
    .I0(\blk00000001/sig0000010d ),
    .I1(\blk00000001/sig00000048 ),
    .O(\blk00000001/sig0000010e )
  );
  FDRE #(
    .INIT ( 1'b0 ))
  \blk00000001/blk0000015c  (
    .C(clk),
    .CE(ce),
    .D(\blk00000001/sig0000010b ),
    .R(sclr),
    .Q(\blk00000001/sig00000109 )
  );
  LUT2 #(
    .INIT ( 4'h8 ))
  \blk00000001/blk0000015b  (
    .I0(\blk00000001/sig0000010a ),
    .I1(\blk00000001/sig00000048 ),
    .O(\blk00000001/sig0000010b )
  );
  FDRE #(
    .INIT ( 1'b0 ))
  \blk00000001/blk0000015a  (
    .C(clk),
    .CE(ce),
    .D(\blk00000001/sig00000104 ),
    .R(sclr),
    .Q(\blk00000001/sig00000102 )
  );
  LUT2 #(
    .INIT ( 4'h8 ))
  \blk00000001/blk00000159  (
    .I0(\blk00000001/sig00000103 ),
    .I1(\blk00000001/sig00000048 ),
    .O(\blk00000001/sig00000104 )
  );
  FDRE   \blk00000001/blk00000158  (
    .C(clk),
    .CE(ce),
    .D(\blk00000001/sig0000004b ),
    .R(sclr),
    .Q(\blk00000001/sig0000004c )
  );
  FDRE   \blk00000001/blk00000157  (
    .C(clk),
    .CE(ce),
    .D(\blk00000001/sig0000004a ),
    .R(sclr),
    .Q(\blk00000001/sig0000004b )
  );
  FDRE   \blk00000001/blk00000156  (
    .C(clk),
    .CE(ce),
    .D(\blk00000001/sig00000049 ),
    .R(sclr),
    .Q(\blk00000001/sig0000004a )
  );
  FDRE   \blk00000001/blk00000155  (
    .C(clk),
    .CE(ce),
    .D(\blk00000001/sig00000048 ),
    .R(sclr),
    .Q(\blk00000001/sig00000049 )
  );
  FDRE   \blk00000001/blk00000154  (
    .C(clk),
    .CE(ce),
    .D(\blk00000001/sig00000047 ),
    .R(sclr),
    .Q(\blk00000001/sig00000048 )
  );
  FDRE   \blk00000001/blk00000153  (
    .C(clk),
    .CE(ce),
    .D(\blk00000001/sig00000046 ),
    .R(sclr),
    .Q(\blk00000001/sig00000047 )
  );
  FDRE   \blk00000001/blk00000152  (
    .C(clk),
    .CE(ce),
    .D(\blk00000001/sig00000044 ),
    .R(sclr),
    .Q(\blk00000001/sig00000046 )
  );
  FDE #(
    .INIT ( 1'b0 ))
  \blk00000001/blk00000151  (
    .C(clk),
    .CE(ce),
    .D(\blk00000001/sig000001c3 ),
    .Q(\blk00000001/sig000001c6 )
  );
  SRL16E #(
    .INIT ( 16'h0000 ))
  \blk00000001/blk00000150  (
    .A0(\blk00000001/sig00000044 ),
    .A1(\blk00000001/sig00000043 ),
    .A2(\blk00000001/sig00000043 ),
    .A3(\blk00000001/sig00000043 ),
    .CE(ce),
    .CLK(clk),
    .D(\blk00000001/sig0000011e ),
    .Q(\blk00000001/sig000001c3 )
  );
  FDE #(
    .INIT ( 1'b0 ))
  \blk00000001/blk0000014f  (
    .C(clk),
    .CE(ce),
    .D(\blk00000001/sig000001c4 ),
    .Q(\blk00000001/sig000001c9 )
  );
  SRL16E #(
    .INIT ( 16'h0000 ))
  \blk00000001/blk0000014e  (
    .A0(\blk00000001/sig00000044 ),
    .A1(\blk00000001/sig00000043 ),
    .A2(\blk00000001/sig00000043 ),
    .A3(\blk00000001/sig00000043 ),
    .CE(ce),
    .CLK(clk),
    .D(\blk00000001/sig0000011f ),
    .Q(\blk00000001/sig000001c4 )
  );
  FDE #(
    .INIT ( 1'b0 ))
  \blk00000001/blk0000014d  (
    .C(clk),
    .CE(ce),
    .D(\blk00000001/sig000001b3 ),
    .Q(\blk00000001/sig000001b8 )
  );
  SRL16E #(
    .INIT ( 16'h0000 ))
  \blk00000001/blk0000014c  (
    .A0(\blk00000001/sig00000043 ),
    .A1(\blk00000001/sig00000044 ),
    .A2(\blk00000001/sig00000043 ),
    .A3(\blk00000001/sig00000043 ),
    .CE(ce),
    .CLK(clk),
    .D(\blk00000001/sig000000fc ),
    .Q(\blk00000001/sig000001b3 )
  );
  FDE #(
    .INIT ( 1'b0 ))
  \blk00000001/blk0000014b  (
    .C(clk),
    .CE(ce),
    .D(\blk00000001/sig000001b4 ),
    .Q(\blk00000001/sig000001bb )
  );
  SRL16E #(
    .INIT ( 16'h0000 ))
  \blk00000001/blk0000014a  (
    .A0(\blk00000001/sig00000043 ),
    .A1(\blk00000001/sig00000044 ),
    .A2(\blk00000001/sig00000043 ),
    .A3(\blk00000001/sig00000043 ),
    .CE(ce),
    .CLK(clk),
    .D(\blk00000001/sig000000fd ),
    .Q(\blk00000001/sig000001b4 )
  );
  FDE #(
    .INIT ( 1'b0 ))
  \blk00000001/blk00000149  (
    .C(clk),
    .CE(ce),
    .D(\blk00000001/sig000001b5 ),
    .Q(\blk00000001/sig000001be )
  );
  SRL16E #(
    .INIT ( 16'h0000 ))
  \blk00000001/blk00000148  (
    .A0(\blk00000001/sig00000044 ),
    .A1(\blk00000001/sig00000043 ),
    .A2(\blk00000001/sig00000043 ),
    .A3(\blk00000001/sig00000043 ),
    .CE(ce),
    .CLK(clk),
    .D(\blk00000001/sig000000f6 ),
    .Q(\blk00000001/sig000001b5 )
  );
  FDE #(
    .INIT ( 1'b0 ))
  \blk00000001/blk00000147  (
    .C(clk),
    .CE(ce),
    .D(\blk00000001/sig000001b6 ),
    .Q(\blk00000001/sig000001c1 )
  );
  SRL16E #(
    .INIT ( 16'h0000 ))
  \blk00000001/blk00000146  (
    .A0(\blk00000001/sig00000044 ),
    .A1(\blk00000001/sig00000043 ),
    .A2(\blk00000001/sig00000043 ),
    .A3(\blk00000001/sig00000043 ),
    .CE(ce),
    .CLK(clk),
    .D(\blk00000001/sig000000f7 ),
    .Q(\blk00000001/sig000001b6 )
  );
  FDE #(
    .INIT ( 1'b0 ))
  \blk00000001/blk00000145  (
    .C(clk),
    .CE(ce),
    .D(\blk00000001/sig000001a4 ),
    .Q(\blk00000001/sig000001ab )
  );
  SRL16E #(
    .INIT ( 16'h0000 ))
  \blk00000001/blk00000144  (
    .A0(\blk00000001/sig00000044 ),
    .A1(\blk00000001/sig00000044 ),
    .A2(\blk00000001/sig00000043 ),
    .A3(\blk00000001/sig00000043 ),
    .CE(ce),
    .CLK(clk),
    .D(\blk00000001/sig000000ea ),
    .Q(\blk00000001/sig000001a4 )
  );
  FDE #(
    .INIT ( 1'b0 ))
  \blk00000001/blk00000143  (
    .C(clk),
    .CE(ce),
    .D(\blk00000001/sig000001a5 ),
    .Q(\blk00000001/sig000001ae )
  );
  SRL16E #(
    .INIT ( 16'h0000 ))
  \blk00000001/blk00000142  (
    .A0(\blk00000001/sig00000043 ),
    .A1(\blk00000001/sig00000044 ),
    .A2(\blk00000001/sig00000043 ),
    .A3(\blk00000001/sig00000043 ),
    .CE(ce),
    .CLK(clk),
    .D(\blk00000001/sig000000e3 ),
    .Q(\blk00000001/sig000001a5 )
  );
  FDE #(
    .INIT ( 1'b0 ))
  \blk00000001/blk00000141  (
    .C(clk),
    .CE(ce),
    .D(\blk00000001/sig000001a3 ),
    .Q(\blk00000001/sig000001a8 )
  );
  SRL16E #(
    .INIT ( 16'h0000 ))
  \blk00000001/blk00000140  (
    .A0(\blk00000001/sig00000044 ),
    .A1(\blk00000001/sig00000044 ),
    .A2(\blk00000001/sig00000043 ),
    .A3(\blk00000001/sig00000043 ),
    .CE(ce),
    .CLK(clk),
    .D(\blk00000001/sig000000e9 ),
    .Q(\blk00000001/sig000001a3 )
  );
  FDE #(
    .INIT ( 1'b0 ))
  \blk00000001/blk0000013f  (
    .C(clk),
    .CE(ce),
    .D(\blk00000001/sig000001a6 ),
    .Q(\blk00000001/sig000001b1 )
  );
  SRL16E #(
    .INIT ( 16'h0000 ))
  \blk00000001/blk0000013e  (
    .A0(\blk00000001/sig00000043 ),
    .A1(\blk00000001/sig00000044 ),
    .A2(\blk00000001/sig00000043 ),
    .A3(\blk00000001/sig00000043 ),
    .CE(ce),
    .CLK(clk),
    .D(\blk00000001/sig000000e4 ),
    .Q(\blk00000001/sig000001a6 )
  );
  FDE #(
    .INIT ( 1'b0 ))
  \blk00000001/blk0000013d  (
    .C(clk),
    .CE(ce),
    .D(\blk00000001/sig00000193 ),
    .Q(\blk00000001/sig00000198 )
  );
  SRL16E #(
    .INIT ( 16'h0000 ))
  \blk00000001/blk0000013c  (
    .A0(\blk00000001/sig00000043 ),
    .A1(\blk00000001/sig00000043 ),
    .A2(\blk00000001/sig00000044 ),
    .A3(\blk00000001/sig00000043 ),
    .CE(ce),
    .CLK(clk),
    .D(\blk00000001/sig000000d1 ),
    .Q(\blk00000001/sig00000193 )
  );
  FDE #(
    .INIT ( 1'b0 ))
  \blk00000001/blk0000013b  (
    .C(clk),
    .CE(ce),
    .D(\blk00000001/sig00000194 ),
    .Q(\blk00000001/sig0000019b )
  );
  SRL16E #(
    .INIT ( 16'h0000 ))
  \blk00000001/blk0000013a  (
    .A0(\blk00000001/sig00000043 ),
    .A1(\blk00000001/sig00000043 ),
    .A2(\blk00000001/sig00000044 ),
    .A3(\blk00000001/sig00000043 ),
    .CE(ce),
    .CLK(clk),
    .D(\blk00000001/sig000000d2 ),
    .Q(\blk00000001/sig00000194 )
  );
  FDE #(
    .INIT ( 1'b0 ))
  \blk00000001/blk00000139  (
    .C(clk),
    .CE(ce),
    .D(\blk00000001/sig00000195 ),
    .Q(\blk00000001/sig0000019e )
  );
  SRL16E #(
    .INIT ( 16'h0000 ))
  \blk00000001/blk00000138  (
    .A0(\blk00000001/sig00000044 ),
    .A1(\blk00000001/sig00000044 ),
    .A2(\blk00000001/sig00000043 ),
    .A3(\blk00000001/sig00000043 ),
    .CE(ce),
    .CLK(clk),
    .D(\blk00000001/sig000000cb ),
    .Q(\blk00000001/sig00000195 )
  );
  FDE #(
    .INIT ( 1'b0 ))
  \blk00000001/blk00000137  (
    .C(clk),
    .CE(ce),
    .D(\blk00000001/sig00000196 ),
    .Q(\blk00000001/sig000001a1 )
  );
  SRL16E #(
    .INIT ( 16'h0000 ))
  \blk00000001/blk00000136  (
    .A0(\blk00000001/sig00000044 ),
    .A1(\blk00000001/sig00000044 ),
    .A2(\blk00000001/sig00000043 ),
    .A3(\blk00000001/sig00000043 ),
    .CE(ce),
    .CLK(clk),
    .D(\blk00000001/sig000000cc ),
    .Q(\blk00000001/sig00000196 )
  );
  FDE #(
    .INIT ( 1'b0 ))
  \blk00000001/blk00000135  (
    .C(clk),
    .CE(ce),
    .D(\blk00000001/sig00000183 ),
    .Q(\blk00000001/sig00000188 )
  );
  SRL16E #(
    .INIT ( 16'h0000 ))
  \blk00000001/blk00000134  (
    .A0(\blk00000001/sig00000044 ),
    .A1(\blk00000001/sig00000043 ),
    .A2(\blk00000001/sig00000044 ),
    .A3(\blk00000001/sig00000043 ),
    .CE(ce),
    .CLK(clk),
    .D(\blk00000001/sig00000062 ),
    .Q(\blk00000001/sig00000183 )
  );
  FDE #(
    .INIT ( 1'b0 ))
  \blk00000001/blk00000133  (
    .C(clk),
    .CE(ce),
    .D(\blk00000001/sig00000185 ),
    .Q(\blk00000001/sig0000018e )
  );
  SRL16E #(
    .INIT ( 16'h0000 ))
  \blk00000001/blk00000132  (
    .A0(\blk00000001/sig00000044 ),
    .A1(\blk00000001/sig00000043 ),
    .A2(\blk00000001/sig00000044 ),
    .A3(\blk00000001/sig00000043 ),
    .CE(ce),
    .CLK(clk),
    .D(\blk00000001/sig00000064 ),
    .Q(\blk00000001/sig00000185 )
  );
  FDE #(
    .INIT ( 1'b0 ))
  \blk00000001/blk00000131  (
    .C(clk),
    .CE(ce),
    .D(\blk00000001/sig00000186 ),
    .Q(\blk00000001/sig00000191 )
  );
  SRL16E #(
    .INIT ( 16'h0000 ))
  \blk00000001/blk00000130  (
    .A0(\blk00000001/sig00000044 ),
    .A1(\blk00000001/sig00000043 ),
    .A2(\blk00000001/sig00000044 ),
    .A3(\blk00000001/sig00000043 ),
    .CE(ce),
    .CLK(clk),
    .D(\blk00000001/sig00000065 ),
    .Q(\blk00000001/sig00000186 )
  );
  FDE #(
    .INIT ( 1'b0 ))
  \blk00000001/blk0000012f  (
    .C(clk),
    .CE(ce),
    .D(\blk00000001/sig00000184 ),
    .Q(\blk00000001/sig0000018b )
  );
  SRL16E #(
    .INIT ( 16'h0000 ))
  \blk00000001/blk0000012e  (
    .A0(\blk00000001/sig00000044 ),
    .A1(\blk00000001/sig00000043 ),
    .A2(\blk00000001/sig00000044 ),
    .A3(\blk00000001/sig00000043 ),
    .CE(ce),
    .CLK(clk),
    .D(\blk00000001/sig00000063 ),
    .Q(\blk00000001/sig00000184 )
  );
  FDE #(
    .INIT ( 1'b0 ))
  \blk00000001/blk0000012d  (
    .C(clk),
    .CE(ce),
    .D(\blk00000001/sig00000173 ),
    .Q(\blk00000001/sig00000178 )
  );
  SRL16E #(
    .INIT ( 16'h0000 ))
  \blk00000001/blk0000012c  (
    .A0(\blk00000001/sig00000043 ),
    .A1(\blk00000001/sig00000043 ),
    .A2(\blk00000001/sig00000044 ),
    .A3(\blk00000001/sig00000043 ),
    .CE(ce),
    .CLK(clk),
    .D(\blk00000001/sig000000c4 ),
    .Q(\blk00000001/sig00000173 )
  );
  FDE #(
    .INIT ( 1'b0 ))
  \blk00000001/blk0000012b  (
    .C(clk),
    .CE(ce),
    .D(\blk00000001/sig00000174 ),
    .Q(\blk00000001/sig0000017b )
  );
  SRL16E #(
    .INIT ( 16'h0000 ))
  \blk00000001/blk0000012a  (
    .A0(\blk00000001/sig00000043 ),
    .A1(\blk00000001/sig00000043 ),
    .A2(\blk00000001/sig00000044 ),
    .A3(\blk00000001/sig00000043 ),
    .CE(ce),
    .CLK(clk),
    .D(\blk00000001/sig000000c5 ),
    .Q(\blk00000001/sig00000174 )
  );
  FDE #(
    .INIT ( 1'b0 ))
  \blk00000001/blk00000129  (
    .C(clk),
    .CE(ce),
    .D(\blk00000001/sig00000175 ),
    .Q(\blk00000001/sig0000017e )
  );
  SRL16E #(
    .INIT ( 16'h0000 ))
  \blk00000001/blk00000128  (
    .A0(\blk00000001/sig00000043 ),
    .A1(\blk00000001/sig00000043 ),
    .A2(\blk00000001/sig00000044 ),
    .A3(\blk00000001/sig00000043 ),
    .CE(ce),
    .CLK(clk),
    .D(\blk00000001/sig000000c6 ),
    .Q(\blk00000001/sig00000175 )
  );
  FDE #(
    .INIT ( 1'b0 ))
  \blk00000001/blk00000127  (
    .C(clk),
    .CE(ce),
    .D(\blk00000001/sig00000176 ),
    .Q(\blk00000001/sig00000181 )
  );
  SRL16E #(
    .INIT ( 16'h0000 ))
  \blk00000001/blk00000126  (
    .A0(\blk00000001/sig00000043 ),
    .A1(\blk00000001/sig00000043 ),
    .A2(\blk00000001/sig00000044 ),
    .A3(\blk00000001/sig00000043 ),
    .CE(ce),
    .CLK(clk),
    .D(\blk00000001/sig000000c7 ),
    .Q(\blk00000001/sig00000176 )
  );
  FDE #(
    .INIT ( 1'b0 ))
  \blk00000001/blk00000125  (
    .C(clk),
    .CE(ce),
    .D(\blk00000001/sig0000014d ),
    .Q(\blk00000001/sig00000152 )
  );
  SRL16E #(
    .INIT ( 16'h0000 ))
  \blk00000001/blk00000124  (
    .A0(\blk00000001/sig00000044 ),
    .A1(\blk00000001/sig00000044 ),
    .A2(\blk00000001/sig00000043 ),
    .A3(\blk00000001/sig00000043 ),
    .CE(ce),
    .CLK(clk),
    .D(\blk00000001/sig000000b9 ),
    .Q(\blk00000001/sig0000014d )
  );
  FDE #(
    .INIT ( 1'b0 ))
  \blk00000001/blk00000123  (
    .C(clk),
    .CE(ce),
    .D(\blk00000001/sig0000014e ),
    .Q(\blk00000001/sig00000155 )
  );
  SRL16E #(
    .INIT ( 16'h0000 ))
  \blk00000001/blk00000122  (
    .A0(\blk00000001/sig00000044 ),
    .A1(\blk00000001/sig00000044 ),
    .A2(\blk00000001/sig00000043 ),
    .A3(\blk00000001/sig00000043 ),
    .CE(ce),
    .CLK(clk),
    .D(\blk00000001/sig000000ba ),
    .Q(\blk00000001/sig0000014e )
  );
  FDE #(
    .INIT ( 1'b0 ))
  \blk00000001/blk00000121  (
    .C(clk),
    .CE(ce),
    .D(\blk00000001/sig00000150 ),
    .Q(\blk00000001/sig0000015b )
  );
  SRL16E #(
    .INIT ( 16'h0000 ))
  \blk00000001/blk00000120  (
    .A0(\blk00000001/sig00000044 ),
    .A1(\blk00000001/sig00000044 ),
    .A2(\blk00000001/sig00000043 ),
    .A3(\blk00000001/sig00000043 ),
    .CE(ce),
    .CLK(clk),
    .D(\blk00000001/sig000000bc ),
    .Q(\blk00000001/sig00000150 )
  );
  FDE #(
    .INIT ( 1'b0 ))
  \blk00000001/blk0000011f  (
    .C(clk),
    .CE(ce),
    .D(\blk00000001/sig00000127 ),
    .Q(\blk00000001/sig0000012c )
  );
  SRL16E #(
    .INIT ( 16'h0000 ))
  \blk00000001/blk0000011e  (
    .A0(\blk00000001/sig00000043 ),
    .A1(\blk00000001/sig00000044 ),
    .A2(\blk00000001/sig00000043 ),
    .A3(\blk00000001/sig00000043 ),
    .CE(ce),
    .CLK(clk),
    .D(\blk00000001/sig000000ad ),
    .Q(\blk00000001/sig00000127 )
  );
  FDE #(
    .INIT ( 1'b0 ))
  \blk00000001/blk0000011d  (
    .C(clk),
    .CE(ce),
    .D(\blk00000001/sig0000014f ),
    .Q(\blk00000001/sig00000158 )
  );
  SRL16E #(
    .INIT ( 16'h0000 ))
  \blk00000001/blk0000011c  (
    .A0(\blk00000001/sig00000044 ),
    .A1(\blk00000001/sig00000044 ),
    .A2(\blk00000001/sig00000043 ),
    .A3(\blk00000001/sig00000043 ),
    .CE(ce),
    .CLK(clk),
    .D(\blk00000001/sig000000bb ),
    .Q(\blk00000001/sig0000014f )
  );
  FDE #(
    .INIT ( 1'b0 ))
  \blk00000001/blk0000011b  (
    .C(clk),
    .CE(ce),
    .D(\blk00000001/sig00000128 ),
    .Q(\blk00000001/sig0000012f )
  );
  SRL16E #(
    .INIT ( 16'h0000 ))
  \blk00000001/blk0000011a  (
    .A0(\blk00000001/sig00000043 ),
    .A1(\blk00000001/sig00000044 ),
    .A2(\blk00000001/sig00000043 ),
    .A3(\blk00000001/sig00000043 ),
    .CE(ce),
    .CLK(clk),
    .D(\blk00000001/sig000000ae ),
    .Q(\blk00000001/sig00000128 )
  );
  FDE #(
    .INIT ( 1'b0 ))
  \blk00000001/blk00000119  (
    .C(clk),
    .CE(ce),
    .D(\blk00000001/sig00000129 ),
    .Q(\blk00000001/sig00000132 )
  );
  SRL16E #(
    .INIT ( 16'h0000 ))
  \blk00000001/blk00000118  (
    .A0(\blk00000001/sig00000043 ),
    .A1(\blk00000001/sig00000044 ),
    .A2(\blk00000001/sig00000043 ),
    .A3(\blk00000001/sig00000043 ),
    .CE(ce),
    .CLK(clk),
    .D(\blk00000001/sig000000af ),
    .Q(\blk00000001/sig00000129 )
  );
  FDE #(
    .INIT ( 1'b0 ))
  \blk00000001/blk00000117  (
    .C(clk),
    .CE(ce),
    .D(\blk00000001/sig00000105 ),
    .Q(\blk00000001/sig0000010a )
  );
  SRL16E #(
    .INIT ( 16'h0000 ))
  \blk00000001/blk00000116  (
    .A0(\blk00000001/sig00000044 ),
    .A1(\blk00000001/sig00000043 ),
    .A2(\blk00000001/sig00000043 ),
    .A3(\blk00000001/sig00000043 ),
    .CE(ce),
    .CLK(clk),
    .D(\blk00000001/sig000000a1 ),
    .Q(\blk00000001/sig00000105 )
  );
  FDE #(
    .INIT ( 1'b0 ))
  \blk00000001/blk00000115  (
    .C(clk),
    .CE(ce),
    .D(\blk00000001/sig00000106 ),
    .Q(\blk00000001/sig0000010d )
  );
  SRL16E #(
    .INIT ( 16'h0000 ))
  \blk00000001/blk00000114  (
    .A0(\blk00000001/sig00000044 ),
    .A1(\blk00000001/sig00000043 ),
    .A2(\blk00000001/sig00000043 ),
    .A3(\blk00000001/sig00000043 ),
    .CE(ce),
    .CLK(clk),
    .D(\blk00000001/sig000000a2 ),
    .Q(\blk00000001/sig00000106 )
  );
  FDE #(
    .INIT ( 1'b0 ))
  \blk00000001/blk00000113  (
    .C(clk),
    .CE(ce),
    .D(\blk00000001/sig0000012a ),
    .Q(\blk00000001/sig00000135 )
  );
  SRL16E #(
    .INIT ( 16'h0000 ))
  \blk00000001/blk00000112  (
    .A0(\blk00000001/sig00000043 ),
    .A1(\blk00000001/sig00000044 ),
    .A2(\blk00000001/sig00000043 ),
    .A3(\blk00000001/sig00000043 ),
    .CE(ce),
    .CLK(clk),
    .D(\blk00000001/sig000000b0 ),
    .Q(\blk00000001/sig0000012a )
  );
  FDE #(
    .INIT ( 1'b0 ))
  \blk00000001/blk00000111  (
    .C(clk),
    .CE(ce),
    .D(\blk00000001/sig00000107 ),
    .Q(\blk00000001/sig00000110 )
  );
  SRL16E #(
    .INIT ( 16'h0000 ))
  \blk00000001/blk00000110  (
    .A0(\blk00000001/sig00000044 ),
    .A1(\blk00000001/sig00000043 ),
    .A2(\blk00000001/sig00000043 ),
    .A3(\blk00000001/sig00000043 ),
    .CE(ce),
    .CLK(clk),
    .D(\blk00000001/sig000000a3 ),
    .Q(\blk00000001/sig00000107 )
  );
  FDE #(
    .INIT ( 1'b0 ))
  \blk00000001/blk0000010f  (
    .C(clk),
    .CE(ce),
    .D(\blk00000001/sig00000108 ),
    .Q(\blk00000001/sig00000113 )
  );
  SRL16E #(
    .INIT ( 16'h0000 ))
  \blk00000001/blk0000010e  (
    .A0(\blk00000001/sig00000044 ),
    .A1(\blk00000001/sig00000043 ),
    .A2(\blk00000001/sig00000043 ),
    .A3(\blk00000001/sig00000043 ),
    .CE(ce),
    .CLK(clk),
    .D(\blk00000001/sig000000a4 ),
    .Q(\blk00000001/sig00000108 )
  );
  FDE #(
    .INIT ( 1'b0 ))
  \blk00000001/blk0000010d  (
    .C(clk),
    .CE(ce),
    .D(\blk00000001/sig00000123 ),
    .Q(\blk00000001/sig00000125 )
  );
  SRL16E #(
    .INIT ( 16'h0000 ))
  \blk00000001/blk0000010c  (
    .A0(\blk00000001/sig00000043 ),
    .A1(\blk00000001/sig00000044 ),
    .A2(\blk00000001/sig00000043 ),
    .A3(\blk00000001/sig00000043 ),
    .CE(ce),
    .CLK(clk),
    .D(\blk00000001/sig000000a8 ),
    .Q(\blk00000001/sig00000123 )
  );
  FDE #(
    .INIT ( 1'b0 ))
  \blk00000001/blk0000010b  (
    .C(clk),
    .CE(ce),
    .D(\blk00000001/sig00000101 ),
    .Q(\blk00000001/sig00000103 )
  );
  SRL16E #(
    .INIT ( 16'h0000 ))
  \blk00000001/blk0000010a  (
    .A0(\blk00000001/sig00000044 ),
    .A1(\blk00000001/sig00000043 ),
    .A2(\blk00000001/sig00000043 ),
    .A3(\blk00000001/sig00000043 ),
    .CE(ce),
    .CLK(clk),
    .D(\blk00000001/sig0000009c ),
    .Q(\blk00000001/sig00000101 )
  );
  FDE #(
    .INIT ( 1'b0 ))
  \blk00000001/blk00000109  (
    .C(clk),
    .CE(ce),
    .D(\blk00000001/sig00000149 ),
    .Q(\blk00000001/sig0000014b )
  );
  SRL16E #(
    .INIT ( 16'h0000 ))
  \blk00000001/blk00000108  (
    .A0(\blk00000001/sig00000044 ),
    .A1(\blk00000001/sig00000044 ),
    .A2(\blk00000001/sig00000043 ),
    .A3(\blk00000001/sig00000043 ),
    .CE(ce),
    .CLK(clk),
    .D(\blk00000001/sig000000b4 ),
    .Q(\blk00000001/sig00000149 )
  );
  VCC   \blk00000001/blk00000107  (
    .P(\blk00000001/sig00000044 )
  );
  LUT1 #(
    .INIT ( 2'h2 ))
  \blk00000001/blk00000106  (
    .I0(\blk00000001/sig00000177 ),
    .O(\blk00000001/sig000001df )
  );
  LUT1 #(
    .INIT ( 2'h2 ))
  \blk00000001/blk00000105  (
    .I0(\blk00000001/sig00000180 ),
    .O(\blk00000001/sig000001de )
  );
  LUT1 #(
    .INIT ( 2'h2 ))
  \blk00000001/blk00000104  (
    .I0(\blk00000001/sig0000017a ),
    .O(\blk00000001/sig000001dc )
  );
  LUT1 #(
    .INIT ( 2'h2 ))
  \blk00000001/blk00000103  (
    .I0(\blk00000001/sig0000017d ),
    .O(\blk00000001/sig000001dd )
  );
  LUT1 #(
    .INIT ( 2'h2 ))
  \blk00000001/blk00000102  (
    .I0(\blk00000001/sig00000151 ),
    .O(\blk00000001/sig0000016d )
  );
  LUT1 #(
    .INIT ( 2'h2 ))
  \blk00000001/blk00000101  (
    .I0(\blk00000001/sig0000015a ),
    .O(\blk00000001/sig0000016c )
  );
  LUT1 #(
    .INIT ( 2'h2 ))
  \blk00000001/blk00000100  (
    .I0(\blk00000001/sig00000154 ),
    .O(\blk00000001/sig0000016a )
  );
  LUT1 #(
    .INIT ( 2'h2 ))
  \blk00000001/blk000000ff  (
    .I0(\blk00000001/sig00000157 ),
    .O(\blk00000001/sig0000016b )
  );
  LUT1 #(
    .INIT ( 2'h2 ))
  \blk00000001/blk000000fe  (
    .I0(\blk00000001/sig0000012b ),
    .O(\blk00000001/sig00000143 )
  );
  LUT1 #(
    .INIT ( 2'h2 ))
  \blk00000001/blk000000fd  (
    .I0(\blk00000001/sig00000134 ),
    .O(\blk00000001/sig00000142 )
  );
  LUT1 #(
    .INIT ( 2'h2 ))
  \blk00000001/blk000000fc  (
    .I0(\blk00000001/sig0000012e ),
    .O(\blk00000001/sig00000140 )
  );
  LUT1 #(
    .INIT ( 2'h2 ))
  \blk00000001/blk000000fb  (
    .I0(\blk00000001/sig00000131 ),
    .O(\blk00000001/sig00000141 )
  );
  LUT1 #(
    .INIT ( 2'h2 ))
  \blk00000001/blk000000fa  (
    .I0(\blk00000001/sig00000109 ),
    .O(\blk00000001/sig0000011d )
  );
  LUT1 #(
    .INIT ( 2'h2 ))
  \blk00000001/blk000000f9  (
    .I0(\blk00000001/sig00000112 ),
    .O(\blk00000001/sig0000011c )
  );
  LUT1 #(
    .INIT ( 2'h2 ))
  \blk00000001/blk000000f8  (
    .I0(\blk00000001/sig0000010c ),
    .O(\blk00000001/sig0000011a )
  );
  LUT1 #(
    .INIT ( 2'h2 ))
  \blk00000001/blk000000f7  (
    .I0(\blk00000001/sig0000010f ),
    .O(\blk00000001/sig0000011b )
  );
  LUT1 #(
    .INIT ( 2'h2 ))
  \blk00000001/blk000000f6  (
    .I0(\blk00000001/sig000000ef ),
    .O(\blk00000001/sig000000fb )
  );
  LUT1 #(
    .INIT ( 2'h2 ))
  \blk00000001/blk000000f5  (
    .I0(\blk00000001/sig000000f2 ),
    .O(\blk00000001/sig000000fa )
  );
  LUT1 #(
    .INIT ( 2'h2 ))
  \blk00000001/blk000000f4  (
    .I0(\blk00000001/sig000000f0 ),
    .O(\blk00000001/sig000000f8 )
  );
  LUT1 #(
    .INIT ( 2'h2 ))
  \blk00000001/blk000000f3  (
    .I0(\blk00000001/sig000000f1 ),
    .O(\blk00000001/sig000000f9 )
  );
  LUT1 #(
    .INIT ( 2'h2 ))
  \blk00000001/blk000000f2  (
    .I0(\blk00000001/sig000000d7 ),
    .O(\blk00000001/sig000000e8 )
  );
  LUT1 #(
    .INIT ( 2'h2 ))
  \blk00000001/blk000000f1  (
    .I0(\blk00000001/sig000000da ),
    .O(\blk00000001/sig000000e7 )
  );
  LUT1 #(
    .INIT ( 2'h2 ))
  \blk00000001/blk000000f0  (
    .I0(\blk00000001/sig000000d8 ),
    .O(\blk00000001/sig000000e5 )
  );
  LUT1 #(
    .INIT ( 2'h2 ))
  \blk00000001/blk000000ef  (
    .I0(\blk00000001/sig000000d9 ),
    .O(\blk00000001/sig000000e6 )
  );
  LUT1 #(
    .INIT ( 2'h2 ))
  \blk00000001/blk000000ee  (
    .I0(\blk00000001/sig0000006e ),
    .O(\blk00000001/sig000000d0 )
  );
  LUT1 #(
    .INIT ( 2'h2 ))
  \blk00000001/blk000000ed  (
    .I0(\blk00000001/sig00000071 ),
    .O(\blk00000001/sig000000cf )
  );
  LUT1 #(
    .INIT ( 2'h2 ))
  \blk00000001/blk000000ec  (
    .I0(\blk00000001/sig0000006f ),
    .O(\blk00000001/sig000000cd )
  );
  LUT1 #(
    .INIT ( 2'h2 ))
  \blk00000001/blk000000eb  (
    .I0(\blk00000001/sig00000070 ),
    .O(\blk00000001/sig000000ce )
  );
  LUT2 #(
    .INIT ( 4'h6 ))
  \blk00000001/blk000000ea  (
    .I0(b[0]),
    .I1(a[0]),
    .O(\blk00000001/sig0000005d )
  );
  LUT2 #(
    .INIT ( 4'h6 ))
  \blk00000001/blk000000e9  (
    .I0(b[4]),
    .I1(a[4]),
    .O(\blk00000001/sig0000006a )
  );
  LUT2 #(
    .INIT ( 4'h6 ))
  \blk00000001/blk000000e8  (
    .I0(b[8]),
    .I1(a[8]),
    .O(\blk00000001/sig0000007b )
  );
  LUT2 #(
    .INIT ( 4'h6 ))
  \blk00000001/blk000000e7  (
    .I0(b[12]),
    .I1(a[12]),
    .O(\blk00000001/sig0000008c )
  );
  LUT2 #(
    .INIT ( 4'h6 ))
  \blk00000001/blk000000e6  (
    .I0(b[16]),
    .I1(a[16]),
    .O(\blk00000001/sig0000009d )
  );
  LUT2 #(
    .INIT ( 4'h6 ))
  \blk00000001/blk000000e5  (
    .I0(b[20]),
    .I1(a[20]),
    .O(\blk00000001/sig000000a9 )
  );
  LUT2 #(
    .INIT ( 4'h6 ))
  \blk00000001/blk000000e4  (
    .I0(b[24]),
    .I1(a[24]),
    .O(\blk00000001/sig000000b5 )
  );
  LUT2 #(
    .INIT ( 4'h6 ))
  \blk00000001/blk000000e3  (
    .I0(b[28]),
    .I1(a[28]),
    .O(\blk00000001/sig000000c0 )
  );
  LUT2 #(
    .INIT ( 4'h6 ))
  \blk00000001/blk000000e2  (
    .I0(b[1]),
    .I1(a[1]),
    .O(\blk00000001/sig0000005e )
  );
  LUT2 #(
    .INIT ( 4'h6 ))
  \blk00000001/blk000000e1  (
    .I0(b[5]),
    .I1(a[5]),
    .O(\blk00000001/sig0000006b )
  );
  LUT2 #(
    .INIT ( 4'h6 ))
  \blk00000001/blk000000e0  (
    .I0(b[9]),
    .I1(a[9]),
    .O(\blk00000001/sig0000007c )
  );
  LUT2 #(
    .INIT ( 4'h6 ))
  \blk00000001/blk000000df  (
    .I0(b[13]),
    .I1(a[13]),
    .O(\blk00000001/sig0000008d )
  );
  LUT2 #(
    .INIT ( 4'h6 ))
  \blk00000001/blk000000de  (
    .I0(b[17]),
    .I1(a[17]),
    .O(\blk00000001/sig0000009e )
  );
  LUT2 #(
    .INIT ( 4'h6 ))
  \blk00000001/blk000000dd  (
    .I0(b[21]),
    .I1(a[21]),
    .O(\blk00000001/sig000000aa )
  );
  LUT2 #(
    .INIT ( 4'h6 ))
  \blk00000001/blk000000dc  (
    .I0(b[25]),
    .I1(a[25]),
    .O(\blk00000001/sig000000b6 )
  );
  LUT2 #(
    .INIT ( 4'h6 ))
  \blk00000001/blk000000db  (
    .I0(b[29]),
    .I1(a[29]),
    .O(\blk00000001/sig000000c1 )
  );
  LUT2 #(
    .INIT ( 4'h6 ))
  \blk00000001/blk000000da  (
    .I0(b[2]),
    .I1(a[2]),
    .O(\blk00000001/sig0000005f )
  );
  LUT2 #(
    .INIT ( 4'h6 ))
  \blk00000001/blk000000d9  (
    .I0(b[6]),
    .I1(a[6]),
    .O(\blk00000001/sig0000006c )
  );
  LUT2 #(
    .INIT ( 4'h6 ))
  \blk00000001/blk000000d8  (
    .I0(b[10]),
    .I1(a[10]),
    .O(\blk00000001/sig0000007d )
  );
  LUT2 #(
    .INIT ( 4'h6 ))
  \blk00000001/blk000000d7  (
    .I0(b[14]),
    .I1(a[14]),
    .O(\blk00000001/sig0000008e )
  );
  LUT2 #(
    .INIT ( 4'h6 ))
  \blk00000001/blk000000d6  (
    .I0(b[18]),
    .I1(a[18]),
    .O(\blk00000001/sig0000009f )
  );
  LUT2 #(
    .INIT ( 4'h6 ))
  \blk00000001/blk000000d5  (
    .I0(b[22]),
    .I1(a[22]),
    .O(\blk00000001/sig000000ab )
  );
  LUT2 #(
    .INIT ( 4'h6 ))
  \blk00000001/blk000000d4  (
    .I0(b[26]),
    .I1(a[26]),
    .O(\blk00000001/sig000000b7 )
  );
  LUT2 #(
    .INIT ( 4'h6 ))
  \blk00000001/blk000000d3  (
    .I0(b[30]),
    .I1(a[30]),
    .O(\blk00000001/sig000000c2 )
  );
  LUT2 #(
    .INIT ( 4'hE ))
  \blk00000001/blk000000d2  (
    .I0(\blk00000001/sig00000072 ),
    .I1(\blk00000001/sig0000004d ),
    .O(\blk00000001/sig00000053 )
  );
  LUT2 #(
    .INIT ( 4'hE ))
  \blk00000001/blk000000d1  (
    .I0(\blk00000001/sig000000d6 ),
    .I1(\blk00000001/sig0000004e ),
    .O(\blk00000001/sig00000054 )
  );
  LUT2 #(
    .INIT ( 4'hE ))
  \blk00000001/blk000000d0  (
    .I0(\blk00000001/sig000000ee ),
    .I1(\blk00000001/sig0000004f ),
    .O(\blk00000001/sig00000055 )
  );
  LUT2 #(
    .INIT ( 4'hE ))
  \blk00000001/blk000000cf  (
    .I0(\blk00000001/sig00000102 ),
    .I1(\blk00000001/sig00000050 ),
    .O(\blk00000001/sig00000056 )
  );
  LUT2 #(
    .INIT ( 4'hE ))
  \blk00000001/blk000000ce  (
    .I0(\blk00000001/sig00000124 ),
    .I1(\blk00000001/sig00000051 ),
    .O(\blk00000001/sig00000057 )
  );
  LUT2 #(
    .INIT ( 4'hE ))
  \blk00000001/blk000000cd  (
    .I0(\blk00000001/sig0000014a ),
    .I1(\blk00000001/sig00000052 ),
    .O(\blk00000001/sig00000058 )
  );
  LUT2 #(
    .INIT ( 4'h6 ))
  \blk00000001/blk000000cc  (
    .I0(b[3]),
    .I1(a[3]),
    .O(\blk00000001/sig00000060 )
  );
  LUT2 #(
    .INIT ( 4'h6 ))
  \blk00000001/blk000000cb  (
    .I0(b[7]),
    .I1(a[7]),
    .O(\blk00000001/sig0000006d )
  );
  LUT2 #(
    .INIT ( 4'h6 ))
  \blk00000001/blk000000ca  (
    .I0(b[11]),
    .I1(a[11]),
    .O(\blk00000001/sig0000007e )
  );
  LUT2 #(
    .INIT ( 4'h6 ))
  \blk00000001/blk000000c9  (
    .I0(b[15]),
    .I1(a[15]),
    .O(\blk00000001/sig0000008f )
  );
  LUT2 #(
    .INIT ( 4'h6 ))
  \blk00000001/blk000000c8  (
    .I0(b[19]),
    .I1(a[19]),
    .O(\blk00000001/sig000000a0 )
  );
  LUT2 #(
    .INIT ( 4'h6 ))
  \blk00000001/blk000000c7  (
    .I0(b[23]),
    .I1(a[23]),
    .O(\blk00000001/sig000000ac )
  );
  LUT2 #(
    .INIT ( 4'h6 ))
  \blk00000001/blk000000c6  (
    .I0(b[27]),
    .I1(a[27]),
    .O(\blk00000001/sig000000b8 )
  );
  LUT2 #(
    .INIT ( 4'h6 ))
  \blk00000001/blk000000c5  (
    .I0(b[31]),
    .I1(a[31]),
    .O(\blk00000001/sig000000c3 )
  );
  FDRE #(
    .INIT ( 1'b0 ))
  \blk00000001/blk000000c4  (
    .C(clk),
    .CE(ce),
    .D(\blk00000001/sig00000166 ),
    .R(sclr),
    .Q(s[24])
  );
  FDRE #(
    .INIT ( 1'b0 ))
  \blk00000001/blk000000c3  (
    .C(clk),
    .CE(ce),
    .D(\blk00000001/sig00000167 ),
    .R(sclr),
    .Q(s[25])
  );
  FDRE #(
    .INIT ( 1'b0 ))
  \blk00000001/blk000000c2  (
    .C(clk),
    .CE(ce),
    .D(\blk00000001/sig00000168 ),
    .R(sclr),
    .Q(s[26])
  );
  FDRE #(
    .INIT ( 1'b0 ))
  \blk00000001/blk000000c1  (
    .C(clk),
    .CE(ce),
    .D(\blk00000001/sig00000169 ),
    .R(sclr),
    .Q(s[27])
  );
  FDRE #(
    .INIT ( 1'b0 ))
  \blk00000001/blk000000c0  (
    .C(clk),
    .CE(ce),
    .D(\blk00000001/sig0000015f ),
    .R(sclr),
    .Q(s[20])
  );
  FDRE #(
    .INIT ( 1'b0 ))
  \blk00000001/blk000000bf  (
    .C(clk),
    .CE(ce),
    .D(\blk00000001/sig00000160 ),
    .R(sclr),
    .Q(s[21])
  );
  FDRE #(
    .INIT ( 1'b0 ))
  \blk00000001/blk000000be  (
    .C(clk),
    .CE(ce),
    .D(\blk00000001/sig00000161 ),
    .R(sclr),
    .Q(s[22])
  );
  FDRE #(
    .INIT ( 1'b0 ))
  \blk00000001/blk000000bd  (
    .C(clk),
    .CE(ce),
    .D(\blk00000001/sig00000162 ),
    .R(sclr),
    .Q(s[23])
  );
  FDRE #(
    .INIT ( 1'b0 ))
  \blk00000001/blk000000bc  (
    .C(clk),
    .CE(ce),
    .D(\blk00000001/sig0000015d ),
    .R(sclr),
    .Q(s[18])
  );
  FDRE #(
    .INIT ( 1'b0 ))
  \blk00000001/blk000000bb  (
    .C(clk),
    .CE(ce),
    .D(\blk00000001/sig0000015e ),
    .R(sclr),
    .Q(s[19])
  );
  FDRE #(
    .INIT ( 1'b0 ))
  \blk00000001/blk000000ba  (
    .C(clk),
    .CE(ce),
    .D(\blk00000001/sig0000013c ),
    .R(sclr),
    .Q(\blk00000001/sig0000015f )
  );
  FDRE #(
    .INIT ( 1'b0 ))
  \blk00000001/blk000000b9  (
    .C(clk),
    .CE(ce),
    .D(\blk00000001/sig0000013d ),
    .R(sclr),
    .Q(\blk00000001/sig00000160 )
  );
  FDRE #(
    .INIT ( 1'b0 ))
  \blk00000001/blk000000b8  (
    .C(clk),
    .CE(ce),
    .D(\blk00000001/sig0000013e ),
    .R(sclr),
    .Q(\blk00000001/sig00000161 )
  );
  FDRE #(
    .INIT ( 1'b0 ))
  \blk00000001/blk000000b7  (
    .C(clk),
    .CE(ce),
    .D(\blk00000001/sig0000013f ),
    .R(sclr),
    .Q(\blk00000001/sig00000162 )
  );
  FDRE #(
    .INIT ( 1'b0 ))
  \blk00000001/blk000000b6  (
    .C(clk),
    .CE(ce),
    .D(\blk00000001/sig00000137 ),
    .R(sclr),
    .Q(\blk00000001/sig0000015d )
  );
  FDRE #(
    .INIT ( 1'b0 ))
  \blk00000001/blk000000b5  (
    .C(clk),
    .CE(ce),
    .D(\blk00000001/sig00000138 ),
    .R(sclr),
    .Q(\blk00000001/sig0000015e )
  );
  FDRE #(
    .INIT ( 1'b0 ))
  \blk00000001/blk000000b4  (
    .C(clk),
    .CE(ce),
    .D(\blk00000001/sig00000118 ),
    .R(sclr),
    .Q(\blk00000001/sig00000137 )
  );
  FDRE #(
    .INIT ( 1'b0 ))
  \blk00000001/blk000000b3  (
    .C(clk),
    .CE(ce),
    .D(\blk00000001/sig00000119 ),
    .R(sclr),
    .Q(\blk00000001/sig00000138 )
  );
  FDRE #(
    .INIT ( 1'b0 ))
  \blk00000001/blk000000b2  (
    .C(clk),
    .CE(ce),
    .D(\blk00000001/sig000000dc ),
    .R(sclr),
    .Q(\blk00000001/sig000000ef )
  );
  FDRE #(
    .INIT ( 1'b0 ))
  \blk00000001/blk000000b1  (
    .C(clk),
    .CE(ce),
    .D(\blk00000001/sig000000dd ),
    .R(sclr),
    .Q(\blk00000001/sig000000f0 )
  );
  FDRE #(
    .INIT ( 1'b0 ))
  \blk00000001/blk000000b0  (
    .C(clk),
    .CE(ce),
    .D(\blk00000001/sig000000de ),
    .R(sclr),
    .Q(\blk00000001/sig000000f1 )
  );
  FDRE #(
    .INIT ( 1'b0 ))
  \blk00000001/blk000000af  (
    .C(clk),
    .CE(ce),
    .D(\blk00000001/sig000000df ),
    .R(sclr),
    .Q(\blk00000001/sig000000f2 )
  );
  FDRE #(
    .INIT ( 1'b0 ))
  \blk00000001/blk000000ae  (
    .C(clk),
    .CE(ce),
    .D(\blk00000001/sig00000090 ),
    .R(sclr),
    .Q(\blk00000001/sig000000dc )
  );
  FDRE #(
    .INIT ( 1'b0 ))
  \blk00000001/blk000000ad  (
    .C(clk),
    .CE(ce),
    .D(\blk00000001/sig00000091 ),
    .R(sclr),
    .Q(\blk00000001/sig000000dd )
  );
  FDRE #(
    .INIT ( 1'b0 ))
  \blk00000001/blk000000ac  (
    .C(clk),
    .CE(ce),
    .D(\blk00000001/sig00000092 ),
    .R(sclr),
    .Q(\blk00000001/sig000000de )
  );
  FDRE #(
    .INIT ( 1'b0 ))
  \blk00000001/blk000000ab  (
    .C(clk),
    .CE(ce),
    .D(\blk00000001/sig00000093 ),
    .R(sclr),
    .Q(\blk00000001/sig000000df )
  );
  FDRE #(
    .INIT ( 1'b0 ))
  \blk00000001/blk000000aa  (
    .C(clk),
    .CE(ce),
    .D(\blk00000001/sig0000007f ),
    .R(sclr),
    .Q(\blk00000001/sig000000d7 )
  );
  FDRE #(
    .INIT ( 1'b0 ))
  \blk00000001/blk000000a9  (
    .C(clk),
    .CE(ce),
    .D(\blk00000001/sig00000080 ),
    .R(sclr),
    .Q(\blk00000001/sig000000d8 )
  );
  FDRE #(
    .INIT ( 1'b0 ))
  \blk00000001/blk000000a8  (
    .C(clk),
    .CE(ce),
    .D(\blk00000001/sig00000081 ),
    .R(sclr),
    .Q(\blk00000001/sig000000d9 )
  );
  FDRE #(
    .INIT ( 1'b0 ))
  \blk00000001/blk000000a7  (
    .C(clk),
    .CE(ce),
    .D(\blk00000001/sig00000082 ),
    .R(sclr),
    .Q(\blk00000001/sig000000da )
  );
  FDRE #(
    .INIT ( 1'b0 ))
  \blk00000001/blk000000a6  (
    .C(clk),
    .CE(ce),
    .D(\blk00000001/sig00000058 ),
    .R(sclr),
    .Q(\blk00000001/sig00000172 )
  );
  FDRE #(
    .INIT ( 1'b0 ))
  \blk00000001/blk000000a5  (
    .C(clk),
    .CE(ce),
    .D(\blk00000001/sig00000057 ),
    .R(sclr),
    .Q(\blk00000001/sig00000148 )
  );
  FDRE #(
    .INIT ( 1'b0 ))
  \blk00000001/blk000000a4  (
    .C(clk),
    .CE(ce),
    .D(\blk00000001/sig00000056 ),
    .R(sclr),
    .Q(\blk00000001/sig00000122 )
  );
  FDRE #(
    .INIT ( 1'b0 ))
  \blk00000001/blk000000a3  (
    .C(clk),
    .CE(ce),
    .D(\blk00000001/sig00000055 ),
    .R(sclr),
    .Q(\blk00000001/sig00000100 )
  );
  FDRE #(
    .INIT ( 1'b0 ))
  \blk00000001/blk000000a2  (
    .C(clk),
    .CE(ce),
    .D(\blk00000001/sig000000db ),
    .R(sclr),
    .Q(\blk00000001/sig000000ee )
  );
  FDRE #(
    .INIT ( 1'b0 ))
  \blk00000001/blk000000a1  (
    .C(clk),
    .CE(ce),
    .D(\blk00000001/sig00000054 ),
    .R(sclr),
    .Q(\blk00000001/sig000000ed )
  );
  FDRE #(
    .INIT ( 1'b0 ))
  \blk00000001/blk000000a0  (
    .C(clk),
    .CE(ce),
    .D(\blk00000001/sig00000094 ),
    .R(sclr),
    .Q(\blk00000001/sig000000db )
  );
  FDRE #(
    .INIT ( 1'b0 ))
  \blk00000001/blk0000009f  (
    .C(clk),
    .CE(ce),
    .D(\blk00000001/sig00000083 ),
    .R(sclr),
    .Q(\blk00000001/sig000000d6 )
  );
  FDRE #(
    .INIT ( 1'b0 ))
  \blk00000001/blk0000009e  (
    .C(clk),
    .CE(ce),
    .D(\blk00000001/sig00000053 ),
    .R(sclr),
    .Q(\blk00000001/sig000000d5 )
  );
  MUXCY   \blk00000001/blk0000009d  (
    .CI(\blk00000001/sig00000043 ),
    .DI(a[24]),
    .S(\blk00000001/sig000000b5 ),
    .O(\blk00000001/sig000000b1 )
  );
  XORCY   \blk00000001/blk0000009c  (
    .CI(\blk00000001/sig00000043 ),
    .LI(\blk00000001/sig000000b5 ),
    .O(\blk00000001/sig000000b9 )
  );
  XORCY   \blk00000001/blk0000009b  (
    .CI(\blk00000001/sig000000b3 ),
    .LI(\blk00000001/sig000000b8 ),
    .O(\blk00000001/sig000000bc )
  );
  MUXCY   \blk00000001/blk0000009a  (
    .CI(\blk00000001/sig000000b3 ),
    .DI(a[27]),
    .S(\blk00000001/sig000000b8 ),
    .O(\blk00000001/sig000000b4 )
  );
  MUXCY   \blk00000001/blk00000099  (
    .CI(\blk00000001/sig000000b1 ),
    .DI(a[25]),
    .S(\blk00000001/sig000000b6 ),
    .O(\blk00000001/sig000000b2 )
  );
  XORCY   \blk00000001/blk00000098  (
    .CI(\blk00000001/sig000000b1 ),
    .LI(\blk00000001/sig000000b6 ),
    .O(\blk00000001/sig000000ba )
  );
  MUXCY   \blk00000001/blk00000097  (
    .CI(\blk00000001/sig000000b2 ),
    .DI(a[26]),
    .S(\blk00000001/sig000000b7 ),
    .O(\blk00000001/sig000000b3 )
  );
  XORCY   \blk00000001/blk00000096  (
    .CI(\blk00000001/sig000000b2 ),
    .LI(\blk00000001/sig000000b7 ),
    .O(\blk00000001/sig000000bb )
  );
  MUXCY   \blk00000001/blk00000095  (
    .CI(\blk00000001/sig00000043 ),
    .DI(a[20]),
    .S(\blk00000001/sig000000a9 ),
    .O(\blk00000001/sig000000a5 )
  );
  XORCY   \blk00000001/blk00000094  (
    .CI(\blk00000001/sig00000043 ),
    .LI(\blk00000001/sig000000a9 ),
    .O(\blk00000001/sig000000ad )
  );
  XORCY   \blk00000001/blk00000093  (
    .CI(\blk00000001/sig000000a7 ),
    .LI(\blk00000001/sig000000ac ),
    .O(\blk00000001/sig000000b0 )
  );
  MUXCY   \blk00000001/blk00000092  (
    .CI(\blk00000001/sig000000a7 ),
    .DI(a[23]),
    .S(\blk00000001/sig000000ac ),
    .O(\blk00000001/sig000000a8 )
  );
  MUXCY   \blk00000001/blk00000091  (
    .CI(\blk00000001/sig000000a5 ),
    .DI(a[21]),
    .S(\blk00000001/sig000000aa ),
    .O(\blk00000001/sig000000a6 )
  );
  XORCY   \blk00000001/blk00000090  (
    .CI(\blk00000001/sig000000a5 ),
    .LI(\blk00000001/sig000000aa ),
    .O(\blk00000001/sig000000ae )
  );
  MUXCY   \blk00000001/blk0000008f  (
    .CI(\blk00000001/sig000000a6 ),
    .DI(a[22]),
    .S(\blk00000001/sig000000ab ),
    .O(\blk00000001/sig000000a7 )
  );
  XORCY   \blk00000001/blk0000008e  (
    .CI(\blk00000001/sig000000a6 ),
    .LI(\blk00000001/sig000000ab ),
    .O(\blk00000001/sig000000af )
  );
  MUXCY   \blk00000001/blk0000008d  (
    .CI(\blk00000001/sig00000043 ),
    .DI(a[16]),
    .S(\blk00000001/sig0000009d ),
    .O(\blk00000001/sig00000099 )
  );
  XORCY   \blk00000001/blk0000008c  (
    .CI(\blk00000001/sig00000043 ),
    .LI(\blk00000001/sig0000009d ),
    .O(\blk00000001/sig000000a1 )
  );
  XORCY   \blk00000001/blk0000008b  (
    .CI(\blk00000001/sig0000009b ),
    .LI(\blk00000001/sig000000a0 ),
    .O(\blk00000001/sig000000a4 )
  );
  MUXCY   \blk00000001/blk0000008a  (
    .CI(\blk00000001/sig0000009b ),
    .DI(a[19]),
    .S(\blk00000001/sig000000a0 ),
    .O(\blk00000001/sig0000009c )
  );
  MUXCY   \blk00000001/blk00000089  (
    .CI(\blk00000001/sig00000099 ),
    .DI(a[17]),
    .S(\blk00000001/sig0000009e ),
    .O(\blk00000001/sig0000009a )
  );
  XORCY   \blk00000001/blk00000088  (
    .CI(\blk00000001/sig00000099 ),
    .LI(\blk00000001/sig0000009e ),
    .O(\blk00000001/sig000000a2 )
  );
  MUXCY   \blk00000001/blk00000087  (
    .CI(\blk00000001/sig0000009a ),
    .DI(a[18]),
    .S(\blk00000001/sig0000009f ),
    .O(\blk00000001/sig0000009b )
  );
  XORCY   \blk00000001/blk00000086  (
    .CI(\blk00000001/sig0000009a ),
    .LI(\blk00000001/sig0000009f ),
    .O(\blk00000001/sig000000a3 )
  );
  MUXCY   \blk00000001/blk00000085  (
    .CI(\blk00000001/sig00000043 ),
    .DI(a[12]),
    .S(\blk00000001/sig0000008c ),
    .O(\blk00000001/sig00000088 )
  );
  XORCY   \blk00000001/blk00000084  (
    .CI(\blk00000001/sig00000043 ),
    .LI(\blk00000001/sig0000008c ),
    .O(\blk00000001/sig00000095 )
  );
  XORCY   \blk00000001/blk00000083  (
    .CI(\blk00000001/sig0000008a ),
    .LI(\blk00000001/sig0000008f ),
    .O(\blk00000001/sig00000098 )
  );
  MUXCY   \blk00000001/blk00000082  (
    .CI(\blk00000001/sig0000008a ),
    .DI(a[15]),
    .S(\blk00000001/sig0000008f ),
    .O(\blk00000001/sig0000008b )
  );
  MUXCY   \blk00000001/blk00000081  (
    .CI(\blk00000001/sig00000088 ),
    .DI(a[13]),
    .S(\blk00000001/sig0000008d ),
    .O(\blk00000001/sig00000089 )
  );
  XORCY   \blk00000001/blk00000080  (
    .CI(\blk00000001/sig00000088 ),
    .LI(\blk00000001/sig0000008d ),
    .O(\blk00000001/sig00000096 )
  );
  MUXCY   \blk00000001/blk0000007f  (
    .CI(\blk00000001/sig00000089 ),
    .DI(a[14]),
    .S(\blk00000001/sig0000008e ),
    .O(\blk00000001/sig0000008a )
  );
  XORCY   \blk00000001/blk0000007e  (
    .CI(\blk00000001/sig00000089 ),
    .LI(\blk00000001/sig0000008e ),
    .O(\blk00000001/sig00000097 )
  );
  FDRE #(
    .INIT ( 1'b0 ))
  \blk00000001/blk0000007d  (
    .C(clk),
    .CE(ce),
    .D(\blk00000001/sig0000008b ),
    .R(sclr),
    .Q(\blk00000001/sig00000094 )
  );
  FDRE #(
    .INIT ( 1'b0 ))
  \blk00000001/blk0000007c  (
    .C(clk),
    .CE(ce),
    .D(\blk00000001/sig00000095 ),
    .R(sclr),
    .Q(\blk00000001/sig00000090 )
  );
  FDRE #(
    .INIT ( 1'b0 ))
  \blk00000001/blk0000007b  (
    .C(clk),
    .CE(ce),
    .D(\blk00000001/sig00000096 ),
    .R(sclr),
    .Q(\blk00000001/sig00000091 )
  );
  FDRE #(
    .INIT ( 1'b0 ))
  \blk00000001/blk0000007a  (
    .C(clk),
    .CE(ce),
    .D(\blk00000001/sig00000097 ),
    .R(sclr),
    .Q(\blk00000001/sig00000092 )
  );
  FDRE #(
    .INIT ( 1'b0 ))
  \blk00000001/blk00000079  (
    .C(clk),
    .CE(ce),
    .D(\blk00000001/sig00000098 ),
    .R(sclr),
    .Q(\blk00000001/sig00000093 )
  );
  MUXCY   \blk00000001/blk00000078  (
    .CI(\blk00000001/sig00000043 ),
    .DI(a[8]),
    .S(\blk00000001/sig0000007b ),
    .O(\blk00000001/sig00000077 )
  );
  XORCY   \blk00000001/blk00000077  (
    .CI(\blk00000001/sig00000043 ),
    .LI(\blk00000001/sig0000007b ),
    .O(\blk00000001/sig00000084 )
  );
  XORCY   \blk00000001/blk00000076  (
    .CI(\blk00000001/sig00000079 ),
    .LI(\blk00000001/sig0000007e ),
    .O(\blk00000001/sig00000087 )
  );
  MUXCY   \blk00000001/blk00000075  (
    .CI(\blk00000001/sig00000079 ),
    .DI(a[11]),
    .S(\blk00000001/sig0000007e ),
    .O(\blk00000001/sig0000007a )
  );
  MUXCY   \blk00000001/blk00000074  (
    .CI(\blk00000001/sig00000077 ),
    .DI(a[9]),
    .S(\blk00000001/sig0000007c ),
    .O(\blk00000001/sig00000078 )
  );
  XORCY   \blk00000001/blk00000073  (
    .CI(\blk00000001/sig00000077 ),
    .LI(\blk00000001/sig0000007c ),
    .O(\blk00000001/sig00000085 )
  );
  MUXCY   \blk00000001/blk00000072  (
    .CI(\blk00000001/sig00000078 ),
    .DI(a[10]),
    .S(\blk00000001/sig0000007d ),
    .O(\blk00000001/sig00000079 )
  );
  XORCY   \blk00000001/blk00000071  (
    .CI(\blk00000001/sig00000078 ),
    .LI(\blk00000001/sig0000007d ),
    .O(\blk00000001/sig00000086 )
  );
  FDRE #(
    .INIT ( 1'b0 ))
  \blk00000001/blk00000070  (
    .C(clk),
    .CE(ce),
    .D(\blk00000001/sig0000007a ),
    .R(sclr),
    .Q(\blk00000001/sig00000083 )
  );
  FDRE #(
    .INIT ( 1'b0 ))
  \blk00000001/blk0000006f  (
    .C(clk),
    .CE(ce),
    .D(\blk00000001/sig00000084 ),
    .R(sclr),
    .Q(\blk00000001/sig0000007f )
  );
  FDRE #(
    .INIT ( 1'b0 ))
  \blk00000001/blk0000006e  (
    .C(clk),
    .CE(ce),
    .D(\blk00000001/sig00000085 ),
    .R(sclr),
    .Q(\blk00000001/sig00000080 )
  );
  FDRE #(
    .INIT ( 1'b0 ))
  \blk00000001/blk0000006d  (
    .C(clk),
    .CE(ce),
    .D(\blk00000001/sig00000086 ),
    .R(sclr),
    .Q(\blk00000001/sig00000081 )
  );
  FDRE #(
    .INIT ( 1'b0 ))
  \blk00000001/blk0000006c  (
    .C(clk),
    .CE(ce),
    .D(\blk00000001/sig00000087 ),
    .R(sclr),
    .Q(\blk00000001/sig00000082 )
  );
  MUXCY   \blk00000001/blk0000006b  (
    .CI(\blk00000001/sig00000043 ),
    .DI(a[4]),
    .S(\blk00000001/sig0000006a ),
    .O(\blk00000001/sig00000066 )
  );
  XORCY   \blk00000001/blk0000006a  (
    .CI(\blk00000001/sig00000043 ),
    .LI(\blk00000001/sig0000006a ),
    .O(\blk00000001/sig00000073 )
  );
  XORCY   \blk00000001/blk00000069  (
    .CI(\blk00000001/sig00000068 ),
    .LI(\blk00000001/sig0000006d ),
    .O(\blk00000001/sig00000076 )
  );
  MUXCY   \blk00000001/blk00000068  (
    .CI(\blk00000001/sig00000068 ),
    .DI(a[7]),
    .S(\blk00000001/sig0000006d ),
    .O(\blk00000001/sig00000069 )
  );
  MUXCY   \blk00000001/blk00000067  (
    .CI(\blk00000001/sig00000066 ),
    .DI(a[5]),
    .S(\blk00000001/sig0000006b ),
    .O(\blk00000001/sig00000067 )
  );
  XORCY   \blk00000001/blk00000066  (
    .CI(\blk00000001/sig00000066 ),
    .LI(\blk00000001/sig0000006b ),
    .O(\blk00000001/sig00000074 )
  );
  MUXCY   \blk00000001/blk00000065  (
    .CI(\blk00000001/sig00000067 ),
    .DI(a[6]),
    .S(\blk00000001/sig0000006c ),
    .O(\blk00000001/sig00000068 )
  );
  XORCY   \blk00000001/blk00000064  (
    .CI(\blk00000001/sig00000067 ),
    .LI(\blk00000001/sig0000006c ),
    .O(\blk00000001/sig00000075 )
  );
  FDRE #(
    .INIT ( 1'b0 ))
  \blk00000001/blk00000063  (
    .C(clk),
    .CE(ce),
    .D(\blk00000001/sig00000069 ),
    .R(sclr),
    .Q(\blk00000001/sig00000072 )
  );
  FDRE #(
    .INIT ( 1'b0 ))
  \blk00000001/blk00000062  (
    .C(clk),
    .CE(ce),
    .D(\blk00000001/sig00000073 ),
    .R(sclr),
    .Q(\blk00000001/sig0000006e )
  );
  FDRE #(
    .INIT ( 1'b0 ))
  \blk00000001/blk00000061  (
    .C(clk),
    .CE(ce),
    .D(\blk00000001/sig00000074 ),
    .R(sclr),
    .Q(\blk00000001/sig0000006f )
  );
  FDRE #(
    .INIT ( 1'b0 ))
  \blk00000001/blk00000060  (
    .C(clk),
    .CE(ce),
    .D(\blk00000001/sig00000075 ),
    .R(sclr),
    .Q(\blk00000001/sig00000070 )
  );
  FDRE #(
    .INIT ( 1'b0 ))
  \blk00000001/blk0000005f  (
    .C(clk),
    .CE(ce),
    .D(\blk00000001/sig00000076 ),
    .R(sclr),
    .Q(\blk00000001/sig00000071 )
  );
  MUXCY   \blk00000001/blk0000005e  (
    .CI(\blk00000001/sig00000043 ),
    .DI(a[0]),
    .S(\blk00000001/sig0000005d ),
    .O(\blk00000001/sig00000059 )
  );
  XORCY   \blk00000001/blk0000005d  (
    .CI(\blk00000001/sig00000043 ),
    .LI(\blk00000001/sig0000005d ),
    .O(\blk00000001/sig00000062 )
  );
  XORCY   \blk00000001/blk0000005c  (
    .CI(\blk00000001/sig0000005b ),
    .LI(\blk00000001/sig00000060 ),
    .O(\blk00000001/sig00000065 )
  );
  MUXCY   \blk00000001/blk0000005b  (
    .CI(\blk00000001/sig0000005b ),
    .DI(a[3]),
    .S(\blk00000001/sig00000060 ),
    .O(\blk00000001/sig0000005c )
  );
  MUXCY   \blk00000001/blk0000005a  (
    .CI(\blk00000001/sig00000059 ),
    .DI(a[1]),
    .S(\blk00000001/sig0000005e ),
    .O(\blk00000001/sig0000005a )
  );
  XORCY   \blk00000001/blk00000059  (
    .CI(\blk00000001/sig00000059 ),
    .LI(\blk00000001/sig0000005e ),
    .O(\blk00000001/sig00000063 )
  );
  MUXCY   \blk00000001/blk00000058  (
    .CI(\blk00000001/sig0000005a ),
    .DI(a[2]),
    .S(\blk00000001/sig0000005f ),
    .O(\blk00000001/sig0000005b )
  );
  XORCY   \blk00000001/blk00000057  (
    .CI(\blk00000001/sig0000005a ),
    .LI(\blk00000001/sig0000005f ),
    .O(\blk00000001/sig00000064 )
  );
  FDRE #(
    .INIT ( 1'b0 ))
  \blk00000001/blk00000056  (
    .C(clk),
    .CE(ce),
    .D(\blk00000001/sig0000005c ),
    .R(sclr),
    .Q(\blk00000001/sig00000061 )
  );
  MUXCY   \blk00000001/blk00000055  (
    .CI(\blk00000001/sig00000043 ),
    .DI(a[28]),
    .S(\blk00000001/sig000000c0 ),
    .O(\blk00000001/sig000000bd )
  );
  XORCY   \blk00000001/blk00000054  (
    .CI(\blk00000001/sig00000043 ),
    .LI(\blk00000001/sig000000c0 ),
    .O(\blk00000001/sig000000c4 )
  );
  XORCY   \blk00000001/blk00000053  (
    .CI(\blk00000001/sig000000bf ),
    .LI(\blk00000001/sig000000c3 ),
    .O(\blk00000001/sig000000c7 )
  );
  MUXCY   \blk00000001/blk00000052  (
    .CI(\blk00000001/sig000000bd ),
    .DI(a[29]),
    .S(\blk00000001/sig000000c1 ),
    .O(\blk00000001/sig000000be )
  );
  XORCY   \blk00000001/blk00000051  (
    .CI(\blk00000001/sig000000bd ),
    .LI(\blk00000001/sig000000c1 ),
    .O(\blk00000001/sig000000c5 )
  );
  MUXCY   \blk00000001/blk00000050  (
    .CI(\blk00000001/sig000000be ),
    .DI(a[30]),
    .S(\blk00000001/sig000000c2 ),
    .O(\blk00000001/sig000000bf )
  );
  XORCY   \blk00000001/blk0000004f  (
    .CI(\blk00000001/sig000000be ),
    .LI(\blk00000001/sig000000c2 ),
    .O(\blk00000001/sig000000c6 )
  );
  MUXCY   \blk00000001/blk0000004e  (
    .CI(\blk00000001/sig00000172 ),
    .DI(\blk00000001/sig00000043 ),
    .S(\blk00000001/sig000001df ),
    .O(\blk00000001/sig000001d5 )
  );
  XORCY   \blk00000001/blk0000004d  (
    .CI(\blk00000001/sig00000172 ),
    .LI(\blk00000001/sig000001df ),
    .O(\blk00000001/sig000001e0 )
  );
  XORCY   \blk00000001/blk0000004c  (
    .CI(\blk00000001/sig000001d7 ),
    .LI(\blk00000001/sig000001de ),
    .O(\blk00000001/sig000001e3 )
  );
  MUXCY   \blk00000001/blk0000004b  (
    .CI(\blk00000001/sig000001d7 ),
    .DI(\blk00000001/sig00000043 ),
    .S(\blk00000001/sig000001de ),
    .O(\NLW_blk00000001/blk0000004b_O_UNCONNECTED )
  );
  MUXCY   \blk00000001/blk0000004a  (
    .CI(\blk00000001/sig000001d5 ),
    .DI(\blk00000001/sig00000043 ),
    .S(\blk00000001/sig000001dc ),
    .O(\blk00000001/sig000001d6 )
  );
  XORCY   \blk00000001/blk00000049  (
    .CI(\blk00000001/sig000001d5 ),
    .LI(\blk00000001/sig000001dc ),
    .O(\blk00000001/sig000001e1 )
  );
  MUXCY   \blk00000001/blk00000048  (
    .CI(\blk00000001/sig000001d6 ),
    .DI(\blk00000001/sig00000043 ),
    .S(\blk00000001/sig000001dd ),
    .O(\blk00000001/sig000001d7 )
  );
  XORCY   \blk00000001/blk00000047  (
    .CI(\blk00000001/sig000001d6 ),
    .LI(\blk00000001/sig000001dd ),
    .O(\blk00000001/sig000001e2 )
  );
  FDRE #(
    .INIT ( 1'b0 ))
  \blk00000001/blk00000046  (
    .C(clk),
    .CE(ce),
    .D(\blk00000001/sig000001e0 ),
    .R(sclr),
    .Q(s[28])
  );
  FDRE #(
    .INIT ( 1'b0 ))
  \blk00000001/blk00000045  (
    .C(clk),
    .CE(ce),
    .D(\blk00000001/sig000001e1 ),
    .R(sclr),
    .Q(s[29])
  );
  FDRE #(
    .INIT ( 1'b0 ))
  \blk00000001/blk00000044  (
    .C(clk),
    .CE(ce),
    .D(\blk00000001/sig000001e2 ),
    .R(sclr),
    .Q(s[30])
  );
  FDRE #(
    .INIT ( 1'b0 ))
  \blk00000001/blk00000043  (
    .C(clk),
    .CE(ce),
    .D(\blk00000001/sig000001e3 ),
    .R(sclr),
    .Q(s[31])
  );
  MUXCY   \blk00000001/blk00000042  (
    .CI(\blk00000001/sig00000148 ),
    .DI(\blk00000001/sig00000043 ),
    .S(\blk00000001/sig0000016d ),
    .O(\blk00000001/sig00000163 )
  );
  XORCY   \blk00000001/blk00000041  (
    .CI(\blk00000001/sig00000148 ),
    .LI(\blk00000001/sig0000016d ),
    .O(\blk00000001/sig0000016e )
  );
  XORCY   \blk00000001/blk00000040  (
    .CI(\blk00000001/sig00000165 ),
    .LI(\blk00000001/sig0000016c ),
    .O(\blk00000001/sig00000171 )
  );
  MUXCY   \blk00000001/blk0000003f  (
    .CI(\blk00000001/sig00000165 ),
    .DI(\blk00000001/sig00000043 ),
    .S(\blk00000001/sig0000016c ),
    .O(\blk00000001/sig00000052 )
  );
  MUXCY   \blk00000001/blk0000003e  (
    .CI(\blk00000001/sig00000163 ),
    .DI(\blk00000001/sig00000043 ),
    .S(\blk00000001/sig0000016a ),
    .O(\blk00000001/sig00000164 )
  );
  XORCY   \blk00000001/blk0000003d  (
    .CI(\blk00000001/sig00000163 ),
    .LI(\blk00000001/sig0000016a ),
    .O(\blk00000001/sig0000016f )
  );
  MUXCY   \blk00000001/blk0000003c  (
    .CI(\blk00000001/sig00000164 ),
    .DI(\blk00000001/sig00000043 ),
    .S(\blk00000001/sig0000016b ),
    .O(\blk00000001/sig00000165 )
  );
  XORCY   \blk00000001/blk0000003b  (
    .CI(\blk00000001/sig00000164 ),
    .LI(\blk00000001/sig0000016b ),
    .O(\blk00000001/sig00000170 )
  );
  FDRE #(
    .INIT ( 1'b0 ))
  \blk00000001/blk0000003a  (
    .C(clk),
    .CE(ce),
    .D(\blk00000001/sig0000016e ),
    .R(sclr),
    .Q(\blk00000001/sig00000166 )
  );
  FDRE #(
    .INIT ( 1'b0 ))
  \blk00000001/blk00000039  (
    .C(clk),
    .CE(ce),
    .D(\blk00000001/sig0000016f ),
    .R(sclr),
    .Q(\blk00000001/sig00000167 )
  );
  FDRE #(
    .INIT ( 1'b0 ))
  \blk00000001/blk00000038  (
    .C(clk),
    .CE(ce),
    .D(\blk00000001/sig00000170 ),
    .R(sclr),
    .Q(\blk00000001/sig00000168 )
  );
  FDRE #(
    .INIT ( 1'b0 ))
  \blk00000001/blk00000037  (
    .C(clk),
    .CE(ce),
    .D(\blk00000001/sig00000171 ),
    .R(sclr),
    .Q(\blk00000001/sig00000169 )
  );
  MUXCY   \blk00000001/blk00000036  (
    .CI(\blk00000001/sig00000122 ),
    .DI(\blk00000001/sig00000043 ),
    .S(\blk00000001/sig00000143 ),
    .O(\blk00000001/sig00000139 )
  );
  XORCY   \blk00000001/blk00000035  (
    .CI(\blk00000001/sig00000122 ),
    .LI(\blk00000001/sig00000143 ),
    .O(\blk00000001/sig00000144 )
  );
  XORCY   \blk00000001/blk00000034  (
    .CI(\blk00000001/sig0000013b ),
    .LI(\blk00000001/sig00000142 ),
    .O(\blk00000001/sig00000147 )
  );
  MUXCY   \blk00000001/blk00000033  (
    .CI(\blk00000001/sig0000013b ),
    .DI(\blk00000001/sig00000043 ),
    .S(\blk00000001/sig00000142 ),
    .O(\blk00000001/sig00000051 )
  );
  MUXCY   \blk00000001/blk00000032  (
    .CI(\blk00000001/sig00000139 ),
    .DI(\blk00000001/sig00000043 ),
    .S(\blk00000001/sig00000140 ),
    .O(\blk00000001/sig0000013a )
  );
  XORCY   \blk00000001/blk00000031  (
    .CI(\blk00000001/sig00000139 ),
    .LI(\blk00000001/sig00000140 ),
    .O(\blk00000001/sig00000145 )
  );
  MUXCY   \blk00000001/blk00000030  (
    .CI(\blk00000001/sig0000013a ),
    .DI(\blk00000001/sig00000043 ),
    .S(\blk00000001/sig00000141 ),
    .O(\blk00000001/sig0000013b )
  );
  XORCY   \blk00000001/blk0000002f  (
    .CI(\blk00000001/sig0000013a ),
    .LI(\blk00000001/sig00000141 ),
    .O(\blk00000001/sig00000146 )
  );
  FDRE #(
    .INIT ( 1'b0 ))
  \blk00000001/blk0000002e  (
    .C(clk),
    .CE(ce),
    .D(\blk00000001/sig00000144 ),
    .R(sclr),
    .Q(\blk00000001/sig0000013c )
  );
  FDRE #(
    .INIT ( 1'b0 ))
  \blk00000001/blk0000002d  (
    .C(clk),
    .CE(ce),
    .D(\blk00000001/sig00000145 ),
    .R(sclr),
    .Q(\blk00000001/sig0000013d )
  );
  FDRE #(
    .INIT ( 1'b0 ))
  \blk00000001/blk0000002c  (
    .C(clk),
    .CE(ce),
    .D(\blk00000001/sig00000146 ),
    .R(sclr),
    .Q(\blk00000001/sig0000013e )
  );
  FDRE #(
    .INIT ( 1'b0 ))
  \blk00000001/blk0000002b  (
    .C(clk),
    .CE(ce),
    .D(\blk00000001/sig00000147 ),
    .R(sclr),
    .Q(\blk00000001/sig0000013f )
  );
  MUXCY   \blk00000001/blk0000002a  (
    .CI(\blk00000001/sig00000100 ),
    .DI(\blk00000001/sig00000043 ),
    .S(\blk00000001/sig0000011d ),
    .O(\blk00000001/sig00000115 )
  );
  XORCY   \blk00000001/blk00000029  (
    .CI(\blk00000001/sig00000100 ),
    .LI(\blk00000001/sig0000011d ),
    .O(\blk00000001/sig0000011e )
  );
  XORCY   \blk00000001/blk00000028  (
    .CI(\blk00000001/sig00000117 ),
    .LI(\blk00000001/sig0000011c ),
    .O(\blk00000001/sig00000121 )
  );
  MUXCY   \blk00000001/blk00000027  (
    .CI(\blk00000001/sig00000117 ),
    .DI(\blk00000001/sig00000043 ),
    .S(\blk00000001/sig0000011c ),
    .O(\blk00000001/sig00000050 )
  );
  MUXCY   \blk00000001/blk00000026  (
    .CI(\blk00000001/sig00000115 ),
    .DI(\blk00000001/sig00000043 ),
    .S(\blk00000001/sig0000011a ),
    .O(\blk00000001/sig00000116 )
  );
  XORCY   \blk00000001/blk00000025  (
    .CI(\blk00000001/sig00000115 ),
    .LI(\blk00000001/sig0000011a ),
    .O(\blk00000001/sig0000011f )
  );
  MUXCY   \blk00000001/blk00000024  (
    .CI(\blk00000001/sig00000116 ),
    .DI(\blk00000001/sig00000043 ),
    .S(\blk00000001/sig0000011b ),
    .O(\blk00000001/sig00000117 )
  );
  XORCY   \blk00000001/blk00000023  (
    .CI(\blk00000001/sig00000116 ),
    .LI(\blk00000001/sig0000011b ),
    .O(\blk00000001/sig00000120 )
  );
  FDRE #(
    .INIT ( 1'b0 ))
  \blk00000001/blk00000022  (
    .C(clk),
    .CE(ce),
    .D(\blk00000001/sig00000120 ),
    .R(sclr),
    .Q(\blk00000001/sig00000118 )
  );
  FDRE #(
    .INIT ( 1'b0 ))
  \blk00000001/blk00000021  (
    .C(clk),
    .CE(ce),
    .D(\blk00000001/sig00000121 ),
    .R(sclr),
    .Q(\blk00000001/sig00000119 )
  );
  MUXCY   \blk00000001/blk00000020  (
    .CI(\blk00000001/sig000000ed ),
    .DI(\blk00000001/sig00000043 ),
    .S(\blk00000001/sig000000fb ),
    .O(\blk00000001/sig000000f3 )
  );
  XORCY   \blk00000001/blk0000001f  (
    .CI(\blk00000001/sig000000ed ),
    .LI(\blk00000001/sig000000fb ),
    .O(\blk00000001/sig000000fc )
  );
  XORCY   \blk00000001/blk0000001e  (
    .CI(\blk00000001/sig000000f5 ),
    .LI(\blk00000001/sig000000fa ),
    .O(\blk00000001/sig000000ff )
  );
  MUXCY   \blk00000001/blk0000001d  (
    .CI(\blk00000001/sig000000f5 ),
    .DI(\blk00000001/sig00000043 ),
    .S(\blk00000001/sig000000fa ),
    .O(\blk00000001/sig0000004f )
  );
  MUXCY   \blk00000001/blk0000001c  (
    .CI(\blk00000001/sig000000f3 ),
    .DI(\blk00000001/sig00000043 ),
    .S(\blk00000001/sig000000f8 ),
    .O(\blk00000001/sig000000f4 )
  );
  XORCY   \blk00000001/blk0000001b  (
    .CI(\blk00000001/sig000000f3 ),
    .LI(\blk00000001/sig000000f8 ),
    .O(\blk00000001/sig000000fd )
  );
  MUXCY   \blk00000001/blk0000001a  (
    .CI(\blk00000001/sig000000f4 ),
    .DI(\blk00000001/sig00000043 ),
    .S(\blk00000001/sig000000f9 ),
    .O(\blk00000001/sig000000f5 )
  );
  XORCY   \blk00000001/blk00000019  (
    .CI(\blk00000001/sig000000f4 ),
    .LI(\blk00000001/sig000000f9 ),
    .O(\blk00000001/sig000000fe )
  );
  FDRE #(
    .INIT ( 1'b0 ))
  \blk00000001/blk00000018  (
    .C(clk),
    .CE(ce),
    .D(\blk00000001/sig000000fe ),
    .R(sclr),
    .Q(\blk00000001/sig000000f6 )
  );
  FDRE #(
    .INIT ( 1'b0 ))
  \blk00000001/blk00000017  (
    .C(clk),
    .CE(ce),
    .D(\blk00000001/sig000000ff ),
    .R(sclr),
    .Q(\blk00000001/sig000000f7 )
  );
  MUXCY   \blk00000001/blk00000016  (
    .CI(\blk00000001/sig000000d5 ),
    .DI(\blk00000001/sig00000043 ),
    .S(\blk00000001/sig000000e8 ),
    .O(\blk00000001/sig000000e0 )
  );
  XORCY   \blk00000001/blk00000015  (
    .CI(\blk00000001/sig000000d5 ),
    .LI(\blk00000001/sig000000e8 ),
    .O(\blk00000001/sig000000e9 )
  );
  XORCY   \blk00000001/blk00000014  (
    .CI(\blk00000001/sig000000e2 ),
    .LI(\blk00000001/sig000000e7 ),
    .O(\blk00000001/sig000000ec )
  );
  MUXCY   \blk00000001/blk00000013  (
    .CI(\blk00000001/sig000000e2 ),
    .DI(\blk00000001/sig00000043 ),
    .S(\blk00000001/sig000000e7 ),
    .O(\blk00000001/sig0000004e )
  );
  MUXCY   \blk00000001/blk00000012  (
    .CI(\blk00000001/sig000000e0 ),
    .DI(\blk00000001/sig00000043 ),
    .S(\blk00000001/sig000000e5 ),
    .O(\blk00000001/sig000000e1 )
  );
  XORCY   \blk00000001/blk00000011  (
    .CI(\blk00000001/sig000000e0 ),
    .LI(\blk00000001/sig000000e5 ),
    .O(\blk00000001/sig000000ea )
  );
  MUXCY   \blk00000001/blk00000010  (
    .CI(\blk00000001/sig000000e1 ),
    .DI(\blk00000001/sig00000043 ),
    .S(\blk00000001/sig000000e6 ),
    .O(\blk00000001/sig000000e2 )
  );
  XORCY   \blk00000001/blk0000000f  (
    .CI(\blk00000001/sig000000e1 ),
    .LI(\blk00000001/sig000000e6 ),
    .O(\blk00000001/sig000000eb )
  );
  FDRE #(
    .INIT ( 1'b0 ))
  \blk00000001/blk0000000e  (
    .C(clk),
    .CE(ce),
    .D(\blk00000001/sig000000eb ),
    .R(sclr),
    .Q(\blk00000001/sig000000e3 )
  );
  FDRE #(
    .INIT ( 1'b0 ))
  \blk00000001/blk0000000d  (
    .C(clk),
    .CE(ce),
    .D(\blk00000001/sig000000ec ),
    .R(sclr),
    .Q(\blk00000001/sig000000e4 )
  );
  MUXCY   \blk00000001/blk0000000c  (
    .CI(\blk00000001/sig00000061 ),
    .DI(\blk00000001/sig00000043 ),
    .S(\blk00000001/sig000000d0 ),
    .O(\blk00000001/sig000000c8 )
  );
  XORCY   \blk00000001/blk0000000b  (
    .CI(\blk00000001/sig00000061 ),
    .LI(\blk00000001/sig000000d0 ),
    .O(\blk00000001/sig000000d1 )
  );
  XORCY   \blk00000001/blk0000000a  (
    .CI(\blk00000001/sig000000ca ),
    .LI(\blk00000001/sig000000cf ),
    .O(\blk00000001/sig000000d4 )
  );
  MUXCY   \blk00000001/blk00000009  (
    .CI(\blk00000001/sig000000ca ),
    .DI(\blk00000001/sig00000043 ),
    .S(\blk00000001/sig000000cf ),
    .O(\blk00000001/sig0000004d )
  );
  MUXCY   \blk00000001/blk00000008  (
    .CI(\blk00000001/sig000000c8 ),
    .DI(\blk00000001/sig00000043 ),
    .S(\blk00000001/sig000000cd ),
    .O(\blk00000001/sig000000c9 )
  );
  XORCY   \blk00000001/blk00000007  (
    .CI(\blk00000001/sig000000c8 ),
    .LI(\blk00000001/sig000000cd ),
    .O(\blk00000001/sig000000d2 )
  );
  MUXCY   \blk00000001/blk00000006  (
    .CI(\blk00000001/sig000000c9 ),
    .DI(\blk00000001/sig00000043 ),
    .S(\blk00000001/sig000000ce ),
    .O(\blk00000001/sig000000ca )
  );
  XORCY   \blk00000001/blk00000005  (
    .CI(\blk00000001/sig000000c9 ),
    .LI(\blk00000001/sig000000ce ),
    .O(\blk00000001/sig000000d3 )
  );
  FDRE #(
    .INIT ( 1'b0 ))
  \blk00000001/blk00000004  (
    .C(clk),
    .CE(ce),
    .D(\blk00000001/sig000000d3 ),
    .R(sclr),
    .Q(\blk00000001/sig000000cb )
  );
  FDRE #(
    .INIT ( 1'b0 ))
  \blk00000001/blk00000003  (
    .C(clk),
    .CE(ce),
    .D(\blk00000001/sig000000d4 ),
    .R(sclr),
    .Q(\blk00000001/sig000000cc )
  );
  GND   \blk00000001/blk00000002  (
    .G(\blk00000001/sig00000043 )
  );

// synthesis translate_on

endmodule

// synthesis translate_off

`ifndef GLBL
`define GLBL

`timescale  1 ps / 1 ps

module glbl ();

    parameter ROC_WIDTH = 100000;
    parameter TOC_WIDTH = 0;

//--------   STARTUP Globals --------------
    wire GSR;
    wire GTS;
    wire GWE;
    wire PRLD;
    tri1 p_up_tmp;
    tri (weak1, strong0) PLL_LOCKG = p_up_tmp;

    wire PROGB_GLBL;
    wire CCLKO_GLBL;

    reg GSR_int;
    reg GTS_int;
    reg PRLD_int;

//--------   JTAG Globals --------------
    wire JTAG_TDO_GLBL;
    wire JTAG_TCK_GLBL;
    wire JTAG_TDI_GLBL;
    wire JTAG_TMS_GLBL;
    wire JTAG_TRST_GLBL;

    reg JTAG_CAPTURE_GLBL;
    reg JTAG_RESET_GLBL;
    reg JTAG_SHIFT_GLBL;
    reg JTAG_UPDATE_GLBL;
    reg JTAG_RUNTEST_GLBL;

    reg JTAG_SEL1_GLBL = 0;
    reg JTAG_SEL2_GLBL = 0 ;
    reg JTAG_SEL3_GLBL = 0;
    reg JTAG_SEL4_GLBL = 0;

    reg JTAG_USER_TDO1_GLBL = 1'bz;
    reg JTAG_USER_TDO2_GLBL = 1'bz;
    reg JTAG_USER_TDO3_GLBL = 1'bz;
    reg JTAG_USER_TDO4_GLBL = 1'bz;

    assign (weak1, weak0) GSR = GSR_int;
    assign (weak1, weak0) GTS = GTS_int;
    assign (weak1, weak0) PRLD = PRLD_int;

    initial begin
	GSR_int = 1'b1;
	PRLD_int = 1'b1;
	#(ROC_WIDTH)
	GSR_int = 1'b0;
	PRLD_int = 1'b0;
    end

    initial begin
	GTS_int = 1'b1;
	#(TOC_WIDTH)
	GTS_int = 1'b0;
    end

endmodule

`endif

// synthesis translate_on
