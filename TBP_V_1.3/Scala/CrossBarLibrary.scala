import Chisel._

class CrossBarLibrary(NAME:String,DATAsize:Int,INnumber:Int,OUTnumber:Int,CONTROLsize:Int,PAR1:String,PAR2:String,PAR3:String,PAR4:String) extends Module 
{
	val io = new CrossBarInterface(DATAsize:Int,INnumber:Int,OUTnumber:Int,CONTROLsize:Int)



/*----------------------SIMPLE CROSSBAR------------------------*/
/* SIMPLE CROSSBAR ,Last update: 29 aug 2014 , route every     */
/* single input with the selected output , OUT(ADD) = IN       */
/* the ADD is inside io.CONTROL , the word size of single ADD  */
/* is automatically calculated like log2(number of OUT)        */
/* The size of io.CONTROL must be of minimum size:             */
/*      log2(number of OUT) * number of IN                     */
/* number of INPUTs must be always < or = of number of OUTPUTs */
/*-------------------------------------------------------------*/

	if(NAME=="SimpleCrossBar")
	{
		 if(CONTROLsize<INnumber*log2Up(OUTnumber))
		 {
		 	println("")
		 	println("\033[31mERROR!\033[0m CROSS BAR CONTROLLER BUS SIZE TOO SMALL!")
		 	println("Minimum BUS size for CrossBar: " + NAME + " with " + INnumber + " INPUT and " + OUTnumber + " OUTPUT is: " + INnumber*log2Up(OUTnumber))
		 	println("Check the error and try again!")
		 	error.error=error.error+1
		 }
		 if(error.error==0)               //check for error
		 {
		 	
		 val ADD = log2Up(OUTnumber)
     io.ERR:=Bool(false)
     for(x<-0 until OUTnumber)
     {
  	  io.OUT(x):=Bits(0)                      //default value
     }

     when(io.CE===Bool(true))
     {
  	  for(x<-0 until INnumber)
  	  {
  		  io.OUT(io.CONTROL((x*ADD)+ADD-1,x*ADD)):=io.IN(x)
  	  }   
     }
	 }  
	}
/*----------------------END OF SIMPLE CROSSBAR------------------*/
/*----------------------END OF SIMPLE CROSSBAR------------------*/
/*----------------------SIMPLE CROSSBAR_1----------------------*/
/* SIMPLE CROSSBAR_1 route every                               */
/* single input with the selected output , OUT(ADD) = IN       */
/* the ADD is inside io.CONTROL , the word size of single ADD  */
/* is automatically calculated like log2(number of OUT)        */
/* The size of io.CONTROL must be of minimum size:             */
/*      log2(number of OUT) * number of IN                     */
/* number of INPUTs must be always < or = of number of OUTPUTs */
/*              LATENCY=1                                      */
/*-------------------------------------------------------------*/

	if(NAME=="SimpleCrossBar_1")
	{
		 if(CONTROLsize<INnumber*log2Up(OUTnumber))
		 {
		 	println("")
		 	println("\033[31mERROR!\033[0m CROSS BAR CONTROLLER BUS SIZE TOO SMALL!")
		 	println("Minimum BUS size for CrossBar: " + NAME + " with " + INnumber + " INPUT and " + OUTnumber + " OUTPUT is: " + INnumber*log2Up(OUTnumber))
		 	println("Check the error and try again!")
		 	error.error=error.error+1
		 }
		 if(error.error==0)               //check for error
		 {	 	
		 	val ADD = log2Up(OUTnumber)
     	io.ERR:=Bool(false)
     	val OUT = Vec.fill(OUTnumber){Reg(init = Bits(0,DATAsize))}
     	for(x<-0 until OUTnumber)
     	{
  	  	io.OUT(x):=OUT(x)                      //default value
     	}

     when(io.CE===Bool(true))
     {
  	  for(x<-0 until INnumber)
  	  {
  		  OUT(io.CONTROL((x*ADD)+ADD-1,x*ADD)):=io.IN(x)
  	  }   
     }
	 }  
	}


}