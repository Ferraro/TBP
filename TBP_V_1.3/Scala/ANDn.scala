import Chisel._
class ANDn(n:Int) extends Module
{
	val io = new ANDInterface(n)
  val ANDarray =  Vec.fill(n-1){ Module(new AND(n)).io }
  
  ANDarray(0).A:=io.IN(0)
  ANDarray(0).B:=io.IN(1)
  
	for(x<-0 until n-2)
	{
		ANDarray(x+1).B:=ANDarray(x).OUT
		ANDarray(x+1).A:=io.IN(x+2)
	}
	io.OUT:=ANDarray(n-2).OUT
}