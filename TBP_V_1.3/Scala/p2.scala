import Chisel._
import scala.collection.mutable.ArrayBuffer
import scala.io.Source
import scala.Console
import java.io.PrintWriter
import java.util.Calendar
import java.text.SimpleDateFormat

      

class P2(file:String) extends Module
{
	
	VB.get(file)                              //build 3 vector-> ModulesVector, MemoriesVector , CrossBarVector

	val OUTMnumber = VB.OutputOfCrossBar(0)-VB.numberoflogic*2                         //calculate the number of OUTM = OUT of corssbar 1 - number of logic
  val OUTLnumber = VB.OutputOfCrossBar(1)-VB.numberofmemory                        //calculate the number of OUTL = OUT of corssbar 2 - number of memory

  val INMnumber = VB.InputOfCrossBar(1)- VB.numberoflogic                        //calculate the number of INM = IN of corssbar 1 - number of MEMORY
  val INLnumber = VB.InputOfCrossBar(0)- VB.numberofmemory                       //calculate the number of INL = IN of corssbar 2 - number of LOGIC
	
	val io = new P2Interface(VB.NumberOfCell,VB.MAXBUSLsize,VB.MAXBUSMsize,VB.MAXBUSCsize,OUTLnumber,OUTMnumber,INLnumber,INMnumber,VB.CrossBarDataSize(0),VB.CrossBarDataSize(1))


  val ORVector = new ArrayBuffer[ORInterface]         //vector of OR logic on n input
  val ANDVector = new ArrayBuffer[ANDInterface]         //vector of AND logic on n input

  

	if(error.error==0)                                      //check for errors and then build the hardware
  {
  	
  ORVector+=Module(new ORn(VB.ModulesVector.length)).io                              //for overflow signal
  ORVector+=Module(new ORn(VB.ModulesVector.length)).io                              //for underflow signal
  ORVector+=Module(new ORn(VB.CrossBarVector.length)).io                             //for ERR signal
  ANDVector+=Module(new ANDn(VB.ModulesVector.length)).io                            //for RDY signal LOGIC
  ANDVector+=Module(new ANDn(VB.MemoriesVector.length)).io                           //for END signal MEMORY
  ANDVector+=Module(new ANDn(VB.ModulesVector.length)).io                           //for END signal LOGIC
/*-------------------------------------------*/

//AGGIUNGERE GLI END DELLE CELLE DI LOGICA -> fare un end aoutput al processore e collegare tutte le end con una porta END come per i rdy

		for(x<- 0 until VB.ModulesVector.length)
		{
				
			VB.ModulesVector(x).A:=VB.CrossBarVector(0).OUT(x*2)             //A and B of the logic cell form the cross bar 0
			VB.ModulesVector(x).B:=VB.CrossBarVector(0).OUT(x*2+1)		
			VB.CrossBarVector(1).IN(x):=VB.ModulesVector(x).C               //C of the logic cell to the crossbar 1			
			ORVector(0).IN(x):=VB.ModulesVector(x).OVERFLOW                 //connect all the overflow and underflow 
			ORVector(1).IN(x):=VB.ModulesVector(x).UNDERFLOW                 //connect all the overflow and underflow 
			ANDVector(0).IN(x):=VB.ModulesVector(x).RDY                     //connect all the RDY
      VB.ModulesVector(x).CE:=io.CE
      VB.ModulesVector(x).START:=io.START
      ANDVector(2).IN(x):=VB.ModulesVector(x).END  
      VB.ModulesVector(x).PE:=io.PE(x)		
      VB.ModulesVector(x).BUS:=io.BUSL
      
		}

		for(x<- 0 until VB.MemoriesVector.length)
		{
			VB.CrossBarVector(0).IN(x):= VB.MemoriesVector(x).DOUT
			VB.MemoriesVector(x).C:=VB.CrossBarVector(1).OUT(x)
			ANDVector(1).IN(x):=VB.MemoriesVector(x).END
			VB.MemoriesVector(x).CE:=io.CE	
			VB.MemoriesVector(x).START:=io.START	
			VB.MemoriesVector(x).PE:=io.PE(x+VB.ModulesVector.length)	                             //connect all PE
			VB.MemoriesVector(x).BUS:=io.BUSM
		}

	
    VB.CrossBarVector(0).CE:=io.CE
    VB.CrossBarVector(1).CE:=io.CE

    VB.CrossBarVector(0).START:=io.START
    VB.CrossBarVector(1).START:=io.START

    VB.CrossBarVector(0).PE:=io.PE(VB.ModulesVector.length+VB.MemoriesVector.length)
    VB.CrossBarVector(1).PE:=io.PE(VB.ModulesVector.length+VB.MemoriesVector.length+1)

    VB.CrossBarVector(0).BUS:=io.BUSC
    VB.CrossBarVector(1).BUS:=io.BUSC
    
      
		
    io.END:= ANDVector(1).OUT & ANDVector(2).OUT &  VB.CrossBarVector(0).END & VB.CrossBarVector(1).END
		io.RDY:=ANDVector(0).OUT 
		io.OVERFLOW:=ORVector(0).OUT
		io.UNDERFLOW:=ORVector(1).OUT

	 for(x<-0 until VB.CrossBarVector.length)
	 {
	 	 ORVector(2).IN(x):= VB.CrossBarVector(x).ERR
	 }

	 io.ERR:=ORVector(2).OUT






   for(x<-(VB.ModulesVector.length*2) until VB.CrossBarVector(0).OUT.length)
   {
   	 io.OUTM(x-(VB.ModulesVector.length*2)):=VB.CrossBarVector(0).OUT(x)             //Connecting the crossbar with output of P2
   }
   
   for(x<-VB.MemoriesVector.length until VB.CrossBarVector(1).OUT.length)
   {
   	 io.OUTL(x-VB.MemoriesVector.length):=VB.CrossBarVector(1).OUT(x)                 //Connecting the crossbar with output of P2
   }

   for(x<-VB.MemoriesVector.length until VB.CrossBarVector(0).IN.length)
   {
   	 VB.CrossBarVector(0).IN(x):=io.INL(x-VB.MemoriesVector.length)                  //Connecting the crossbar 1 with INPUT of P2
   }

   for(x<-VB.ModulesVector.length until VB.CrossBarVector(1).IN.length)
   {
   	 VB.CrossBarVector(1).IN(x):=io.INM(x-VB.ModulesVector.length)                   //Connecting the crossbar 2 with INPUT of P2
   }
   
		
	}
}