import Chisel._
import scala.collection.mutable.ArrayBuffer
import scala.io.Source
import scala.Console
import java.io.PrintWriter
import java.util.Calendar
import java.text.SimpleDateFormat
import java.util.Date

class Board(File:String) extends Module
{

	
	val P2 = Module (new P2(File))

		
	val io = new Bundle
	{
	  val START = Bits(INPUT,1)
	  val END = Bits(OUTPUT,1)
	  val ERR = Bits(OUTPUT,1)
	  val OVERFLOW = Bits(OUTPUT,1)
	  val UNDERFLOW = Bits(OUTPUT,1)
	  val OUTL = Vec.fill(VB.OutputOfCrossBar(1)-VB.numberofmemory ){Bits(OUTPUT,VB.CrossBarDataSize(0))}
	  val OUTM = Vec.fill(VB.OutputOfCrossBar(0)-VB.numberoflogic*2){Bits(OUTPUT,VB.CrossBarDataSize(1))}
	  val INL = Vec.fill(VB.InputOfCrossBar(0)- VB.numberofmemory){Bits(INPUT,VB.CrossBarDataSize(0))}
	  val INM = Vec.fill(VB.InputOfCrossBar(1)- VB.numberoflogic){Bits(INPUT,VB.CrossBarDataSize(1))}
	}

  
  val OUTM = VB.OutputOfCrossBar(0)-VB.numberoflogic*2                         //calculate the number of OUTM = OUT of corssbar 1 - number of logic
  val OUTL = VB.OutputOfCrossBar(1)-VB.numberofmemory                        //calculate the number of OUTL = OUT of corssbar 2 - number of memory

  
  val Programmer = Module (new Programmer(VB.NumberOfCell,VB.numberoflogic,VB.numberofmemory,VB.MAXBUSLsize,VB.MAXBUSMsize,VB.MAXBUSCsize))

  P2.io.START:=Programmer.io.START
  P2.io.CE:=Programmer.io.CEOUT
  P2.io.BUSL:=Programmer.io.BUSL
  P2.io.BUSM:=Programmer.io.BUSM
  P2.io.BUSC:=Programmer.io.BUSC
  P2.io.PE:=Programmer.io.PE
  P2.io.BUSL:=Programmer.io.BUSL
  P2.io.BUSM:=Programmer.io.BUSM
  P2.io.BUSC:=Programmer.io.BUSC

  io.END:=P2.io.END
  io.ERR:=P2.io.ERR
  io.OVERFLOW:=P2.io.OVERFLOW
  io.UNDERFLOW:=P2.io.UNDERFLOW

  io.OUTL:=P2.io.OUTL
  io.OUTM:=P2.io.OUTM

  P2.io.INL:=io.INL
  P2.io.INM:=io.INM
  
  


  Programmer.io.PROGRAM:=io.START
}