import Chisel._

class CrossBarCell(CNAME:String,DATAsize:Int,INnumber:Int,OUTnumber:Int,CONTROLsize:Int,C1:String,C2:String,C3:String,C4:String,NAME:String,TMEsize:Int,TYPE:String,DEPTH:Int, ADDRsize: Int, M1:String, M2:String, M3:String, M4:String) extends Module 
{

	val io = new CrossBarCellInterface(DATAsize:Int,INnumber:Int,OUTnumber:Int,CONTROLsize:Int)

	val INSTRUCTIONsize = TMEsize + Variables.CMDsize + CONTROLsize // < ------------

	// Modules Instatiating
	val Crossbar = Module(new CrossBarLibrary(CNAME,DATAsize,INnumber,OUTnumber,CONTROLsize-TMEsize-Variables.CMDsize,C1,C2,C3,C4))    
	val Controller = Module(new ControllerCrossbarLibrary("advanced", DATAsize, INnumber, CONTROLsize, CONTROLsize, TMEsize, ADDRsize))
	val CodeMemory = Module(new MemoryLibrary(NAME, INSTRUCTIONsize, TYPE, DEPTH, ADDRsize, M1, M2, M3, M4))

	// Cell <------> Controller
	Controller.io.IN := io.IN
	Controller.io.CE := io.CE
	Controller.io.PE := io.PE
	Controller.io.START := io.START
	Controller.io.BUS := io.BUS
	io.END := Controller.io.END

	// Controller <------> Crossbar
	Crossbar.io.IN := Controller.io.OUT
	Crossbar.io.CE := Controller.io.CEC
	Crossbar.io.CONTROL := Controller.io.CONTROL

	// Controller <------> CodeMemory
	CodeMemory.io.EN := Controller.io.EN
	CodeMemory.io.WE := Controller.io.WE
	CodeMemory.io.ADDR := Controller.io.ADDR
	CodeMemory.io.DIN := Controller.io.DOUT
	Controller.io.DIN := CodeMemory.io.DOUT

	// Crossbar <-----> Cell
	io.ERR := Crossbar.io.ERR
	io.OUT := Crossbar.io.OUT
}