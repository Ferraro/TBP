import Chisel._

class ControllerCrossbarLibrary(NAME:String, DATAsize:Int, INnumber:Int, CONTROLsize:Int, BUSsize:Int, TMEsize:Int, ADDRsize: Int) extends Module
{
	val io = new ControllerCrossbarInterface(DATAsize, INnumber, CONTROLsize, BUSsize, TMEsize, ADDRsize)
	
	if(NAME=="standard") {
		
		// Registers
		val WritePointer = Reg(init = UInt(0, ADDRsize))										// Store address of Code Mem in Programming Mode
		val ReadPointer = Reg(init = UInt(0, ADDRsize))											// Store address of Code Mem in Running Mode
		val LastTME = Reg(init = UInt(0, ADDRsize))													// Store the address of the Last Instruction in Code Mem
		val TimeCounter = Reg(init = UInt(0, TMEsize))											// Count the Time between the TMEs
		val EndWord = Reg(init = Bits(65535, 16))														// "Fully One" bit stream

		// Flags
		val EndFlag = Reg(init = UInt(0))
		val PulseFlag = Reg(init = UInt(0))
		val ContinousFlag = Reg(init = UInt(0))

		// Default values for the output wires
		io.DOUT := Bits(0)
		io.EN := Bits(0)
		io.WE := Bits(0)
		io.ADDR := Bits(0)
		io.END := Bits(0)
		io.CEC := Bits(0)
		io.CONTROL := UInt(0, CONTROLsize)
		io.OUT := UInt(0)

		// Connections
		io.OUT := io.IN																											// connect directly inputs with the outputs
		io.DOUT := io.BUS																										// connect the BUS to the Code Memory	
		io.END := (EndFlag | PulseFlag | ContinousFlag)											// End is an OR of several Flags

		when(io.CE === Bool(true)) {																				// When the Cell is ENABLED

			when(io.PE === Bool(true) && io.START === Bool(false)) {					// Programming or Init Mode ----------------------------
				io.EN := Bool(true)																							// Enable the Code Memory
				io.WE := Bool(true)																							// Writing Mode in Code Memory
				io.DOUT := io.BUS																								// Pass the CMD + TME + CONFIG to the Code Mem
				io.ADDR := WritePointer																					// Pass the address
				WritePointer := WritePointer + Bits(1)													// Increment the Pointer to the Code Memory
				LastTME := WritePointer																					// Save the address of the last instruction in Code Mem
			}
			.elsewhen((io.PE === Bool(false) && io.START === Bool(true)) && EndFlag === Bool(false)) {	// Runnning Mode -----------------
				io.EN := Bool(true)																																				// Enable the Code Memory
				io.CONTROL := io.DIN(CONTROLsize-1,Variables.CMDsize+TMEsize)
				io.ADDR := ReadPointer
				when(io.DIN(TMEsize+Variables.CMDsize-1,Variables.CMDsize) === EndWord(TMEsize-1,0)) {		// EndWord found ! ---------------
					EndFlag := UInt(1)																																			// End = 1 
				}
				.elsewhen(LastTME === ReadPointer) {																											// Last Instruction Found ! ------
					when(TimeCounter === io.DIN(TMEsize+Variables.CMDsize-1,Variables.CMDsize)) { 					// Time MATCH (no STOP word)
						io.CEC := Bool(true) 																																	// Enable the Crossbar
						TimeCounter := UInt(0)																																// Reset Time Count
						EndFlag := Bool(true)																																	// END = 1
						ReadPointer := ReadPointer + UInt(0)																									// Next Instruction
						}	
					.otherwise {																																						// Time does NOT match
						TimeCounter := TimeCounter + UInt(1)																									// Increment the Time Counter
						io.CEC := Bool(false)																																	// Disable the Crossbar
						EndFlag := Bool(false)																																// END = 0
					}
				}
				.otherwise {																																							// Normal execution
					when(TimeCounter === io.DIN(TMEsize+Variables.CMDsize-1,Variables.CMDsize)) { 					// TME MATCH(no Wordend no LastTME)
						io.CEC := Bool(true) 																																	// Enable the Crossbar
						TimeCounter := UInt(0)																																// Reset Time Count
						EndFlag := Bool(false)																																// END = 0	
						ReadPointer := ReadPointer + UInt(1)																									// Next Instruction
					}
					.otherwise {																																						// Time does NOT match
						TimeCounter := TimeCounter + UInt(1)																									// Increment the Time Counter
						io.CEC := Bool(false)																																	// Disable the Crossbar
						EndFlag := Bool(false)																																// END = 0	
					}
				}
				when(io.DIN(Variables.CMDsize-1,0)=== UInt(1)) {																					// CMD = Pulse End ----------> 1.
						when(TimeCounter != UInt(0)){
							PulseFlag := Bool(false)
						}
						.otherwise {
							PulseFlag := Bool(true)
						}
					}
					.elsewhen(io.DIN(Variables.CMDsize-1,0)=== UInt(2)) {																	// CMD = Continous End -----> 2.
						ContinousFlag := Bool(true)
					}
			}
			.elsewhen(io.PE === Bool(true) && io.START === Bool(true)) {																// NOP 01 -----------------------
				io.EN := Bits(0)																																					// Disable the Code Memory
				io.END := Bits(0)																																					// End = 0
				io.CEC := Bits(0)																																					// Disable the Crossbar
			}
			.elsewhen(io.PE === Bool(false) && io.START === Bool(false)) {															// NOP 02 -----------------------
				io.EN := Bits(0)																																					// Disable the Code Memory
				io.END := Bits(0)																																					// End = 0
				io.CEC := Bits(0)																																					// Disable the Crossbar
			}	
		}																																															// Finish CE = 1
		.otherwise {																																									// When the Cell is DISABLED
			io.EN := Bits(0)																																						// Disable the Code Memory
			io.END := Bits(0)																																						// End = 0
			io.CEC := Bits(0)																																						// Disable the Crossbar
		}
	}																																																// End if Standard
/*--------------------------------------------------------------------------------------------------------------------------
																  				ADVANCED CONTROLLER FOR LOOP USAGE
----------------------------------------------------------------------------------------------------------------------------*/
	if(NAME=="advanced")
	{
		// Registers
		val WritePointer = Reg(init = UInt(0, ADDRsize))										// Store address of Code Mem in Programming Mode
		val ReadPointer = Reg(init = UInt(0, ADDRsize))											// Store address of Code Mem in Running Mode
		val LastTME = Reg(init = UInt(0, ADDRsize))													// Store the address of the Last Instruction in Code Mem
		val TimeCounter = Reg(init = UInt(0, TMEsize))											// Count the Time between the TMEs
		val EndWord = Reg(init = Bits(65535, 16))														// "Fully One" bit stream
		val LoopCounter0 = Reg(init = UInt(0, TMEsize))											// Count the number of loops n already done
		val LoopCounter1 = Reg(init = UInt(0, TMEsize))											// Count the number of loops n already done
		val LastLoop = Reg(init = UInt(0,ADDRsize))													// Keep track of the last loop found (address)

		// Flags
		val EndFlag = Reg(init = UInt(0))
		val PulseFlag = Reg(init = UInt(0))
		val ContinousFlag = Reg(init = UInt(0))

		// Default values for the output wires
		io.DOUT := Bits(0)
		io.EN := Bits(0)
		io.WE := Bits(0)
		io.ADDR := Bits(0)
		io.END := Bits(0)
		io.CEC := Bits(0)
		io.CONTROL := UInt(0, CONTROLsize)
		io.OUT := UInt(0)

		// Connections
		io.OUT := io.IN																											// connect directly inputs with the outputs
		io.DOUT := io.BUS																										// connect the BUS to the Code Memory	
		io.END := (EndFlag | PulseFlag | ContinousFlag)											// End is an OR of several Flags

		when(io.CE === Bool(true)) 																					// When the Cell is ENABLED
		{
			when(io.PE === Bool(true) && io.START === Bool(false)) 						// Programming or Init Mode ----------------------------
			{
				io.EN := Bool(true)																							// Enable the Code Memory
				io.WE := Bool(true)																							// Writing Mode in Code Memory
				io.DOUT := io.BUS																								// Pass the CMD + TME + CONFIG to the Code Mem
				io.ADDR := WritePointer																					// Pass the address
				WritePointer := WritePointer + Bits(1)													// Increment the Pointer to the Code Memory
				LastTME := WritePointer																					// Save the address of the last instruction in Code Mem
			}
			.elsewhen((io.PE === Bool(false) && io.START === Bool(true)) && EndFlag === Bool(false)) 		// Runnning Mode -----------------
			{
				io.EN := Bool(true)																																				// Enable the Code Memory
				io.CONTROL := io.DIN(CONTROLsize-1,Variables.CMDsize+TMEsize)
				io.ADDR := ReadPointer
				
				when(io.DIN(Variables.CMDsize-1,0)!= UInt(3))																							// Loop type 3 NOT found 
				{						
					when(io.DIN(TMEsize+Variables.CMDsize-1,Variables.CMDsize) === EndWord(TMEsize-1,0)) 		// EndWord found ! ---------------
					{
						EndFlag := UInt(1)																																		// End = 1 
					}
					.elsewhen(LastTME === ReadPointer) 																											// Last Instruction Found ! ------
					{
						when(TimeCounter === io.DIN(TMEsize+Variables.CMDsize-1,Variables.CMDsize)) 	 				// Time MATCH (no STOP word)
						{
							io.CEC := Bool(true) 																																// Enable the Crossbar
							TimeCounter := UInt(0)																															// Reset Time Count
							EndFlag := Bool(true)																																// END = 1
							//ReadPointer := ReadPointer + UInt(0)																								// Next Instruction
						}	
						.otherwise 																																						// Time does NOT match
						{
							TimeCounter := TimeCounter + UInt(1)																								// Increment the Time Counter
							io.CEC := Bool(false)																																// Disable the Crossbar
							EndFlag := Bool(false)																															// END = 0
						}
					}
					.otherwise 																																							// Normal execution
					{
						when(TimeCounter === io.DIN(TMEsize+Variables.CMDsize-1,Variables.CMDsize)) 	 				// TME MATCH(no Wordend no LastTME)
						{
							io.CEC := Bool(true) 																																// Enable the Crossbar
							TimeCounter := UInt(0)																															// Reset Time Count
							EndFlag := Bool(false)																															// END = 0	
							ReadPointer := ReadPointer + UInt(1)																								// Next Instruction
						}
						.otherwise 																																						// Time does NOT match
						{
							TimeCounter := TimeCounter + UInt(1)																								// Increment the Time Counter
							io.CEC := Bool(false)																																// Disable the Crossbar
							EndFlag := Bool(false)																															// END = 0	
						}
					}
				}
/*--------------------------------------------------------------------------------------------------------------------------
																  											COMMAND SECTION
----------------------------------------------------------------------------------------------------------------------------*/
			
				when(io.DIN(Variables.CMDsize-1,0)=== UInt(1)) 																		// CMD = Pulse End -------------> 1.
				{
					when(TimeCounter != UInt(0))
					{
						PulseFlag := Bool(false)
					}
					.otherwise
					{
						PulseFlag := Bool(true)
					}
				}
				.elsewhen(io.DIN(Variables.CMDsize-1,0)=== UInt(2)) 															// CMD = Continous End ---------> 2.
				{
					ContinousFlag := Bool(true)
				}
				.elsewhen(io.DIN(Variables.CMDsize-1,0)=== UInt(3)) 															// CMD = Loop n times from x ---> 3.
				{
					when(LastTME === ReadPointer)																													// Loop is Last Instruction!
					{
						when(io.DIN(CONTROLsize-1,Variables.CMDsize + TMEsize) >= LastLoop)									// x > LastLoop
						{
							when(LoopCounter0 < io.DIN(TMEsize+Variables.CMDsize-1,Variables.CMDsize))				// if loopCount < n
							{
								ReadPointer := io.DIN(CONTROLsize-1,Variables.CMDsize + TMEsize)								// ReadPointer = x
								TimeCounter := UInt(0)																													// Reset TimeCounter
								LoopCounter0 := LoopCounter0 + UInt(1)																					// Increment LoopCounter
								io.CEC := Bool(false)
								EndFlag := Bool(false)
							}
							.otherwise
							{
								ReadPointer := ReadPointer + UInt(1)																						// incrementare ReadPointer
								LoopCounter0 := UInt(0)																													// Reset LoopCounter
								TimeCounter := UInt(0)																													// Reset TimeCount				
								EndFlag := Bool(true)																														// EndFlag = 0
								LastLoop := ReadPointer																													// Update address of last loop
								io.CEC := Bool(false)
							}
						}
						.otherwise																																					// ReadPointer =< LastLoop
						{
							when(LoopCounter1 < io.DIN(TMEsize+Variables.CMDsize-1,Variables.CMDsize))				// if loopCount < n
							{
								ReadPointer := io.DIN(CONTROLsize-1,Variables.CMDsize + TMEsize)								// ReadPointer = x
								TimeCounter := UInt(0)																													// Reset TimeCounter
								LoopCounter1 := LoopCounter1 + UInt(1)																					// Increment LoopCounter
								io.CEC := Bool(false)
								EndFlag := Bool(false)
								LastLoop := UInt(0)
							}
							.otherwise
							{
								ReadPointer := ReadPointer + UInt(1)																						// incrementare ReadPointer
								LoopCounter1 := UInt(0)																													// Reset LoopCounter
								TimeCounter := UInt(0)																													// Reset TimeCount				
								EndFlag := Bool(true)																														// EndFlag = 0
								io.CEC := Bool(false)
								LastLoop := UInt(0)																															// Update address of last loop
							}
						}
					}																																											// close last instruction
					.otherwise
					{																																											// open NO last instruction
						when(io.DIN(CONTROLsize-1,Variables.CMDsize + TMEsize) >= LastLoop)									// x > LastLoop
						{
							when(LoopCounter0 < io.DIN(TMEsize+Variables.CMDsize-1,Variables.CMDsize))				// if loopCount < n
							{
								ReadPointer := io.DIN(CONTROLsize-1,Variables.CMDsize + TMEsize)								// ReadPointer = x
								TimeCounter := UInt(0)																													// Reset TimeCounter
								LoopCounter0 := LoopCounter0 + UInt(1)																					// Increment LoopCounter
								io.CEC := Bool(false)
								EndFlag := Bool(false)
							}
							.otherwise
							{
								ReadPointer := ReadPointer + UInt(1)																						// incrementare ReadPointer
								LoopCounter0 := UInt(0)																													// Reset LoopCounter
								TimeCounter := UInt(0)																													// Reset TimeCount				
								EndFlag := Bool(false)																													// EndFlag = 0
								LastLoop := ReadPointer																													// Update address of last loop
								io.CEC := Bool(false)
							}
						}
						.otherwise																																						// ReadPointer =< LastLoop
						{
							when(LoopCounter1 < io.DIN(TMEsize+Variables.CMDsize-1,Variables.CMDsize))					// if loopCount < n
							{
								ReadPointer := io.DIN(CONTROLsize-1,Variables.CMDsize + TMEsize)									// ReadPointer = x
								TimeCounter := UInt(0)																														// Reset TimeCounter
								LoopCounter1 := LoopCounter1 + UInt(1)																						// Increment LoopCounter
								io.CEC := Bool(false)
								EndFlag := Bool(false)
								LastLoop := UInt(0)
							}
							.otherwise
							{
								ReadPointer := ReadPointer + UInt(1)																						// incrementare ReadPointer
								LoopCounter1 := UInt(0)																													// Reset LoopCounter
								TimeCounter := UInt(0)																													// Reset TimeCount				
								EndFlag := Bool(false)																													// EndFlag = 0
								io.CEC := Bool(false)
								LastLoop := UInt(0)																															// Update address of last loop
							}
						}
					}
				}	// close cmd 3
			}																																											// End Running Mode
			.elsewhen(io.PE === Bool(true) && io.START === Bool(true)) {													// NOP 01 -----------------------
				io.EN := Bits(0)																																		// Disable the Code Memory
				io.END := Bits(0)																																		// End = 0
				io.CEC := Bits(0)																																		// Disable the Crossbar
			}
			.elsewhen(io.PE === Bool(false) && io.START === Bool(false)) {												// NOP 02 -----------------------
				io.EN := Bits(0)																																		// Disable the Code Memory
				io.END := Bits(0)																																		// End = 0
				io.CEC := Bits(0)																																		// Disable the Crossbar
			}	
		}																																												// Finish CE = 1
		.otherwise {																																						// When the Cell is DISABLED
			io.EN := Bits(0)																																			// Disable the Code Memory
			io.END := Bits(0)																																			// End = 0
			io.CEC := Bits(0)																																			// Disable the Crossbar
		}
	}
}																																														// End Module