import Chisel._

class Decoder(n: Int) extends Module 
{
  if(log2Up(n)>log2Up(Variables.PROGRAMMABLECELLnumber))
  {
		error.error=error.error+1
		error.errorST+="ERROR! too much input for decoder , try to modify global variable 'PROGRAMMABLECELLnumber'"
		println("ERROR! too much input for decoder , try to modify global variable 'PROGRAMMABLECELLnumber'")
  }

		val io = new Bundle 
		{
			val IN = UInt(INPUT, log2Up(Variables.PROGRAMMABLECELLnumber))
			val OUT = Vec.fill(n){ Bits(OUTPUT, width = 1) }
			val CE = Bits(INPUT,1)
		}

		
  if(error.error==0)
  {
		io.OUT := UInt(0)
		when(io.CE===Bool(true))
		{
    	io.OUT(io.IN) := Bool(true)
		}
		.otherwise
		{
			io.OUT := UInt(0)
		}
  }

  
}