import scala.collection.mutable.ArrayBuffer


object error
{
	var error = 0
	var warning = 0
  val errorST = new ArrayBuffer[String]
  val warningST = new ArrayBuffer[String]



def clean()
{
	errorST.clear
	warningST.clear
	error=0
	warning=0
}

def print()
{
	for(x<- 0 until errorST.length)
	{
		println(x + "->   \033[31m" + errorST(x) + "\033[0m")
	}
	for(x<- 0 until warningST.length)
	{
		println(x + "->   \033[33m" + warningST(x) + "\033[0m")
	}
}

	
def logo()
{
                         
println("\033[33m                ```  ::  ")
println("               ``,#:`.@. ")
println("               .'##.`:@. ")
println("                  ``.@#  ")
println("                 ``:@+   ")
println("\033[36m   `````````.\033[33m   ``,:,`'` ")
println("\033[36m ````````````#.\033[33m```````@: ")
println("\033[36m `````````` `:#.\033[33m:;;;:,,` ")
println("\033[36m `````;+;````.@.         ")
println(" ````.@'`````.@,         ")
println(" ````.@' ````,@,         ")
println(" ````.@;`````;@.         ")
println(" ````.```````@#          ")
println(" ```  ``````'@;          ")
println(" ``````  `.'@;           ")
println(" `````:;+##'`            ")
println(" ````.@'                 ")
println("  ```.@'                 ")
println("  ` `.@'                 ")
println("    `.@'                 ")
println(" `` `.@'                 ")
println(" ``.`.@'                 ")
println("  .,.``                  \033[0m")

}
}