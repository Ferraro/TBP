/*      08/05/2014      
 
To build, from terminal:
"sbt "run --backend v"
*/


import Chisel._

// ------------------------------------------ CROSSBAR ---------------------------------------------

class CrossBarCellInterface(DATAsize:Int,INnumber:Int,OUTnumber:Int,BUSsize:Int) extends Bundle 
{
  val IN  = Vec.fill(INnumber){Bits(INPUT,DATAsize)}
  val BUS = Bits(INPUT,BUSsize)
  val START = Bits(INPUT,1)
  val CE = Bits(INPUT,1)
  val PE = Bits(INPUT,1)
  
  val OUT = Vec.fill(OUTnumber){Bits(OUTPUT,DATAsize)}
  val ERR = Bits(OUTPUT,1)
  val END = Bits(OUTPUT,1)
}

class ControllerCrossbarInterface(DATAsize:Int ,INnumber:Int, CONTROLsize:Int, BUSsize:Int, TMEsize:Int, ADDRsize:Int) extends Bundle {
	val IN  = Vec.fill(INnumber){Bits(INPUT,DATAsize)}
	val BUS = Bits(INPUT,BUSsize)
  val START = Bits(INPUT,1)
  val CE = Bits(INPUT,1)
  val PE = Bits(INPUT,1)
  val DIN = Bits(INPUT, CONTROLsize)
  val EN = Bits(OUTPUT, 1)
	val WE = Bits(OUTPUT, 1)
	val ADDR = Bits(OUTPUT, ADDRsize)
	val DOUT = Bits(OUTPUT, CONTROLsize)

	val END = Bits(OUTPUT,1)
	val CONTROL = Bits(OUTPUT, CONTROLsize-TMEsize-Variables.CMDsize)
	val CEC = Bits(OUTPUT,1)
	val OUT = Vec.fill(INnumber){Bits(OUTPUT,DATAsize)}
}

class CrossBarInterface(DATAsize:Int,INnumber:Int,OUTnumber:Int,CONTROLsize:Int) extends Bundle 
{
  val IN  = Vec.fill(INnumber){Bits(INPUT,DATAsize)}
  val CONTROL = Bits(INPUT,CONTROLsize)
  val CE = Bits(INPUT,1)
  val OUT = Vec.fill(OUTnumber){Bits(OUTPUT,DATAsize)}
  val ERR = Bits(OUTPUT,1)
}

// ------------------------------------------ MEMORY -----------------------------------------------

class CellMemoryInterface(DATAsize:Int, BUSsize: Int) extends Bundle {
	val C = Bits(INPUT, DATAsize)
	val CE = Bits(INPUT, 1)
	val PE = Bits(INPUT, 1)
	val START = Bits(INPUT, 1)
	val BUS = Bits(INPUT, (BUSsize))

	val DOUT = Bits(OUTPUT, DATAsize)
	val END = Bits(OUTPUT, 1)
}

class ControllerMemoryInterface(DATAsize: Int, BUSsize: Int, TMEsize: Int, ADDRsize: Int, ADDRsize1: Int) extends Bundle {
	val C = Bits(INPUT,DATAsize)
	val CE = Bits(INPUT,1)
	val PE = Bits(INPUT,1)
	val START = Bits(INPUT,1)
	val BUS = Bits(INPUT, BUSsize)
	val DIN1 = Bits(INPUT, (TMEsize + Variables.CMDsize + 1 + ADDRsize))

	val EN = Bits(OUTPUT, 1)
	val WE = Bits(OUTPUT, 1)
	val ADDR = Bits(OUTPUT, ADDRsize)
	val DOUT = Bits(OUTPUT, DATAsize)

	val END = Bits(OUTPUT, 1)
	
	val EN1 = Bits(OUTPUT, 1)
	val WE1 = Bits(OUTPUT, 1)
	val ADDR1 = Bits(OUTPUT, ADDRsize1)
	val DOUT1 = Bits(OUTPUT, (TMEsize + Variables.CMDsize + 1 + ADDRsize))	
}

class MemoryInterface(DATAsize:Int,ADDRsize:Int) extends Bundle {

	 	val EN = Bits(INPUT,1)
	 	val WE = Bits(INPUT,1)
	 	val ADDR = Bits(INPUT,ADDRsize)
	 	val DIN = Bits(INPUT,DATAsize)
	 	val DOUT = Bits(OUTPUT,DATAsize) 
}

// -------------------------------------- LOGIC -----------------------------------------------------------

class CellLogicInterface(DATAsize: Int, BUSsize: Int) extends Bundle {	
	  val A = Bits(INPUT, DATAsize)
	  val B = Bits(INPUT, DATAsize)
	  
	  val BUS = Bits(INPUT, BUSsize)
	  val CE = Bits(INPUT, 1)
	  val START = Bits(INPUT, 1)
	  val PE = Bits(INPUT, 1)

	  val C = Bits(OUTPUT, DATAsize)
	  val END = Bits(OUTPUT, 1)
	 	val OVERFLOW = Bits(OUTPUT, 1)
	 	val UNDERFLOW = Bits(OUTPUT, 1)
	 	val RDY = Bits(OUTPUT, 1)
}

class ControllerLogicInterface(DATAsize: Int, BUSsize: Int, TMEsize: Int, ADDRsize: Int) extends Bundle {
	val A = Bits(INPUT, DATAsize)
	val B = Bits(INPUT, DATAsize)
	
	val BUS = Bits(INPUT, BUSsize)
	val CE = Bits(INPUT, 1)
	val START = Bits(INPUT, 1)
	val PE = Bits(INPUT, 1)
	val DIN1 = Bits(INPUT, TMEsize + Variables.CMDsize)
	
	val DOUTA = Bits(OUTPUT, DATAsize)
	val DOUTB = Bits(OUTPUT, DATAsize)
	val CEL = Bits(OUTPUT, 1)

	val END = Bits(OUTPUT, 1)

	val EN = Bits(OUTPUT, 1)
	val WE = Bits(OUTPUT, 1)
	val ADDR = Bits(OUTPUT, ADDRsize)
	val DOUT1 = Bits(OUTPUT, TMEsize + Variables.CMDsize)
}

class LogicInterface(DATAsize:Int) extends Bundle {	
	  val A = Bits(INPUT, DATAsize)
	  val B = Bits(INPUT, DATAsize)
	  val C = Bits(OUTPUT, DATAsize)
	  val CE = Bits(INPUT, 1)
	  val RDY = Bits(OUTPUT, 1)
	 	val OVERFLOW = Bits(OUTPUT, 1)
	 	val UNDERFLOW = Bits(OUTPUT, 1)
}

class LatencyControllerInterface extends Bundle {
		val CE = Bits(INPUT, 1)
		val END = Bits(INPUT, 1)
		val CEout = Bits(OUTPUT, 1)
		val ENDout = Bits(OUTPUT, 1)
}

// ----------------------------------- MISCELLANEOUS -----------------------------------

class ORInterface(INPUTnumber:Int) extends Bundle
{
	 val IN  = Vec.fill(INPUTnumber){Bits(INPUT,1)}
	 val OUT = Bits(OUTPUT,1)
}
class ANDInterface(INPUTnumber:Int) extends Bundle
{
	 val IN  = Vec.fill(INPUTnumber){Bits(INPUT,1)}
	 val OUT = Bits(OUTPUT,1)
}

class P2Interface(CELLnumber:Int,BUSLsize:Int,BUSMsize:Int,BUSCsize:Int,OUTLnumber:Int,OUTMnumber:Int,INLnumber:Int,INMnumber:Int,OUTLsize:Int,OUTMsize:Int) extends Bundle
{
  val CE = Bits(INPUT,1)
  val START = Bits(INPUT,1)
  val PE  = Vec.fill(CELLnumber){Bits(INPUT,1)}
  val BUSL = Bits(INPUT,BUSLsize)
  val BUSM = Bits(INPUT,BUSMsize)
  val BUSC = Bits(INPUT,BUSCsize)
	val END = Bits(OUTPUT,1)
	val RDY = Bits(OUTPUT,1)
	val ERR = Bits(OUTPUT,1)
	val OVERFLOW = Bits(OUTPUT,1)
	val UNDERFLOW = Bits(OUTPUT,1)
	val OUTL = Vec.fill(OUTLnumber){Bits(OUTPUT,OUTLsize)}
	val OUTM = Vec.fill(OUTMnumber){Bits(OUTPUT,OUTMsize)}
	val INL = Vec.fill(INLnumber){Bits(INPUT,OUTLsize)}
	val INM = Vec.fill(INMnumber){Bits(INPUT,OUTMsize)}
}