import Chisel._

class ControllerLogicLibrary(NAME:String,BUSsize:Int,DATAsize:Int,TMEsize:Int,ADDRsize:Int,PS:Int,LATENCY:Int) extends Module 
{
	val io = new ControllerLogicInterface(DATAsize, BUSsize, TMEsize, ADDRsize)
	
	if(NAME=="standard")
	{
		// Registers
		val WritePointer = Reg(init = UInt(0, ADDRsize))												// Store address of Code Mem in Programming Mode
		val ReadPointer = Reg(init = UInt(0, ADDRsize))													// Store address of Code Mem in Running Mode
		val LastTME = Reg(init = UInt(0, ADDRsize))															// Store the address of the Last Instruction in Code Mem
		val TimeCounter = Reg(init = UInt(0, TMEsize))													// Count the Time between the TMEs
		val EndWord = Reg(init = Bits(65535, 16))																// "Fully one" bit stream

		// Flags
		val DirectlyEND = Reg(init = Bool(false))
		val ContinousFlag = Reg(init = UInt(0))
		val PulseFlag = Reg(init = UInt(0))

		// Modules instantiates
		val LatencyController0 = Module(new LatencyController(LATENCY))
		
		// Connections
		io.DOUT1 := io.BUS																											// connect BUS to Code Memory
		io.DOUTA := io.A																												// Operands directly to the output (Logic)
		io.DOUTB := io.B
		io.END := (LatencyController0.io.ENDout | ContinousFlag | PulseFlag | DirectlyEND)

		// Default Value
		io.EN := Bool(false)
		io.WE := Bool(false)
		io.ADDR := UInt(0)
		LatencyController0.io.CE := UInt(0)
		LatencyController0.io.END := UInt(0)
		io.CEL := Bool(false)

		// --------------------------------------------------------------------------------------------------------------------------
		// ---------------------------------------------- POWER SAVE MODE ON --------------------------------------------------------
		// --------------------------------------------------------------------------------------------------------------------------
		if(PS == 1) {																												// Power Save Mode ON -----------------------------------
			io.CEL := (LatencyController0.io.CEout | UInt(0,1))
			when(io.CE === Bool(true)) {																			// When the Cell is ENABLED
				
				when(io.PE === Bool(true) && io.START === Bool(false)) {				// Programming Mode -----------------------------
					io.EN := Bool(true)																						// Enable the Code Memory
					io.WE := Bool(true)																						// Writing Mode in Code Memory
					LatencyController0.io.CE := Bool(false)												// Disable the Logic
					io.ADDR := WritePointer																				//
					WritePointer := WritePointer + Bits(1)												// Increment the Pointer to the Code Memory
					LastTME := WritePointer																				// Save the address of the last instruction in Code Mem
				}
				
				// ------------------------------------------------------------------------------------------------------------------
				// --------------------------------------------- RUNNING MODE -------------------------------------------------------
				// ------------------------------------------------------------------------------------------------------------------
				
				.elsewhen((io.PE === Bool(false) && io.START === Bool(true))) {	// Running Mode -------------------------------
					io.EN := Bool(true)																										// Enable the Code Memory
					io.WE := Bool(false)																									// Reading Mode in Code Memory
					io.ADDR := ReadPointer																								// Give the correct address to the Code Memory

					when(io.DIN1(Variables.CMDsize+TMEsize-1,Variables.CMDsize) === EndWord(TMEsize-1,0)) {		// EndWord found !
						DirectlyEND := UInt(1)
					}
					.elsewhen(LastTME === ReadPointer) {																	// Last Instruction Found !
						when(TimeCounter === io.DIN1(Variables.CMDsize+TMEsize-1,Variables.CMDsize)) {	// if Time MATCH (no last TME, no STOP word)
							LatencyController0.io.CE := Bool(true)														// Enable the Logic
							LatencyController0.io.END := Bool(true)														//
							TimeCounter := UInt(0)																						// Reset Time counter
						}
						.otherwise {																												// Time does NOT match
							TimeCounter := TimeCounter + UInt(1)															// Increment the Time Counter
							LatencyController0.io.CE := Bool(false)														// Disable the Logic
						}
					}
					.otherwise{																														// Standard TME
						when(TimeCounter === io.DIN1(Variables.CMDsize+TMEsize-1,Variables.CMDsize)) {	// if Time MATCH (no last TME, no STOP word)
							LatencyController0.io.CE := Bool(true)														// Enable the Logic
							TimeCounter := UInt(0)																						// Reset Time Counter
							ReadPointer := ReadPointer + UInt(1)
						}
						.otherwise {																												// Time does NOT match
							TimeCounter := TimeCounter + UInt(1)															// Increment the Time Counter
							LatencyController0.io.CE := Bool(false)														// Disable the Logic
							LatencyController0.io.CE := Bool(false)
						}
					}
					when(io.DIN1(Variables.CMDsize-1,0)=== UInt(1)) {											// CMD = Pulse End ----------> 1.
						when(TimeCounter != UInt(0)){
							PulseFlag := Bool(false)
						}
						.otherwise {
							PulseFlag := Bool(true)
						}
					}
					.elsewhen(io.DIN1(Variables.CMDsize-1,0)=== UInt(2)) {								// CMD = Continous End -----> 2.
						ContinousFlag := Bool(true)
					}
				}																																				// Finish Running Mode
				.elsewhen(io.PE === Bool(true) && io.START === Bool(true)) {						// NOP 01
					io.EN := Bool(false)
					io.WE := Bool(false)
					LatencyController0.io.CE := Bool(false)																// Disable the Logic					
				}
				.elsewhen(io.PE === Bool(false) && io.START === Bool(false)) {					// NOP 02
					io.EN := Bool(false)
					io.WE := Bool(false)
					LatencyController0.io.CE := Bool(false)																// Disable the Logic					
				}
			}																																					// Finish CE = 1
			.otherwise {																															// when the Cell is DISABLED
				io.EN := Bool(false)
				io.WE := Bool(false)
			}									
		}
		
		// -------------------------------------------------------------------------------------------------------------------------
		// ---------------------------------- CHANGE ONLY THE FIRST LINE -----------------------------------------------------------
		// -------------------------------------------------------------------------------------------------------------------------
		
		else {																															// Power Save Mode OFF ----------------------------------
			io.CEL := ((LatencyController0.io.CEout | UInt(1,1)) & io.START)
			when(io.CE === Bool(true)) {																			// When the Cell is ENABLED
				
				when(io.PE === Bool(true) && io.START === Bool(false)) {				// Programming Mode -----------------------------
					io.EN := Bool(true)																						// Enable the Code Memory
					io.WE := Bool(true)																						// Writing Mode in Code Memory
					LatencyController0.io.CE := Bool(false)												// Disable the Logic
					io.ADDR := WritePointer																				//
					WritePointer := WritePointer + Bits(1)												// Increment the Pointer to the Code Memory
					LastTME := WritePointer																				// Save the address of the last instruction in Code Mem
				}
				
				// ------------------------------------------------------------------------------------------------------------------
				// --------------------------------------------- RUNNING MODE -------------------------------------------------------
				// ------------------------------------------------------------------------------------------------------------------
				
				.elsewhen(io.PE === Bool(false) && io.START === Bool(true)) {						// Running Mode -------------------------------
					io.EN := Bool(true)																										// Enable the Code Memory
					io.WE := Bool(false)																									// Reading Mode in Code Memory
					io.ADDR := ReadPointer																								// Give the correct address to the Code Memory

					when(io.DIN1(Variables.CMDsize+TMEsize-1,Variables.CMDsize) === EndWord(TMEsize-1,0)) {		// EndWord found !
						DirectlyEND := UInt(1)
					}
					.elsewhen(LastTME === ReadPointer) {																	// Last Instruction Found !
						when(TimeCounter === io.DIN1(Variables.CMDsize+TMEsize-1,Variables.CMDsize)) {	// if Time MATCH (no last TME, no STOP word)
							LatencyController0.io.CE := Bool(true)														// Enable the Logic
							LatencyController0.io.END := Bool(true)														//
							TimeCounter := UInt(0)																						// Reset Time counter
						}
						.otherwise {																												// Time does NOT match
							TimeCounter := TimeCounter + UInt(1)															// Increment the Time Counter
							LatencyController0.io.CE := Bool(false)														// Disable the Logic
						}
					}
					.otherwise{																														// Standard TME
						when(TimeCounter === io.DIN1(Variables.CMDsize+TMEsize-1,Variables.CMDsize)) {	// if Time MATCH (no last TME, no STOP word)
							LatencyController0.io.CE := Bool(true)														// Enable the Logic
							TimeCounter := UInt(0)																						// Reset Time Counter
							ReadPointer := ReadPointer + UInt(1)
						}
						.otherwise {																												// Time does NOT match
							TimeCounter := TimeCounter + UInt(1)															// Increment the Time Counter
							LatencyController0.io.CE := Bool(false)														// Disable the Logic
						}
					}
					when(io.DIN1(Variables.CMDsize-1,0)=== UInt(1)) {											// CMD = Pulse End ----------> 1.
						when(TimeCounter != UInt(0)){
							PulseFlag := Bool(false)
						}
						.otherwise {
							PulseFlag := Bool(true)
						}
					}
					.elsewhen(io.DIN1(Variables.CMDsize-1,0)=== UInt(2)) {								// CMD = Continous End -----> 2.
						ContinousFlag := Bool(true)
					}
				}																																				// Finish Running Mode
				.elsewhen(io.PE === Bool(true) && io.START === Bool(true)) {						// NOP 01
					io.EN := Bool(false)
					io.WE := Bool(false)
					LatencyController0.io.CE := Bool(false)																// Disable the Logic					
				}
				.elsewhen(io.PE === Bool(false) && io.START === Bool(false)) {					// NOP 02
					io.EN := Bool(false)
					io.WE := Bool(false)
					LatencyController0.io.CE := Bool(false)																// Disable the Logic					
				}
			}																																					// Finish CE = 1
			.otherwise {																															// when the Cell is DISABLED
				io.EN := Bool(false)
				io.WE := Bool(false)
			}								
		}
	}																																							// End if Name Standard
}																																								// End Module