/*
VB -> Vector Builder , return 3 vector:
 ModulesVector, MemoriesVector , CrossBarVector

this array are builded following the instructions in the external file.
to build the vectors call the def VB.get(file)
*/



import Chisel._
import scala.collection.mutable.ArrayBuffer
import scala.io.Source
import scala.Console
import java.io.PrintWriter
import java.util.Calendar
import java.text.SimpleDateFormat
import java.util.Date



object VB
{
	val ModulesVector = new ArrayBuffer[CellLogicInterface]
  val MemoriesVector = new ArrayBuffer[CellMemoryInterface]    //array of modules
  val CrossBarVector = new ArrayBuffer[CrossBarCellInterface]
	var NumberOfCell = 0
  var numberoflogic=0
  var numberofmemory=0
  var numberofcrossbar=0
  var OutputOfCrossBar= new ArrayBuffer[Int]
  var InputOfCrossBar= new ArrayBuffer[Int]
  var MAXBUSLsize = 0
  var MAXBUSMsize = 0
  var MAXBUSCsize = 0
  var CrossBarDataSize = new ArrayBuffer[Int]


  
 println("") 
	def get(file:String)
	{

		numberoflogic=0
    numberofmemory=0                   //clear old variables
    numberofcrossbar=0
    OutputOfCrossBar.clear
    InputOfCrossBar.clear
    CrossBarDataSize.clear
    MAXBUSLsize = 0
    MAXBUSMsize = 0
    MAXBUSCsize = 0
    NumberOfCell = 0

  
		/* READING FROM FILE THE PROCESSOR STRUCTURE */
  if(ModulesVector.isEmpty && MemoriesVector.isEmpty && CrossBarVector.isEmpty)
  {
  	println("OK! Vectors buffer empty...")
  	println("Starting to fill the vectors...")
  }
  else
  {
  	println("! Vector buffer full...")
  	println("cleaning the vectors...")
  	ModulesVector.clear
  	MemoriesVector.clear
  	CrossBarVector.clear
  	println("Starting to fill the vectors...")
  }
 println("")  
  val wfile = new PrintWriter("Detail.txt" )
  val source = Source.fromFile(file)
  var errorFlag = 0
  var vectorswitch = 0
  var detail=false
  var header=true
  var moduletype="Logic"
  var counter = 0
  var n=0
  var animation=0
  var animationdelay=0
  var optimization=false
  var optimizationWord="Manual"
  

  
 // var error=false
  
  println("<--------------\033[4m\033[5m\033[34PROCESSOR BUILDING\033[0m------------->")
  println("Start reading from file...")
  val before = Calendar.getInstance().getTime()
  val lines = source.getLines.toArray
  
  println("PROGRESS:")

  for(x<-0 until lines.length)
  {
  
  	if(lines(x).isEmpty)
  	{
  		                                                    //SPACE IN THE FILE
  	}
  	if(!lines(x).isEmpty)
  	{
  		if(lines(x)(0)=='#')                                //change of vector logic->memory->crossbar  '#"
  		{
  		vectorswitch = vectorswitch+1
  		}
  		
  		else if(lines(x)(0)=='/')                          //COMMENT IN THE FILE    "/"
  		{
  			
  		}

////-------------- options handling -----------------////


  		else if(lines(x)(0)=='-')
  		{
  			if(lines(x).substring(1,lines(x).length)=="o")                 //"-o" auto calculate values
  			{
  				optimization=true
  				optimizationWord="Auto"
  			}
  			if(lines(x).substring(1,lines(x).length)=="m")                 //"-m" manual explicit values 
  			{
  				optimization=false
  				optimizationWord="Manual"
  			}
  			if(lines(x).substring(1,lines(x).length)=="detail")            //"-detail" generate file with details
  			{
  				detail=true
  			}
  		}


//////------------- reading from file with no optimization-----------------////
  		
  		else if(optimization==false)                                    
  		{
  		var space = lines(x).indexOf(" ")        //find the index of the space char inside every line of the file

     		
  		var modulenumber = lines(x).substring(0,space).toInt
  
  		
  		for(y<-0 until modulenumber)
  		{ 

  			
  			
  			if(animation==0)
  			{
  				print("\033[1m\033[35m\b\\\033[0m")
        	counter = counter +1
        	n=n+1
  			}
  			if(animation==1)
  			{
  				print("\033[1m\033[32m\b|\033[0m")
        	counter = counter +1
        	n=n+1
  			}
  			if(animation==2)
  			{
					print("\033[1m\033[31m\b/\033[0m")                  //animation
        	counter = counter +1
        	n=n+1
  			}
  			if(animation==3)
  			{
  				print("\033[1m\033[33m\b-\033[0m")
        	counter = counter +1
        	n=n+1
  			}
  			
  			if(animation>3)
  			{
  					animation=0
  					print("\b" + 9658.toChar + " ")
  			}
  			else
  			{
  				animation=animation+1
  			}
  			
        
  			if(vectorswitch==0)                                                   //logic vector
  			{
  				var space1 = lines(x).indexOf(' ',space+1)
  				var par1 = lines(x).substring(space+1,space1)
  				var space2 = lines(x).indexOf(' ',space1+1)
  				var par2 = lines(x).substring(space1+1,space2)
  				var space3 = lines(x).indexOf(' ',space2+1)
  				var par3 = lines(x).substring(space2+1,space3)
  				var space4 = lines(x).indexOf(' ',space3+1)
  				var par4 = lines(x).substring(space3+1,space4)
  				var space5 = lines(x).indexOf(' ',space4+1)
  				var par5 = lines(x).substring(space4+1,space5)
  				var space6 = lines(x).indexOf(' ',space5+1)
  				var par6 = lines(x).substring(space5+1,space6)
  				var space7 = lines(x).indexOf(' ',space6+1)
  				var par7 = lines(x).substring(space6+1,space7)
  				var space8 = lines(x).indexOf(' ',space7+1)                           //take the prameters
  				var par8 = lines(x).substring(space7+1,space8)
  				var space9 = lines(x).indexOf(' ',space8+1)
  				var par9 = lines(x).substring(space8+1,space9)
  				var space10 = lines(x).indexOf(' ',space9+1)
  				var par10 = lines(x).substring(space9+1,space10)
  				var space11 = lines(x).indexOf(' ',space10+1)
  				var par11 = lines(x).substring(space10+1,space11)
  				var space12 = lines(x).indexOf(' ',space11+1)
  				var par12 = lines(x).substring(space11+1,space12)
  				var space13 = lines(x).indexOf(' ',space12+1)
  				var par13 = lines(x).substring(space12+1,space13)
  				var space14 = lines(x).indexOf(' ',space13+1)
  				var par14 = lines(x).substring(space13+1,space14)
  				var space15 = lines(x).indexOf(' ',space14+1)
  				var par15 = lines(x).substring(space14+1,space15)
  				var space16 = lines(x).indexOf(' ',space15+1)
  				var par16 = lines(x).substring(space15+1,space16)
  				var space17 = lines(x).indexOf(' ',space16+1)
  				var par17 = lines(x).substring(space16+1,space17)
  				var space18 = lines(x).indexOf(' ',space17+1)
  				var par18 = lines(x).substring(space17+1,space18)
  				var par19 = lines(x).substring(space18+1,lines(x).length)
  				
          if(par3.toInt<(Variables.CMDsize+par12.toInt))                               //check fot the BUS size
          {
          	println("ERROR! BUSsize too small on logic module: " + par1 )
          	error.error=error.error+1
          	error.errorST+= "ERROR! BUSsize too small on logic module: " + par1        
          }
          
          if(par15.toInt<log2Up(par14.toInt))                                             //check for the ADDR
          {
          	println("ERROR! ADDR too small on logic module: " + par1 )
          	error.error=error.error+1
          	error.errorST+= "ERROR! ADDR too small on logic module: " + par1 
          }
          
  				ModulesVector+=Module(new LogicCell(par1,par2.toInt,par3.toInt,par4.toInt,par5,par6.toInt,par7,par8,par9,par10,par11,par12.toInt,par13,par14.toInt,par15.toInt,par16,par17,par18,par19)).io     //build the module and push it in the vector

  				if(par3.toInt>MAXBUSLsize)
  				{
  					MAXBUSLsize = par3.toInt                     //find the max BUSsize for logic modules
  				}
  				
  			}
  			
  			if(vectorswitch==1)                        //memory vector
  			{
  				var space1 = lines(x).indexOf(' ',space+1)
  				var par1 = lines(x).substring(space+1,space1)
  				var space2 = lines(x).indexOf(' ',space1+1)
  				var par2 = lines(x).substring(space1+1,space2)
  				var space3 = lines(x).indexOf(' ',space2+1)
  				var par3 = lines(x).substring(space2+1,space3)
  				var space4 = lines(x).indexOf(' ',space3+1)
  				var par4 = lines(x).substring(space3+1,space4)
  				var space5 = lines(x).indexOf(' ',space4+1)
  				var par5 = lines(x).substring(space4+1,space5)
  				var space6 = lines(x).indexOf(' ',space5+1)
  				var par6 = lines(x).substring(space5+1,space6)
  				var space7 = lines(x).indexOf(' ',space6+1)
  				var par7 = lines(x).substring(space6+1,space7)
  				var space8 = lines(x).indexOf(' ',space7+1)                           //take the prameters
  				var par8 = lines(x).substring(space7+1,space8)
  				var space9 = lines(x).indexOf(' ',space8+1)
  				var par9 = lines(x).substring(space8+1,space9)
  				var space10 = lines(x).indexOf(' ',space9+1)
  				var par10 = lines(x).substring(space9+1,space10)
  				var space11 = lines(x).indexOf(' ',space10+1)
  				var par11 = lines(x).substring(space10+1,space11)
  				var space12 = lines(x).indexOf(' ',space11+1)
  				var par12 = lines(x).substring(space11+1,space12)
  				var space13 = lines(x).indexOf(' ',space12+1)
  				var par13 = lines(x).substring(space12+1,space13)
  				var space14 = lines(x).indexOf(' ',space13+1)
  				var par14 = lines(x).substring(space13+1,space14)
  				var space15 = lines(x).indexOf(' ',space14+1)
  				var par15 = lines(x).substring(space14+1,space15)
  				var space16 = lines(x).indexOf(' ',space15+1)
  				var par16 = lines(x).substring(space15+1,space16)
  				var space17 = lines(x).indexOf(' ',space16+1)
  				var par17 = lines(x).substring(space16+1,space17)
  				var space18 = lines(x).indexOf(' ',space17+1)
  				var par18 = lines(x).substring(space17+1,space18)
  				var par19 = lines(x).substring(space18+1,lines(x).length)

  				if(par2.toInt<(Variables.CMDsize+par12.toInt+1+par6.toInt))                               //check fot the BUS size
          {
          	println("ERROR! BUSsize too small on memory module: " + par1 )
          	error.error=error.error+1
          	error.errorST+= "ERROR! BUSsize too small on memory module: " + par1        
          }

          if(par2.toInt<(Variables.CMDsize+par3.toInt))                               //check fot the BUS size
          {
          	println("ERROR! BUSsize too small on memory module: " + par1 )
          	error.error=error.error+1
          	error.errorST+= "ERROR! BUSsize too small on memory module: " + par1        
          }

          if(par6.toInt<log2Up(par5.toInt))                                             //check for the ADDR
          {
          	println("ERROR! ADDR too small on memory module: " + par1 )
          	error.error=error.error+1
          	error.errorST+= "ERROR! ADDR too small on memory module: " + par1 
          }

          if(par15.toInt<log2Up(par14.toInt))                                             //check for the ADDR
          {
          	println("ERROR! ADDR1 too small on memory module: " + par1 )
          	error.error=error.error+1
          	error.errorST+= "ERROR! ADDR1 too small on memory module: " + par1 
          }
          
  				
  				MemoriesVector+=Module(new MemoryCell(par1,par2.toInt,par3.toInt,par4,par5.toInt,par6.toInt,par7,par8,par9, par10,par11,par12.toInt,par13,par14.toInt,par15.toInt,par16,par17,par18,par19)).io   //build the module and push it in the vector

  				if(par2.toInt>MAXBUSMsize)
  				{
  					MAXBUSMsize = par2.toInt                     //find the max BUSsize for memory modules
  				}

  				

  			}
  			if(vectorswitch==2)                        //crossbar vector
  			{

  				var space1 = lines(x).indexOf(' ',space+1)
  				var par1 = lines(x).substring(space+1,space1)
  				var space2 = lines(x).indexOf(' ',space1+1)
  				var par2 = lines(x).substring(space1+1,space2)
  				var space3 = lines(x).indexOf(' ',space2+1)
  				var par3 = lines(x).substring(space2+1,space3)
  				var space4 = lines(x).indexOf(' ',space3+1)
  				var par4 = lines(x).substring(space3+1,space4)
  				var space5 = lines(x).indexOf(' ',space4+1)
  				var par5 = lines(x).substring(space4+1,space5)
  				var space6 = lines(x).indexOf(' ',space5+1)
  				var par6 = lines(x).substring(space5+1,space6)
  				var space7 = lines(x).indexOf(' ',space6+1)
  				var par7 = lines(x).substring(space6+1,space7)
  				var space8 = lines(x).indexOf(' ',space7+1)                           //take the prameters
  				var par8 = lines(x).substring(space7+1,space8)
  				var space9 = lines(x).indexOf(' ',space8+1)
  				var par9 = lines(x).substring(space8+1,space9)
  				var space10 = lines(x).indexOf(' ',space9+1)
  				var par10 = lines(x).substring(space9+1,space10)
  				var space11 = lines(x).indexOf(' ',space10+1)
  				var par11 = lines(x).substring(space10+1,space11)
  				var space12 = lines(x).indexOf(' ',space11+1)
  				var par12 = lines(x).substring(space11+1,space12)
  				var space13 = lines(x).indexOf(' ',space12+1)
  				var par13 = lines(x).substring(space12+1,space13)
  				var space14 = lines(x).indexOf(' ',space13+1)
  				var par14 = lines(x).substring(space13+1,space14)
  				var space15 = lines(x).indexOf(' ',space14+1)
  				var par15 = lines(x).substring(space14+1,space15)
  				var space16 = lines(x).indexOf(' ',space15+1)
  				var par16 = lines(x).substring(space15+1,space16)
  				var space17 = lines(x).indexOf(' ',space16+1)
  				var par17 = lines(x).substring(space16+1,space17)
  				var par18 = lines(x).substring(space17+1,lines(x).length)
  				
          CrossBarVector+=Module(new CrossBarCell(par1,par2.toInt,par3.toInt,par4.toInt,par5.toInt,par6,par7,par8,par9, par10,par11.toInt,par12,par13.toInt,par14.toInt,par15,par16,par17,par18)).io   //build the module and push it in the vector
          OutputOfCrossBar+= par4.toInt              //memorize the number of output of each cross bar
          CrossBarDataSize+= par2.toInt              //memorize the data size of the crossbars
          InputOfCrossBar+= par3.toInt

          if(par5.toInt>MAXBUSCsize)
  				{
  					MAXBUSCsize = par5.toInt                     //find the max BUSsize for crossbar modules
  				}

  				
  			}

  			if(vectorswitch==0)
				{
						numberoflogic=numberoflogic+1                   //count number of logic modules
				}
  				
  				if(vectorswitch==1)
  				{
  					moduletype="Memory"                             //count number of memory modules
  					numberofmemory=numberofmemory+1
  				}
  				if(vectorswitch==2)
  				{
  					moduletype="Cross Bar"                         //count number of crossbar modules
  					numberofcrossbar=numberofcrossbar+1
  				}
  		}


  		
  			if(detail==true)
  			{
  				if(header)
  				{
  					wfile.println("---------------P2 v 1.0----------------")
  					wfile.println("<----" + before + "---->")                             //header of the detail file
  					wfile.println("---------------------------------------")
  				  wfile.println("Ferraro Luigi -> aloisius@berkeley.edu")
			      wfile.println("Rizzo Francesco -> rizzof87@berkeley.edu")
			      wfile.println("---------------------------------------")
			      wfile.println("")
			      wfile.println("configuration file number of lines: " + lines.length)
			      wfile.println("configuration file name: " + file)
			      wfile.println("")
  					header=false
  				}
  					wfile.println("number: " + modulenumber + " of " + lines(x).substring(space+1,lines(x).length) + " ---> " + moduletype + " builded in: " + optimizationWord + " mode" )        //write infos in the file

  					
  					if(x==lines.length-1)
  					{
  						val after = Calendar.getInstance().getTime()
  						wfile.println("") 
  						wfile.println("number of logic modules: " + numberoflogic) 
  						wfile.println("number of Memory modules: " + numberofmemory) 
  						wfile.println("number of Cross Bar modules: " + numberofcrossbar) 
  						wfile.println("number of OUTL BUS: " + (OutputOfCrossBar(1)-numberofmemory))
  						wfile.println("number of OUTM BUS: " + (OutputOfCrossBar(0)-(numberoflogic*2)))
  						wfile.println("MAX width of BUSL: " + MAXBUSLsize)
  						wfile.println("MAX width of BUSM: " + MAXBUSMsize)
  						wfile.println("MAX width of BUSC: " + MAXBUSCsize)
  						wfile.println("") 
  						wfile.println("-------------------------------------------------------------------------")
  						wfile.println("               TOTAL MODULES: " + (numberoflogic+numberofmemory+numberofcrossbar)) 			
							val dif = (new java.util.Date()).getTime() - before.getTime()
							if(dif>1000)
							{
								wfile.println("               TOTAL VECTORS BUILDING TIME: " + ((dif)/1000) + " seconds and " + (dif-(1000*((dif)/1000))) + " milliseconds") 
							}
							else
							{
								wfile.println("               TOTAL VECTOR BUILDING TIME: " + dif + " milliseconds")
							}
              wfile.println("") 
							wfile.println("               Ending time: " + after) 
					
							
  						wfile.close()     //close the file
  						println("Output file closed")
  					}
  			}
  		}
  	}
   }
   NumberOfCell = numberoflogic+numberofmemory+numberofcrossbar
   println("\033[32m\b| Loaded: " + (NumberOfCell) + " Modules\033[0m")
   println("")
   if(detail==true)
   	{
   		println("\033[32mInfo: \033[0m\"Detail.txt\" file generated.")
   	}
   
   	if(error.error==0)                              //check for errors
 		{
 			println("\033[32m       SUCCES!\033[0m")
 		}
 		else
 		{
 			println("Cleaning the buffer")                    //if errors detected delete the moules vectors
 			ModulesVector.clear
  	  MemoriesVector.clear
  	  CrossBarVector.clear
 		}
 		println("<---------------------------------------->")
   	println("")
    println("\033[32mPress enter to continue\033[0m")
 		
	  var read = readLine
	}
	
}