import Chisel._

class TestCrossBarStandard(NAME:String,DATAsize:Int,INnumber:Int,OUTnumber:Int,CONTROLsize:Int,PAR1:String,PAR2:String,PAR3:String,PAR4:String) extends Module 
{
	 val io = new Bundle
	 {
	 	 val IN  = Vec.fill(INnumber){Bits(INPUT,DATAsize)}
	 	 val OUT = Vec.fill(OUTnumber){Bits(OUTPUT,DATAsize)}
	 	 val CE = Bool(INPUT)
	 	 val CTR = Bits(INPUT,CONTROLsize)
	 }
	 val CrossBar = Module (new CrossBarLibrary(NAME,DATAsize,INnumber,OUTnumber,CONTROLsize,"0","0","0","0"))

	 for(x<- 0 until INnumber)
	 {
	 	 CrossBar.io.IN(x) := io.IN(x)
	 }
	 for(x<- 0 until OUTnumber)
	 {
	 	 io.OUT(x) := CrossBar.io.OUT(x)
	 }
	 CrossBar.io.CE := io.CE
	 CrossBar.io.CONTROL := io.CTR
}