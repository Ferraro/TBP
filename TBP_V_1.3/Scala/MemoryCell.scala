import Chisel._

class MemoryCell(NAME:String,BUSsize:Int,DATAsize:Int,TYPE:String,DEPTH:Int,ADDRsize:Int,M1:String,M2:String,M3:String,M4:String,NAME1:String,TMEsize1:Int,TYPE1:String,DEPTH1:Int,ADDRsize1:Int,M11:String,M21:String,M31:String,M41:String) extends Module {

	// Interface
	val io = new CellMemoryInterface(DATAsize, BUSsize)

	val DATAsize1 = (TMEsize1 + Variables.CMDsize + 1 + ADDRsize)

	// Modules Instatiating
	val Memory0 = Module(new MemoryLibrary(NAME,DATAsize,TYPE,DEPTH, ADDRsize, M1, M2, M3, M4))
	val CodeMemory = Module(new MemoryLibrary(NAME1, DATAsize1, TYPE1, DEPTH1, ADDRsize1, M11, M21, M31, M41))
	val Controller0 = Module(new ControllerMemoryLibrary("advanced", DATAsize, BUSsize, TMEsize1, ADDRsize, ADDRsize1))

	// Cell <-------> Controller0
	io.C <> Controller0.io.C
	io.CE <> Controller0.io.CE
	io.PE <> Controller0.io.PE
	io.START <> Controller0.io.START
	io.BUS <> Controller0.io.BUS	
	Controller0.io.END <> io.END

	// Controller0 <-------> Memory0
	Controller0.io.EN <> Memory0.io.EN 
	Controller0.io.WE <> Memory0.io.WE
	Controller0.io.ADDR <> Memory0.io.ADDR
	Controller0.io.DOUT <> Memory0.io.DIN  

	// Controller0 <-------> CodeMemory0
	Controller0.io.EN1 <> CodeMemory.io.EN 
	Controller0.io.WE1 <> CodeMemory.io.WE
	Controller0.io.ADDR1 <> CodeMemory.io.ADDR
	Controller0.io.DOUT1 <> CodeMemory.io.DIN
	CodeMemory.io.DOUT <> Controller0.io.DIN1
	
	// Cell <--------> Memory0
	Memory0.io.DOUT <> io.DOUT
}