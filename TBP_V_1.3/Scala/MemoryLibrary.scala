import Chisel._

class MemoryLibrary(NAME:String,DATAsize:Int,TYPE:String,DEPTH:Int,ADDRsize:Int,PAR1:String,PAR2:String,PAR3:String,PAR4:String) extends Module {

	val io = new MemoryInterface(DATAsize, ADDRsize)

	if(NAME=="DsbMemory") {
		val Mem0 = Mem(Bits(width = DATAsize),DEPTH, seqRead = true)
		io.DOUT := Bits(0)							// Default value for wire DOUT
	  
		when(reset){
			for(i <- 0 until DEPTH) {			// Reset Memory with 0
	  		Mem0(i) := Bits(0)
	  	}
		}
		.elsewhen(io.EN === Bits(1)){
			when(io.WE === Bits(0)){			// Read Mode
				io.DOUT := Mem0(io.ADDR)
			}
			.otherwise {									// WE = 1 Write Enable
				Mem0(io.ADDR) := io.DIN
			}
		}

	}
}