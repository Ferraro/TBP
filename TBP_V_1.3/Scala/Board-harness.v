module test;
  reg [0:0] io_START;
  reg [7:0] io_INL_17;
  reg [7:0] io_INL_16;
  reg [7:0] io_INL_15;
  reg [7:0] io_INL_14;
  reg [7:0] io_INL_13;
  reg [7:0] io_INL_12;
  reg [7:0] io_INL_11;
  reg [7:0] io_INL_10;
  reg [7:0] io_INL_9;
  reg [7:0] io_INL_8;
  reg [7:0] io_INL_7;
  reg [7:0] io_INL_6;
  reg [7:0] io_INL_5;
  reg [7:0] io_INL_4;
  reg [7:0] io_INL_3;
  reg [7:0] io_INL_2;
  reg [7:0] io_INL_1;
  reg [7:0] io_INL_0;
  reg [7:0] io_INM_0;
  wire [0:0] io_END;
  wire [0:0] io_ERR;
  wire [0:0] io_OVERFLOW;
  wire [0:0] io_UNDERFLOW;
  wire [7:0] io_OUTM_0;
  reg reset;
  reg clk = 1;
  parameter clk_length = 50;
  always #clk_length clk = ~clk;
  /*** DUT instantiation ***/
    Board
      Board(
        .clk(clk),
        .reset(reset),
        .io_START(io_START),
        .io_INL_17(io_INL_17),
        .io_INL_16(io_INL_16),
        .io_INL_15(io_INL_15),
        .io_INL_14(io_INL_14),
        .io_INL_13(io_INL_13),
        .io_INL_12(io_INL_12),
        .io_INL_11(io_INL_11),
        .io_INL_10(io_INL_10),
        .io_INL_9(io_INL_9),
        .io_INL_8(io_INL_8),
        .io_INL_7(io_INL_7),
        .io_INL_6(io_INL_6),
        .io_INL_5(io_INL_5),
        .io_INL_4(io_INL_4),
        .io_INL_3(io_INL_3),
        .io_INL_2(io_INL_2),
        .io_INL_1(io_INL_1),
        .io_INL_0(io_INL_0),
        .io_INM_0(io_INM_0),
        .io_END(io_END),
        .io_ERR(io_ERR),
        .io_OVERFLOW(io_OVERFLOW),
        .io_UNDERFLOW(io_UNDERFLOW),
        .io_OUTM_0(io_OUTM_0)
 );

  /*** VCD / VPD dumps ***/
  initial begin
  end

  parameter reset_period = clk_length * 4;
  initial begin
    reset = 1;
    #1
    
    io_START = 0;
    io_INM_0 = 0;
    io_INL_0 = 200;
    io_INL_1 = 230;
    io_INL_2 = 189;
    io_INL_3 = 57;
    io_INL_4 = 100;              //KERNEL 1
    io_INL_5 = 200;
    io_INL_6 = 46;
    io_INL_7 = 90;
    io_INL_8 = 147;
    
    io_INL_9 = 190;
    io_INL_10 = 200;
    io_INL_11 = 170;
    io_INL_12 = 52;
    io_INL_13= 94;             //KERNEL 2
    io_INL_14= 189;
    io_INL_15= 23;
    io_INL_16= 86;
    io_INL_17= 141;
    #reset_period;
    reset = 0;
    #100
    io_START = 1;                   //risultato deve essere 113
    #20200
    #15300
    io_INL_0 = 200;
    io_INL_1 = 230;
    io_INL_2 = 189;
    io_INL_3 = 57;
    io_INL_4 = 100;              //KERNEL 1
    io_INL_5 = 200;
    io_INL_6 = 46;
    io_INL_7 = 90;
    io_INL_8 = 147;
    
    io_INL_9 = 192;
    io_INL_10 = 200;
    io_INL_11 = 170;
    io_INL_12 = 52;
    io_INL_13= 94;             //KERNEL 2
    io_INL_14= 189;
    io_INL_15= 23;
    io_INL_16= 86;
    io_INL_17= 141;
    
  end

 
endmodule