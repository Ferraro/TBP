import Chisel._

class ControllerMemoryLibrary(NAME:String, DATAsize: Int, BUSsize: Int, TMEsize: Int, ADDRsize: Int, ADDRsize1: Int) extends Module
{
	val io = new ControllerMemoryInterface(DATAsize: Int, BUSsize: Int, TMEsize: Int, ADDRsize: Int, ADDRsize1: Int)
	
	if(NAME=="standard")
	{
		// Registers
		val WritePointerCM = Reg(init = Bits(0, ADDRsize1))															// Pointer for writing in CodeMemory0	
		val ReadPointer = Reg(init = Bits(0, ADDRsize1))																// Pointer for reading in CodeMemory0	
		val InitPointer = Reg(init = Bits(0, ADDRsize))																	// Pointer for writing in Memory0 during init
		val TimeCounter = Reg(init = Bits(0, TMEsize))																	// Count the clock ticks
		val LastTME = Reg(init = Bits(0, ADDRsize1))																		// Register to Save the address of the last TME

		val LastINIT = Reg(init = Bits(0, ADDRsize1))																		// Register to Save the address of the last DATA
		val WritePointerMM = Reg(init = Bits(0, ADDRsize))															// Pointer for writing in Main Memory

		val EndWord = Reg(init = Bits(65535, 16))

		// Flags
		val EndFlag = Reg(init = UInt(0))
		val PulseFlag = Reg(init = UInt(0))
		val ContinousFlag = Reg(init = UInt(0))

		// Default values for the output wires
		io.DOUT := Bits(0)
		io.EN := Bits(0)
		io.WE := Bits(0)
		io.ADDR := Bits(0)
		io.END := Bits(0)

		io.EN1 := Bits(0)
		io.WE1 := Bits(0)
		io.ADDR1 := Bits(0)
		io.DOUT1 := Bits(0)	

		// Connections
		io.DOUT1 := io.BUS																															// connect BUS to Code Memory
		io.END := (EndFlag | PulseFlag | ContinousFlag)
		
		//IN: C CE START PE BUS DIN1
		//OUT: DOUT EN WE ADDR END DOUT1 EN1 WE1 ADDR1

		when(io.CE === Bool(true)) 																							// When the Cell is ENABLED
		{				
			when(io.PE === Bool(true) && io.START === Bool(false)) 								// Programming or Init Mode -----------------------------
			{
				when(io.BUS(Variables.CMDsize-1,0) != UInt(10)) 										// Programming !!! <-------------
				{
					io.EN1 := Bool(true)																							// Enable the Code Memory
					io.WE1 := Bool(true)																							// Writing Mode in Code Memory
					io.DOUT1 := io.BUS
					io.ADDR1 := WritePointerCM																				
					WritePointerCM := WritePointerCM + Bits(1)												// Increment the Pointer to the Code Memory
					LastTME := WritePointerCM																					// Save the address of the last instruction in Code Mem
				}																																		// Finish Programming Mode
				.otherwise 																													// INIT !!! <--------------------
				{
					io.EN := Bool(true)																								// Enable the Main Memory
					io.WE := Bool(true)																								// Writing Mode in Main Memory
					io.DOUT := io.BUS(DATAsize+Variables.CMDsize-1,Variables.CMDsize)	// Put the DATA into DOUT (Main Memory)
					io.ADDR := WritePointerMM																				
					WritePointerMM := WritePointerMM + Bits(1)												// Increment the Pointer to the Main Memory
					LastINIT := WritePointerMM																				// Save the address of the last DATA
				}
			}
			.elsewhen((io.PE === Bool(false) && io.START === Bool(true))&& EndFlag === Bool(false)) 		// Runnning Mode ----------------------------------------
			{
				io.EN1 := Bool(true)																																			// Enable Code Memory
				io.WE1 := Bool(false)																																			// Reading Mode for Code Memory
				io.ADDR1 := ReadPointer																																		//
				io.DOUT := io.C																																						// Pass the Data to Main Mem
				
				when(io.DIN1(TMEsize+Variables.CMDsize-1,Variables.CMDsize) === EndWord(TMEsize-1,0)) 		// EndWord found ! ---------------
				{
					EndFlag := UInt(1)
					io.EN := Bool(false)
				}
				.elsewhen(LastTME === ReadPointer) 																												// Last Instruction Found ! ------
				{
					when(TimeCounter === io.DIN1(TMEsize+Variables.CMDsize-1,Variables.CMDsize))  					// Time MATCH (no STOP word)
					{
						io.EN := Bool(true)																																		// Enable Main Memory
						io.WE := io.DIN1(TMEsize+Variables.CMDsize)																						// Pass the CMD R/W
						io.ADDR := io.DIN1(TMEsize+Variables.CMDsize+ADDRsize,TMEsize+Variables.CMDsize+1)		// Pass the Address to Main Mem
						TimeCounter := UInt(0)																																// Reset Time Count
						EndFlag := Bool(true)																																	// End = 1
					}
					.otherwise 																																							// Time does NOT match
					{
						TimeCounter := TimeCounter + UInt(1)																									// Increment time count
						io.EN := Bool(false)																																	// Disable Main Memory
					}
				}
				.otherwise 																																								// Normal execution
				{	
					when(TimeCounter === io.DIN1(TMEsize+Variables.CMDsize-1,Variables.CMDsize))  					// Time MATCH (no Wordend no LastTME)
					{
						io.EN := Bool(true)																																		// Enable Main Memory
						io.WE := io.DIN1(TMEsize+Variables.CMDsize)																						// Pass the CMD R/W
						io.ADDR := io.DIN1(TMEsize+Variables.CMDsize+ADDRsize,TMEsize+Variables.CMDsize+1)		// Pass the address
						TimeCounter := UInt(0)																																// Reset the TimeCounter
						ReadPointer := ReadPointer + UInt(1)																									// Inc Code Mem Pointer
						EndFlag := Bool(false)																																// END = 0
					}
					.otherwise 																																							// Time does NOT match
					{
						TimeCounter := TimeCounter + UInt(1)																									// Increment time count
						io.EN := Bool(false)																																	// Disable Main Memory
						EndFlag := Bool(false)																																// END = 0
					}
				}
				when(io.DIN1(Variables.CMDsize-1,0)=== UInt(1)) 																					// CMD = Pulse End ---------> 1.
				{	
					when(TimeCounter != UInt(0))
					{
						PulseFlag := Bool(false)
					}
					.otherwise 
					{
						PulseFlag := Bool(true)
					}
				}
				.elsewhen(io.DIN1(Variables.CMDsize-1,0)=== UInt(2)) 																			// CMD = Continous End ----> 2.
				{	
					ContinousFlag := Bool(true)
				}
			}																																														// Finish Running Mode
			.elsewhen(io.PE === Bool(true) && io.START === Bool(true)) 					// NOP 01 ---------------------------------------------
			{	
				io.EN1 := Bool(false)
				io.WE1 := Bool(false)
				io.EN := Bool(false)
				io.WE := Bool(false)
			}
			.elsewhen(io.PE === Bool(false) && io.START === Bool(false)) 				// NOP 02 ---------------------------------------------
			{
				io.EN1 := Bool(false)
				io.WE1 := Bool(false)
				io.EN := Bool(false)
				io.WE := Bool(false)
			}	
		}																																			// Finish CE = 1
		.otherwise 																														// When the Cell is DISABLED
		{	
			io.EN1 := Bool(false)
			io.WE1 := Bool(false)
			io.EN := Bool(false)
			io.WE := Bool(false)
		}
	}																																				// End if Standard
	if(NAME=="advanced")
	{
		// Registers
		val WritePointerCM = Reg(init = Bits(0, ADDRsize1))															// Pointer for writing in CodeMemory0	
		val ReadPointer = Reg(init = Bits(0, ADDRsize1))																// Pointer for reading in CodeMemory0	
		val InitPointer = Reg(init = Bits(0, ADDRsize))																	// Pointer for writing in Memory0 during init
		val TimeCounter = Reg(init = Bits(0, TMEsize))																	// Count the clock ticks
		val LastTME = Reg(init = Bits(0, ADDRsize1))																		// Register to Save the address of the last TME
		val LoopCounter0 = Reg(init = UInt(0, TMEsize))																	// Count the number of loops n already done
		val LoopCounter1 = Reg(init = UInt(0, TMEsize))																	// Count the number of loops n already done
		val LastLoop = Reg(init = UInt(0,ADDRsize))																			// Keep track of the last loop found (address)
		
		val LastINIT = Reg(init = Bits(0, ADDRsize1))																		// Register to Save the address of the last DATA
		val WritePointerMM = Reg(init = Bits(0, ADDRsize))															// Pointer for writing in Main Memory

		val EndWord = Reg(init = Bits(65535, 16))

		// Flags
		val EndFlag = Reg(init = UInt(0))
		val PulseFlag = Reg(init = UInt(0))
		val ContinousFlag = Reg(init = UInt(0))

		// Default values for the output wires
		io.DOUT := Bits(0)
		io.EN := Bits(0)
		io.WE := Bits(0)
		io.ADDR := Bits(0)
		io.END := Bits(0)

		io.EN1 := Bits(0)
		io.WE1 := Bits(0)
		io.ADDR1 := Bits(0)
		io.DOUT1 := Bits(0)	

		// Connections
		io.DOUT1 := io.BUS																											// connect BUS to Code Memory
		io.END := (EndFlag | PulseFlag | ContinousFlag)
		
		//IN: C CE START PE BUS DIN1
		//OUT: DOUT EN WE ADDR END DOUT1 EN1 WE1 ADDR1

		when(io.CE === Bool(true))	 																						// When the Cell is ENABLED
		{
			when(io.PE === Bool(true) && io.START === Bool(false)) 								// Programming or Init Mode -----------------------------
				{
				when(io.BUS(Variables.CMDsize-1,0) != UInt(10)) 										// Programming !!! <-------------
				{
					io.EN1 := Bool(true)																							// Enable the Code Memory
					io.WE1 := Bool(true)																							// Writing Mode in Code Memory
					io.DOUT1 := io.BUS
					io.ADDR1 := WritePointerCM																				
					WritePointerCM := WritePointerCM + Bits(1)												// Increment the Pointer to the Code Memory
					LastTME := WritePointerCM																					// Save the address of the last instruction in Code Mem
				}																																		// Finish Programming Mode
				.otherwise																													// INIT !!! <--------------------
				{	
					io.EN := Bool(true)																								// Enable the Main Memory
					io.WE := Bool(true)																								// Writing Mode in Main Memory
					io.DOUT := io.BUS(DATAsize+Variables.CMDsize-1,Variables.CMDsize)	// Put the DATA into DOUT (Main Memory)
					io.ADDR := WritePointerMM																				
					WritePointerMM := WritePointerMM + Bits(1)												// Increment the Pointer to the Main Memory
					LastINIT := WritePointerMM																				// Save the address of the last DATA
				}
			}
			.elsewhen((io.PE === Bool(false) && io.START === Bool(true))&& EndFlag === Bool(false)) 		// Runnning Mode ----------------------------------------
			{	
				io.EN1 := Bool(true)																																			// Enable Code Memory
				io.WE1 := Bool(false)																																			// Reading Mode for Code Memory
				io.ADDR1 := ReadPointer																																		//
				io.DOUT := io.C																																						// Pass the Data to Main Mem
				
				when(io.DIN1(Variables.CMDsize-1,0) != UInt(3))																						// Loop type 3 NOT found		
				{
					when(io.DIN1(TMEsize+Variables.CMDsize-1,Variables.CMDsize) === EndWord(TMEsize-1,0)) 	// EndWord found ! ---------------
					{	
						EndFlag := UInt(1)
						io.EN := Bool(false)
					}
					.elsewhen(LastTME === ReadPointer) 																											// Last Instruction Found ! ------
					{	
						when(TimeCounter === io.DIN1(TMEsize+Variables.CMDsize-1,Variables.CMDsize))  				// Time MATCH (no STOP word)
						{
							io.EN := Bool(true)																																	// Enable Main Memory
							io.WE := io.DIN1(TMEsize+Variables.CMDsize)																					// Pass the CMD R/W
							io.ADDR := io.DIN1(TMEsize+Variables.CMDsize+ADDRsize,TMEsize+Variables.CMDsize+1)	// Pass the Address to Main Mem
							TimeCounter := UInt(0)																															// Reset Time Count
							EndFlag := Bool(true)																																// End = 1
						}
						.otherwise 																																						// Time does NOT match
						{
							TimeCounter := TimeCounter + UInt(1)																								// Increment time count
							io.EN := Bool(false)																																// Disable Main Memory
						}
					}
					.otherwise 																																							// Normal execution
					{	
						when(TimeCounter === io.DIN1(TMEsize+Variables.CMDsize-1,Variables.CMDsize))  				// Time MATCH (no Wordend no LastTME)
						{	
							io.EN := Bool(true)																																	// Enable Main Memory
							io.WE := io.DIN1(TMEsize+Variables.CMDsize)																					// Pass the CMD R/W
							io.ADDR := io.DIN1(TMEsize+Variables.CMDsize+ADDRsize,TMEsize+Variables.CMDsize+1)	// Pass the address
							TimeCounter := UInt(0)																															// Reset the TimeCounter
							ReadPointer := ReadPointer + UInt(1)																								// Inc Code Mem Pointer
							EndFlag := Bool(false)																															// END = 0
						}
						.otherwise	 																																						// Time does NOT match
						{	
							TimeCounter := TimeCounter + UInt(1)																									// Increment time count
							io.EN := Bool(false)																																	// Disable Main Memory
							EndFlag := Bool(false)																																// END = 0
						}
					}
				}
				
				when(io.DIN1(Variables.CMDsize-1,0)=== UInt(1)) 														// CMD = Pulse End --------------> 1.
				{	
					when(TimeCounter != UInt(0))
					{
						PulseFlag := Bool(false)
					}
					.otherwise 
					{
						PulseFlag := Bool(true)
					}
				}
				.elsewhen(io.DIN1(Variables.CMDsize-1,0)=== UInt(2)) 												// CMD = Continous End ----------> 2.
				{	
					ContinousFlag := Bool(true)
				}
				// -------------------------- LOOP COMMAND ---------------------------------------------------------------------
				.elsewhen(io.DIN1(Variables.CMDsize-1,0)=== UInt(3))												// CMD = Loop n times from x ----> 3.
				{																																						// open cmd 3
					when(LastTME === ReadPointer)																							// Loop is Last Instruction!
					{ 																																				// open last instruction
						when(io.DIN1((ADDRsize+Variables.CMDsize + TMEsize),(Variables.CMDsize + TMEsize)) >= LastLoop)	// x > LastLoop
						{	// choose LoopCounter0
							when(LoopCounter0 < io.DIN1(TMEsize+Variables.CMDsize-1,Variables.CMDsize))							// if loopCount < n
							{
								ReadPointer := io.DIN1((ADDRsize+Variables.CMDsize + TMEsize),(Variables.CMDsize + TMEsize))// ReadPointer = x
								TimeCounter := UInt(0)																																			// Reset TimeCounter
								LoopCounter0 := LoopCounter0 + UInt(1)																											// Increment LoopCounter
							}
							.otherwise
							{
								ReadPointer := ReadPointer + UInt(1)																						// incrementare ReadPointer
								TimeCounter := UInt(0)																													// Reset TimeCount				
								EndFlag := Bool(true)																														// EndFlag = 1
								LoopCounter0 := UInt(0)																													// Reset LoopCounter
								LastLoop := ReadPointer																													// Update address of last loop
							}
						}
						.otherwise
						{
							when(LoopCounter1 < io.DIN1(TMEsize+Variables.CMDsize-1,Variables.CMDsize))							// if loopCount < n
							{
								ReadPointer := io.DIN1((ADDRsize+Variables.CMDsize + TMEsize),(Variables.CMDsize + TMEsize))// ReadPointer = x
								TimeCounter := UInt(0)																																			// Reset TimeCounter
								LoopCounter1 := LoopCounter1 + UInt(1)																											// Increment LoopCounter
								LastLoop := UInt(0)
							}
							.otherwise
							{
								ReadPointer := ReadPointer + UInt(1)																						// incrementare ReadPointer
								TimeCounter := UInt(0)																													// Reset TimeCount				
								EndFlag := Bool(true)																														// EndFlag = 1
								LoopCounter1 := UInt(0)																													// Reset LoopCounter
/*toc*/								LastLoop := UInt(0)																													// Update address of last loop
							}
						}
					}																																											// close last instruction
					.otherwise
					{																																											// open NO last instruction
						when(io.DIN1((ADDRsize+Variables.CMDsize + TMEsize),(Variables.CMDsize + TMEsize)) >= LastLoop)	// x > LastLoop
						{																																													// choose LoopCounter0
							when(LoopCounter0 < io.DIN1(TMEsize+Variables.CMDsize-1,Variables.CMDsize))							// if loopCount < n
							{
								ReadPointer := io.DIN1((ADDRsize+Variables.CMDsize + TMEsize),(Variables.CMDsize + TMEsize))// ReadPointer = x
								TimeCounter := UInt(0)																																			// Reset TimeCounter
								LoopCounter0 := LoopCounter0 + UInt(1)																											// Increment LoopCounter
							}
							.otherwise
							{
								ReadPointer := ReadPointer + UInt(1)																							// incrementare ReadPointer
								TimeCounter := UInt(0)																														// Reset TimeCount				
								EndFlag := Bool(false)																														// EndFlag = 0
								LoopCounter0 := UInt(0)																														// Reset LoopCounter
								LastLoop := ReadPointer																														// Update address of last loop
							}
						}
						.otherwise
						{	// Choose LoopCounter1
							when(LoopCounter1 < io.DIN1(TMEsize+Variables.CMDsize-1,Variables.CMDsize))							// if loopCount < n
							{
								ReadPointer := io.DIN1((ADDRsize+Variables.CMDsize + TMEsize),(Variables.CMDsize + TMEsize))// ReadPointer = x
								TimeCounter := UInt(0)																																			// Reset TimeCounter
								LoopCounter1 := LoopCounter1 + UInt(1)																											// Increment LoopCounter
								LastLoop := UInt(0)
							}
							.otherwise
							{
								ReadPointer := ReadPointer + UInt(1)																						// incrementare ReadPointer
								TimeCounter := UInt(0)																													// Reset TimeCount				
								EndFlag := Bool(false)																													// EndFlag = 0
								LoopCounter1 := UInt(0)																													// Reset LoopCounter
/*toc*/								LastLoop := UInt(0)																													// Update address of last loop
							}
						}
					} 																																										// close NO last instruction	
				}																																												// close cmd 3
			// ----------------------------------------------------------------------------------------------------------------
			}																																													// Finish Running Mode
			.elsewhen(io.PE === Bool(true) && io.START === Bool(true)) 					// NOP 01 ---------------------------------------------
			{	
				io.EN1 := Bool(false)
				io.WE1 := Bool(false)
				io.EN := Bool(false)
				io.WE := Bool(false)
			}
			.elsewhen(io.PE === Bool(false) && io.START === Bool(false)) 				// NOP 02 ---------------------------------------------
			{	
				io.EN1 := Bool(false)
				io.WE1 := Bool(false)
				io.EN := Bool(false)
				io.WE := Bool(false)
			}	
		}																																			// Finish CE = 1
		.otherwise																														// When the Cell is DISABLED
		{	
			io.EN1 := Bool(false)
			io.WE1 := Bool(false)
			io.EN := Bool(false)
			io.WE := Bool(false)
		}
	}																																				// End if Standard
}																																					// End Module