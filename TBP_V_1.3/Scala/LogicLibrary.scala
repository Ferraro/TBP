import Chisel._

class LogicLibrary(OP:String,DATAsize:Int,DATAtype:String,LATENCY:Int,PAR1:String,PAR2:String,PAR3:String,PAR4:String) extends Module {

	val io = new LogicInterface(DATAsize)

	val OUT = Reg(init = Bits(0,DATAsize))

	if(OP=="SimplyAdd") 
	{
		io.RDY := Bool(false)
		when(io.CE === Bool(true)) 
		{
			OUT := io.A + io.B
			io.RDY := Bool(true)
		}
		io.UNDERFLOW := UInt(0)
		io.OVERFLOW := UInt(0)
		io.C := OUT
	}

	if(OP=="SimplySub") 
	{
		io.RDY:=Bool(false)
		when(io.CE===Bool(true)) 
		{
			OUT:=io.A-io.B
			io.RDY:=Bool(true)
		}
		io.UNDERFLOW:=UInt(0)
		io.OVERFLOW:=UInt(0)
		io.C:=OUT
	}

	if(OP=="SimplyMul") 
	{
		io.RDY:=Bool(false)
		when(io.CE===Bool(true)) 
		{
			OUT:=io.A*io.B
			io.RDY:=Bool(true)
		}
		io.UNDERFLOW:=UInt(0)
		io.OVERFLOW:=UInt(0)
		io.C:=OUT
	}

	if(OP=="SimplyDiv") 
	{
		io.RDY:=Bool(false)
		when(io.CE===Bool(true)) 
		{
			OUT:=io.A/io.B
			io.RDY:=Bool(true)
		}
		io.UNDERFLOW:=UInt(0)
		io.OVERFLOW:=UInt(0)
		io.C:=OUT
	}

	if(OP=="SimplyComp")
	{
		io.RDY:=Bool(false)
		when(io.CE===Bool(true)) 
		{
			when(io.A>io.B)
			{
				OUT:=UInt(0)
			}
			when(io.A<io.B)
			{
				OUT:=UInt(1)
			}
			when(io.A===io.B)
			{
				OUT:=UInt(2)
			}
			io.RDY:=Bool(true)
		}
		io.UNDERFLOW:=UInt(0)
		io.OVERFLOW:=UInt(0)
		io.C:=OUT
	}

  if(OP=="<=")
	{
		io.RDY:=Bool(false)
		when(io.CE===Bool(true)) 
		{
			when(io.A>io.B)
			{
				OUT:=io.B
			}
			when(io.A<io.B)
			{
				OUT:=io.A
			}
			when(io.A===io.B)
			{
				OUT:=io.A
			}
			io.RDY:=Bool(true)
		}
		io.UNDERFLOW:=UInt(0)
		io.OVERFLOW:=UInt(0)
		io.C:=OUT
	}
	
	
	if(OP=="Add" || OP=="Sqt" || OP=="Sub" || OP=="Mul" || OP=="Div") {
			class LogicBbox(DATAsize: Int) extends BlackBox {
				val io = new Bundle {
				val A = Bits(INPUT, DATAsize)
	  		val B = Bits(INPUT, DATAsize)
	  		val C = Bits(OUTPUT, DATAsize)
	  		val CE = Bits(INPUT, 1)
	  		val SCLR = Bits(INPUT, 1)
	  		val RDY = Bits(OUTPUT, 1)
	 			val OVERFLOW = Bits(OUTPUT, 1)
	 			val UNDERFLOW = Bits(OUTPUT, 1)
			}
				addClock(Driver.implicitClock)
				//addResetPin(Driver.implicitReset)
			}
				
			val Logic = Module(new LogicBbox(DATAsize))
			Logic.moduleName=	"W" + OP + "_" + DATAsize + "_" + LATENCY + "_1_" + DATAtype

			Logic.io.A <> io.A
			Logic.io.B <> io.B
			io.C := Logic.io.C
			Logic.io.CE <> io.CE
			Logic.io.RDY <> io.RDY
			Logic.io.UNDERFLOW <> io.UNDERFLOW
			Logic.io.OVERFLOW <> io.OVERFLOW
			Logic.io.SCLR <> reset		
	}
}