/*
08/05/2014      

Counter
*/

import Chisel._


class Counter(n: Int) extends Module {


val io = new Bundle {

val cen = Bits(INPUT, 1)

val addr = Bits(OUTPUT, log2Up(n))

}


val c = Reg(init = Bits(0, log2Up(n)))


when(io.cen === Bits(1)){

c := c + Bits(1)

}


io.addr := c

}
