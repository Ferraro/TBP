import Chisel._
import scala.collection.mutable.ArrayBuffer
import scala.io.Source
import scala.Console
import java.io.PrintWriter
import java.util.Calendar
import java.text.SimpleDateFormat
import java.util.Date

class Programmer(CELLnumber:Int,LOGICnumber:Int,MEMORYnumber:Int,BUSLsize:Int,BUSMsize:Int,BUSCsize:Int) extends Module 
{
  val io = new Bundle
  {
    val CEOUT = Bits(OUTPUT,1)
	  val START = Bits(OUTPUT,1)
	  val PE  = Vec.fill(CELLnumber){Bits(OUTPUT,1)}
	  val BUSL = Bits(OUTPUT,BUSLsize)
    val BUSM = Bits(OUTPUT,BUSMsize)
    val BUSC = Bits(OUTPUT,BUSCsize)
    val PROGRAM = Bits(INPUT,1)	
  }

  io.PE:=Bits(0)
  io.CEOUT:=Bool(true)
  io.START:=Bits(0)
  io.BUSL:=Bits(0)
  io.BUSM:=Bits(0)
  io.BUSC:=Bits(0)
  
  print("\033[33mInsert Program file name or digit 'info': \033[0m ")
  var file = readLine
  if(file == "info")
  {
  	println(" ")
  	println("Total Logic Cell: " + LOGICnumber + " with programmation bus size: " + BUSLsize)
  	println("Total Memory Cell: " + MEMORYnumber + " with programmation bus size: " + BUSMsize)
  	println("Total CrossBar: 2 with programmation bus size: " + BUSCsize)
  	println("TOTAL CELL NUMBER: " + CELLnumber)
  	println("Do not use the 'Tab' char in the program file")
  	println("To comment use the char '/' at the beginning of the line")
  	print("\033[33mInsert Program file name: \033[0m ")
    file = readLine
  }

  /////////////
  //file="program.p2"
  ////////////

  var XL=0
  var XM=0
  var XC=0
  val ST = "b"
  var NumberOfCell = UInt(0,log2Up(Variables.PROGRAMMABLECELLnumber))                              //maximum implementable number of CELL is defined in Variables.PROGRAMMABLECELLnumber
  var caseswitch = 0
   
  val source = Source.fromFile(file)
  val lines = source.getLines.toArray

  if(lines.isEmpty)                                                     //check for empty file
  {
  	println("\033[33mWARNING!\033[0m Program file empty!")
  	error.warning=error.warning+1
  	error.warningST+="\033[33mWARNING!\033[0m Program file empty!"
  	var pause = readLine
  }

  //------------- READING FROM FILE ---------------

 
  val MemoryL = Mem(Bits(width=BUSLsize+log2Up(Variables.PROGRAMMABLECELLnumber)), lines.length)                   //distribuited internal memory
  val MemoryM = Mem(Bits(width=BUSMsize+log2Up(Variables.PROGRAMMABLECELLnumber)), lines.length)                  //distribuited internal memory
  val MemoryC = Mem(Bits(width=BUSCsize+log2Up(Variables.PROGRAMMABLECELLnumber)), lines.length)                     //distribuited internal memory

  println("Start writing program inside programmer memory")
  println("")
  
  for(x<-0 until lines.length)
  {
  	  if(lines(x)=="-logic" || lines(x)=="-LOGIC" || lines(x)=="-Logic")
  		{
  			caseswitch = 1
  		}
  		if(lines(x)=="-memory" || lines(x)=="-MEMORY" || lines(x)=="-Memory")
  		{
  			caseswitch = 2
  		}
  		if(lines(x)=="-crossbar" || lines(x)=="-CROSSBAR" || lines(x)=="-CrossBar")
  		{
  			caseswitch = 3
  		}
  	
  	if(lines(x).isEmpty==false && lines(x)(0)!='/' && lines(x)(0)!='-')                  //check for empty lines , comment or change of cells
    {
        var space = lines(x).lastIndexOf(' ')
        var numbersize = lines(x).indexOf(" ")
  			var NUMBER = lines(x).substring(0,numbersize).toInt

  			if(NUMBER>=Variables.PROGRAMMABLECELLnumber)
  			{
  				error.error=error.error+1
  				error.errorST+="ERROR! too much celles, reduce the number of cells or modify the global variable 'PROGRAMMABLECELLnumber'"
  				println("ERROR! too much celles, reduce the number of cells or modify the global variable 'PROGRAMMABLECELLnumber'")
  			}


  		
    
    if(error.error==0)                                        //check for errors
    {
  			if(caseswitch==1)                                                                              //LOGIC CELL
  			{
  				print(".")
  				if(lines(x).substring(space+1,lines(x).length).length<=BUSLsize)                                 //check if the string is not too long
  				{
  					var string = lines(x).substring(space+1,lines(x).length)                                   //program line                          
  					val CellNumber = UInt(NUMBER,log2Up(Variables.PROGRAMMABLECELLnumber))
  					val COMMAND = Cat(CellNumber,Bits('b'+string))                                             //create string CELLnumber + PRORAMline
  					MemoryL(XL) := COMMAND                                                                          //PROGRAMMING THE FIRST MEMORY
  					XL=XL+1                                                                                          //update cell number
  				}
  				else
  				{
  					println("\033[33mWARNING!\033[0m Program line number " + (x+1) + " in file: " + file + " too long, the length should be: " + BUSLsize + " find: \033[33m" + lines(x).substring(space+1,lines(x).length) + "\033[0m , will be truncated!")
  					error.warning=error.warning+1
  					error.warningST+="\033[33mWARNING!\033[0m Program line number " + (x+1) + " in file: " + file + " too long, the length should be: " + BUSLsize.toString + " find: \033[33m" + lines(x).substring(space+1,lines(x).length).length.toString + "\033[0m , will be truncated!"
  				}
  			}



  			
  			if(caseswitch==2)                                         //MEMORY CELL
  			{
  				print(".")

  				
  				if(lines(x).substring(space+1,lines(x).length).length<=BUSMsize)                                 //check if the string is not too long
  				{
  					var string = lines(x).substring(space+1,lines(x).length)                                   //program line                          
  					val CellNumber = UInt(NUMBER,log2Up(Variables.PROGRAMMABLECELLnumber))
  					val COMMAND = Cat(CellNumber,Bits('b'+string))                                             //create string CELLnumber + PRORAMline
  					MemoryM(XM) := COMMAND                                                                          //PROGRAMMING THE SECOND MEMORY
  					XM=XM+1                                                                                          //update cell number
  				}
  				else
  				{
  					print("\033[33mWARNING!\033[0m Program line number " + (x+1) + " in file: " + file +  " too long, the length should be: " + BUSMsize + " find: \033[33m" + lines(x).substring(space+1,lines(x).length) + "\033[0m , will be truncated!")
  					error.warning=error.warning+1
  					error.warningST+="\033[33mWARNING!\033[0m Program line number " + (x+1) + " in file: " + file +  " too long, the length should be: " + BUSMsize.toString + " find: \033[33m" + lines(x).substring(space+1,lines(x).length).length.toString + "\033[0m , will be truncated!"
  				}
  			}


  			if(caseswitch==3)                           //CROSSBAR CELL
  			{
  				print(".")
  				if(lines(x).substring(space+1,lines(x).length).length<=BUSCsize)                                 //check if the string is too long
  				{
  					var string = lines(x).substring(space+1,lines(x).length)                                   //program line                         
  					val CellNumber = UInt(NUMBER,log2Up(Variables.PROGRAMMABLECELLnumber))
  					val COMMAND = Cat(CellNumber,Bits('b'+string))                                             //create string CELLnumber + PRORAMline
  					MemoryC(XC) := COMMAND                                                                          //PROGRAMMING THE THIRD MEMORY
  					XC=XC+1                                                                                           //update cell number
  				} 
  				else
  				{
  					println("\033[33mWARNING!\033[0m Program line number " + (x+1) + " in file: " + file +  " too long, the length should be: " + BUSCsize + " find: \033[33m" + lines(x).substring(space+1,lines(x).length) + "\033[0m , will be truncated!")
  					error.warning=error.warning+1
  					error.warningST+="\033[33mWARNING!\033[0m Program line number " + (x+1) + " in file: " + file +  " too long, the length should be: " + BUSCsize.toString + " find: \033[33m" + lines(x).substring(space+1,lines(x).length).length.toString + "\033[0m , will be truncated!"
  				}
  			}
      }   
    }
  } 

     //-------------------- BUILDING THE DECODERS ---------------------------//

     println("")
     println("")
     println("Building the decoders:")
     println("")
     
     val DecoderL = Module (new Decoder(LOGICnumber))
     val DecoderM = Module (new Decoder(MEMORYnumber))
     val DecoderC = Module (new Decoder(2))

     for(x<-0 until LOGICnumber)
     {
     	io.PE(x):=DecoderL.io.OUT(x)
     	println("PE " + x + " -> LogicCell " + (x+1) )
     }
     for(x<-0 until MEMORYnumber)
     {
     	io.PE(x+LOGICnumber):=DecoderM.io.OUT(x)
     	println("PE " + (x+LOGICnumber) + " -> MemoryCell " + (x+1))
     }
     for(x<-0 until 2)
     {
     	io.PE(x+LOGICnumber+MEMORYnumber):=DecoderC.io.OUT(x)
     	println("PE " + (x+LOGICnumber+MEMORYnumber) + " -> CrossBar " + (x+1))
     }

     //-------------------- STARTING PROGRAMMI P2 ---------------------------//
     //----------- 3 PARALLEL MEMORY WRITE ON THE BUSL BUSM BUSC ------------//

     val FinishL = Reg(init = Bool(false))
     val FinishM = Reg(init = Bool(false))
     val FinishC = Reg(init = Bool(false))
     val COUNTL = Reg(init = UInt(0,100))
     val COUNTM = Reg(init = UInt(0,100))                            //// AGGIUSTARE IL 100 E STATO MESSO PER TEST DEVE CALCOLARSELO IN BASE ALLE LINEE DI CODICE ANCHE SU QUANDO INIZIALIZZO LA MEMORIA STESSA COSA
     val COUNTC = Reg(init = UInt(0,100))
     val DecoderADDL = Reg(init = UInt(0,log2Up(Variables.PROGRAMMABLECELLnumber)))
     val DecoderADDM = Reg(init = UInt(0,log2Up(Variables.PROGRAMMABLECELLnumber)))
     val DecoderADDC = Reg(init = UInt(0,log2Up(Variables.PROGRAMMABLECELLnumber)))

     val regOUTBUSL = Reg(init = UInt(0,BUSLsize))
     val regOUTBUSM = Reg(init = UInt(0,BUSMsize))
     val regOUTBUSC = Reg(init = UInt(0,BUSCsize))

     val DecoderLCE = Reg(init = Bool(false))
     val DecoderMCE = Reg(init = Bool(false))
     val DecoderCCE = Reg(init = Bool(false))

     io.BUSL:=regOUTBUSL
     io.BUSM:=regOUTBUSM
     io.BUSC:=regOUTBUSC


     DecoderL.io.IN:=DecoderADDL
     DecoderM.io.IN:=DecoderADDM
     DecoderC.io.IN:=DecoderADDC

     DecoderL.io.CE:=DecoderLCE
     DecoderM.io.CE:=DecoderMCE
     DecoderC.io.CE:=DecoderCCE
     
     println("")
     println("Generating hardware:")
     println("")

 //IMPULSI ALLA FINE
 // I BUS SEMBRA CHE RIMANGANO A ZERO TRANNE QUELL ODELLE LOGICHE
    
     
   when(io.PROGRAM===Bool(true))
   {
     when(COUNTL< UInt(XL))
     {
     	 regOUTBUSL:=MemoryL(COUNTL)(BUSLsize-1,0)
     	 DecoderADDL:=MemoryL(COUNTL)(BUSLsize+log2Up(Variables.PROGRAMMABLECELLnumber)-1,BUSLsize)
     	 COUNTL:=COUNTL+UInt(1)                                                                           // 1 memory
    	 DecoderLCE:=Bool(true)
     }
     when(COUNTL>= UInt(XL))
     {
     	 DecoderLCE:=Bool(false)
     	 FinishL := Bool(true)
     }




   when(COUNTM< UInt(XM))
     {
     	 regOUTBUSM:=MemoryM(COUNTM)(BUSMsize-1,0)
     	 DecoderADDM:=MemoryM(COUNTM)(BUSMsize+log2Up(Variables.PROGRAMMABLECELLnumber)-1,BUSMsize)
     	 COUNTM:=COUNTM+UInt(1) 
     	 DecoderMCE:=Bool(true)                                                         // 2 memory
     }
     when(COUNTM >= UInt(XM))
     {
     	 FinishM := Bool(true)
     	 DecoderMCE:=Bool(false)
     }





     when(COUNTC< UInt(XC))
     {
     	 regOUTBUSC:=MemoryC(COUNTC)(BUSCsize-1,0)
     	 DecoderADDC:=MemoryC(COUNTC)(BUSCsize+log2Up(Variables.PROGRAMMABLECELLnumber)-1,BUSCsize)
     	 COUNTC:=COUNTC+UInt(1)                                                                       // 3 CROSSBAR
       DecoderCCE:=Bool(true)
     }
     when(COUNTC >= UInt(XC))
     {
     	 FinishC := Bool(true)
     	 DecoderCCE:=Bool(false)
     }


     
   }
     
     io.START := FinishL & FinishM & FinishC

  	 
 		 var pause = readLine
}