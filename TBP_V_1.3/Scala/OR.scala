import Chisel._
class OR(n:Int) extends Module
{
	val io = new Bundle
	{
	  val A  = Bits(INPUT,1)
	  val B  = Bits(INPUT,1)
	  val OUT = Bits(OUTPUT,1)
	}
io.OUT:=io.A|io.B

}