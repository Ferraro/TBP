import Chisel._
class ORn(n:Int) extends Module
{
	val io = new ORInterface(n)
  val ORarray =  Vec.fill(n-1){ Module(new OR(n)).io }
  
  ORarray(0).A:=io.IN(0)
  ORarray(0).B:=io.IN(1)
  
	for(x<-0 until n-2)
	{
		ORarray(x+1).B:=ORarray(x).OUT
		ORarray(x+1).A:=io.IN(x+2)
	}
	io.OUT:=ORarray(n-2).OUT
}