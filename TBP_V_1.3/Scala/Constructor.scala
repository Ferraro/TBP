import Chisel._


object CMain 
{
	def main(args: Array[String]): Unit =
	{
		var exit = true
		while(exit)
		{
			println("\033[32mPress enter to continue\033[0m")                //printing menu and logo
			val y = readLine
			print("\033[2J")
			error.logo()
			println("<----------------------------->")
			println("\033[32m   \033[4mParameterizable Processor\033[0m")
			println("Ferraro Luigi -> \033[34maloisius@berkeley.edu\033[0m")
			println("Rizzo Francesco -> \033[34mrizzof87@berkeley.edu\033[0m")
			println("<----------------------------->")
			print("File configuration name:")
			val FILEname = readLine


			chiselMain(args,()=> Module (new Board(FILEname)))

		
			 
      print("\033[2J")
      error.logo()
      if(error.error != 0 || error.warning !=0)
      {
      	println("")
				println("FINISH with: \033[33m" + error.warning +" WARNINGS\033[0m    and     \033[31m" + error.error + " ERRORS\033[0m")
				println("")
				error.print()          //print the errors
				error.clean()          //clear the errors
				println("")
      }
      else
      {
      	println("\033[32mFINISH\033[0m")
      }
		}             
	}
}