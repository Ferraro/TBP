import Chisel._

class ControllerMemory(DATAsize: Int, BUSsize: Int, TMEsize: Int, ADDRsize: Int, ADDRsize1: Int) extends Module {

	// Interface
	val io = new ControllerMemoryInterface(DATAsize: Int, BUSsize: Int, TMEsize: Int, ADDRsize: Int, ADDRsize1: Int)

	// Registers
	val WritePointer = Reg(init = Bits(0, ADDRsize1))																		// Pointer for writing in CodeMemory0	
	val ReadPointer = Reg(init = Bits(0, ADDRsize1))																		// Pointer for reading in CodeMemory0	
	val InitPointer = Reg(init = Bits(0, ADDRsize))																			// Pointer for writing in Memory0 during init
	val TimeCounter = Reg(init = Bits(0, TMEsize))																			// Count the clock ticks
	val LastCmd = Reg(init = Bits(0, ADDRsize1))																				// Register to Save the address of the last cmd
	
	// Reset
	

	// Default values for the output wires
	io.DOUT := Bits(0)
	io.EN := Bits(0)
	io.WE := Bits(0)
	io.ADDR := Bits(0)

	io.END := Bits(0)

	io.EN1 := Bits(0)
	io.WE1 := Bits(0)
	io.ADDR1 := Bits(0)
	io.DOUT1 := Bits(0)	

	val DATAsize1 = (TMEsize + Variables.CMDsize + 1 + ADDRsize)

	// combination: 0 0 0 ---> NOTHING
	when(io.CE === Bool(false) && io.START === Bool(false) && io.PE === Bool(false)) {
		io.EN := Bits(0)																											// Disable Memory0
		io.WE := Bits(0)																											// Writing Mode OFF of Memory0
		io.ADDR := Bits(0)
		
		io.EN1 := Bits(0)																											// Disable CodeMemory0
		io.WE1 := Bits(0)																											// Writing Mode OFF of CodeMemory0
		io.ADDR1 := Bits(0)
		WritePointer := Bits(0)
		LastCmd := Bits(0)
	}

	// combination: 0 0 1 ---> NOTHING
	when(io.CE === Bool(false) && io.START === Bool(false) && io.PE === Bool(true)) {
		io.EN := Bits(0)																											// Disable Memory0
		io.WE := Bits(0)																											// Writing Mode OFF of Memory0
		io.ADDR := Bits(0)
		
		io.EN1 := Bits(0)																											// Disable CodeMemory0
		io.WE1 := Bits(0)																											// Writing Mode OFF of CodeMemory0
		io.ADDR1 := Bits(0)
		WritePointer := Bits(0)
		LastCmd := Bits(0)
	}

	// combination: 0 1 0 ---> NOTHING
	when(io.CE === Bool(false) && io.START === Bool(true) && io.PE === Bool(false)) {
		io.EN := Bits(0)																											// Disable Memory0
		io.WE := Bits(0)																											// Writing Mode OFF of Memory0
		io.ADDR := Bits(0)
		
		io.EN1 := Bits(0)																											// Disable CodeMemory0
		io.WE1 := Bits(0)																											// Writing Mode OFF of CodeMemory0
		io.ADDR1 := Bits(0)
		WritePointer := Bits(0)
		LastCmd := Bits(0)
	}

	// combination: 0 1 1 ---> NOTHING
	when(io.CE === Bool(false) && io.START === Bool(true) && io.PE === Bool(true)) {
		io.EN := Bits(0)																											// Disable Memory0
		io.WE := Bits(0)																											// Writing Mode OFF of Memory0
		io.ADDR := Bits(0)
		
		io.EN1 := Bits(0)																											// Disable CodeMemory0
		io.WE1 := Bits(0)																											// Writing Mode OFF of CodeMemory0
		io.ADDR1 := Bits(0)
		WritePointer := Bits(0)
		LastCmd := Bits(0)
	}

	// combination: 1 0 0 ---> NOTHING
	when(io.CE === Bool(true) && io.START === Bool(false) && io.PE === Bool(false)) {
		io.EN := Bits(0)																											// Disable Memory0
		io.WE := Bits(0)																											// Writing Mode OFF of Memory0
		io.ADDR := Bits(0)
		
		io.EN1 := Bits(0)																											// Disable CodeMemory0
		io.WE1 := Bits(0)																											// Writing Mode OFF of CodeMemory0
		io.ADDR1 := Bits(0)
		WritePointer := Bits(0)
		LastCmd := Bits(0)
	}

	// combination: 1 0 1 ---> PROGRAMMATION
	when(io.CE === Bool(true) && io.START === Bool(false) && io.PE === Bool(true)) {
		io.EN1 := Bits(1)																											// Enable Code Memory
		io.WE1 := Bits(1)																											// Writing Mode
		io.ADDR1 := WritePointer
		io.DOUT1 := io.BUS
		WritePointer := WritePointer + Bits(1)
		LastCmd := WritePointer																								// Insert the addr of the last command in LastCmd 
	}

	// combination: 1 1 0 ---> EXECUTION
	when(io.CE === Bool(true) && io.START === Bool(true) && io.PE === Bool(false)) {
		io.DOUT := io.C
		io.EN1 := Bits(1)																											// Enable Code Memory
		io.WE1 := Bits(0)																											// Reading Mode
		io.ADDR1 := ReadPointer
		when(ReadPointer != LastCmd){						
			when(TimeCounter === io.DOUT(DATAsize1 - 1 - Variables.CMDsize, (ADDRsize + 1))) {
				when(io.DIN1(ADDRsize) === Bits(1)) {															// if R/W = 1 -> Write
					io.WE := Bits(1)																								// Writing Mode
					io.EN := Bits(1)																								// Enable the Memory
					io.ADDR := io.DIN1((ADDRsize - 1), 0)
				}
				.otherwise {																											// if R/W = 0 -> Read
					io.WE := Bits(0)																								// Reading Mode
					io.EN := Bits(1)																								// Enable the Memory
					io.ADDR := io.DOUT((ADDRsize - 1), 0)
				}	
				TimeCounter := Bits(0)
				ReadPointer := ReadPointer + Bits(1)
			}	
			.otherwise { 
				TimeCounter := TimeCounter + Bits(1)
				io.EN := Bits(0)
			}
		}
		.otherwise {
			WritePointer := Bits(0)
			io.EN := Bits(0)
			io.EN1 := Bits(0)
			io.END := Bits(1)
		}
	}

	// combination: 1 1 1 ---> INIT
	when(io.CE === Bool(true) && io.START === Bool(true) && io.PE === Bool(true)) {
		io.DOUT := io.BUS(DATAsize - 1, 0)
		io.EN := Bits(1)																											// Enable Memory0
		io.WE := Bits(1)																											// Writing Mode ON of Memory0
		io.ADDR := InitPointer
		InitPointer := InitPointer + Bits(1)
		
		io.EN1 := Bits(0)																											// Disable CodeMemory0
		io.WE1 := Bits(0)																											// Writing Mode OFF of CodeMemory0
		io.ADDR1 := Bits(0)
	}
}																																					// end Module