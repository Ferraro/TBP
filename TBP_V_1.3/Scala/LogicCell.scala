import Chisel._

class LogicCell(OP:String,PS: Int,BUSsize:Int,DATAsize:Int,DATAtype:String,LATENCY:Int,L1:String,L2:String,L3:String,L4:String,NAME:String,TMEsize:Int,TYPE:String,DEPTH:Int,ADDRsize: Int, M1:String, M2:String, M3:String, M4:String) extends Module {

	// Interface
	val io = new CellLogicInterface(DATAsize, BUSsize)

	val DATAsize1 = TMEsize + Variables.CMDsize

	// Modules Instantiating
	val Logic0 = Module(new LogicLibrary(OP,DATAsize,DATAtype,LATENCY,L1,L2,L3,L4))
	val CodeMemory0 = Module(new MemoryLibrary(NAME, DATAsize1, TYPE, DEPTH, ADDRsize, M1, M2, M3, M4))
	val Controller0 = Module(new ControllerLogicLibrary("standard", BUSsize, DATAsize, TMEsize, ADDRsize, PS, LATENCY))

	// Cell <-----> Controller0
	Controller0.io.A := io.A
	Controller0.io.B := io.B
	Controller0.io.CE := io.CE
	Controller0.io.PE := io.PE
	Controller0.io.START := io.START
	Controller0.io.BUS := io.BUS
	io.END := Controller0.io.END	

	// Controller <------> Logic0
	Logic0.io.A := Controller0.io.DOUTA
	Logic0.io.B := Controller0.io.DOUTB
	Logic0.io.CE := Controller0.io.CEL
	
	// Controller0 <------> CodeMemory0
	CodeMemory0.io.EN := Controller0.io.EN
	CodeMemory0.io.WE := Controller0.io.WE
	CodeMemory0.io.ADDR := Controller0.io.ADDR
	Controller0.io.DIN1 := CodeMemory0.io.DOUT
	CodeMemory0.io.DIN := Controller0.io.DOUT1

	// Cell <------> Logic0
	io.OVERFLOW := Logic0.io.OVERFLOW
	io.UNDERFLOW := Logic0.io.UNDERFLOW
	io.C := Logic0.io.C
	io.RDY := Logic0.io.RDY
}