import Chisel._

class LatencyController(Latency: Int) extends Module {

	// Interface
	val io = new LatencyControllerInterface

	// Default Values for wires
	io.CEout := Bits(0)
	io.ENDout := Bits(0)

	// Registers
	val counterC = Reg(init = Bits(Latency + 1, log2Up(Latency) + 1))
	val counterE = Reg(init = Bits(0, log2Up(Latency) + 1))
	val x = Reg(init = Bool(false))
  
	// RESET SIGNALS
	when(reset) { io.ENDout := Bits(0); io.CEout := Bits(0); }
	
	// Clock Enable Stuff ------------------------------------------------------
	when(io.CE === Bool(true)) { 
		counterC := UInt(1) 
	} 
	when(counterC <= UInt(Latency - 1) || (io.CE === Bool(true))) { 
		io.CEout := Bits(1) 
		when(io.CE === Bool(true)) { 
	   counterC := UInt(1) 
	  } 
	  .otherwise {
		 counterC := counterC + UInt(1)
	  } 
	} 
	.otherwise { 
		io.CEout := Bits(0) 
	}
	
	// Ready Stuff ---------------------------------------------------------------
	when(io.END === Bool(true) || x === Bool(true)) {		
		when(counterE === UInt(Latency)) {
			io.ENDout := Bool(true)
		}	
		when(counterE < UInt(Latency))
		{
      counterE := counterE + UInt(1)
		}
		x := Bool(true)
	}
	
}